package com.pigatron.trading.exchanges.exceptions;


public class ExchangeClientException extends RuntimeException {

    private ExchangeClientExceptionType type;

    public ExchangeClientException(ExchangeClientExceptionType type, Throwable cause, String message) {
        super(message, cause);
        this.type = type;
    }


    public ExchangeClientExceptionType getType() {
        return type;
    }
}
