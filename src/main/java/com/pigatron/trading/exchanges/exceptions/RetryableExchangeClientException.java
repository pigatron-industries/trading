package com.pigatron.trading.exchanges.exceptions;


public class RetryableExchangeClientException extends ExchangeClientException {

    public RetryableExchangeClientException(ExchangeClientExceptionType type, Throwable cause, String message) {
        super(type, cause, message);
    }
}
