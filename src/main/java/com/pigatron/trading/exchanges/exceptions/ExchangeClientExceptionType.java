package com.pigatron.trading.exchanges.exceptions;


public enum ExchangeClientExceptionType {

    ORDER_NOT_FOUND,
    INSUFFICIENT_FUNDS,
    ORDER_FILLED,
    UNKNOWN_CURRENCY,
    ORDER_PARITALLY_FILLED,
    RATE_LIMIT,
    HANDLED,
    UNKNOWN

}
