package com.pigatron.trading.exchanges;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.stats.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.*;

public abstract class AbstractWebsocketExchangeClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(AbstractWebsocketExchangeClient.class);

    private final String websocketUrl;

    protected ObjectMapper mapper = new ObjectMapper();

    protected Session session;

    protected Map<CurrencyPair, ProductData> productData = new HashMap<>();



    @Override
    public void poll() {
        connectWebsocket();
        try {
            for (Map.Entry<CurrencyPair, List<ExchangeDataSubscriber>> currencyPairListEntry : subscribers.entrySet()) {
                CurrencyPair currencyPair = currencyPairListEntry.getKey();
                List<ExchangeDataSubscriber> subscribers = currencyPairListEntry.getValue();
                notify(subscribers, currencyPair);
            }
        } catch(Exception e) {
            logger.error("Unexpected error", e);
        }
    }

    private void notify(List<ExchangeDataSubscriber> subscribers, CurrencyPair currencyPair) throws IOException {
        ProductData productData = getProductData(currencyPair);

        if(productData.getOrderBook().isReady()) {
            validateOrderBook(productData);

            List<Trade> newTrades;
            synchronized(productData.getNewTrades()) {
                newTrades = new ArrayList<>(productData.getNewTrades());
                productData.clearNewTrades();
            }

            recordTrades(newTrades, currencyPair);
            for (ExchangeDataSubscriber subscriber : subscribers) {
                try {
                    subscriber.onOrderBookChanged(productData.getOrderBook(), newTrades);
                } catch (Exception e) {
                    logger.error("Unexpected error in task", e);
                }
            }

        }
    }

    private OrderBook validateOrderBook(ProductData productData) throws IOException {
        if(!productData.getOrderBook().isValid()) {
            logger.info("Invalid order book for " + productData.getOrderBook().getCurrencyPair() + ". Disconnecting...");
            disconnect();
        }
        return productData.getOrderBook();
    }



    @Autowired
    public AbstractWebsocketExchangeClient(StatsService statsService, String websocketUrl) {
        super(statsService);
        this.websocketUrl = websocketUrl;
    }

    public synchronized void connectWebsocket() {
        try {
            if((session == null || !session.isOpen()) && !subscribers.keySet().isEmpty()) {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                session = container.connectToServer(this, new URI(websocketUrl));
            }
        } catch (Exception e) {
            // Catch exceptions that will be thrown in case of invalid configuration
            logger.error("connectWebsocket error", e);
            onWebsocketConnectionError();
        }
    }

    protected void onWebsocketConnectionError() {

    }

    @Override
    public void subscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair) throws IOException {
        super.subscribe(subscriber, currencyPair);
        //session.close();
        if(session == null || !session.isOpen()) {
            connectWebsocket();
        } else {
            subscribeWebsocket();
        }
    }

    public void subscribeWebsocket() throws IOException {
        try {
            subscribe(subscribers.keySet());
        } catch(Exception e) {
            logger.error("Unexpected error in subscribeWebsocket", e);
            session.close();
        }
    }

    public abstract void subscribe(Set<CurrencyPair> currencyPairs) throws IOException;

    public void disconnect() {
        try {
            session.close();
        } catch (IOException e) {
            logger.error("Error disconnecting", e);
        }
    }

    protected ProductData getProductData(CurrencyPair currencyPair) {
        if(!productData.containsKey(currencyPair)) {
            productData.put(currencyPair, new ProductData(this, currencyPair));
        }
        return productData.get(currencyPair);
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        if(this.session != null && this.session.isOpen()) {
            logger.warn("New session has opened, but old session is still open.");
        }

        logger.info("Websocket opened." + session.isOpen());
        this.session = session;
        subscribeWebsocket();
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        logger.info("Websocket closed.");
        connectWebsocket();
    }

}
