package com.pigatron.trading.exchanges;

import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.FullOrderBook;
import com.pigatron.trading.exchanges.entity.Trade;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductData {

    private CurrencyPair currencyPair;

    private FullOrderBook orderBook;

    private List<Trade> oldTrades;

    private List<Trade> newTrades;

    private String lastTradeId;

    private Instant lastHeartbeat;


    public ProductData(ExchangeClient exchange, CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
        orderBook = new FullOrderBook(exchange, currencyPair);
        newTrades = Collections.synchronizedList(new ArrayList<>());
        oldTrades = Collections.synchronizedList(new ArrayList<>());
        heartbeat();
    }

    public FullOrderBook getOrderBook() {
        return orderBook;
    }

    public void setOrderBook(FullOrderBook orderBook) {
        this.orderBook = orderBook;
    }

    public List<Trade> getNewTrades() {
        return newTrades;
    }

    public void addNewTrades(List<Trade> newTrades) {
        newTrades.forEach(this::addNewTrade);
    }

    public synchronized void addNewTrade(Trade newTrade) {
        if(!newTrades.contains(newTrade) && !oldTrades.contains(newTrade)) {
            this.newTrades.add(newTrade);
            lastTradeId = newTrade.getTradeId();
        }
    }

    public void clearNewTrades() {
        oldTrades.addAll(newTrades);
        while(oldTrades.size() > 100) {
            oldTrades.remove(0);
        }

        newTrades.clear();
    }

    public String getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(String lastTradeId) {
        this.lastTradeId = lastTradeId;
    }

    public Duration getLastHeartbeat() {
        return Duration.between(lastHeartbeat, Instant.now());
    }

    public void heartbeat() {
        lastHeartbeat = Instant.now();
    }

    public void clearOrderBook() {
        heartbeat();
        orderBook.clear();
    }
}
