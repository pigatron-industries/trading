package com.pigatron.trading.exchanges.entity;


import com.pigatron.trading.exchanges.ExchangeClient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.util.Collections.reverseOrder;
import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;

public class OrderBook {

    protected ExchangeClient exchange;
    protected CurrencyPair currencyPair;

    protected Set<MarketOrder> asks;
    protected Set<MarketOrder> bids;

    private boolean ready = false;

    public OrderBook(Set<MarketOrder> asks, Set<MarketOrder> bids) {
        this();
        this.asks.addAll(asks);
        this.bids.addAll(bids);
    }

    public OrderBook() {
        this.asks = new TreeSet<>(comparing(MarketOrder::getPrice, naturalOrder()));
        this.bids = new TreeSet<>(comparing(MarketOrder::getPrice, reverseOrder()));
    }

    private OrderBook(Builder builder) {
        this(builder.asks, builder.bids);
    }

    public static Builder anOrderBook() {
        return new Builder();
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setExchange(ExchangeClient exchange) {
        this.exchange = exchange;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public Set<MarketOrder> getAsks() {
        return asks;
    }

    public Set<MarketOrder> getBids() {
        return bids;
    }

    public void addOrder(MarketOrder order) {
        getOrders(order.getType()).add(order);
    }

    private Set<MarketOrder> getOrders(TradeType side) {
        return side.equals(TradeType.buy) ? getBids() : getAsks();
    }

    public MarketOrder getOrderByIndex(TradeType side, int index) {
        Iterator<MarketOrder> it = getOrders(side).iterator();
        int i = 0;
        while(it.hasNext()) {
            if(i == index) {
                return it.next();
            }
            i++;
        }
        return null;
    }

    public MarketOrder getBidByIndex(int index) {
        return getOrderByIndex(TradeType.buy, index);
    }

    public MarketOrder getAskByIndex(int index) {
        return getOrderByIndex(TradeType.sell, index);
    }

    public BigDecimal getMidPrice() {
        BigDecimal highestBid = getBids().iterator().next().getPrice();
        BigDecimal lowestAsk = getAsks().iterator().next().getPrice();
        return highestBid.add(lowestAsk).divide(new BigDecimal("2"), 5, RoundingMode.HALF_UP);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderBook orderBook = (OrderBook) o;
        return Objects.equals(getAsks(), orderBook.getAsks()) &&
                Objects.equals(getBids(), orderBook.getBids());
    }

    @Override
    public int hashCode() {
        return Objects.hash(asks, bids);
    }

    public void clear() {
        getAsks().clear();
        getBids().clear();
        setReady(false);
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }



    public static final class Builder {
        private Set<MarketOrder> asks = new HashSet<>();
        private Set<MarketOrder> bids = new HashSet<>();

        private Builder() {
        }

        public Builder withAsks(Set<MarketOrder> val) {
            asks = val;
            return this;
        }

        public Builder addAsk(MarketOrder ask) {
            asks.add(ask);
            return this;
        }

        public Builder withBids(Set<MarketOrder> val) {
            bids = val;
            return this;
        }

        public Builder addBid(MarketOrder bid) {
            bids.add(bid);
            return this;
        }

        public OrderBook build() {
            return new OrderBook(this);
        }
    }
}
