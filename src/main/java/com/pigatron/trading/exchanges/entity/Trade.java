package com.pigatron.trading.exchanges.entity;


import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class Trade {

    private String tradeId;
    private String orderId;
    private TradeType type;
    private CurrencyPair currencyPair;
    private Date date;
    private BigDecimal price;    //USD
    private BigDecimal quantity; //BTC
    private BigDecimal total;    //USD
    private BigDecimal fee;      // percent

    public Trade() {
    }

    public Trade(BigDecimal price, BigDecimal quantity) {
        this.price = price;
        this.quantity = quantity;
    }

    private Trade(Builder builder) {
        setTradeId(builder.tradeId);
        setOrderId(builder.orderId);
        setType(builder.type);
        setDate(builder.date);
        setPrice(builder.price);
        setQuantity(builder.quantity);
        setTotal(builder.total);
        setFee(builder.fee);
        setCurrencyPair(builder.currencyPair);
    }

    public static Builder aTrade() {
        return new Builder();
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trade trade = (Trade) o;
        return Objects.equals(tradeId, trade.tradeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tradeId);
    }

    @Override
    public String toString() {
        return "Trade{" +
                "tradeId='" + tradeId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", type=" + type +
                ", date=" + date +
                ", price=" + price +
                ", quantity=" + quantity +
                ", total=" + total +
                ", fee=" + fee +
                '}';
    }


    public static final class Builder {
        private String tradeId;
        private String orderId;
        private TradeType type;
        private Date date;
        private BigDecimal price;
        private BigDecimal quantity;
        private BigDecimal total;
        private BigDecimal fee;
        private CurrencyPair currencyPair;

        private Builder() {
        }

        public Builder withTradeId(String val) {
            this.tradeId = val;
            return this;
        }

        public Builder withOrderId(String val) {
            this.orderId = val;
            return this;
        }

        public Builder withType(TradeType val) {
            this.type = val;
            return this;
        }

        public Builder withDate(Date val) {
            this.date = val;
            return this;
        }

        public Builder withPrice(BigDecimal val) {
            this.price = val;
            return this;
        }

        public Builder withQuantity(BigDecimal val) {
            this.quantity = val;
            return this;
        }

        public Builder withTotal(BigDecimal val) {
            this.total = val;
            return this;
        }

        public Builder withFee(BigDecimal val) {
            this.fee = val;
            return this;
        }

        public Builder withCurrencyPair(CurrencyPair currencyPair) {
            this.currencyPair = currencyPair;
            return this;
        }

        public Trade build() {
            return new Trade(this);
        }
    }
}
