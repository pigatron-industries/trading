package com.pigatron.trading.exchanges.entity;

import java.util.Objects;

public class CurrencyPair {

    private String currency1;
    private String currency2;

    public CurrencyPair(String currency1, String currency2) {
        this.currency1 = currency1;
        this.currency2 = currency2;
    }

    public String getCurrency1() {
        return currency1;
    }

    public String getCurrency2() {
        return currency2;
    }

    public String toString() {
        return currency1 + "_" + currency2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyPair that = (CurrencyPair) o;
        return Objects.equals(currency1, that.currency1) &&
                Objects.equals(currency2, that.currency2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency1, currency2);
    }
}
