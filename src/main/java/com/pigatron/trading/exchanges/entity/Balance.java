package com.pigatron.trading.exchanges.entity;


import java.math.BigDecimal;

public class Balance {
    private BigDecimal available;
    private BigDecimal onOrders;
    private BigDecimal btcValue;

    public Balance(BigDecimal available, BigDecimal onOrders, BigDecimal btcValue) {
        this.available = available;
        this.onOrders = onOrders;
        this.btcValue = btcValue;
    }

    public Balance() {

    }

    public BigDecimal getAvailable() {
        return available;
    }

    public BigDecimal getOnOrders() {
        return onOrders;
    }

    public BigDecimal getBtcValue() {
        return btcValue;
    }

    public BigDecimal getTotal() {
        return available.add(onOrders);
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    public void setOnOrders(BigDecimal onOrders) {
        this.onOrders = onOrders;
    }

    public void setBtcValue(BigDecimal btcValue) {
        this.btcValue = btcValue;
    }
}
