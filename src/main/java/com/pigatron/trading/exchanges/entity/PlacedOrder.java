package com.pigatron.trading.exchanges.entity;


import java.math.BigDecimal;

public class PlacedOrder extends MarketOrder {

    private CurrencyPair currencyPair;
    private String orderId;
    private OrderStatus status;
    private boolean cancelled;
    private BigDecimal filledSize = new BigDecimal("0");

    public PlacedOrder() {
        super();
    }

    public PlacedOrder(String orderId, TradeType type, CurrencyPair currencyPair, BigDecimal price, BigDecimal quantity) {
        super(type, price, quantity);
        this.orderId = orderId;
        this.currencyPair = currencyPair;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }


    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public void setCancelled() {
        cancelled = true;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public BigDecimal getFilledSize() {
        return filledSize;
    }

    public void setFilledSize(BigDecimal filledSize) {
        this.filledSize = filledSize;
    }

    public BigDecimal getAmountAfterFills() {
        return getAmount().subtract(filledSize);
    }

    @Override
    public String toString() {
        return "PlacedOrder{" +
                "currencyPair=" + currencyPair.toString() +
                ", orderId='" + orderId + '\'' +
                ", type=" + getType() +
                '}';
    }
}
