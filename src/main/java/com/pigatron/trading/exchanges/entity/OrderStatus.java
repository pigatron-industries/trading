package com.pigatron.trading.exchanges.entity;


public enum OrderStatus {

    OPEN, DONE

}
