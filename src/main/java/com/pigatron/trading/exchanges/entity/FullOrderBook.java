package com.pigatron.trading.exchanges.entity;


import com.pigatron.trading.exchanges.ExchangeClient;

import java.math.BigDecimal;
import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.Comparator.naturalOrder;
import static java.util.Comparator.reverseOrder;


public class FullOrderBook extends OrderBook {

    private SortedSet<PlacedOrder> asks;
    private SortedSet<PlacedOrder> bids;

    private static final int MAX_REMOVED_ORDERS = 1000;
    private Set<String> removedOrderIds = new LinkedHashSet<>();


    public FullOrderBook(ExchangeClient exchange, CurrencyPair currencyPair) {
        this.exchange = exchange;
        this.currencyPair = currencyPair;
        asks = Collections.synchronizedSortedSet(new TreeSet<>(comparing(PlacedOrder::getPrice, naturalOrder()).thenComparing(PlacedOrder::getOrderId)));
        bids = Collections.synchronizedSortedSet(new TreeSet<>(comparing(PlacedOrder::getPrice, reverseOrder()).thenComparing(PlacedOrder::getOrderId)));
    }


    public ExchangeClient getExchange() {
        return exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Set<MarketOrder> getAsks() {
        return (Set<MarketOrder>)(Set<?>)asks;
    }

    public Set<MarketOrder> getBids() {
        return (Set<MarketOrder>)(Set<?>)bids;
    }

    public synchronized void clear() {
        super.clear();
        removedOrderIds.clear();
    }

    private int indexOf(String id, TradeType side) {
        SortedSet<PlacedOrder> orders = side.equals(TradeType.buy) ? bids : asks;
        Optional<PlacedOrder> order = orders.stream()
                .filter(o -> o.getOrderId().equals(id))
                .findFirst();
        if(order.isPresent()) {
            return orders.headSet(order.get()).size();
        } else {
            return -1;
        }
    }

    private int indexOf(PlacedOrder order) {
        SortedSet<PlacedOrder> orders = order.getType().equals(TradeType.buy) ? bids : asks;
        if(orders.contains(order)) {
            return orders.headSet(order).size();
        } else {
            return -1;
        }
    }

    public synchronized void addOrder(PlacedOrder order) {
        if(removedOrderIds.contains(order.getOrderId())) {
            removedOrderIds.remove(order.getOrderId());
            return;
        }
        SortedSet<PlacedOrder> orders = order.getType().equals(TradeType.buy) ? bids : asks;
        orders.add(order);
    }

    public synchronized void removeOrder(String id, TradeType side) {
        SortedSet<PlacedOrder> orders = side.equals(TradeType.buy) ? bids : asks;
        boolean removed = orders.removeIf(o -> o.getOrderId().equals(id));
        if(!removed) {
            cacheRemovedOrder(id);
        }
    }

    private void cacheRemovedOrder(String id) {
        removedOrderIds.add(id);
        if(removedOrderIds.size() > MAX_REMOVED_ORDERS) {
            Iterator<String> it = removedOrderIds.iterator();
            it.next();
            it.remove();
        }
    }

    public synchronized void removeOrder(String id) {
        asks.removeIf(o -> o.getOrderId().equals(id));
        bids.removeIf(o -> o.getOrderId().equals(id));
    }

    public synchronized void updateOrder(String id, TradeType side, BigDecimal size) {
        SortedSet<PlacedOrder> orders = side.equals(TradeType.buy) ? bids : asks;
        orders.stream()
                .filter(o -> o.getOrderId().equals(id))
                .forEach(o -> o.setAmount(size));
    }

    public String toString() {
        String output = "";
        for (MarketOrder ask : asks) {
            output += "Ask: Price = " + ask.getPrice().toPlainString() + " : Size = " + ask.getAmount() + "\n";
            break;
        }
        for (MarketOrder bid : bids) {
            output += "Bids: Price = " + bid.getPrice().toPlainString() + " : Size = " + bid.getAmount() + "\n";
            break;
        }
        output += "\n";
        return output;
    }

    public boolean isValid() {
        if(getAskByIndex(0).getPrice().compareTo(getBidByIndex(0).getPrice()) < 0) {
            return false;
        }
        return true;
    }
}
