package com.pigatron.trading.exchanges.entity;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Balances {

    private Map<String, Balance> balances;

    public Balances() {
        balances = new HashMap<>();
    }

    public Map<String, Balance> getBalances() {
        return balances;
    }

    public void setBalance(String currency, BigDecimal available, BigDecimal onOrders, BigDecimal btcValue) {
        balances.put(currency, new Balance(available, onOrders, btcValue));
    }

    public Balance getBalance(String currency) {
        return balances.get(currency);
    }

    public void setBalances(Map<String, Balance> balances) {
        this.balances = balances;
    }

    public void addBalanceOnOrder(String currency, BigDecimal amount) {
        Balance balance = balances.get(currency);
        balance.setOnOrders(balance.getOnOrders().add(amount));
    }
}
