package com.pigatron.trading.exchanges.entity;


import java.util.ArrayList;
import java.util.List;

public class ChartData {

    private List<Double> open;
    private List<Double> high;
    private List<Double> low;
    private List<Double> close;

    public ChartData() {
        open = new ArrayList<>();
        high = new ArrayList<>();
        low = new ArrayList<>();
        close = new ArrayList<>();
    }

    /**
     * Adds data to beginning of list
     * @param open
     * @param high
     * @param low
     * @param close
     */
    public void addData(Double open, Double high, Double low, Double close) {
        this.open.add(0, open);
        this.high.add(0, high);
        this.low.add(0, low);
        this.close.add(0, close);
    }

    public List<Double> getOpen() {
        return open;
    }
    public List<Double> getHigh() {
        return high;
    }
    public List<Double> getLow() {
        return low;
    }
    public List<Double> getClose() {
        return close;
    }
}
