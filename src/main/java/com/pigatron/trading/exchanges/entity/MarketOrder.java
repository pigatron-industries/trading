package com.pigatron.trading.exchanges.entity;


import java.math.BigDecimal;
import java.util.Objects;

public class MarketOrder {

    private BigDecimal price;
    private BigDecimal amount;
    private TradeType type;

    public MarketOrder() {
    }

    public MarketOrder(TradeType type, BigDecimal price, BigDecimal amount) {
        this.price = price;
        this.amount = amount;
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCost() {
        return amount.multiply(price);
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MarketOrder that = (MarketOrder) o;
        return Objects.equals(price, that.price) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(price, amount);
    }
}
