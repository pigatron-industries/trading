package com.pigatron.trading.exchanges;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ExchangeProvider {

    @Autowired
    List<ExchangeClient> exchanges;

    public ExchangeClient getExchange(String exchangeName) {
         return getExchanges().stream()
                 .filter(e -> e.getName().equals(exchangeName))
                 .findFirst()
                 .orElseThrow(() -> new RuntimeException("Exchange Not Found or Not Enabled: " + exchangeName));
    }

    public List<String> getExchangeNames() {
        return getExchanges().stream()
                .map(ExchangeClient::getName)
                .collect(Collectors.toList());
    }

    public List<ExchangeClient> getExchanges() {
        return exchanges.stream()
                .filter(ExchangeClient::isEnabled)
                .collect(Collectors.toList());
    }

}
