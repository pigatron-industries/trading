package com.pigatron.trading.exchanges;

import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;

import java.util.List;

public interface ExchangeDataSubscriber {

    void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades);

    int getProcessId();

    void start() throws Exception;

    void stop();

    void pause();

    void unpause();
}
