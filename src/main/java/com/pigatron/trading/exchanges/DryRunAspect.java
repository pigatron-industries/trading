package com.pigatron.trading.exchanges;

import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.TradeType;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Aspect
@Component
@Profile("dryrun")
public class DryRunAspect {

    private static final Logger logger = LoggerFactory.getLogger(DryRunAspect.class);

    @Around("execution(public * com.pigatron.trading.exchanges.ExchangeClient.buy(..)) && args(currencyPair, price, amount)")
    public Object buy(ProceedingJoinPoint joinPoint, CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws Throwable {
        logger.info("Place buy order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return new PlacedOrder("1", TradeType.buy, currencyPair, price, amount);
    }

    @Around("execution(public * com.pigatron.trading.exchanges.ExchangeClient.sell(..)) && args(currencyPair, price, amount)")
    public Object sell(ProceedingJoinPoint joinPoint, CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws Throwable {
        logger.info("Place sell order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return new PlacedOrder("1", TradeType.buy, currencyPair, price, amount);
    }

    @Around("execution(public * com.pigatron.trading.exchanges.ExchangeClient.cancelOrder(..)) && args(orderId, currencyPair)")
    public Object cancel(ProceedingJoinPoint joinPoint, String orderId, CurrencyPair currencyPair) throws Throwable {
        logger.info("Cancel order: " + currencyPair + " " + orderId);
        return true;
    }

    @Around("execution(public * com.pigatron.trading.exchanges.ExchangeClient.moveOrder(..)) && args(order, price, amount)")
    public Object move(ProceedingJoinPoint joinPoint, PlacedOrder order, BigDecimal price, BigDecimal amount) throws Throwable {
        logger.info("Move order: " + order.getCurrencyPair() + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return new PlacedOrder("1", order.getType(), order.getCurrencyPair(), price, amount);
    }

}
