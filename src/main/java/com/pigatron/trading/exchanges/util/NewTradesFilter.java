package com.pigatron.trading.exchanges.util;


import com.pigatron.trading.exchanges.entity.*;

import java.util.*;

import static java.lang.Math.min;

public class NewTradesFilter {

    private int maxTrades = 100;

    private Map<CurrencyPair, List<String>> tradeIdsForPairs = new HashMap<>();

    public NewTradesFilter() {
    }

    public NewTradesFilter(int maxTrades) {
        this.maxTrades = maxTrades;
    }

    public List<Trade> filter(List<Trade> trades, CurrencyPair currencyPair) {

        // truncate trades list at 100
        trades = trades.subList(0, min(trades.size(), maxTrades));

        // First time called, register all trade ids and discard trades
        if(tradeIdsForPairs.get(currencyPair) == null) {
            registerTrades(currencyPair, trades);
            return new ArrayList<>();
        }

        // Get all trades not registered.
        List<Trade> newTrades = new ArrayList<>();
        for(Trade trade : trades) {
            if(!isRegistered(currencyPair, trade)) {
                newTrades.add(trade);
            }
        }

        registerTrades(currencyPair, newTrades);
        return newTrades;
    }

    private boolean isRegistered(CurrencyPair currencyPair, Trade trade) {
        List<String> tradeIds = tradeIdsForPairs.get(currencyPair);
        return tradeIds.contains(trade.getTradeId());
    }

    private void registerTrades(CurrencyPair currencyPair, List<Trade> trades) {
        List<String> tradeIds = tradeIdsForPairs.get(currencyPair);
        if(tradeIds == null) {
            tradeIds = new ArrayList<>();
            tradeIdsForPairs.put(currencyPair, tradeIds);
        }

        for (int i = trades.size() - 1; i >= 0; i--) {
            tradeIds.add(0, trades.get(i).getTradeId());
        }

        // truncate registered trades at 100
        if(tradeIds.size() > maxTrades) {
            tradeIds.subList(maxTrades, tradeIds.size()).clear();
        }
    }

    public Optional<String> getLastTradeId(CurrencyPair currencyPair) {
        List<String> tradeIds = tradeIdsForPairs.get(currencyPair);
        if(tradeIds != null && tradeIds.size() > 0) {
            return Optional.ofNullable(tradeIds.get(0));
        } else {
            return Optional.empty();
        }
    }

}
