package com.pigatron.trading.exchanges;


import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.exceptions.OrderNotPlacedException;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface ExchangeClient {

    /** Info **/

    String getName();

    Boolean isEnabled();

    BigDecimal getMakerFee();

    BigDecimal getTakerFee();

    BigDecimal getWithdrawFee(String currency);

    BigDecimal getMinOrderAmount(String currency);

    BigDecimal getPriceIncrement(String currency);

    List<CurrencyPair> getCurrencyPairs();


    /** Public API **/

    TickData getTicker(CurrencyPair currencyPair);

    ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds);

    OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException;

    List<Trade> getTrades(CurrencyPair currencyPair);


    /** Trading API **/

    Balances getBalances();

    PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException;

    PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException;

    PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) throws ExchangeClientException;

    PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException;

    boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException;

    void cancelOrders(CurrencyPair currencyPair, TradeType side) throws ExchangeClientException;

    List<Trade> getOrderTrades(PlacedOrder order);

    List<Trade> getNewTrades(CurrencyPair currencyPair);

    List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair);

    List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair, TradeType side);

    Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException;


    /** Polling **/

    void subscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair) throws IOException;

    void unsubscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair);

    List<ExchangeDataSubscriber> getSubscribers(CurrencyPair currencyPair);

    Map<CurrencyPair, List<ExchangeDataSubscriber>> getSubscribers();

    int getTimeResolutionMillis();

    void poll();
}
