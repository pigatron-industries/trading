package com.pigatron.trading.exchanges;


import com.pigatron.trading.analysis.average.MarketAverageAnalyser;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.exceptions.OrderNotPlacedException;
import com.pigatron.trading.exchanges.util.NewTradesFilter;
import com.pigatron.trading.stats.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public abstract class AbstractExchangeClient implements ExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(AbstractExchangeClient.class);

    protected StatsService statsService;

    protected Map<CurrencyPair, List<ExchangeDataSubscriber>> subscribers = new ConcurrentHashMap<>();

    private Map<CurrencyPair, OrderBook> orderBooks = new HashMap<>();

    protected NewTradesFilter newTradesFilter = new NewTradesFilter();

    @Autowired
    MarketAverageAnalyser marketAverageAnalyser;

    @Autowired
    protected AbstractExchangeClient(StatsService statsService) {
        this.statsService = statsService;
    }


    @Override
    public List<CurrencyPair> getCurrencyPairs() {
        throw new NotImplementedException();
    }

    /** Public API **/

    @Override
    public TickData getTicker(CurrencyPair currencyPair) {
        throw new NotImplementedException();
    }

    public List<Trade> getTrades(CurrencyPair currencyPair) {
        throw new NotImplementedException();
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        throw new NotImplementedException();
    }


    /** Private API **/

    @Override
    public Balances getBalances() {
        throw new NotImplementedException();
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        throw new NotImplementedException();
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair, TradeType side) {
        return getOpenOrders(currencyPair).stream()
                .filter(o -> o.getType().equals(side))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
        return getOpenOrders(currencyPair).stream()
                .filter(o -> o.getOrderId().equals(orderId))
                .findFirst();
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        throw new NotImplementedException();
    }

    @Override
    public List<Trade> getNewTrades(CurrencyPair currencyPair) {
        List<Trade> trades = getPrivateTrades(currencyPair);
        return newTradesFilter.filter(trades, currencyPair);
    }

    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        throw new NotImplementedException();
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place buy order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return placeOrder(currencyPair, TradeType.buy, price, amount);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place sell order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return placeOrder(currencyPair, TradeType.sell, price, amount);
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        throw new NotImplementedException();
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Move order: " + order.getCurrencyPair() + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        try {
            cancelOrder(order.getOrderId(), order.getCurrencyPair());
        } catch(ExchangeClientException e) {
            if(!e.getType().equals(ExchangeClientExceptionType.ORDER_FILLED) && !e.getType().equals(ExchangeClientExceptionType.ORDER_NOT_FOUND)) {
                throw e;
            }
        }
        return placeOrder(order.getCurrencyPair(), order.getType(), price, amount);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        throw new NotImplementedException();
    }

    private boolean cancelOrderNoThrow(String orderId, CurrencyPair currencyPair) {
        try {
            return cancelOrder(orderId, currencyPair);
        } catch (ExchangeClientException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void cancelOrders(CurrencyPair currencyPair, TradeType side) throws ExchangeClientException {
        getOpenOrders(currencyPair).stream()
                .filter(o -> side == null || o.getType().equals(side))
                .forEach(o -> cancelOrderNoThrow(o.getOrderId(), currencyPair));
    }


    /** Polling **/

    @Override
    public void subscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair) throws IOException {
        List<ExchangeDataSubscriber> existingSubscribers = subscribers.get(currencyPair);
        if(existingSubscribers == null) {
            existingSubscribers = new ArrayList<>();
            subscribers.put(currencyPair, existingSubscribers);
        }
        if(!existingSubscribers.contains(subscriber)) {
            existingSubscribers.add(subscriber);
        }
    }

    @Override
    public void unsubscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair) {
        List<ExchangeDataSubscriber> existingSubscribers = subscribers.get(currencyPair);
        if(existingSubscribers != null) {
            existingSubscribers.remove(subscriber);
        }
    }

    @Override
    public List<ExchangeDataSubscriber> getSubscribers(CurrencyPair currencyPair) {
        List<ExchangeDataSubscriber> subs = subscribers.get(currencyPair);
        if(subs == null) {
            return new ArrayList<>();
        } else {
            return subs;
        }
    }

    @Override
    public Map<CurrencyPair, List<ExchangeDataSubscriber>> getSubscribers() {
        return subscribers;
    }


    @Override
    public void poll() {
        try {
            for (Map.Entry<CurrencyPair, List<ExchangeDataSubscriber>> currencyPairListEntry : subscribers.entrySet()) {
                CurrencyPair currencyPair = currencyPairListEntry.getKey();
                List<ExchangeDataSubscriber> subscribers = currencyPairListEntry.getValue();
                if(subscribers.size() > 0) {
                    OrderBook orderBook = getOrderBook(currencyPair, 10);
                    orderBook.setExchange(this);
                    orderBook.setCurrencyPair(currencyPair);
                    //if (orderBookChanged(currencyPair, orderBook)) {
                        List<Trade> newTrades = getNewTrades(currencyPair);
                        recordTrades(newTrades, currencyPair);
                        for (ExchangeDataSubscriber subscriber : subscribers) {
                            try {
                                subscriber.onOrderBookChanged(orderBook, newTrades);
                            } catch(Exception e) {
                                logger.error("Unexpected error in task", e);
                            }
                        }
                    //}
                }
            }
        } catch(Exception e) {
            logger.error("Unexpected error", e);
        }
    }

    @Override
    public int getTimeResolutionMillis() {
        throw new NotImplementedException();
    }

    public void notifyTrade(Trade trade) {
        CurrencyPair currencyPair = trade.getCurrencyPair();
        recordTrades(Collections.singletonList(trade), currencyPair);
        OrderBook orderBook = getOrderBook(currencyPair, 10);
        for(ExchangeDataSubscriber subscriber : subscribers.get(currencyPair)) {
            subscriber.onOrderBookChanged(orderBook, Collections.singletonList(trade));
        }
    }


    protected void recordTrades(List<Trade> trades, CurrencyPair currencyPair) {
        if(trades.size() > 0) {
            statsService.recordTrades(trades, getName(), currencyPair);
            for (Trade trade : trades) {
                logger.info((trade.getType().equals(TradeType.buy) ? "Bought: " : "Sold: ") + currencyPair.toString() +
                        " Price: " + trade.getPrice() + "  Amount: " + trade.getQuantity());
            }
        }
    }

    protected boolean orderBookChanged(CurrencyPair currencyPair, OrderBook orderBook) {
        OrderBook prevOrderBook = orderBooks.get(currencyPair);
        boolean changed = prevOrderBook == null || !prevOrderBook.equals(orderBook);
        orderBooks.put(currencyPair, orderBook);
        return changed;
    }


    /** Utils **/

    protected void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected String createQueryString(Map<String, String> params, boolean urlEncoded) {
        try {
            List<String> paramStrings = new ArrayList<>();
            for (Map.Entry<String, String> param : params.entrySet()) {
                String paramString;
                if(urlEncoded) {
                    paramString = URLEncoder.encode(param.getKey(), "UTF-8") + "=" + URLEncoder.encode(param.getValue(), "UTF-8");
                } else {
                    paramString = param.getKey() + "=" + param.getValue();
                }
                paramStrings.add(paramString);
            }
            return String.join("&", paramStrings);
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractExchangeClient that = (AbstractExchangeClient) o;
        return Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
