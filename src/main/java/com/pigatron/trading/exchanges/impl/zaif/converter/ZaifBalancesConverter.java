package com.pigatron.trading.exchanges.impl.zaif.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.SpaceBtcAccount;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifInfo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

@Component
public class ZaifBalancesConverter implements Converter<ZaifInfo, Balances> {

    @Override
    public Balances convert(ZaifInfo account) {
        Balances balances = new Balances();

        for (Map.Entry<String, BigDecimal> entry : account.getFunds().entrySet()) {
            String currency = entry.getKey().toUpperCase();
            BigDecimal amount = entry.getValue();
            balances.setBalance(currency, amount, new BigDecimal("0"), null);
        }

        return balances;
    }
}
