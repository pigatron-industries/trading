package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class HitBtcBalance {

    @JsonProperty("currency_code")
    private String currencyCode;

    private BigDecimal cash;

    private BigDecimal reserved;

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getCash() {
        return cash;
    }

    public void setCash(BigDecimal cash) {
        this.cash = cash;
    }

    public BigDecimal getReserved() {
        return reserved;
    }

    public void setReserved(BigDecimal reserved) {
        this.reserved = reserved;
    }
}
