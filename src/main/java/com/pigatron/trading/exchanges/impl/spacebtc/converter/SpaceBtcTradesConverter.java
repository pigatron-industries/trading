package com.pigatron.trading.exchanges.impl.spacebtc.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.SpaceBtcTrade;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.SpaceBtcTrades;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpaceBtcTradesConverter implements Converter<SpaceBtcTrades, List<Trade>> {

    @Override
    public List<Trade> convert(SpaceBtcTrades source) {
        List<Trade> trades = new ArrayList<>();
        for(SpaceBtcTrade sourceTrade : source.getResult()) {
            Trade trade = new Trade();
            trade.setTradeId(sourceTrade.getId());
            trade.setOrderId(sourceTrade.getOrderId());
            trade.setType(sourceTrade.getSide() == 0 ? TradeType.buy : TradeType.sell);
            trade.setPrice(sourceTrade.getPrice());
            trade.setQuantity(sourceTrade.getQty());
            trades.add(trade);
        }
        return trades;
    }
}
