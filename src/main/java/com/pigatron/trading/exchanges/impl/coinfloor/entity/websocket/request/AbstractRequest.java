package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request;


public class AbstractRequest {

    private Integer tag;
    private String method;

    public AbstractRequest(String method) {
        this.method = method;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
