package com.pigatron.trading.exchanges.impl.coinfloor.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.rest.CoinFloorOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class CoinFloorOrderConverter implements Converter<CoinFloorOrder, PlacedOrder> {

    @Override
    public PlacedOrder convert(CoinFloorOrder sourceOrder) {
        PlacedOrder placedOrder = new PlacedOrder();
        placedOrder.setOrderId(sourceOrder.getId());
        placedOrder.setAmount(sourceOrder.getAmount());
        placedOrder.setPrice(sourceOrder.getPrice());
        placedOrder.setType(sourceOrder.getType());
        return placedOrder;
    }

    public PlacedOrder convert(CoinFloorOrder sourceOrder, CurrencyPair currencyPair) {
        PlacedOrder placedOrder = convert(sourceOrder);
        placedOrder.setCurrencyPair(currencyPair);
        return placedOrder;
    }

    public List<PlacedOrder> convert(List<CoinFloorOrder> sourceOrders, CurrencyPair currencyPair) {
        return sourceOrders.stream().map(o -> convert(o, currencyPair)).collect(Collectors.toList());
    }

}
