package com.pigatron.trading.exchanges.impl.quoine.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class QuoineOrderBook extends CommonOrderBook {

    @JsonSetter("sell_price_levels")
    public void setSellPriceLevels(List<List<BigDecimal>> asks) {
        setAsks(asks);
    }

    @JsonSetter("buy_price_levels")
    public void setBuyPriceLevels(List<List<BigDecimal>> bids) {
        setBids(bids);
    }
}
