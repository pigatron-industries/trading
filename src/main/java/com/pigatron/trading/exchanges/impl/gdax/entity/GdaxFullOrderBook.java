package com.pigatron.trading.exchanges.impl.gdax.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.pigatron.trading.exchanges.entity.CurrencyPair;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxFullOrderBook {

    private List<List<String>> asks;
    private List<List<String>> bids;
    private CurrencyPair currencyPair;

    public List<List<String>> getAsks() {
        return asks;
    }

    public void setAsks(List<List<String>> asks) {
        this.asks = asks;
    }

    public List<List<String>> getBids() {
        return bids;
    }

    public void setBids(List<List<String>> bids) {
        this.bids = bids;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }
}
