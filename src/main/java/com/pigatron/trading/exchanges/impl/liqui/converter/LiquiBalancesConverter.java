package com.pigatron.trading.exchanges.impl.liqui.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiInfo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
public class LiquiBalancesConverter implements Converter<LiquiInfo, Balances> {

    @Override
    public Balances convert(LiquiInfo source) {
        Balances balances = new Balances();
        for (Map.Entry<String, BigDecimal> sourceEntry : source.getFunds().entrySet()) {
            String currency = sourceEntry.getKey().toUpperCase();
            balances.setBalance(currency, sourceEntry.getValue(), BigDecimal.ZERO, null);
        }

        return balances;
    }
}
