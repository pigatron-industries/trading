package com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "event")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "info", value = BfxWebsocketInfo.class),
        @JsonSubTypes.Type(name = "subscribed", value = BfxWebsocketSubscribed.class),
})
//@JsonIgnoreProperties(ignoreUnknown = true)
public class BfxWebsocketMessage {

    private String event;
    private String channel;

    public BfxWebsocketMessage(String channel) {
        this.event = "subscribe";
        this.channel = channel;
    }

    public BfxWebsocketMessage() {
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
