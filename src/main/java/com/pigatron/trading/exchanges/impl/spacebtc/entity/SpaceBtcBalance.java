package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import java.math.BigDecimal;

public class SpaceBtcBalance {

    private BigDecimal available;
    private BigDecimal blocked;

    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    public BigDecimal getBlocked() {
        return blocked;
    }

    public void setBlocked(BigDecimal blocked) {
        this.blocked = blocked;
    }
}
