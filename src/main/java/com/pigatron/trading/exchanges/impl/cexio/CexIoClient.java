package com.pigatron.trading.exchanges.impl.cexio;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.cexio.converter.CexIoBalancesConverter;
import com.pigatron.trading.exchanges.impl.cexio.converter.CexIoTradesConverter;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoBalances;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoOrder;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoOrderDetailsWrapper;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoOrderStatus;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
@Profile("cexio")
public class CexIoClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(CexIoClient.class);

    private static final String API_URL = "https://cex.io/api/";

    private static final String GET_ORDER_BOOK_URL = API_URL + "order_book/%s/%s/?depth=%d";
    private static final String GET_BALANCE_URL = API_URL + "balance/";
    private static final String PLACE_ORDER_URL = API_URL + "place_order/%s/%s";
    private static final String MOVE_ORDER_URL = API_URL + "cancel_replace_order/%s/%s";
    private static final String CANCEL_ORDER_URL = API_URL + "cancel_order/";
    private static final String GET_ORDER_TRADES_URL = API_URL + "get_order_tx/";
    private static final String GET_ORDER_STATUS_URL = API_URL + "active_orders_status/";

    @Value("${exchange.cexio.enabled}")
    private Boolean enabled;

    @Value("${exchange.cexio.userid}")
    private String userId;

    @Value("${exchange.cexio.key}")
    private String key;

    @Value("${exchange.cexio.secret}")
    private String secret;

    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;

    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private CexIoBalancesConverter balancesConverter;

    @Autowired
    private CexIoTradesConverter tradesConverter;

    private RestTemplate restTemplate;

    private Map<CurrencyPair, List<PlacedOrder>> openOrders = new HashMap<>();

    private Map<CurrencyPair, Long> lastTradeIds = new HashMap<>();


    @Autowired
    public CexIoClient(StatsService statsService) {
        super(statsService);
        // exchange responds with 'text/json' media type so this needs adding to rest template
        restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory());
        List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter jsonConverter = (MappingJackson2HttpMessageConverter) converter;
                jsonConverter.setObjectMapper(new ObjectMapper());
                List<MediaType> mediaTypes = new ArrayList<>();
                mediaTypes.add(new MediaType("*", "json", MappingJackson2HttpMessageConverter.DEFAULT_CHARSET));
                jsonConverter.setSupportedMediaTypes(mediaTypes);
            }
        }
    }

    @Override
    public String getName() {
        return "cexio";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.01");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.0001");
    }



    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        String url = String.format(GET_ORDER_BOOK_URL, currencyPair.getCurrency2(), currencyPair.getCurrency1(), depth);
        ResponseEntity<CommonOrderBook> response = restTemplate.exchange(url, HttpMethod.GET, createEntity(""), CommonOrderBook.class);
        return orderBookConverter.convert(response.getBody());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        HttpEntity<String> entity = createEntity(createQueryString(getSignatureParams(), false));
        String url = GET_BALANCE_URL;
        ResponseEntity<CexIoBalances> response = restTemplate.exchange(url, HttpMethod.POST, entity, CexIoBalances.class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrder(currencyPair, TradeType.buy, price, amount);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrder(currencyPair, TradeType.sell, price, amount);
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        String url = String.format(PLACE_ORDER_URL, currencyPair.getCurrency2(), currencyPair.getCurrency1());
        Map<String, String> params = getSignatureParams();
        params.put("type", side.name());
        params.put("amount", amount.toPlainString());
        params.put("price", price.toPlainString());
        HttpEntity<String> entity = createEntity(createQueryString(params, false));

        ResponseEntity<CexIoOrder> response = restTemplate.exchange(url, HttpMethod.POST, entity, CexIoOrder.class);
        if(response.getBody().getError() == null) {
            PlacedOrder newOrder = new PlacedOrder(response.getBody().getId(), side, currencyPair, price, amount);
            trackOrder(newOrder);
            return newOrder;
        } else {
            throw convertError(response.getBody().getError());
        }
    }


    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        String url = String.format(MOVE_ORDER_URL, order.getCurrencyPair().getCurrency2(), order.getCurrencyPair().getCurrency1());
        Map<String, String> params = getSignatureParams();
        params.put("type", order.getType().name());
        params.put("amount", amount.toPlainString());
        params.put("price", price.toPlainString());
        params.put("order_id", order.getOrderId());
        HttpEntity<String> entity = createEntity(createQueryString(params, false));

        ResponseEntity<CexIoOrder> response = restTemplate.exchange(url, HttpMethod.POST, entity, CexIoOrder.class);
        if(response.getBody().getError() == null) {
            PlacedOrder newOrder = new PlacedOrder(response.getBody().getId(), order.getType(), order.getCurrencyPair(), price, amount);
            trackOrder(newOrder);
            return newOrder;
        } else {
            throw convertError(response.getBody().getError());
        }
    }

    private void trackOrder(PlacedOrder order) {
        List<PlacedOrder> orders = openOrders.get(order.getCurrencyPair());
        if(orders == null) {
            orders = new ArrayList<>();
            openOrders.put(order.getCurrencyPair(), orders);
        }
        orders.add(order);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        String url = CANCEL_ORDER_URL;
        Map<String, String> params = getSignatureParams();
        params.put("id", orderId);
        HttpEntity<String> entity = createEntity(createQueryString(params, false));

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        return Boolean.valueOf(response.getBody());
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        CexIoOrderDetailsWrapper orderDetails = getOrderDetails(order);
        return tradesConverter.convert(orderDetails.getData().getVtx());
    }

    private CexIoOrderDetailsWrapper getOrderDetails(PlacedOrder order) {
        String url = GET_ORDER_TRADES_URL;
        Map<String, String> params = getSignatureParams();
        params.put("id", order.getOrderId());
        HttpEntity<String> entity = createEntity(createQueryString(params, false));
        ResponseEntity<CexIoOrderDetailsWrapper> response = restTemplate.exchange(url, HttpMethod.POST, entity, CexIoOrderDetailsWrapper.class);

        return response.getBody();
    }

    public CexIoOrderStatus getOrderStatus() { //TODO
        String url = GET_ORDER_STATUS_URL;
        Map<String, String> params = getSignatureParams();
        params.put("orders_list", "'3609510361'");
        HttpEntity<String> entity = createEntity(createQueryString(params, false));
//        ResponseEntity<CexIoOrderStatus> response = restTemplate.exchange(url, HttpMethod.POST, entity, CexIoOrderStatus.class);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        return null;
    }

    @Override
    public List<Trade> getNewTrades(CurrencyPair currencyPair) {
        List<Trade> newTrades = new ArrayList<>();
        List<PlacedOrder> orders = openOrders.get(currencyPair);
        if(orders != null) {
            Iterator<PlacedOrder> it = orders.iterator();
            while(it.hasNext()) {
                PlacedOrder order = it.next();

                CexIoOrderDetailsWrapper orderDetails = getOrderDetails(order);

                if(orderDetails.getData().getStatus().equals("cd")) {
                    System.out.println("Done order: " + orderDetails.getData().getId());
                }

                List<Trade> orderTrades = tradesConverter.convert(orderDetails.getData().getVtx());
                newTrades.addAll(filterNewTrades(currencyPair, orderTrades));

                //remove order from tracking if done or cancelled
                if((orderDetails.getData().getStatus().equals("d") && orderTrades.size() > 0) ||
                        (orderDetails.getData().getStatus().equals("cd") && orderTrades.size() > 0) ||
                        orderDetails.getData().getStatus().equals("c")) {
                    it.remove();
                }
            }
        }

        return newTrades;
    }

    private List<Trade> filterNewTrades(CurrencyPair currencyPair, List<Trade> orderTrades) {
        Long lastTradeId = lastTradeIds.get(currencyPair);
        if(lastTradeId == null) {
            lastTradeId = 0L;
        }

        List<Trade> newTrades = new ArrayList<>();

        for (Trade orderTrade : orderTrades) {
            Long tradeId = Long.parseLong(orderTrade.getTradeId());
            if(Long.parseLong(orderTrade.getTradeId()) > lastTradeId) {
                newTrades.add(orderTrade);
                setLastTradeId(currencyPair, tradeId);
            }
        }

        return newTrades;
    }

    private void setLastTradeId(CurrencyPair currencyPair, Long tradeId) {
        Long lastTradeId = lastTradeIds.get(currencyPair);
        if(lastTradeId == null) {
            lastTradeId = 0L;
        }
        if(tradeId > lastTradeId) {
            lastTradeIds.put(currencyPair, tradeId);
        }
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        return null;
    }


    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 1000;
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2() + "-" + currencyPair.getCurrency1();
    }

    private HttpEntity<String> createEntity(String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.add("User-Agent", "Java");
        return new HttpEntity<>(body, headers);
    }

    private Map<String, String> getSignatureParams() {
        Map<String, String> params = new HashMap<>();
        String nonce = createNonce()+"";
        params.put("key", key);
        params.put("signature", getSignature(nonce));
        params.put("nonce", nonce);
        return params;
    }

    private long createNonce() {
        return new Date().getTime();
    }

    private String getSignature(String nonce) {
        try {
            String prehash = nonce + userId + key;
            //byte[] secretDecoded = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(prehash.getBytes())).toUpperCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertError(String error) {
        if(error.contains("Insufficient funds")) {
            return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, error);
        } else if(error.contains("Order not found")) {
            return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, error);
        } else {
            return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, error);
        }
    }
}
