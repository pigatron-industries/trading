package com.pigatron.trading.exchanges.impl.cexio.converter;

import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoTrades;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

@Component
public class CexIoTradesConverter implements Converter<List<CexIoTrades>, List<Trade>> {

    @Override
    public List<Trade> convert(List<CexIoTrades> transactions) {
        List<Trade> trades = new ArrayList<>();

        for(CexIoTrades transaction : transactions) {
            if((transaction.getType().equals("buy") || transaction.getType().equals("sell")) &&
                    transaction.getPrice() != null) {
                Trade trade = new Trade();
                trade.setTradeId(transaction.getId());
                trade.setOrderId(transaction.getOrder());
                trade.setType(TradeType.valueOf(transaction.getType()));
                trade.setPrice(transaction.getPrice());
                if(transaction.getType().equals("buy")) {
                    trade.setQuantity(transaction.getAmount());
                } else {
                    trade.setQuantity(transaction.getAmount().divide(trade.getPrice(), BigDecimal.ROUND_HALF_UP)
                                                 .setScale(2, RoundingMode.HALF_UP));
                }
                trade.setTotal(trade.getPrice().multiply(trade.getQuantity()));
                trades.add(trade);
            }
        }

        return trades;
    }
}
