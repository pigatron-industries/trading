package com.pigatron.trading.exchanges.impl.hitbtc.entity;


public class HitBtcExecutionReport {

    private String orderId;
    private String clientOrderId;
    private String execReportType;
    private String orderRejectReason;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClientOrderId() {
        return clientOrderId;
    }

    public void setClientOrderId(String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    public String getExecReportType() {
        return execReportType;
    }

    public void setExecReportType(String execReportType) {
        this.execReportType = execReportType;
    }

    public String getOrderRejectReason() {
        return orderRejectReason;
    }

    public void setOrderRejectReason(String orderRejectReason) {
        this.orderRejectReason = orderRejectReason;
    }
}
