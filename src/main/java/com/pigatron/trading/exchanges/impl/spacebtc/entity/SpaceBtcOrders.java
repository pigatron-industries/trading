package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import java.util.List;

public class SpaceBtcOrders {

    private List<SpaceBtcOrderResult> result;

    public List<SpaceBtcOrderResult>  getResult() {
        return result;
    }

    public void setResult(List<SpaceBtcOrderResult>  result) {
        this.result = result;
    }
}
