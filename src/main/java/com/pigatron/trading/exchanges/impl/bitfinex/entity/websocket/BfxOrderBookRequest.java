package com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket;


public class BfxOrderBookRequest extends BfxWebsocketMessage {

    private String pair;
    private String prec;

    public BfxOrderBookRequest(String pair) {
        super("book");
        prec = "R0";
        this.pair = pair;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public String getPrec() {
        return prec;
    }

    public void setPrec(String prec) {
        this.prec = prec;
    }
}
