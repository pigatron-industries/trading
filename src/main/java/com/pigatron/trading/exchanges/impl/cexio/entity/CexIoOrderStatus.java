package com.pigatron.trading.exchanges.impl.cexio.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CexIoOrderStatus {

    private List<List<BigDecimal>> data;

    public List<List<BigDecimal>> getData() {
        return data;
    }

    public void setData(List<List<BigDecimal>> data) {
        this.data = data;
    }
}
