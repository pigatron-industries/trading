package com.pigatron.trading.exchanges.impl.zaif.converter;


import com.pigatron.trading.exchanges.entity.ChartData;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifChartData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ZaifChartDataConverter implements Converter<List<ZaifChartData>, ChartData> {

    @Override
    public ChartData convert(List<ZaifChartData> source) {
        ChartData chartData = new ChartData();
        for (ZaifChartData zaifChartData : source) {
            chartData.addData(zaifChartData.getOpen(), zaifChartData.getHigh(), zaifChartData.getLow(), zaifChartData.getClose());
        }
        return chartData;
    }
}
