package com.pigatron.trading.exchanges.impl.kraken.converter;

import com.pigatron.trading.exchanges.entity.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KrakenCurrencyPairConverter implements Converter<String, CurrencyPair> {

    private KrakenCurrencyCodeConverter currencyCodeConverter;

    @Autowired
    public KrakenCurrencyPairConverter(KrakenCurrencyCodeConverter currencyCodeConverter) {
        this.currencyCodeConverter = currencyCodeConverter;
    }

    @Override
    public CurrencyPair convert(String krakenPair) {
        String currency1;
        String currency2;
        if(krakenPair.length() == 6) {
            currency1 = krakenPair.substring(3, 6);
            currency2 = krakenPair.substring(0, 3);
        } else {
            currency1 = krakenPair.substring(4, 8);
            currency2 = krakenPair.substring(0, 4);
        }
        return new CurrencyPair(currencyCodeConverter.convert(currency1), currencyCodeConverter.convert(currency2));
    }

}
