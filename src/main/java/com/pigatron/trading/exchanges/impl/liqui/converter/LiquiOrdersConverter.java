package com.pigatron.trading.exchanges.impl.liqui.converter;


import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiOrder;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiOrdersWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class LiquiOrdersConverter implements Converter<LiquiOrdersWrapper, List<PlacedOrder>> {

    @Autowired
    private LiquiCurrencyPairConverter currencyPairConverter;

    @Override
    public List<PlacedOrder> convert(LiquiOrdersWrapper source) {
        List<PlacedOrder> orders = new ArrayList<>();

        for (Map.Entry<String, LiquiOrder> entry : source.getTrades().entrySet()) {
            PlacedOrder order = new PlacedOrder();
            order.setOrderId(entry.getKey());
            order.setType(entry.getValue().getType());
            order.setCurrencyPair(currencyPairConverter.convert(entry.getValue().getPair()));
            order.setPrice(entry.getValue().getRate());
            order.setAmount(entry.getValue().getAmount());
            orders.add(order);
        }

        return orders;
    }



}
