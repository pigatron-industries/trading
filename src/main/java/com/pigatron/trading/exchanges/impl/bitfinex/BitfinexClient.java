package com.pigatron.trading.exchanges.impl.bitfinex;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractWebsocketExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.impl.bitfinex.converter.BitfinexBalanceConverter;
import com.pigatron.trading.exchanges.impl.bitfinex.converter.BitfinexOrderBookV2Converter;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.BitfinexBalance;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket.BfxWebsocketMessage;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket.BfxOrderBookRequest;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket.BfxWebsocketSubscribed;
import com.pigatron.trading.exchanges.util.NewTradesFilter;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
@ClientEndpoint
@Profile("bitfinex")
public class BitfinexClient extends AbstractWebsocketExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(BitfinexClient.class);

    private static final String API_URL = "https://api.bitfinex.com";
    private static final String WS_API_URL = "wss://api.bitfinex.com/ws/2";

    private static final String GET_ORDER_BOOK_URL = "/v1/book/%s";
    private static final String GET_BALANCES_URL = "/v1/balances";
    private static final String PLACE_ORDER_URL = API_URL + "/v1/order/new";
    private static final String CANCEL_ORDER_URL = API_URL + "/v1/order/cancel";
    private static final String MOVE_ORDER_URL = API_URL + "/v1/order/cancel/replace";
    private static final String GET_TRADES_URL = API_URL + "/v1/mytrades";

    @Value("${exchange.bitfinex.enabled}")
    private Boolean enabled;

    @Value("${exchange.bitfinex.key}")
    private String key;

    @Value("${exchange.bitfinex.secret}")
    private String secret;

    private BigDecimal makerFee = new BigDecimal("0.0004");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;

    ObjectMapper objectMapper = new ObjectMapper();

    private NewTradesFilter newTradesFilter = new NewTradesFilter();

    private Map<CurrencyPair, OrderBook> orderBooks = new HashMap<>();

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BitfinexBalanceConverter balanceConverter;
    @Autowired
    private BitfinexOrderBookV2Converter orderBookConverter;


    @Autowired
    public BitfinexClient(StatsService statsService) {
        super(statsService, WS_API_URL);
    }

    @Override
    public String getName() {
        return "bitfinex";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.01");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.1");
    }


    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        String url = String.format(API_URL + GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair));
        Map<String, String> params = new HashMap<>();
        params.put("limit_bids", depth+"");
        params.put("limit_asks", depth+"");
        url += "?" + createQueryString(params, false);

        ResponseEntity<OrderBook> response = restTemplate.getForEntity(url, OrderBook.class);
        return response.getBody();
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        Map<String, Object> params = new HashMap<>();
        params.put("request", GET_BALANCES_URL);

        HttpEntity<String> entity = createPrivateEntity(params);
        String url = API_URL + GET_BALANCES_URL;

        ResponseEntity<List<BitfinexBalance>> response = restTemplate.exchange(url, HttpMethod.POST, entity, new ParameterizedTypeReference<List<BitfinexBalance>>() {});
        return balanceConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return false;
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        return null;
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2().toUpperCase() + currencyPair.getCurrency1().toUpperCase();
    }

    private HttpEntity<String> createPrivateEntity(Map<String,Object> params) {
        try {
            params.put("nonce", createNonce()+"");
            String body = objectMapper.writeValueAsString(params);
            String encodedBody = Base64.getEncoder().encodeToString(body.getBytes("UTF-8"));
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("User-Agent", "Java");
            headers.add("X-BFX-APIKEY", key);
            headers.add("X-BFX-PAYLOAD", encodedBody);
            headers.add("X-BFX-SIGNATURE", getSignature(encodedBody));
            return new HttpEntity<>(body, headers);
        } catch(UnsupportedEncodingException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpEntity<String> createPrivateEntityV2(Map<String,Object> params) {
        try {
            String body = objectMapper.writeValueAsString(params);
            String encodedBody = Base64.getEncoder().encodeToString(body.getBytes("UTF-8"));
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("User-Agent", "Java");
            headers.add("bfx-nonce", createNonce()+"");
            headers.add("bfx-apikey", key);
            headers.add("bfx-signature", getSignature(encodedBody));
            return new HttpEntity<>(body, headers);
        } catch(UnsupportedEncodingException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSignature(String encodedBody) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA384");
            Mac mac = Mac.getInstance("HmacSHA384");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(encodedBody.getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private long createNonce() {
        return new Date().getTime();
    }



    /**************************************** Websocket *****************************************/

    private Map<Integer, CurrencyPair> channelIds = new HashMap<>();

    @Override
    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
    }

    @Override
    public int getTimeResolutionMillis() {
        return 1000;
    }


    public void subscribe(Set<CurrencyPair> currencyPairs) throws IOException {
        if(currencyPairs.size() == 0) {
            return;
        }

        // clear current order books
        currencyPairs.stream()
                .map(c -> getProductData(c).getOrderBook())
                .forEach(OrderBook::clear);

        for (CurrencyPair currencyPair : currencyPairs) {
            BfxOrderBookRequest message = new BfxOrderBookRequest(formatCurrencyPair(currencyPair));
            session.getBasicRemote().sendText(mapper.writeValueAsString(message));
        }
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        try {
            //System.out.println(message);
            Object websocketMessage = mapper.readValue(message, Object.class);
            if(websocketMessage instanceof LinkedHashMap) {
                BfxWebsocketMessage bfxWebsocketMessage = mapper.convertValue(websocketMessage, BfxWebsocketMessage.class);
                if(bfxWebsocketMessage instanceof BfxWebsocketSubscribed) {
                    onSubscribed((BfxWebsocketSubscribed)bfxWebsocketMessage);
                }
            } else if(websocketMessage instanceof ArrayList) {
                ArrayList websocketArrayMessage = (ArrayList) websocketMessage;
                onUpdateMessage(websocketArrayMessage);
            }

        } catch(Exception e) {
            logger.error("Unexpected error in websocket handler", e);
            logger.info("Message: " + message);
        }
    }

    private void onSubscribed(BfxWebsocketSubscribed message) {
        CurrencyPair currencyPair = formatCurrencyPair(message.getPair());
        channelIds.put(message.getChanId(), currencyPair);
    }

    private void onUpdateMessage(ArrayList updateMessage) {
        int channelId = (Integer)updateMessage.get(0);
        CurrencyPair currencyPair = channelIds.get(channelId);

        if(updateMessage.get(1) instanceof ArrayList) {
            if (((ArrayList) updateMessage.get(1)).get(0) instanceof ArrayList) {
                for (Object update : (ArrayList) updateMessage.get(1)) {
                    onUpdateMessage(currencyPair, (ArrayList) update);
                }
                productData.get(currencyPair).getOrderBook().setReady(true);
            } else {
                onUpdateMessage(currencyPair, (ArrayList) updateMessage.get(1));
            }
        }
    }

    private void onUpdateMessage(CurrencyPair currencyPair, ArrayList update) {
        FullOrderBook orderBook = productData.get(currencyPair).getOrderBook();
        String orderId = update.get(0).toString();
        BigDecimal price = new BigDecimal(update.get(1).toString());
        BigDecimal amount = new BigDecimal(update.get(2).toString());

        if(price.compareTo(BigDecimal.ZERO) == 0) {
            // remove
            orderBook.removeOrder(orderId);
        } else {
            // add
            PlacedOrder placedOrder = new PlacedOrder(orderId, amount.compareTo(BigDecimal.ZERO) < 0 ? TradeType.sell : TradeType.buy, currencyPair, price, amount.abs());
            orderBook.addOrder(placedOrder);
        }
    }

    private CurrencyPair formatCurrencyPair(String currencyPair) {
        String currency1 = currencyPair.substring(3, 6);
        String currency2 = currencyPair.substring(0, 3);
        return new CurrencyPair(currency1, currency2);
    }

}
