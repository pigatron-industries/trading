package com.pigatron.trading.exchanges.impl.spacebtc.entity;


public class SpaceBtcAccount {

    private SpaceBtcAccountResult result;

    public SpaceBtcAccountResult getResult() {
        return result;
    }

    public void setResult(SpaceBtcAccountResult result) {
        this.result = result;
    }
}
