package com.pigatron.trading.exchanges.impl.gdax.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxWebsocketSubscribe {

    private String type = "subscribe";

    @JsonProperty("product_ids")
    private Set<String> productIds = new HashSet<>();

    private String signature;

    private String key;

    private String passphrase;

    private String timestamp;



    public GdaxWebsocketSubscribe(Set<String> productIds, String signature, String key, String passphrase, String timestamp) {
        this.productIds = productIds;
        this.signature = signature;
        this.key = key;
        this.passphrase = passphrase;
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(Set<String> productIds) {
        this.productIds = productIds;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPassphrase() {
        return passphrase;
    }

    public void setPassphrase(String passphrase) {
        this.passphrase = passphrase;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
