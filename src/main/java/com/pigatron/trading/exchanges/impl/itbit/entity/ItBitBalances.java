package com.pigatron.trading.exchanges.impl.itbit.entity;


import java.util.List;

public class ItBitBalances {

    private String id;

    private List<ItBitBalance> balances;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ItBitBalance> getBalances() {
        return balances;
    }

    public void setBalances(List<ItBitBalance> balances) {
        this.balances = balances;
    }
}
