package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request;


public class AbstractWatchRequest extends AbstractRequest {

    private Boolean watch = true;
    private Integer base;
    private Integer counter;

    public AbstractWatchRequest(String method) {
        super(method);
    }

    public Boolean getWatch() {
        return watch;
    }

    public void setWatch(Boolean watch) {
        this.watch = watch;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }
}
