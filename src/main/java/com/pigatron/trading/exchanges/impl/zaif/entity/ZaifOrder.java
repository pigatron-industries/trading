package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ZaifOrder {

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("currency_pair")
    private String currencyPair;

    private String action;

    private BigDecimal amount;

    private BigDecimal price;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
