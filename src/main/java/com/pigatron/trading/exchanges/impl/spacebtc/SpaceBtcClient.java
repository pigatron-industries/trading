package com.pigatron.trading.exchanges.impl.spacebtc;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.spacebtc.converter.SpaceBtcBalancesConverter;
import com.pigatron.trading.exchanges.impl.spacebtc.converter.SpaceBtcOrdersConverter;
import com.pigatron.trading.exchanges.impl.spacebtc.converter.SpaceBtcTradesConverter;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.*;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.ClientEndpoint;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
@ClientEndpoint
@Profile("spacebtc")
public class SpaceBtcClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(SpaceBtcClient.class);

    private static final String API_URL = "https://tapi.spacebtc.com/";
    private static final String WS_API_URL = "ws://wsapi.spacebtc.com/";

    private static final String GET_ORDER_BOOK_URL = API_URL + "depth/%s?limit=%d";
    private static final String GET_ACCOUNT_URL = API_URL + "account";
    private static final String PLACE_ORDER_URL = API_URL + "place_limit/%s";
    private static final String CANCEL_ORDER_URL = API_URL + "cancel_order/%s";
    private static final String GET_ORDERS_URL = API_URL + "open_orders/%s";
    private static final String GET_TRADES_URL = API_URL + "trade_history/%s";

    @Value("${exchange.spacebtc.enabled}")
    private Boolean enabled;

    @Value("${exchange.spacebtc.userid}")
    private String userId;

    @Value("${exchange.spacebtc.info1.key}")
    private String infoKey1;

    @Value("${exchange.spacebtc.info1.secret}")
    private String infoSecret1;

    @Value("${exchange.spacebtc.info2.key}")
    private String infoKey2;

    @Value("${exchange.spacebtc.info2.secret}")
    private String infoSecret2;

    @Value("${exchange.spacebtc.info3.key}")
    private String infoKey3;

    @Value("${exchange.spacebtc.info3.secret}")
    private String infoSecret3;


    @Value("${exchange.spacebtc.trade1.key}")
    private String tradeKey1;

    @Value("${exchange.spacebtc.trade1.secret}")
    private String tradeSecret1;

    @Value("${exchange.spacebtc.trade2.key}")
    private String tradeKey2;

    @Value("${exchange.spacebtc.trade2.secret}")
    private String tradeSecret2;

    private int infoKeyRotation = 1;

    private int tradeKeyRotation = 1;

    private BigDecimal makerFee = new BigDecimal("0.0002");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private SpaceBtcBalancesConverter balancesConverter;

    @Autowired
    private SpaceBtcTradesConverter tradesConverter;

    @Autowired
    private SpaceBtcOrdersConverter ordersConverter;

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    public SpaceBtcClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "spacebtc";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.01");
        } else if(currency.equals("ETH")) {
            return new BigDecimal("0.1");
        } else {
            return new BigDecimal("0.1");
        }
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.01");
    }



    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair), depth);

        ResponseEntity<SpaceBtcOrderBook> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), SpaceBtcOrderBook.class);
        return orderBookConverter.convert(response.getBody().getResult());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        HttpEntity<String> entity = createPrivateEntity("");
        String url = GET_ACCOUNT_URL + "?" + createQueryString(getInfoSignatureParams(), false);
        ResponseEntity<SpaceBtcAccount> response = restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcAccount.class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        try {
            return placeOrder(currencyPair, TradeType.buy, price, amount);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        try {
            return placeOrder(currencyPair, TradeType.sell, price, amount);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }

    @Override
    public  PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        String url = String.format(PLACE_ORDER_URL, formatCurrencyPair(currencyPair));
        Map<String, String> params = getTradeSignatureParams();
        url += "?" + createQueryString(params, false);
        String body = String.format("{\"side\":%s,\"amount\":\"%s\",\"price\":\"%s\"}",
                side==TradeType.buy ? "0" : "1", amount.toPlainString(), price.toPlainString());
        HttpEntity<String> entity = createPrivateEntity(body);
        ResponseEntity<SpaceBtcOrder> response = restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcOrder.class);
        return new PlacedOrder(response.getBody().getResult().getId()+"", side, currencyPair, price, amount);
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        cancelOrder(order.getOrderId(), null);
        return placeOrder(order.getCurrencyPair(), order.getType(), price, amount);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        try {
            String url = String.format(CANCEL_ORDER_URL, orderId);
            Map<String, String> params = getTradeSignatureParams();
            url += "?" + createQueryString(params, false);
            HttpEntity<String> entity = createPrivateEntity("");
            restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcMessage.class);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
        return true;
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        return null;
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        String url = String.format(GET_TRADES_URL, formatCurrencyPair(currencyPair));
        Map<String, String> params = getInfoSignatureParams();
        url += "?" + createQueryString(params, false);
        HttpEntity<String> entity = createPrivateEntity("");
        ResponseEntity<SpaceBtcTrades> response = restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcTrades.class);
        return tradesConverter.convert(response.getBody());
    }


    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        String url = String.format(GET_ORDERS_URL, formatCurrencyPair(currencyPair));
        Map<String, String> params = getInfoSignatureParams();
        url += "?" + createQueryString(params, false);
        HttpEntity<String> entity = createPrivateEntity("");
        ResponseEntity<SpaceBtcOrders> response = restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcOrders.class);
        return ordersConverter.convert(response.getBody());
    }


    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 2000;
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2() + "_" + currencyPair.getCurrency1();
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("User-Agent", "Java");
        return new HttpEntity<>("", headers);
    }

    private HttpEntity<String> createPrivateEntity(String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("User-Agent", "Java");
        return new HttpEntity<>(body, headers);
    }

    private Map<String, String> getInfoSignatureParams() {
        if(infoKeyRotation == 1) {
            infoKeyRotation = 2;
            return getSignatureParams(infoKey1, infoSecret1);
        } else if(infoKeyRotation == 2) {
            infoKeyRotation = 3;
            return getSignatureParams(infoKey2, infoSecret2);
        } else {
            infoKeyRotation = 1;
            return getSignatureParams(infoKey3, infoSecret3);
        }
    }

    private Map<String, String> getTradeSignatureParams() {
        if(tradeKeyRotation == 1) {
            tradeKeyRotation = 2;
            return getSignatureParams(tradeKey1, tradeSecret1);
        } else {
            tradeKeyRotation = 1;
            return getSignatureParams(tradeKey2, tradeSecret2);
        }
    }

    private Map<String, String> getSignatureParams(String key, String secret) {
        Map<String, String> params = new HashMap<>();
        String nonce = createNonce()+"";
        params.put("key", key);
        params.put("signature", getSignature(nonce, key, secret));
        params.put("nonce", nonce);
        return params;
    }

    private long createNonce() {
        return new Date().getTime();
    }

    private String getSignature(String nonce, String key, String secret) {
        try {
            String prehash = nonce + userId + key;
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(prehash.getBytes("UTF-8"))).toUpperCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(HttpClientErrorException exception) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SpaceBtcMessage message = objectMapper.readValue(exception.getResponseBodyAsByteArray(), SpaceBtcMessage.class);
            if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND) || message.getErrorDescription().equals("OrderNotFound")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, exception, message.getErrorDescription());
            } else if(message.getErrorDescription().equals("InsufficientFunds")) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, exception, message.getErrorDescription());
            //} else if(message.getErrorDescription().equals("Order already done")) {
            //    return new ExchangeClientException(ExchangeClientExceptionType.ORDER_FILLED, exception);
            } else {
                logger.error(message.getErrorDescription(), exception);
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, exception, message.getErrorDescription());
            }
        } catch(IOException e) {
            logger.error("IOException", e);
            return null;
        }
    }

}
