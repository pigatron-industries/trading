package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import java.math.BigDecimal;

public class HitBtcMarketOrder {

    private BigDecimal price;
    private BigDecimal size;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }
}
