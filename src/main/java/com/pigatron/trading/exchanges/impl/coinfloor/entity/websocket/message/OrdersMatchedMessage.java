package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.CoinFloorAssetCode;

import java.math.BigDecimal;

public class OrdersMatchedMessage extends AbstractMessage {

    private Long bid;
    @JsonProperty("bid_tonce")
    private Long bidTonce;
    private Long ask;
    @JsonProperty("ask_tonce")
    private Long askTonce;
    private int base;
    private int counter;
    private BigDecimal quantity;
    private BigDecimal price;
    private BigDecimal total;
    @JsonProperty("bid_rem")
    private BigDecimal bidRem;
    @JsonProperty("ask_rem")
    private BigDecimal askRem;
    private long time;
    @JsonProperty("bid_base_fee")
    private BigDecimal bidBaseFee;
    @JsonProperty("bid_counter_fee")
    private BigDecimal bidCounterFee;
    @JsonProperty("ask_base_fee")
    private BigDecimal askBaseFee;
    @JsonProperty("ask_counter_fee")
    private BigDecimal askCounterFee;

    public Long getBid() {
        return bid;
    }

    public void setBid(Long bid) {
        this.bid = bid;
    }

    public Long getBidTonce() {
        return bidTonce;
    }

    public void setBidTonce(Long bidTonce) {
        this.bidTonce = bidTonce;
    }

    public Long getAsk() {
        return ask;
    }

    public void setAsk(Long ask) {
        this.ask = ask;
    }

    public Long getAskTonce() {
        return askTonce;
    }

    public void setAskTonce(Long askTonce) {
        this.askTonce = askTonce;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getBidRem() {
        return bidRem;
    }

    public void setBidRem(BigDecimal bidRem) {
        this.bidRem = bidRem;
    }

    public BigDecimal getAskRem() {
        return askRem;
    }

    public void setAskRem(BigDecimal askRem) {
        this.askRem = askRem;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public BigDecimal getBidBaseFee() {
        return bidBaseFee;
    }

    public void setBidBaseFee(BigDecimal bidBaseFee) {
        this.bidBaseFee = bidBaseFee;
    }

    public BigDecimal getBidCounterFee() {
        return bidCounterFee;
    }

    public void setBidCounterFee(BigDecimal bidCounterFee) {
        this.bidCounterFee = bidCounterFee;
    }

    public BigDecimal getAskBaseFee() {
        return askBaseFee;
    }

    public void setAskBaseFee(BigDecimal askBaseFee) {
        this.askBaseFee = askBaseFee;
    }

    public BigDecimal getAskCounterFee() {
        return askCounterFee;
    }

    public void setAskCounterFee(BigDecimal askCounterFee) {
        this.askCounterFee = askCounterFee;
    }

    public CurrencyPair getCurrencyPair() {
        return new CurrencyPair(CoinFloorAssetCode.getById(counter).name(), CoinFloorAssetCode.getById(base).name());
    }

    //TODO there doesn't seem to be any way of getting the side based on other values???
    public TradeType getType() {
        return ask != null ? TradeType.sell : TradeType.buy;
    }
}
