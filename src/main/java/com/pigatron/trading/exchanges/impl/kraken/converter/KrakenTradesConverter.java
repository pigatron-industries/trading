package com.pigatron.trading.exchanges.impl.kraken.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenTrade;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenTradesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Component
public class KrakenTradesConverter implements Converter<KrakenTradesResponse, List<Trade>> {

    @Autowired
    KrakenCurrencyPairConverter currencyPairConverter;

    @Override
    public List<Trade> convert(KrakenTradesResponse krakenTradesResponse) {
        List<Trade> trades = new ArrayList<>();

        Map<String, KrakenTrade> krakenTrades = krakenTradesResponse.getResult().getTrades();
        for(Map.Entry<String, KrakenTrade> entry : krakenTrades.entrySet()) {
            Trade trade = new Trade();
            trade.setTradeId(entry.getKey());
            trade.setDate(new Date(entry.getValue().getTime()));
            trade.setOrderId(entry.getValue().getOrdertxid());
            trade.setType(entry.getValue().getType());
            trade.setPrice(entry.getValue().getPrice());
            trade.setQuantity(entry.getValue().getVol());
            trade.setFee(entry.getValue().getFee());
            trade.setCurrencyPair(currencyPairConverter.convert(entry.getValue().getPair()));
            trades.add(trade);
        }

        return trades;
    }
}
