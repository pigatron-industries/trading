package com.pigatron.trading.exchanges.impl.itbit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.itbit.converter.ItBitBalancesConverter;
import com.pigatron.trading.exchanges.impl.itbit.entity.ItBitBalances;
import com.pigatron.trading.exchanges.impl.itbit.entity.ItBitMessage;
import com.pigatron.trading.stats.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Date;

@Component
@Profile("itbit")
public class ItBitClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(ItBitClient.class);

    private static final String API_URL = "https://api.itbit.com/v1";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/markets/%s/order_book";
    private static final String GET_WALLET_URL = API_URL + "/wallets/%s";
    private static final String PLACE_ORDER_URL = API_URL + "wallets/%s/orders";

    @Value("${exchange.itbit.enabled}")
    private Boolean enabled;

    @Value("${exchange.itbit.key}")
    private String userId;

    @Value("${exchange.itbit.secret}")
    private String secret;

    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;


    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private ItBitBalancesConverter balancesConverter;


    @Autowired
    public ItBitClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "itbit";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return null;
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.01");
    }



    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair));
        CommonOrderBook orderBook = restTemplate.getForObject(url, CommonOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        String url = String.format(GET_WALLET_URL, userId);
        HttpEntity<String> entity = createPrivateEntity("", "GET", url);
        ResponseEntity<ItBitBalances> response = restTemplate.exchange(url, HttpMethod.GET, entity, ItBitBalances.class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        try {
            return placeOrder(currencyPair, TradeType.buy, price, amount);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        try {
            return placeOrder(currencyPair, TradeType.buy, price, amount);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }

    @Override
    public  PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
//        String url = String.format(PLACE_ORDER_URL, formatCurrencyPair(currencyPair));
//        Map<String, String> params = getTradeSignatureParams();
//        url += "?" + createQueryString(params);
//        String body = String.format("{\"side\":%s,\"amount\":\"%s\",\"price\":\"%s\"}",
//                side==TradeType.buy ? "0" : "1", amount.toPlainString(), price.toPlainString());
//        HttpEntity<String> entity = createPrivateEntity(body);
//        ResponseEntity<SpaceBtcOrder> response = restTemplate.exchange(url, HttpMethod.POST, entity, SpaceBtcOrder.class);
//        return new PlacedOrder(response.getBody().getResult().getId()+"", side, currencyPair, price, amount);
        return null; //TODO
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        cancelOrder(order.getOrderId(), null);
        return placeOrder(order.getCurrencyPair(), order.getType(), price, amount);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return false;
    }



    private HttpEntity<String> createPrivateEntity(String body, String method, String url) {
        String timestamp = getTimestamp()+"";
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", getSignature(timestamp, method, url, body));
        headers.add("X-Auth-Timestamp", timestamp);
        headers.add("X-Auth-Nonce", timestamp);
        headers.add("Content-Type", "application/json");
        return new HttpEntity<>(body, headers);
    }

    private long getTimestamp() {
        return new Date().getTime();
    }

    private String getSignature(String timestamp, String method, String url, String body) {
        try {
            String prehash = String.format("%s[\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"]", timestamp, method, url, body, timestamp, timestamp);

            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(prehash.getBytes("UTF-8"));
            byte[] digest = md.digest();
            String messageHash = url + new String(digest);

            //byte[] secretDecoded = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            return Base64.getEncoder().encodeToString(mac.doFinal(messageHash.getBytes()));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(HttpClientErrorException exception) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ItBitMessage message = objectMapper.readValue(exception.getResponseBodyAsByteArray(), ItBitMessage.class);
            if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND) || message.getCode() == 80005) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, exception, message.getDescription());
            } else if(message.getCode() == 81001) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, exception, message.getDescription());
            } else if(message.getCode() == 81002) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_FILLED, exception, message.getDescription());
            } else {
                logger.error(message.getDescription(), exception);
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, exception, message.getDescription());
            }
        } catch(IOException e) {
            logger.error("IOException", e);
            return null;
        }
    }

    private String formatCurrencyPair(CurrencyPair currencyPair) {
        String currency1 = currencyPair.getCurrency1();
        if(currency1.equals("BTC")) {
            currency1 = "XBT";
        }
        String currency2 = currencyPair.getCurrency2();
        if(currency2.equals("BTC")) {
            currency2 = "XBT";
        }
        return currency2 + currency1;
    }

}
