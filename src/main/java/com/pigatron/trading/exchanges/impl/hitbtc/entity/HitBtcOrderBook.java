package com.pigatron.trading.exchanges.impl.hitbtc.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HitBtcOrderBook {

    private List<HitBtcMarketOrder> ask;
    private List<HitBtcMarketOrder> bid;

    public List<HitBtcMarketOrder> getAsk() {
        return ask;
    }

    public void setAsk(List<HitBtcMarketOrder> ask) {
        this.ask = ask;
    }

    public List<HitBtcMarketOrder> getBid() {
        return bid;
    }

    public void setBid(List<HitBtcMarketOrder> bid) {
        this.bid = bid;
    }
}
