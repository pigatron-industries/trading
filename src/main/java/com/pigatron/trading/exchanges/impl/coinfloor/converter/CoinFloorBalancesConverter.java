package com.pigatron.trading.exchanges.impl.coinfloor.converter;


import com.pigatron.trading.exchanges.entity.Balance;
import com.pigatron.trading.exchanges.entity.Balances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


@Component
public class CoinFloorBalancesConverter implements Converter<Map<String, BigDecimal>, Balances> {

    @Override
    public Balances convert(Map<String, BigDecimal> sourceBalances) {
        Map<String, Balance> currencyBalanceMap = new HashMap<>();
        for (Map.Entry<String, BigDecimal> sourceBalance : sourceBalances.entrySet()) {
            String key = sourceBalance.getKey();
            String currency = formatCurrency(key.substring(0, 3));
            if(!currencyBalanceMap.containsKey(currency)) {
                currencyBalanceMap.put(currency, new Balance());
            }
            if(key.endsWith("available")) {
                currencyBalanceMap.get(currency).setAvailable(sourceBalance.getValue());
            } else if(key.endsWith("reserved")) {
                currencyBalanceMap.get(currency).setOnOrders(sourceBalance.getValue());
            }
        }

        Balances balances = new Balances();
        balances.setBalances(currencyBalanceMap);
        return balances;
    }

    private String formatCurrency(String currency) {
        if(currency.equals("xbt")) {
            return "BTC";
        } else {
            return currency.toUpperCase();
        }
    }
}
