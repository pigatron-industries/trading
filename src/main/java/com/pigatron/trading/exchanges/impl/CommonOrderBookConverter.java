package com.pigatron.trading.exchanges.impl;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class CommonOrderBookConverter implements Converter<CommonOrderBook, OrderBook> {

    private static int PRICE_INDEX = 0;
    private static int QUANTITY_INDEX = 1;

    @Override
    public OrderBook convert(CommonOrderBook commonOrderBook) {
        OrderBook orderBook = new OrderBook();

        for(List<BigDecimal> order : commonOrderBook.getAsks()) {
            BigDecimal price = order.get(PRICE_INDEX);
            BigDecimal quantity = order.get(QUANTITY_INDEX);
            orderBook.addOrder(new MarketOrder(TradeType.sell, price, quantity));
        }

        for(List<BigDecimal> order : commonOrderBook.getBids()) {
            BigDecimal price = order.get(PRICE_INDEX);
            BigDecimal quantity = order.get(QUANTITY_INDEX);
            orderBook.addOrder(new MarketOrder(TradeType.buy, price, quantity));
        }

        return orderBook;
    }
}
