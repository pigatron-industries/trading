package com.pigatron.trading.exchanges.impl.liqui.converter;


import com.google.common.collect.Lists;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiTrade;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiTradesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class LiquiTradesConverter implements Converter<LiquiTradesResponse, List<Trade>> {

    @Override
    public List<Trade> convert(LiquiTradesResponse source) {
        List<Trade> trades = new ArrayList<>();

        for (Map.Entry<String, LiquiTrade> entry : source.getTrades().entrySet()) {
            Trade trade = new Trade();
            trade.setTradeId(entry.getKey());
            trade.setType(entry.getValue().getType());
            trade.setPrice(entry.getValue().getRate());
            trade.setQuantity(entry.getValue().getAmount());
            trades.add(trade);
        }

        return Lists.reverse(trades);
    }
}
