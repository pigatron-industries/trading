package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "notice")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "Welcome", value = WelcomeMessage.class),
        @JsonSubTypes.Type(name = "BalanceChanged", value = BalanceChangedMessage.class),
        @JsonSubTypes.Type(name = "OrderOpened", value = OrderOpenedMessage.class),
        @JsonSubTypes.Type(name = "OrdersMatched", value = OrdersMatchedMessage.class),
        @JsonSubTypes.Type(name = "OrderClosed", value = OrderClosedMessage.class)
})
public class AbstractMessage {

    private String notice;

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
}
