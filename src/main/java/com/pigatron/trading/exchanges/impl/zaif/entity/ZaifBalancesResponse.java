package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ZaifBalancesResponse {

    private int success;

    @JsonProperty("return")
    private ZaifInfo balances;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ZaifInfo getBalances() {
        return balances;
    }

    public void setBalances(ZaifInfo balances) {
        this.balances = balances;
    }
}
