package com.pigatron.trading.exchanges.impl.coinfloor.entity.rest;


import com.fasterxml.jackson.annotation.JsonProperty;

public class CoinFloorMessage {

    @JsonProperty("error_code")
    private int code;

    @JsonProperty("error_msg")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
