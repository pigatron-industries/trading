package com.pigatron.trading.exchanges.impl.poloniex.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
public class PoloniexBalancesConverter implements Converter<Map, Balances> {

    public static final String AVAILABLE_KEY = "available";
    public static final String ONORDERS_KEY = "onOrders";
    public static final String BTCVALUE_KEY = "btcValue";

    @Override
    public Balances convert(Map sourceBalances) {
        Balances balances = new Balances();
        sourceBalances.forEach((currencySymbol, sourceBalance) -> {
            Map<String, String> sourceBalanceMap = (Map<String, String>)sourceBalance;
            balances.setBalance((String)currencySymbol,
                    new BigDecimal(sourceBalanceMap.get(AVAILABLE_KEY)),
                    new BigDecimal(sourceBalanceMap.get(ONORDERS_KEY)),
                    new BigDecimal(sourceBalanceMap.get(BTCVALUE_KEY)));
        });
        return balances;
    }
}
