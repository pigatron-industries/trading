package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class AuthenticateRequest extends AbstractRequest {

    //private Long tag;
    @JsonProperty("user_id")
    private Long userId;
    private String cookie;
    private String nonce;
    private List<String> signature;

    public AuthenticateRequest(Long userId, String cookie, String nonce, String signatureR, String signatureS) {
        super("Authenticate");
        this.userId = userId;
        this.cookie = cookie;
        this.nonce = nonce;
        signature = new ArrayList<>();
        signature.add(signatureR);
        signature.add(signatureS);
    }
//
//    public Long getTag() {
//        return tag;
//    }
//
//    public void setTag(Long tag) {
//        this.tag = tag;
//    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public List<String> getSignature() {
        return signature;
    }

    public void setSignature(List<String> signature) {
        this.signature = signature;
    }
}
