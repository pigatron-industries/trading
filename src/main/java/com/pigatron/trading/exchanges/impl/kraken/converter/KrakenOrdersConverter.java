package com.pigatron.trading.exchanges.impl.kraken.converter;


import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenOrder;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenOrders;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenTrade;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenTradesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Component
public class KrakenOrdersConverter implements Converter<Map<String, KrakenOrder>, List<PlacedOrder>> {

    @Autowired
    KrakenCurrencyPairConverter currencyPairConverter;

    @Override
    public List<PlacedOrder> convert(Map<String, KrakenOrder> krakenOrders) {
        List<PlacedOrder> placedOrders = new ArrayList<>();

        for(Map.Entry<String, KrakenOrder> entry : krakenOrders.entrySet()) {
            PlacedOrder placedOrder = new PlacedOrder();
            placedOrder.setOrderId(entry.getKey());
            placedOrder.setAmount(entry.getValue().getVol());
            placedOrder.setPrice(entry.getValue().getDescr().getPrice());
            placedOrder.setType(entry.getValue().getDescr().getType());
            placedOrder.setCurrencyPair(currencyPairConverter.convert(entry.getValue().getDescr().getPair()));
            placedOrders.add(placedOrder);
        }

        return placedOrders;
    }
}
