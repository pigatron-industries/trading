package com.pigatron.trading.exchanges.impl.gdax.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(name = "change", value = GdaxWebsocketChange.class),
        @JsonSubTypes.Type(name = "done", value = GdaxWebsocketDone.class),
        @JsonSubTypes.Type(name = "match", value = GdaxWebsocketMatch.class),
        @JsonSubTypes.Type(name = "open", value = GdaxWebsocketOpen.class),
        @JsonSubTypes.Type(name = "received", value = GdaxWebsocketReceived.class),
        @JsonSubTypes.Type(name = "heartbeat", value = GdaxWebsocketHeartbeat.class),
})
public abstract class GdaxWebsocketMessage {

    private String type;
    private Date time;
    @JsonProperty("product_id")
    private String productId;
    private Long sequence;

    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("profile_id")
    private String profileId;



    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getSequence() {
        return sequence;
    }

    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }
}
