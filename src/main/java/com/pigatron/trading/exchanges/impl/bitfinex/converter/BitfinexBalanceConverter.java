package com.pigatron.trading.exchanges.impl.bitfinex.converter;

import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.BitfinexBalance;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BitfinexBalanceConverter implements Converter<List<BitfinexBalance>, Balances> {

    @Override
    public Balances convert(List<BitfinexBalance> source) {
        Balances balances = new Balances();
        for (BitfinexBalance bitfinexBalance : source) {
            balances.setBalance(bitfinexBalance.getCurrency().toUpperCase(), bitfinexBalance.getAvailable(),
                    bitfinexBalance.getAmount().subtract(bitfinexBalance.getAvailable()), null);
        }
        return balances;
    }
}
