package com.pigatron.trading.exchanges.impl.kraken.entity;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class KrakenTrade {

    private String ordertxid;
    private String pair;
    private long time;
    private TradeType type;
    private BigDecimal price;
    private BigDecimal cost;
    private BigDecimal fee;
    private BigDecimal vol;

    public String getOrdertxid() {
        return ordertxid;
    }

    public void setOrdertxid(String ordertxid) {
        this.ordertxid = ordertxid;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getVol() {
        return vol;
    }

    public void setVol(BigDecimal vol) {
        this.vol = vol;
    }
}
