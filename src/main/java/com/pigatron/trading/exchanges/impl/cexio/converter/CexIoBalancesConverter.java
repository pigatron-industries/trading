package com.pigatron.trading.exchanges.impl.cexio.converter;

import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.cexio.entity.CexIoBalances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CexIoBalancesConverter implements Converter<CexIoBalances, Balances> {

    @Override
    public Balances convert(CexIoBalances source) {
        Balances balances = new Balances();
        balances.setBalance("EUR", source.getEUR().getAvailable(), source.getEUR().getOrders(), null);
        balances.setBalance("USD", source.getUSD().getAvailable(), source.getUSD().getOrders(), null);
        balances.setBalance("BTC", source.getBTC().getAvailable(), source.getBTC().getOrders(), null);
        balances.setBalance("LTC", source.getLTC().getAvailable(), source.getLTC().getOrders(), null);
        balances.setBalance("ETH", source.getETH().getAvailable(), source.getETH().getOrders(), null);
        return balances;
    }
}
