package com.pigatron.trading.exchanges.impl.quoine.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.quoine.entity.QuoineTrade;
import com.pigatron.trading.exchanges.impl.quoine.entity.QuoineTrades;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTrade;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTradesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class QuoineTradesConverter implements Converter<List<QuoineTrade>, List<Trade>> {

    @Override
    public List<Trade> convert(List<QuoineTrade> source) {
        List<Trade> trades = new ArrayList<>();

        for (QuoineTrade sourceTrade : source) {
            Trade trade = new Trade();
            trade.setTradeId(sourceTrade.getId());
            trade.setType(sourceTrade.getMySide());
            trade.setPrice(sourceTrade.getPrice());
            trade.setQuantity(sourceTrade.getQuantity());
            trades.add(trade);
        }

        return trades;
    }
}
