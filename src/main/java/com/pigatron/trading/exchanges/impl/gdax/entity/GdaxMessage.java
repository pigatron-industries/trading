package com.pigatron.trading.exchanges.impl.gdax.entity;


public class GdaxMessage {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
