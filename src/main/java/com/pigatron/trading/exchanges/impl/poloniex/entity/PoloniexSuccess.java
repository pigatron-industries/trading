package com.pigatron.trading.exchanges.impl.poloniex.entity;


public class PoloniexSuccess {

    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
