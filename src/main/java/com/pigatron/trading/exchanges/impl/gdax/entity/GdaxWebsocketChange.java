package com.pigatron.trading.exchanges.impl.gdax.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxWebsocketChange extends GdaxWebsocketMessage {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("new_size")
    private BigDecimal newSize;
    @JsonProperty("old_size")
    private BigDecimal oldSize;
    private BigDecimal price;
    private TradeType side;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getNewSize() {
        return newSize;
    }

    public void setNewSize(BigDecimal newSize) {
        this.newSize = newSize;
    }

    public BigDecimal getOldSize() {
        return oldSize;
    }

    public void setOldSize(BigDecimal oldSize) {
        this.oldSize = oldSize;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TradeType getSide() {
        return side;
    }

    public void setSide(TradeType side) {
        this.side = side;
    }
}
