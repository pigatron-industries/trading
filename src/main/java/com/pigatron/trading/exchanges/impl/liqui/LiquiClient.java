package com.pigatron.trading.exchanges.impl.liqui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.liqui.converter.LiquiBalancesConverter;
import com.pigatron.trading.exchanges.impl.liqui.converter.LiquiOrdersConverter;
import com.pigatron.trading.exchanges.impl.liqui.converter.LiquiTradesConverter;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiInfoWrapper;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiOrderWrapper;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiOrdersWrapper;
import com.pigatron.trading.exchanges.impl.liqui.entity.LiquiTradesResponse;
import com.pigatron.trading.stats.StatsService;
import com.pigatron.trading.util.RateLimiter;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@Profile("liqui")
public class LiquiClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(LiquiClient.class);

    private static final String API_URL = "https://api.liqui.io/api/3";
    private static final String TRADE_API_URL = "https://api.liqui.io/tapi";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/depth/%s";
    private static final String GET_BALANCES_URL = API_URL + "/getInfo";

    private RateLimiter rateLimiter = new RateLimiter(200);


    @Value("${exchange.liqui.key}")
    private String key;

    @Value("${exchange.liqui.secret}")
    private String secret;

    @Value("${exchange.liqui.enabled}")
    private Boolean enabled;


    private BigDecimal makerFee = new BigDecimal("0.001");
    private BigDecimal takerFee = new BigDecimal("0.0025");
    private BigDecimal withdrawFee = null;

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private LiquiBalancesConverter balancesConverter;

    @Autowired
    private LiquiTradesConverter tradesConverter;

    @Autowired
    private LiquiOrdersConverter ordersConverter;


    @Autowired
    public LiquiClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.001");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.00000001");
    }

    @Override
    public String getName() {
        return "liqui";
    }

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair));
        ResponseEntity<HashMap<String, CommonOrderBook>> response = restTemplate.exchange(url, HttpMethod.GET,
                createPublicEntity(), new ParameterizedTypeReference<HashMap<String, CommonOrderBook>>() {});
        return orderBookConverter.convert(response.getBody().get(formatCurrencyPair(currencyPair)));
    }


    @Override
    public Balances getBalances() {
        Map<String, String> params = new HashMap<>();
        params.put("method", "getInfo");
        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiInfoWrapper> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiInfoWrapper.class);
        Balances balances = balancesConverter.convert(response.getBody().getInfo());

        List<PlacedOrder> orders = getOpenOrders();
        for (PlacedOrder order : orders) {
            String currency = order.getType().equals(TradeType.buy) ?
                    order.getCurrencyPair().getCurrency1() : order.getCurrencyPair().getCurrency2();
            Balance balance = balances.getBalance(currency);
            BigDecimal ordersAmount = balance.getOnOrders();
            balance.setOnOrders(ordersAmount.add(order.getAmount()));
        }

        return balances;
    }

    private List<PlacedOrder> getOpenOrders() {
        Map<String, String> params = new HashMap<>();
        params.put("method", "activeOrders");
        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiOrdersWrapper> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiOrdersWrapper.class);
        if(response.getBody().getError() != null && response.getBody().getError().equals("no orders")) {
            return new ArrayList<>();
        }
        return ordersConverter.convert(response.getBody());
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        Map<String, String> params = new HashMap<>();
        params.put("method", "activeOrders");
        params.put("pair", formatCurrencyPair(currencyPair));
        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiOrdersWrapper> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiOrdersWrapper.class);
        if(response.getBody().getError() != null && response.getBody().getError().equals("no orders")) {
            return new ArrayList<>();
        }
        return ordersConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        return placeOrder(currencyPair, side, price, amount, true);
    }

    private PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount, Boolean rateLimited) {
        rateLimiter.acquire();
        Map<String, String> params = new HashMap<>();
        params.put("method", "trade");
        params.put("pair", formatCurrencyPair(currencyPair));
        params.put("type", side.name());
        params.put("rate", price.toPlainString());
        params.put("amount", amount.toPlainString());
        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiOrderWrapper> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiOrderWrapper.class);

        if(response.getBody().isSuccess()) {
            return new PlacedOrder(response.getBody().getOrder().getOrderId().toString(), side, currencyPair, price, amount);
        } else {
            throw convertException(response.getBody().getError());
        }
    }


    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return cancelOrder(orderId, currencyPair, true);
    }


    private boolean cancelOrder(String orderId, CurrencyPair currencyPair, Boolean rateLimited) throws ExchangeClientException {
        rateLimiter.acquire();
        Map<String, String> params = new HashMap<>();
        params.put("method", "cancelOrder");
        params.put("order_id", orderId);

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiOrderWrapper> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiOrderWrapper.class);

        if(response.getBody().isSuccess()) {
            return true;
        } else {
            throw convertException(response.getBody().getError());
        }
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        rateLimiter.acquire();
        Map<String, String> params = new HashMap<>();
        params.put("method", "tradeHistory");
        params.put("pair", formatCurrencyPair(currencyPair));
        params.put("count", "100");
        params.put("from_id", newTradesFilter.getLastTradeId(currencyPair)
                .orElseGet(() -> getLastTradeId(currencyPair)));

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<LiquiTradesResponse> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiTradesResponse.class);
        return tradesConverter.convert(response.getBody());
    }

    private String getLastTradeId(CurrencyPair currencyPair) {
        String lastTradeId = null;
        List<Trade> trades = null;

        while(trades == null || trades.size() == 1000) {
            Map<String, String> params = new HashMap<>();
            params.put("method", "tradeHistory");
            params.put("pair", formatCurrencyPair(currencyPair));
            params.put("count", "1000");
            if (lastTradeId != null) {
                params.put("from_id", lastTradeId);
            }
            HttpEntity<String> entity = createPrivateEntity(params);
            ResponseEntity<LiquiTradesResponse> response = restTemplate.exchange(TRADE_API_URL, HttpMethod.POST, entity, LiquiTradesResponse.class);
            trades = tradesConverter.convert(response.getBody());
            lastTradeId = trades.get(0).getTradeId();
            rateLimiter.acquire();
        }

        return lastTradeId;
    }

//    public List<PlacedOrder> getOpenOrders() {
//        rateLimiter.acquire();
//        String url = "/orders";
//        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
//        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);
//        return ((List<Map>)response.getBody()).stream()
//                .map(o -> orderConverter.convert((Map)o))
//                .collect(Collectors.toList());
//    }

//    @Override
//    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
//        try {
//            rateLimiter.acquire();
//            PlacedOrder placedOrder = getOrderById(orderId);
//            if (placedOrder.getStatus().equals(OrderStatus.DONE)) {
//                return Optional.empty();
//            } else {
//                return Optional.of(placedOrder);
//            }
//        } catch(HttpClientErrorException e) {
//            logger.error("getOpenOrderById error", e);
//            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
//                throw new OrderNotPlacedException();
//            } else {
//                throw new RuntimeException(e);
//            }
//        }
//    }



    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2().toLowerCase() + "_" + currencyPair.getCurrency1().toLowerCase();
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        return new HttpEntity<>("", headers);
    }

    private HttpEntity<String> createPrivateEntity(Map<String, String> params) {
        long nonce = createNonce();
        params.put("nonce", nonce+"");
        String body = createQueryString(params, false);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("User-Agent", "Java");
        headers.add("Key", key);
        headers.add("Sign", getSignature(body));
        return new HttpEntity<>(body, headers);
    }

    private long createNonce() {
        try {
            long base_date = (new SimpleDateFormat("yyyy-MM-dd")).parse("2016-01-01").getTime();
            return (System.currentTimeMillis()-base_date)/100;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSignature(String body) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(), "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(body.getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(String errorMessage) {
        if(errorMessage.startsWith("Not enough")) {
            return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, errorMessage);
        } else if(errorMessage.startsWith("Order") && errorMessage.endsWith("not found.")) {
            return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, errorMessage);
        } else {
            logger.error(errorMessage);
            return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, errorMessage);
        }
    }


    @Override
    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
    }

    @Override
    public int getTimeResolutionMillis() {
        return 1000;
    }



}
