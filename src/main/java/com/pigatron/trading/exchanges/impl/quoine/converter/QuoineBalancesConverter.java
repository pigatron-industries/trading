package com.pigatron.trading.exchanges.impl.quoine.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.quoine.entity.QuoineBalance;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifInfo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;

@Component
public class QuoineBalancesConverter implements Converter<QuoineBalance[], Balances> {

    @Override
    public Balances convert(QuoineBalance[] sourceBalances) {
        Balances balances = new Balances();

        for (QuoineBalance sourceBalance : sourceBalances) {
            balances.setBalance(sourceBalance.getCurrency(), sourceBalance.getBalance(), new BigDecimal("0"), null);
        }

        return balances;
    }
}
