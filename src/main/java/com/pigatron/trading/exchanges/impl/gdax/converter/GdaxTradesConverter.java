package com.pigatron.trading.exchanges.impl.gdax.converter;

import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class GdaxTradesConverter implements Converter<List, List<Trade>> {

    @Override
    public List<Trade> convert(List source) {
        List<Trade> trades = new ArrayList<>();
        for(Map sourceTrade : (List<Map>)source) {
            Trade trade = new Trade();

            BigDecimal price = new BigDecimal(sourceTrade.get("price").toString());
            BigDecimal quantity = new BigDecimal(sourceTrade.get("size").toString());

            try {
                trade.setDate(toDate(sourceTrade.get("created_at").toString()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            trade.setTradeId(sourceTrade.get("trade_id").toString());
            trade.setOrderId(sourceTrade.get("order_id").toString());
            trade.setType(TradeType.valueOf(sourceTrade.get("side").toString()));
            trade.setPrice(price);
            trade.setQuantity(quantity);
            trade.setFee(new BigDecimal(sourceTrade.get("fee").toString()));

            trade.setTotal(price.multiply(quantity));

            trades.add(trade);
        }
        return trades;
    }

    public Date toDate(String dateString) throws ParseException {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
            return format.parse(dateString);
        } catch(ParseException e) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            return format.parse(dateString);
        }
    }
}
