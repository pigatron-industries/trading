package com.pigatron.trading.exchanges.impl.kraken.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.kraken.entity.KrakenBalancesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Map;


@Component
public class KrakenBalancesConverter implements Converter<KrakenBalancesResponse, Balances> {

    @Autowired
    KrakenCurrencyCodeConverter currencyCodeConverter;

    @Override
    public Balances convert(KrakenBalancesResponse krakenBalances) {
        Balances balances = new Balances();

        for(Map.Entry<String, BigDecimal> entry : krakenBalances.getResult().entrySet()) {
            String currency = currencyCodeConverter.convert(entry.getKey());
            BigDecimal balance = entry.getValue();
            balances.setBalance(currency, balance, BigDecimal.ZERO, null);
        }

        return balances;
    }
}
