package com.pigatron.trading.exchanges.impl.kraken.entity;


import com.pigatron.trading.exchanges.impl.CommonOrderBook;

import java.util.List;
import java.util.Map;

public class KrakenOrderBookResponse {

    private List<String> error;
    private Map<String, CommonOrderBook> result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public Map<String, CommonOrderBook> getResult() {
        return result;
    }

    public void setResult(Map<String, CommonOrderBook> result) {
        this.result = result;
    }
}
