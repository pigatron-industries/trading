package com.pigatron.trading.exchanges.impl.bitmex.converter;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

@Component
public class BitMexOrderBookConverter implements Converter<List, OrderBook> {

    @Override
    public OrderBook convert(List source) {

        Set<MarketOrder> asks = new TreeSet<>();
        Set<MarketOrder> bids = new TreeSet<>();

        for (Map order : (List<Map>)source) {
            MarketOrder marketOrder = new MarketOrder();
            marketOrder.setPrice(new BigDecimal(order.get("price").toString()));
            marketOrder.setAmount(new BigDecimal(order.get("size").toString()));
            if(order.get("side").equals("Sell")) {
                asks.add(marketOrder);
            } else {
                bids.add(marketOrder);
            }
        }

        return new OrderBook(asks, bids);
    }
}
