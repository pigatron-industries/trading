package com.pigatron.trading.exchanges.impl.kraken;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.exceptions.OrderNotPlacedException;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxBalancesConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxChartDataConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxOrderConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxTradesConverter;
import com.pigatron.trading.exchanges.impl.gdax.entity.GdaxMessage;
import com.pigatron.trading.exchanges.impl.kraken.converter.KrakenBalancesConverter;
import com.pigatron.trading.exchanges.impl.kraken.converter.KrakenOrdersConverter;
import com.pigatron.trading.exchanges.impl.kraken.converter.KrakenTradesConverter;
import com.pigatron.trading.exchanges.impl.kraken.entity.*;
import com.pigatron.trading.stats.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Profile("kraken")
public class KrakenClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(KrakenClient.class);

    private static final String API_URL = "https://api.kraken.com";

    private static final String GET_ORDER_BOOK_PATH = "/0/public/Depth";
    private static final String GET_BALANCES_PATH = "/0/private/Balance";
    private static final String GET_TRADES_PATH = "/0/private/TradesHistory";
    private static final String GET_ORDERS_PATH = "/0/private/OpenOrders";
    private static final String PLACE_ORDER_PATH = "/0/private/AddOrder";
    private static final String CANCEL_ORDER_PATH = "/0/private/CancelOrder";

    @Value("${exchange.kraken.key}")
    private String key;

    @Value("${exchange.kraken.secret}")
    private String secret;

    @Value("${exchange.kraken.enabled}")
    private Boolean enabled;

    private Double timeSkew;

    private BigDecimal makerFee = new BigDecimal("0.0016");
    private BigDecimal takerFee = new BigDecimal("0.0026");

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private GdaxChartDataConverter chartDataConverter;

    @Autowired
    private KrakenBalancesConverter balancesConverter;

    @Autowired
    private KrakenTradesConverter tradesConverter;

    @Autowired
    private KrakenOrdersConverter ordersConverter;


    @Autowired
    public KrakenClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "kraken";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.0005");
        }
        if(currency.equals("LTC")) {
            return new BigDecimal("0.02");
        }
        if(currency.equals("ETH")) {
            return new BigDecimal("0.005");
        }
        if(currency.equals("ZEC")) {
            return new BigDecimal("0.0001");
        }
        return null;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.001");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("USD") || currency.equals("EUR") || currency.equals("GBP")) {
            return new BigDecimal("0.001");
        } else { //BTC
            return new BigDecimal("0.000001");
        }
    }


    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        Map<String, String> params = new HashMap<>();
        params.put("pair", formatCurrencyPair(currencyPair));
        params.put("count", depth+"");
        String url = API_URL + GET_ORDER_BOOK_PATH + "?" + createQueryString(params, false);

        KrakenOrderBookResponse orderBook = restTemplate.getForObject(url, KrakenOrderBookResponse.class);
        return orderBookConverter.convert(orderBook.getResult().entrySet().stream().findFirst().get().getValue());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        throw new NotImplementedException();
    }


    @Override
    public Balances getBalances() {
        String url = API_URL + GET_BALANCES_PATH;
        Map<String, String> params = new HashMap<>();
        HttpEntity<String> entity = createPrivateEntity(params, GET_BALANCES_PATH);
        ResponseEntity<KrakenBalancesResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, KrakenBalancesResponse.class);
        return balancesConverter.convert(response.getBody()); //TODO get balances on orders
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        String url = API_URL + PLACE_ORDER_PATH;
        Map<String, String> params = new HashMap<>();
        params.put("pair", formatCurrencyPair(currencyPair));
        params.put("type", side.name());
        params.put("ordertype", "limit");
        params.put("price", price.toPlainString());
        params.put("volume", amount.toPlainString());
        params.put("oflags", "post,fciq");

        HttpEntity<String> entity = createPrivateEntity(params, PLACE_ORDER_PATH);
        ResponseEntity<KrakenOrderResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, KrakenOrderResponse.class);
        if(response.getBody().getError().size() > 0) {
            throw convertException(response.getBody().getError().get(0));
        }
        return new PlacedOrder(response.getBody().getResult().getTxid().get(0), side, currencyPair, price, amount);
    }


    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        String url = API_URL + CANCEL_ORDER_PATH;
        Map<String, String> params = new HashMap<>();
        params.put("txid", orderId);

        HttpEntity<String> entity = createPrivateEntity(params, CANCEL_ORDER_PATH);
        ResponseEntity<KrakenResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, KrakenResponse.class);
        if(response.getBody().getError().size() > 0) {
            throw convertException(response.getBody().getError().get(0));
        }
        return true;
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        throw new NotImplementedException();
    }

    private List<Trade> getPrivateTrades() {
        String url = API_URL + GET_TRADES_PATH;
        Map<String, String> params = new HashMap<>();
        HttpEntity<String> entity = createPrivateEntity(params, GET_TRADES_PATH);
        ResponseEntity<KrakenTradesResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, KrakenTradesResponse.class);
        if(response.getBody().getError().size() > 0) {
            throw convertException(response.getBody().getError().get(0));
        }
        return tradesConverter.convert(response.getBody());
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        return getPrivateTrades().stream()
                .filter(t -> t.getCurrencyPair().equals(currencyPair))
                .collect(Collectors.toList());
    }

    public List<PlacedOrder> getOpenOrders() {
        String url = API_URL + GET_ORDERS_PATH;
        Map<String, String> params = new HashMap<>();
        HttpEntity<String> entity = createPrivateEntity(params, GET_ORDERS_PATH);
        ResponseEntity<KrakenOrdersResponse> response = restTemplate.exchange(url, HttpMethod.POST, entity, KrakenOrdersResponse.class);
        return ordersConverter.convert(response.getBody().getResult().getOpen());
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        return getOpenOrders().stream()
                .filter(o -> o.getCurrencyPair().equals(currencyPair))
                .collect(Collectors.toList());
    }


    @Override
    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
        throw new NotImplementedException();
    }



    @Scheduled(fixedDelay=5000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 6000;
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        String currency2 = currencyPair.getCurrency2();
        if(currency2.equals("BTC")) {
            currency2 = "XBT";
        }

        String currency1 = currencyPair.getCurrency1();
        if(currency1.equals("BTC")) {
            currency1 = "XBT";
        }

        return currency2 + currency1;
    }

    private String formatDate(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }


    private long createNonce() {
        return new Date().getTime();
    }

    private HttpEntity<String> createPrivateEntity(Map<String, String> params, String requestPath) {
        String nonce = createNonce()+"";
        params.put("nonce", nonce);
        String body = createQueryString(params, true);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.add("API-Key", key);
        headers.add("API-Sign", getSignature(requestPath, nonce, body));
        return new HttpEntity<>(body, headers);
    }

    private String getSignature(String requestPath, String nonce, String body) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(nonce.getBytes("UTF-8"));
            md.update(body.getBytes("UTF-8"));

            byte[] secretDecoded = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(secretDecoded, "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            mac.update(requestPath.getBytes());
            mac.update(md.digest());
            return Base64.getEncoder().encodeToString(mac.doFinal());
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(String error) {
        if(error.contains("Unknown order")) {
            return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, error);
        } else if(error.contains("Insufficient funds")) {
            return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, error);
        //} else if(error.equals("Order already done")) {
        //    return new ExchangeClientException(ExchangeClientExceptionType.ORDER_FILLED, null, error);
        } else {
            logger.error(error);
            return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, error);
        }
    }

}
