package com.pigatron.trading.exchanges.impl.zaif.entity;


import java.math.BigDecimal;
import java.util.Map;

public class ZaifInfo {

    private Map<String, BigDecimal> funds;

    public Map<String, BigDecimal> getFunds() {
        return funds;
    }

    public void setFunds(Map<String, BigDecimal> funds) {
        this.funds = funds;
    }
}
