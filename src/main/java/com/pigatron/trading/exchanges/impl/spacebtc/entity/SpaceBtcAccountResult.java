package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class SpaceBtcAccountResult {

    @JsonProperty("EUR")
    private SpaceBtcBalance EUR;
    @JsonProperty("BTC")
    private SpaceBtcBalance BTC;
    @JsonProperty("LTC")
    private SpaceBtcBalance LTC;
    @JsonProperty("ETH")
    private SpaceBtcBalance ETH;

    public SpaceBtcBalance getEUR() {
        return EUR;
    }

    public void setEUR(SpaceBtcBalance EUR) {
        this.EUR = EUR;
    }

    public SpaceBtcBalance getBTC() {
        return BTC;
    }

    public void setBTC(SpaceBtcBalance BTC) {
        this.BTC = BTC;
    }

    public SpaceBtcBalance getLTC() {
        return LTC;
    }

    public void setLTC(SpaceBtcBalance LTC) {
        this.LTC = LTC;
    }

    public SpaceBtcBalance getETH() {
        return ETH;
    }

    public void setETH(SpaceBtcBalance ETH) {
        this.ETH = ETH;
    }
}
