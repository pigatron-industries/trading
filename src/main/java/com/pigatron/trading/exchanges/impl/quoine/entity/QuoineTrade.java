package com.pigatron.trading.exchanges.impl.quoine.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class QuoineTrade {

    private String id;
    private BigDecimal quantity;
    private BigDecimal price;
    @JsonProperty("my_side")
    private TradeType mySide;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TradeType getMySide() {
        return mySide;
    }

    public void setMySide(TradeType mySide) {
        this.mySide = mySide;
    }
}
