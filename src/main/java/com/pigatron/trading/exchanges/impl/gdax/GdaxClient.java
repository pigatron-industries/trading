package com.pigatron.trading.exchanges.impl.gdax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractWebsocketExchangeClient;
import com.pigatron.trading.exchanges.ProductData;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.OrderNotPlacedException;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxBalancesConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxChartDataConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxOrderConverter;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxTradesConverter;
import com.pigatron.trading.exchanges.impl.gdax.entity.*;
import com.pigatron.trading.stats.StatsService;
import com.pigatron.trading.util.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.pigatron.trading.exchanges.entity.Trade.aTrade;

@Component
@ClientEndpoint
@Profile("gdax")
public class GdaxClient extends AbstractWebsocketExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(GdaxClient.class);

    private static final String API_URL = "https://api.gdax.com";
    private static final String WS_API_URL = "wss://ws-feed.gdax.com";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/products/%s/book?level=%d";
    private static final String GET_CHART_URL = API_URL + "/products/%s/candles";

    private RateLimiter rateLimiter = new RateLimiter(200);


    @Value("${exchange.gdax.key}")
    private String key;

    @Value("${exchange.gdax.secret}")
    private String secret;

    @Value("${exchange.gdax.passphrase}")
    private String passphrase;

    @Value("${exchange.gdax.enabled}")
    private Boolean enabled;

    private Double timeSkew;

    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private GdaxChartDataConverter chartDataConverter;

    @Autowired
    private GdaxBalancesConverter balancesConverter;

    @Autowired
    private GdaxTradesConverter tradesConverter;

    @Autowired
    private GdaxOrderConverter orderConverter;


    @Autowired
    public GdaxClient(StatsService statsService) {
        super(statsService, WS_API_URL);
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.001");
        } else if (currency.equals("BCH")) {
            return new BigDecimal("0.001");
        } else {
            return new BigDecimal("0.01");
        }
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("USD") || currency.equals("EUR") || currency.equals("GBP")) {
            return new BigDecimal("0.01");
        } else { //BTC/ETH/LTC
            return new BigDecimal("0.00000001");
        }
    }

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair), 2);
        CommonOrderBook orderBook = restTemplate.getForObject(url, CommonOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }

    private GdaxFullOrderBook getFullOrderBook(CurrencyPair currencyPair) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair), 3);
        GdaxFullOrderBook fullOrderBook = restTemplate.getForObject(url, GdaxFullOrderBook.class);
        fullOrderBook.setCurrencyPair(currencyPair);
        return fullOrderBook;
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        Long now = new Date().getTime()/1000;
        Long start = now - (periodSeconds*dataSize);

        Map<String, String> params = new HashMap<>();
        params.put("start", formatDate(new Date(start*1000)));
        params.put("end", formatDate(new Date(now*1000)));
        params.put("granularity", periodSeconds+"");

        String url = String.format(GET_CHART_URL, formatCurrencyPair(currencyPair)) + "?" + createQueryString(params, false);
        List response = restTemplate.getForObject(url, List.class);

        return chartDataConverter.convert(response);
    }

    @Override
    public String getName() {
        return "gdax";
    }


    @Override
    public Balances getBalances() {
        rateLimiter.acquire();
        HttpEntity<String> entity = createPrivateEntity("GET", "/accounts", "");
        ResponseEntity<List> response = restTemplate.exchange(API_URL + "/accounts", HttpMethod.GET, entity, List.class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrderWithRetry(currencyPair, TradeType.buy, price, amount, true);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrderWithRetry(currencyPair, TradeType.sell, price, amount, true);
    }

    private PlacedOrder placeOrderWithRetry(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount, Boolean rateLimited) throws ExchangeClientException {
        ExchangeClientException exception = null;
        for(int i = 0; i < 3; i++) {
            try {
                return placeOrder(currencyPair, side, price, amount, rateLimited);
            } catch(HttpClientErrorException e) {
                exception = convertException(e);
            }
        }
        throw exception;
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        return placeOrder(currencyPair, side, price, amount, true);
    }

    private PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount, Boolean rateLimited) {
        if(rateLimited) {
            rateLimiter.acquire();
        }
        String request = String.format("{\"size\":\"%s\",\"price\":\"%s\",\"side\":\"%s\",\"product_id\":\"%s\",\"post_only\":true}",
                amount.toPlainString(), price.toPlainString(), side.name(), formatCurrencyPair(currencyPair));
        HttpEntity<String> entity = createPrivateEntity("POST", "/orders", request);
        ResponseEntity<Map> response = restTemplate.postForEntity(API_URL + "/orders", entity, Map.class);
        PlacedOrder placedOrder = new PlacedOrder(response.getBody().get("id").toString(), side, currencyPair, price, amount);
        getProductData(currencyPair).getOrderBook().addOrder(placedOrder);
        return placedOrder;
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        rateLimiter.acquire(2);
        try {
            cancelOrder(order.getOrderId(), null, false);
        } catch(ExchangeClientException e) {
            if(!e.getType().equals(ExchangeClientExceptionType.ORDER_FILLED) && !e.getType().equals(ExchangeClientExceptionType.ORDER_NOT_FOUND)) {
                throw e;
            }
        }
        return placeOrderWithRetry(order.getCurrencyPair(), order.getType(), price, amount, false);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return cancelOrder(orderId, currencyPair, true);
    }

    private boolean cancelOrder(String orderId, CurrencyPair currencyPair, Boolean rateLimited) throws ExchangeClientException {
        try {
            if(rateLimited) {
                rateLimiter.acquire();
            }
            String url = "/orders/" + orderId;
            //logger.info("cancel: " + url);
            HttpEntity<String> entity = createPrivateEntity("DELETE", url, "");
            restTemplate.exchange(API_URL + url, HttpMethod.DELETE, entity, String.class);
            if(currencyPair != null) {
                getProductData(currencyPair).getOrderBook().removeOrder(orderId);
            } else {
                removeOrderFromBook(orderId);
            }
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
        return true;
    }

    private void removeOrderFromBook(String orderId) {
        for (Map.Entry<CurrencyPair, ProductData> productDataEntry : productData.entrySet()) {
            productDataEntry.getValue().getOrderBook().removeOrder(orderId);
        }
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        rateLimiter.acquire();
        Map<String, String> params = new HashMap<>();
        params.put("order_id", order.getOrderId());
        String url = "/fills?" + createQueryString(params, false);
        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);
        return tradesConverter.convert(response.getBody());
    }

    @Override
    public List<Trade> getNewTrades(CurrencyPair currencyPair) {
        rateLimiter.acquire();
        ProductData productData = getProductData(currencyPair);
        Map<String, String> params = new HashMap<>();
        String url = "/fills";
        params.put("product_id", formatCurrencyPair(currencyPair));
        if(productData.getLastTradeId() != null) {
            params.put("before", productData.getLastTradeId());
        }
        if(params.size() > 0) {
            url += "?" + createQueryString(params, false);
        }
        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);

        List<Trade> trades = tradesConverter.convert(response.getBody());

        if(productData.getLastTradeId() == null) {
            trades = new ArrayList<>();
        }

        if(response.getHeaders().get("CB-BEFORE") != null) {
            productData.setLastTradeId(response.getHeaders().get("CB-BEFORE").get(0));
        }
        return trades;
    }

    public List<PlacedOrder> getOpenOrders() {
        rateLimiter.acquire();
        String url = "/orders";
        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);
        return ((List<Map>)response.getBody()).stream()
                .map(o -> orderConverter.convert((Map)o))
                .collect(Collectors.toList());
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        return getOpenOrders().stream()
                .filter(o -> o.getCurrencyPair().equals(currencyPair)).collect(Collectors.toList());
    }


    @Override
    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
        try {
            rateLimiter.acquire();
            PlacedOrder placedOrder = getOrderById(orderId);
            if (placedOrder.getStatus().equals(OrderStatus.DONE)) {
                return Optional.empty();
            } else {
                return Optional.of(placedOrder);
            }
        } catch(HttpClientErrorException e) {
            logger.error("getOpenOrderById error", e);
            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                throw new OrderNotPlacedException();
            } else {
                throw new RuntimeException(e);
            }
        }
    }

    private PlacedOrder getOrderById(String orderId) {
        rateLimiter.acquire();
        String url = "/orders/" + orderId;
        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
        ResponseEntity<Map> response = null;
        try {
            response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, Map.class);
        } catch(HttpClientErrorException e) {
            logger.error(e.getResponseBodyAsString());
            sleep(1000);
            response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, Map.class);
        }

        return orderConverter.convert(response.getBody());
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2() + "-" + currencyPair.getCurrency1();
    }

    private CurrencyPair formatCurrencyPair(String currencyPair) {
        String[] pairArray = currencyPair.split("-");
        return new CurrencyPair(pairArray[1], pairArray[0]);
    }

    private String formatDate(Date date) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        return df.format(date);
    }


    private String getTimestamp() {
        double localTime = (int)new Date().getTime() / 1000;
        if(timeSkew == null) {
            ResponseEntity<Map> response = restTemplate.getForEntity(API_URL + "/time", Map.class, new HashMap<>());
            double serverTime = ((Double)response.getBody().get("epoch"));
            timeSkew = serverTime - localTime;
            logger.info("time skew = " + timeSkew);
        }

        return String.format("%.0f", localTime + timeSkew );
    }

    private HttpEntity<String> createPrivateEntity(String method, String requestPath, String body) {
        String timestamp = getTimestamp();
        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("CB-ACCESS-KEY", key);
        headers.add("CB-ACCESS-SIGN", getSignature(timestamp, method, requestPath, body));
        headers.add("CB-ACCESS-TIMESTAMP", timestamp);
        headers.add("CB-ACCESS-PASSPHRASE", passphrase);
        return new HttpEntity<>(body, headers);
    }

    private String getSignature(String timestamp, String method, String requestPath, String body) {
        try {
            String prehash = timestamp + method + requestPath + body;
            byte[] secretDecoded = Base64.getDecoder().decode(secret);
            SecretKeySpec secretKey = new SecretKeySpec(secretDecoded, "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            return Base64.getEncoder().encodeToString(mac.doFinal(prehash.getBytes()));
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(HttpClientErrorException exception) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            GdaxMessage gdaxMessage = objectMapper.readValue(exception.getResponseBodyAsByteArray(), GdaxMessage.class);
            if(exception.getStatusCode().equals(HttpStatus.NOT_FOUND) || gdaxMessage.getMessage().equals("order not found")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, exception, gdaxMessage.getMessage());
            } else if(gdaxMessage.getMessage().equals("Insufficient funds")) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, exception, gdaxMessage.getMessage());
            } else if(gdaxMessage.getMessage().equals("Order already done")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_FILLED, exception, gdaxMessage.getMessage());
            } else if(gdaxMessage.getMessage().equals("request timestamp expired")) {
                logger.error(gdaxMessage.getMessage(), exception);
                logger.error("Correcting time skew.");
                timeSkew = null;
                getTimestamp();
                return new ExchangeClientException(ExchangeClientExceptionType.HANDLED, exception, gdaxMessage.getMessage());
            } else {
                logger.error(gdaxMessage.getMessage(), exception);
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, exception, gdaxMessage.getMessage());
            }
        } catch(IOException e) {
            logger.error("IOException", e);
            return null;
        }
    }



    /***************************************** Websockets ***************************************/

    @Override
    @Scheduled(fixedDelay=1000)
    public void poll() {
        checkHeartbeat();
        super.poll();
    }

    @Override
    public int getTimeResolutionMillis() {
        return 1000;
    }

    private Duration maxIdleDuration = Duration.ofMinutes(5);

    private void checkHeartbeat() {
        try {
            if (session != null && session.isOpen()) {
                for (Map.Entry<CurrencyPair, ProductData> productDataEntry : productData.entrySet()) {
                    if (productDataEntry.getValue() != null && productDataEntry.getKey() != null &&
                            productDataEntry.getValue().getLastHeartbeat().compareTo(maxIdleDuration) > 0) {
                        logger.warn("Heart is not beating. Attempting recovery...");
                        session.close();
                        productDataEntry.getValue().heartbeat();
                    }
                }
            }
        } catch(IOException e) {
            logger.error("Unexpected error", e);
        }
    }

    protected void onWebsocketConnectionError() {
        Set<CurrencyPair> currencyPairs = subscribers.keySet();
        currencyPairs.parallelStream()
                .forEach(this::registerNewTrades);
        currencyPairs.parallelStream()
                .forEach(this::fillCachedOrderBook);
    }

    public synchronized void subscribe(Set<CurrencyPair> currencyPairs) throws IOException {
        if(currencyPairs.size() == 0) {
            return;
        }

        logger.info("Subscribing to " + currencyPairs.toString());

        // clear current order books
        currencyPairs.stream()
                .map(this::getProductData)
                .forEach(ProductData::clearOrderBook);

        //get new trades which occured while websocket was down
        currencyPairs.forEach(this::registerNewTrades);

        // subscribe to websocket
        String timestamp = getTimestamp();
        String signature = getSignature(timestamp, "GET", "/users/self", "");
        Set<String> productIds = currencyPairs.stream()
                .map(this::formatCurrencyPair)
                .collect(Collectors.toSet());
        GdaxWebsocketSubscribe subscribeMessage = new GdaxWebsocketSubscribe(productIds, signature, key, passphrase, timestamp);
        String message = mapper.writeValueAsString(subscribeMessage);
        session.getBasicRemote().sendText(message);

        // get full order book from rest api
        currencyPairs.forEach(this::fillCachedOrderBook);

        //session.getBasicRemote().sendText("{\"type\":\"heartbeat\",\"on\":true}");
    }

    private void registerNewTrades(CurrencyPair currencyPair) {
        List<Trade> newTrades = getNewTrades(currencyPair);
        getProductData(currencyPair).addNewTrades(newTrades);
    }

    private void fillCachedOrderBook(CurrencyPair currencyPair) {
        GdaxFullOrderBook gdaxFullOrderBook = getFullOrderBook(currencyPair);
        FullOrderBook cachedOrderBook = getProductData(gdaxFullOrderBook.getCurrencyPair()).getOrderBook();
        for (List<String> values : gdaxFullOrderBook.getBids()) {
            BigDecimal price = new BigDecimal(values.get(0));
            BigDecimal size = new BigDecimal(values.get(1));
            String orderId = values.get(2);
            cachedOrderBook.addOrder(new PlacedOrder(orderId, TradeType.buy, currencyPair, price, size));
        }
        for (List<String> values : gdaxFullOrderBook.getAsks()) {
            BigDecimal price = new BigDecimal(values.get(0));
            BigDecimal size = new BigDecimal(values.get(1));
            String orderId = values.get(2);
            cachedOrderBook.addOrder(new PlacedOrder(orderId, TradeType.sell, currencyPair, price, size));
        }
        cachedOrderBook.setReady(true);
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
        try {
            //System.out.println(message);
            GdaxWebsocketMessage gdaxWebsocketMessage = mapper.readValue(message, GdaxWebsocketMessage.class);
            if (gdaxWebsocketMessage instanceof GdaxWebsocketDone) {
                onOrderDone((GdaxWebsocketDone) gdaxWebsocketMessage);
            } else if (gdaxWebsocketMessage instanceof GdaxWebsocketOpen) {
                onOrderOpen((GdaxWebsocketOpen) gdaxWebsocketMessage);
            } else if (gdaxWebsocketMessage instanceof GdaxWebsocketMatch) {
                onOrderMatched((GdaxWebsocketMatch) gdaxWebsocketMessage);
            }
//            else if (gdaxWebsocketMessage instanceof GdaxWebsocketHeartbeat) {
//                onHeartbeat((GdaxWebsocketHeartbeat) gdaxWebsocketMessage);
//            }
        } catch(Exception e) {
            logger.error("Unexpected error in websocket handler", e);
        }
    }

    private void onHeartbeat(GdaxWebsocketHeartbeat heartbeat) {
        CurrencyPair currencyPair = formatCurrencyPair(heartbeat.getProductId());
        ProductData productData = this.productData.get(currencyPair);
        productData.heartbeat();
    }

    private void onOrderOpen(GdaxWebsocketOpen openMessage) {
        CurrencyPair currencyPair = formatCurrencyPair(openMessage.getProductId());
        ProductData productData = getProductData(currencyPair);
        FullOrderBook orderBook = productData.getOrderBook();
        PlacedOrder placedOrder = new PlacedOrder(openMessage.getOrderId(), openMessage.getSide(),
                currencyPair, openMessage.getPrice(), openMessage.getRemainingSize());
        orderBook.addOrder(placedOrder);
        productData.heartbeat();
    }

    private void onOrderDone(GdaxWebsocketDone doneMessage) {
        CurrencyPair currencyPair = formatCurrencyPair(doneMessage.getProductId());
        FullOrderBook orderBook = getProductData(currencyPair).getOrderBook();
        if(doneMessage.getReason().equals("canceled") || doneMessage.getRemainingSize() == null ||
                doneMessage.getRemainingSize().compareTo(BigDecimal.ZERO) == 0) {
            orderBook.removeOrder(doneMessage.getOrderId(), doneMessage.getSide());
        } else {
            orderBook.updateOrder(doneMessage.getOrderId(), doneMessage.getSide(), doneMessage.getRemainingSize());
        }
    }

    private void onOrderMatched(GdaxWebsocketMatch matchMessage) {
        if(matchMessage.getUserId() != null) {
            logger.info("MATCH Price:" + matchMessage.getPrice().toPlainString() +
                    " Size:" + matchMessage.getSize().toPlainString() + " Side:" + matchMessage.getSide());

            CurrencyPair currencyPair = formatCurrencyPair(matchMessage.getProductId());
            ProductData productData = getProductData(currencyPair);

            Trade trade = aTrade()
                    .withTradeId(matchMessage.getTradeId())
                    .withType(matchMessage.getSide())
                    .withPrice(matchMessage.getPrice())
                    .withQuantity(matchMessage.getSize())
                    .build();

            productData.addNewTrade(trade);
        }
    }

}
