package com.pigatron.trading.exchanges.impl.livecoin.entity;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class LiveCoinTrade {

    private String id;
    private String datetime;
    private TradeType type;
    private String symbol;
    private BigDecimal price;
    private BigDecimal quantity;
    private BigDecimal commission;
    private String clientorderid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public String getClientorderid() {
        return clientorderid;
    }

    public void setClientorderid(String clientorderid) {
        this.clientorderid = clientorderid;
    }
}
