package com.pigatron.trading.exchanges.impl.hitbtc.converter;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcMarketOrder;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcOrderBook;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class HitBtcOrderBookConverter implements Converter<HitBtcOrderBook, OrderBook> {

    @Override
    public OrderBook convert(HitBtcOrderBook commonOrderBook) {
        OrderBook orderBook = new OrderBook();

        for(HitBtcMarketOrder order : commonOrderBook.getAsk()) {
            BigDecimal price = order.getPrice();
            BigDecimal quantity = order.getSize();
            orderBook.addOrder(new MarketOrder(TradeType.sell, price, quantity));
        }

        for(HitBtcMarketOrder order : commonOrderBook.getBid()) {
            BigDecimal price = order.getPrice();
            BigDecimal quantity = order.getSize();
            orderBook.addOrder(new MarketOrder(TradeType.buy, price, quantity));
        }

        return orderBook;
    }
}
