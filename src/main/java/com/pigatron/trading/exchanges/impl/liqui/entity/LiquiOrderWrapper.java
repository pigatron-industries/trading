package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiquiOrderWrapper {

    private boolean success;
    @JsonProperty("return")
    private LiquiOrder order;
    private String error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public LiquiOrder getOrder() {
        return order;
    }

    public void setOrder(LiquiOrder order) {
        this.order = order;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
