package com.pigatron.trading.exchanges.impl.poloniex;


import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.BitfinexBalance;
import com.pigatron.trading.exchanges.impl.poloniex.converter.PoloniexBalancesConverter;
import com.pigatron.trading.exchanges.impl.poloniex.converter.PoloniexChartDataConverter;
import com.pigatron.trading.exchanges.impl.poloniex.converter.PoloniexPlacedOrderConverter;
import com.pigatron.trading.exchanges.impl.poloniex.converter.PoloniexTradeConverter;
import com.pigatron.trading.exchanges.impl.poloniex.entity.PoloniexPlacedOrder;
import com.pigatron.trading.exchanges.impl.poloniex.entity.PoloniexSuccess;
import com.pigatron.trading.exchanges.impl.poloniex.entity.PoloniexTrade;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.SpaceBtcTrades;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.*;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
@ClientEndpoint
@Profile("poloniex")
public class PoloniexClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(PoloniexClient.class);

    private static final String EXCHANGE_NAME = "poloniex";
    private static final String PUBLIC_API_URL = "https://poloniex.com/public";
    private static final String TRADING_API_URL = "https://poloniex.com/tradingApi";

    private static final String GET_ORDER_BOOK_URL = PUBLIC_API_URL + "?command=returnOrderBook&currencyPair=%s&depth=%d";

    private RestTemplate restTemplate = new RestTemplate();

    @Value("${exchange.poloniex.key}")
    private String key;

    @Value("${exchange.poloniex.secret}")
    private String secret;

    @Value("${exchange.poloniex.enabled}")
    private Boolean enabled;

    private BigDecimal makerFee = new BigDecimal("0.0015");
    private BigDecimal takerFee = new BigDecimal("0.0025");


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private PoloniexChartDataConverter chartDataConverter;

    @Autowired
    private PoloniexBalancesConverter balancesConveter;

    @Autowired
    private PoloniexPlacedOrderConverter placedOrderConverter;

    @Autowired
    private PoloniexTradeConverter tradeConverter;


    private Map<CurrencyPair, OrderBook> orderBooks = new HashMap<>();


    @Autowired
    public PoloniexClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return EXCHANGE_NAME;
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.0005");
        }
        if(currency.equals("LTC")) {
            return new BigDecimal("0.02");
        }
        if(currency.equals("ETH")) {
            return new BigDecimal("0.005");
        }
        if(currency.equals("ZEC")) {
            return new BigDecimal("0.0001");
        }
        return null;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.001");
        } else if(currency.equals("LTC")) {
            return new BigDecimal("0.02");
        } else {
            // ???
            return new BigDecimal("0.1");
        }
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.00000001");
    }


    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, currencyPair.toString(), depth);
        CommonOrderBook orderBook = restTemplate.getForObject(url, CommonOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        Long now = new Date().getTime()/1000;
        Long start = now - (periodSeconds*dataSize);

        Map<String, String> params = new HashMap<>();
        params.put("command", "returnChartData");
        params.put("currencyPair", currencyPair.toString());
        params.put("start", start+"");
        params.put("end", now+"");
        params.put("period", periodSeconds+"");

        String url = PUBLIC_API_URL + "?" + createQueryString(params, false);
        List response = restTemplate.getForObject(url, List.class);
        return chartDataConverter.convert(response);
    }

    @Override
    public Balances getBalances() {
        Map<String, String> params = new HashMap<>();
        params.put("command", "returnCompleteBalances");

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<Map> response = restTemplate.postForEntity(TRADING_API_URL, entity, Map.class);
        return balancesConveter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new HashMap<>();
        params.put("command", "buy");
        params.put("currencyPair", currencyPair.toString());
        params.put("rate", price.toPlainString());
        params.put("amount", amount.toPlainString());
        params.put("postOnly", "1");

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<PoloniexPlacedOrder> response = restTemplate.postForEntity(TRADING_API_URL, entity, PoloniexPlacedOrder.class);
        PoloniexPlacedOrder poloniexPlacedOrder = response.getBody();
        if(poloniexPlacedOrder.getOrderNumber() == null) {
            throw new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, null);
        }
        return new PlacedOrder(poloniexPlacedOrder.getOrderNumber(), TradeType.buy, currencyPair, price, amount);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new HashMap<>();
        params.put("command", "sell");
        params.put("currencyPair", currencyPair.toString());
        params.put("rate", price.toPlainString());
        params.put("amount", amount.toPlainString());
        params.put("postOnly", "1");

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<PoloniexPlacedOrder> response = restTemplate.postForEntity(TRADING_API_URL, entity, PoloniexPlacedOrder.class);
        PoloniexPlacedOrder poloniexPlacedOrder = response.getBody();
        if(poloniexPlacedOrder.getOrderNumber() == null) {
            throw new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, null);
        }
        return new PlacedOrder(poloniexPlacedOrder.getOrderNumber(), TradeType.sell, currencyPair, price, amount);
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new HashMap<>();
        params.put("command", "moveOrder");
        params.put("orderNumber", order.getOrderId());
        params.put("rate", price.toPlainString());
        // TODO change amount

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<PoloniexPlacedOrder> response = restTemplate.postForEntity(TRADING_API_URL, entity, PoloniexPlacedOrder.class);

        PoloniexPlacedOrder poloniexPlacedOrder = response.getBody();
        if(poloniexPlacedOrder.getOrderNumber() == null) {
            throw new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, null);
        }
        order.setOrderId(poloniexPlacedOrder.getOrderNumber());
        order.setPrice(price);
        return order;
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) {
        Map<String, String> params = new HashMap<>();
        params.put("command", "cancelOrder");
        params.put("orderNumber", orderId);

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<PoloniexSuccess> response = restTemplate.postForEntity(TRADING_API_URL, entity, PoloniexSuccess.class);
        return response.getBody().isSuccess();
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        Map<String, String> params = new HashMap<>();
        params.put("command", "returnOrderTrades");
        params.put("orderNumber", order.getOrderId());

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        try {
            ResponseEntity<List> response = restTemplate.postForEntity(TRADING_API_URL, entity, List.class);
            return tradeConverter.convertList(response.getBody());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    private Date lastTradeDate;

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        Map<String, String> params = new HashMap<>();
        params.put("command", "returnTradeHistory");
        params.put("currencyPair", currencyPair.toString());
        if(lastTradeDate != null) {
            params.put("start", "" + (int)(lastTradeDate.getTime()/1000));
        }

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        try {
            ResponseEntity<List<PoloniexTrade>> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, new ParameterizedTypeReference<List<PoloniexTrade>>() {});
            if(response.getBody().size() > 0) {
                lastTradeDate = response.getBody().get(0).getDate();
            }
            return tradeConverter.convertList(response.getBody());
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }


    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        Map<String, String> params = new HashMap<>();
        params.put("command", "returnOpenOrders");
        params.put("currencyPair", currencyPair.toString());

        HttpEntity<String> entity = createPostEntity(createQueryString(params, false));
        ResponseEntity<List> response = restTemplate.postForEntity(TRADING_API_URL, entity, List.class);

        List<PlacedOrder> placedOrders = new ArrayList<>();
        for(Map orders : (List<Map>)response.getBody()) {
            PlacedOrder placedOrder = new PlacedOrder();
            placedOrder.setOrderId(orders.get("orderNumber").toString());
            placedOrder.setType(TradeType.valueOf(orders.get("type").toString()));
            placedOrder.setPrice(new BigDecimal(orders.get("rate").toString()));
            placedOrder.setAmount(new BigDecimal(orders.get("amount").toString()));
            placedOrders.add(placedOrder);
        }

        return placedOrders;
    }


    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 2000;
    }



    public void testWamp() {
        try {

            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, new URI("wss://api.poloniex.com"));
            //"wss://api.poloniex.com"
        } catch (Exception e) {
            // Catch exceptions that will be thrown in case of invalid configuration
            e.printStackTrace();
        }

        while(true) {

        }
    }

    @OnOpen
    public void onOpen(Session userSession) {
        logger.info("worked!");
    }


    private HttpEntity<String> createPostEntity(String body){
        body += "&nonce=" + createNonce();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Key", key);
        headers.add("Sign", calculateHash(body));
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        return new HttpEntity<>(body, headers);
    }

    private long createNonce() {
        return new Date().getTime();
    }

    private String calculateHash(String body) {
        try {
            Mac mac = Mac.getInstance("HmacSHA512");
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA512");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(body.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

}
