package com.pigatron.trading.exchanges.impl.quoine.entity;


import java.util.List;

public class QuoineOrderResponse {

    private List<QuoineOrder> models;

    public List<QuoineOrder> getModels() {
        return models;
    }

    public void setModels(List<QuoineOrder> models) {
        this.models = models;
    }
}
