package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcTrade;

import java.util.List;

public class HitBtcTradesWrapper {

    private List<HitBtcTrade> trades;

    public List<HitBtcTrade> getTrades() {
        return trades;
    }

    public void setTrades(List<HitBtcTrade> trades) {
        this.trades = trades;
    }
}
