package com.pigatron.trading.exchanges.impl.gemini;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.impl.bitfinex.converter.BitfinexBalanceConverter;
import com.pigatron.trading.exchanges.impl.bitfinex.entity.BitfinexBalance;
import com.pigatron.trading.exchanges.util.NewTradesFilter;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Component
@Profile("gemini")
public class GeminiClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(GeminiClient.class);

    private static final String API_URL = "https://api.gemini.com";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/v1/book/%s";
    private static final String GET_BALANCES_URL = API_URL + "/v1/balances";
    private static final String PLACE_ORDER_URL = API_URL + "place_limit/%s";
    private static final String CANCEL_ORDER_URL = API_URL + "cancel_order/%s";
    private static final String GET_TRADES_URL = API_URL + "trade_history/%s";

    @Value("${exchange.gemini.enabled}")
    private Boolean enabled;

    @Value("${exchange.gemini.key}")
    private String key;

    @Value("${exchange.gemini.secret}")
    private String secret;

    private BigDecimal makerFee = null;
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;


    ObjectMapper objectMapper = new ObjectMapper();

    private NewTradesFilter newTradesFilter = new NewTradesFilter();

    private Map<CurrencyPair, OrderBook> orderBooks = new HashMap<>();

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private BitfinexBalanceConverter balanceConverter;


    @Autowired
    public GeminiClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "gemini";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.01");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("0.01");
    }



    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair), depth);

        ResponseEntity<OrderBook> response = restTemplate.getForEntity(url, OrderBook.class);
        return response.getBody();
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        Map<String, Object> params = new HashMap<>();
        params.put("request", "/v1/balances");

        HttpEntity<String> entity = createPrivateEntity(params);
        String url = GET_BALANCES_URL;
        ResponseEntity<List<BitfinexBalance>> response = restTemplate.exchange(url, HttpMethod.POST, entity, new ParameterizedTypeReference<List<BitfinexBalance>>() {});
        return balanceConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return false;
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        return null;
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        return null;
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        return null;
    }


    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2().toLowerCase() + currencyPair.getCurrency1().toLowerCase();
    }

    private HttpEntity<String> createPrivateEntity(Map<String,Object> params) {
        try {
            params.put("nonce", createNonce());
            String body = objectMapper.writeValueAsString(params);
            String encodedBody = Base64.getEncoder().encodeToString(body.getBytes("UTF-8"));
            HttpHeaders headers = new HttpHeaders();
            headers.add("accept", "application/json");
            headers.add("Content-Type", "application/json");
            headers.add("User-Agent", "Java");
            headers.add("X-GEMINI-APIKEY", key);
            headers.add("X-GEMINI-PAYLOAD", encodedBody);
            headers.add("X-GEMINI-SIGNATURE", getSignature(encodedBody));
            return new HttpEntity<>(body, headers);
        } catch(UnsupportedEncodingException | JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSignature(String encodedBody) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA384");
            Mac mac = Mac.getInstance("HmacSHA384");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(encodedBody.getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private long createNonce() {
        return new Date().getTime();
    }
}
