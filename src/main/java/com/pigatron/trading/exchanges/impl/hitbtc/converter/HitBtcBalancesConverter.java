package com.pigatron.trading.exchanges.impl.hitbtc.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcBalances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


@Component
public class HitBtcBalancesConverter implements Converter<HitBtcBalances, Balances> {

    private static final String CURRENCY_KEY = "currency";
    private static final String AVAILABLE_KEY = "available";
    private static final String ONORDERS_KEY = "hold";

    @Override
    public Balances convert(HitBtcBalances sourceBalances) {
        Balances balances = new Balances();
        sourceBalances.getBalance().forEach(sourceBalance ->
                balances.setBalance(sourceBalance.getCurrencyCode(), sourceBalance.getCash(), sourceBalance.getReserved(),null));
        return balances;
    }
}
