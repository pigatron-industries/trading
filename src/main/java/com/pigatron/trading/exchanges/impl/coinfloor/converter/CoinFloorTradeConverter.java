package com.pigatron.trading.exchanges.impl.coinfloor.converter;

import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message.OrdersMatchedMessage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;


@Component
public class CoinFloorTradeConverter implements Converter<OrdersMatchedMessage, Trade> {

    @Override
    public Trade convert(OrdersMatchedMessage message) {
        Trade trade = new Trade();
        trade.setCurrencyPair(message.getCurrencyPair());
        trade.setType(message.getType());
        trade.setPrice(message.getPrice().divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP));
        trade.setQuantity(message.getQuantity().divide(new BigDecimal("10000"), 5, RoundingMode.HALF_UP));
        trade.setDate(new Date());
        return trade;
    }
}
