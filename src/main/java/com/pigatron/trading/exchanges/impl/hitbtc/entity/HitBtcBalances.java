package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import java.util.List;

public class HitBtcBalances {

    private List<HitBtcBalance> balance;

    public List<HitBtcBalance> getBalance() {
        return balance;
    }

    public void setBalance(List<HitBtcBalance> balance) {
        this.balance = balance;
    }
}
