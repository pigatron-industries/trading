package com.pigatron.trading.exchanges.impl.gdax.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


@Component
public class GdaxBalancesConverter implements Converter<List, Balances> {

    private static final String CURRENCY_KEY = "currency";
    private static final String AVAILABLE_KEY = "available";
    private static final String ONORDERS_KEY = "hold";

    @Override
    public Balances convert(List sourceBalances) {
        Balances balances = new Balances();
        sourceBalances.forEach(sourceBalance -> {
            Map<String, String> sourceBalanceMap = (Map<String, String>)sourceBalance;
            balances.setBalance(sourceBalanceMap.get(CURRENCY_KEY),
                    new BigDecimal(sourceBalanceMap.get(AVAILABLE_KEY)),
                    new BigDecimal(sourceBalanceMap.get(ONORDERS_KEY)),
                    null);
        });
        return balances;
    }
}
