package com.pigatron.trading.exchanges.impl.liqui.converter;

import com.pigatron.trading.exchanges.entity.CurrencyPair;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LiquiCurrencyPairConverter implements Converter<String, CurrencyPair> {

    @Override
    public CurrencyPair convert(String stringPair) {
        String[] split = stringPair.split("_");
        return new CurrencyPair(split[1].toUpperCase(), split[0].toUpperCase());
    }

}
