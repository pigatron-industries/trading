package com.pigatron.trading.exchanges.impl.quoine.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class QuoineOrder {

    private String id;
    private BigDecimal quantity;
    private TradeType side;
    @JsonProperty("filled_quantity")
    private BigDecimal filledQuantity;
    private BigDecimal price;
    private String status;
    @JsonProperty("currency_pair_code")
    private String currencyPair;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public TradeType getSide() {
        return side;
    }

    public void setSide(TradeType side) {
        this.side = side;
    }

    public BigDecimal getFilledQuantity() {
        return filledQuantity;
    }

    public void setFilledQuantity(BigDecimal filledQuantity) {
        this.filledQuantity = filledQuantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }
}
