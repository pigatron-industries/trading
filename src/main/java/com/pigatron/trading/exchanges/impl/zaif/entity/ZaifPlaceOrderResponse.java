package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ZaifPlaceOrderResponse {

    private int success;

    private String error;

    @JsonProperty("return")
    private ZaifOrder order;

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public ZaifOrder getOrder() {
        return order;
    }

    public void setOrder(ZaifOrder order) {
        this.order = order;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
