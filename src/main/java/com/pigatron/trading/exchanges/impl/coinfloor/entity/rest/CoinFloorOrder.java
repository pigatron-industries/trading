package com.pigatron.trading.exchanges.impl.coinfloor.entity.rest;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class CoinFloorOrder {

    private String id;
    private String datetime;
    private TradeType type;
    private BigDecimal price;
    private BigDecimal amount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
