package com.pigatron.trading.exchanges.impl.bitfinex.converter;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class BitfinexOrderBookV2Converter implements Converter<List<List<BigDecimal>>, OrderBook> {

    private static int PRICE_INDEX = 0;
    private static int QUANTITY_INDEX = 2;

    @Override
    public OrderBook convert(List<List<BigDecimal>> bitfinexOrderBook) {
        OrderBook orderBook = new OrderBook();

        for(List<BigDecimal> order : bitfinexOrderBook) {
            BigDecimal amount = order.get(QUANTITY_INDEX);
            BigDecimal price = order.get(PRICE_INDEX);
            MarketOrder marketOrder = new MarketOrder(amount.compareTo(BigDecimal.ZERO) < 0 ? TradeType.sell : TradeType.buy, price, amount.abs());
            orderBook.addOrder(marketOrder);
        }

        return orderBook;
    }
}
