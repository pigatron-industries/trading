package com.pigatron.trading.exchanges.impl.quoine;


import com.auth0.jwt.JWTSigner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.gdax.entity.GdaxMessage;
import com.pigatron.trading.exchanges.impl.quoine.converter.QuoineBalancesConverter;
import com.pigatron.trading.exchanges.impl.quoine.converter.QuoineOrdersConverter;
import com.pigatron.trading.exchanges.impl.quoine.converter.QuoineTradesConverter;
import com.pigatron.trading.exchanges.impl.quoine.entity.*;
import com.pigatron.trading.exchanges.impl.zaif.converter.*;
import com.pigatron.trading.exchanges.impl.zaif.entity.*;
import com.pigatron.trading.stats.StatsService;
import com.pigatron.trading.util.RateLimiter;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Component
@Profile("quoine")
public class QuoineClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(QuoineClient.class);

    private static final String API_URL = "https://api.quoine.com";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/products/%d/price_levels";

    private static final String GET_BALANCE_REQUEST = "/accounts/balance";
    private static final String GET_ORDERS_REQUEST = "/orders";
    private static final String GET_TRADES_REQUEST = "/executions/me?product_id=%d";
    private static final String PLACE_ORDER_REQUEST = "/orders/";
    private static final String MOVE_ORDER_REQUEST = "/orders/%s";
    private static final String CANCEL_ORDER_REQUEST = "/orders/%s/cancel";

    private RateLimiter rateLimiter = new RateLimiter(1000);

    @Value("${exchange.quoine.key}")
    private String key;

    @Value("${exchange.quoine.secret}")
    private String secret;

    @Value("${exchange.quoine.enabled}")
    private Boolean enabled;

    private BigDecimal tradeFeeBTC = new BigDecimal("0");
    private BigDecimal tradeFeeETH = new BigDecimal("0.0025");

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private QuoineBalancesConverter balancesConverter;

    @Autowired
    private QuoineTradesConverter tradesConverter;

    @Autowired
    private QuoineOrdersConverter ordersConverter;

    @Autowired
    private ZaifChartDataConverter chartDataConverter;


    @Autowired
    public QuoineClient(StatsService statsService) {
        super(statsService);
    }


    /** Info **/

    @Override
    public String getName() {
        return "quoine";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return tradeFeeBTC;
    }

    @Override
    public BigDecimal getTakerFee() {
        return tradeFeeBTC;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0");
        } else {
            return null;
        }
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.01");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        switch (currency) {
            case "JPY":
                return new BigDecimal("1");
            case "BTC":
                return new BigDecimal("0.00001");
            default: //USD
                return new BigDecimal("0.01");
        }
    }



    /** Public API **/

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        String url = String.format(GET_ORDER_BOOK_URL, getProductId(currencyPair));
        QuoineOrderBook orderBook = restTemplate.getForObject(url, QuoineOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }


    /** Private API **/

    @Override
    public Balances getBalances() {
        HttpEntity<String> entity = createPrivateEntity(GET_BALANCE_REQUEST, "");
        ResponseEntity<QuoineBalance[]> response = restTemplate.exchange(API_URL + GET_BALANCE_REQUEST, HttpMethod.GET, entity, QuoineBalance[].class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place buy order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return placeOrder(currencyPair, TradeType.buy, price, amount);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place sell order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
        return placeOrder(currencyPair, TradeType.sell, price, amount);
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        String body = String.format("{\"order\":{\"order_type\":\"limit\", \"product_id\":%d, \"side\":\"%s\", \"quantity\":\"%s\", \"price\":\"%s\"}}",
                                    getProductId(currencyPair), side.name(), amount.toPlainString(), price.toPlainString());
        HttpEntity<String> entity = createPrivateEntity(PLACE_ORDER_REQUEST, body);
        try {
            ResponseEntity<QuoineOrder> response = restTemplate.exchange(API_URL + PLACE_ORDER_REQUEST, HttpMethod.POST, entity, QuoineOrder.class);
            return new PlacedOrder(response.getBody().getId(), side, currencyPair, price, amount);
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }


    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        if(order.getAmount().compareTo(amount) == 0) {
            String request = String.format(MOVE_ORDER_REQUEST, order.getOrderId());
            String body = String.format("{\"order\":{\"quantity\":\"%s\", \"price\":\"%s\"}}",
                    amount.toPlainString(), price.toPlainString());
            HttpEntity<String> entity = createPrivateEntity(request, body);
            try {
                ResponseEntity<QuoineOrder> response = restTemplate.exchange(API_URL + request, HttpMethod.PUT, entity, QuoineOrder.class);
                return new PlacedOrder(response.getBody().getId(), order.getType(), order.getCurrencyPair(), price, amount);
            } catch (HttpClientErrorException e) {
                throw convertException(e);
            }
        } else {
            return super.moveOrder(order, price, amount);
        }
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        String request = String.format(CANCEL_ORDER_REQUEST, orderId);
        HttpEntity<String> entity = createPrivateEntity(request, "");
        try {
            ResponseEntity<ZaifPlaceOrderResponse> response = restTemplate.exchange(API_URL + request, HttpMethod.PUT, entity, ZaifPlaceOrderResponse.class);
            return true;
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }


    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        String request = String.format(GET_TRADES_REQUEST, getProductId(currencyPair));
        HttpEntity<String> entity = createPrivateEntity(request, "");
        ResponseEntity<QuoineTrades> response = restTemplate.exchange(API_URL + request, HttpMethod.GET, entity, QuoineTrades.class);
        return tradesConverter.convert(response.getBody().getModels());
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        Map<String, String> params = new HashMap<>();
        params.put("product_id", getProductId(currencyPair)+"");
        params.put("status", "live");
        String request = GET_ORDERS_REQUEST + "?" + createQueryString(params, false);

        HttpEntity<String> entity = createPrivateEntity(request, "");
        ResponseEntity<QuoineOrderResponse> response = restTemplate.exchange(API_URL + request, HttpMethod.GET, entity, QuoineOrderResponse.class);
        return ordersConverter.convert(response.getBody().getModels());
    }

//    public List<PlacedOrder> getOpenOrders() {
//        Map<String, String> params = new LinkedHashMap<>();
//        params.put("nonce", createNonce()+"");
//        params.put("method", "active_orders");
//
//        HttpEntity<String> entity = createPrivateEntity(params);
//        ResponseEntity<ZaifOrdersResponse> response = restTemplate.exchange(API_URL, HttpMethod.POST, entity, ZaifOrdersResponse.class);
//        return ordersConverter.convert(response.getBody());
//    }


    /** Polling **/

    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 2000;
    }


    /** Utils **/

    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2().toLowerCase() + "_" + currencyPair.getCurrency1().toLowerCase();
    }

    /**
     * Products ids from api: https://api.quoine.com/products
     * @param currencyPair
     * @return
     */
    private int getProductId(CurrencyPair currencyPair) {
        if(currencyPair.getCurrency1().equals("USD") && currencyPair.getCurrency2().equals("BTC")) {
            return 1;
        } else if (currencyPair.getCurrency1().equals("JPY") && currencyPair.getCurrency2().equals("BTC")) {
            return 5;
        } else {
            return 0;
        }
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        return new HttpEntity<>("", headers);
    }

    private HttpEntity<String> createPrivateEntity(String request, String body) {
        String nonce = createNonce() + "";

        JWTSigner signer = new JWTSigner(secret);
        HashMap<String, Object> claims = new HashMap<String, Object>();
        claims.put("path", request);
        claims.put("nonce", nonce);
        claims.put("token_id", key);
        String jwt = signer.sign(claims);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/json");
        headers.add("X-Quoine-API-Version", "2");
        headers.add("X-Quoine-Auth", jwt);
        return new HttpEntity<>(body, headers);
    }

    private long createNonce() {
        return new Date().getTime();
    }


    private ExchangeClientException convertException(HttpClientErrorException exception) {
        //try {
            // {"errors":"Can not update partially filled order"}
            //ObjectMapper objectMapper = new ObjectMapper();
            //QuoineMessage quoineMessage = objectMapper.readValue(exception.getResponseBodyAsByteArray(), QuoineMessage.class);
            String message = exception.getResponseBodyAsString();

            if(message.contains("Can not update partially filled order")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_PARITALLY_FILLED, exception, message);
            } else if(message.contains("Can not update non-live order") ||
                    message.contains("Order not found")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, exception, message);
            } else if(message.contains("not_enough_fund")) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, exception, message);
            } else {
                logger.error(message, exception);
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, exception, message);
            }
        //} catch(IOException e) {
        //    logger.error("Error reading error response: " + exception.getResponseBodyAsString());
        //    logger.error("IOException", e);
        //    return null;
        //}
    }

}
