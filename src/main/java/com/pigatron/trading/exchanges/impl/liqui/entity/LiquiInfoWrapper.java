package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiquiInfoWrapper {

    private boolean success;
    @JsonProperty("return")
    private LiquiInfo info;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public LiquiInfo getInfo() {
        return info;
    }

    public void setInfo(LiquiInfo info) {
        this.info = info;
    }
}
