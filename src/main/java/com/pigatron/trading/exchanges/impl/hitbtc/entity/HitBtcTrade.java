package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class HitBtcTrade {

    private String tradeId;
    private String originalOrderId;
    private String clientOrderId;
    private String symbol;
    private TradeType side;
    private BigDecimal execQuantity;
    private BigDecimal execPrice;

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getOriginalOrderId() {
        return originalOrderId;
    }

    public void setOriginalOrderId(String originalOrderId) {
        this.originalOrderId = originalOrderId;
    }

    public String getClientOrderId() {
        return clientOrderId;
    }

    public void setClientOrderId(String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public TradeType getSide() {
        return side;
    }

    public void setSide(TradeType side) {
        this.side = side;
    }

    public BigDecimal getExecQuantity() {
        return execQuantity;
    }

    public void setExecQuantity(BigDecimal execQuantity) {
        this.execQuantity = execQuantity;
    }

    public BigDecimal getExecPrice() {
        return execPrice;
    }

    public void setExecPrice(BigDecimal execPrice) {
        this.execPrice = execPrice;
    }
}
