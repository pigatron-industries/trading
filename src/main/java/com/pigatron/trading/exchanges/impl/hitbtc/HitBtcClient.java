package com.pigatron.trading.exchanges.impl.hitbtc;

import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcBalancesConverter;
import com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcOrderBookConverter;
import com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcOrderConverter;
import com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcTradeConverter;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.*;
import com.pigatron.trading.stats.StatsService;
import com.pigatron.trading.util.RateLimiter;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.ClientEndpoint;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.convertCurrencyPair;
import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.quantityToLots;


/**
 * https://api.hitbtc.com/api/2/explore/
 */

@Component
@ClientEndpoint
@Profile("hitbtc")
public class HitBtcClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(HitBtcClient.class);

    private static final String API_URL = "https://api.hitbtc.com";
    private static final String WS_API_URL = "ws://api.hitbtc.com";

    private static final String GET_ORDER_BOOK_URL2 = "/api/2/public/orderbook/%s";
    private static final String GET_ORDER_BOOK_URL1 = "/api/1/public/%s/orderbook";
    private static final String GET_BALANCES_URL = "/api/1/trading/balance";
    private static final String NEW_ORDER_URL = "/api/1/trading/new_order";
    private static final String CANCEL_ORDER_URL = "/api/1/trading/cancel_order";
    private static final String GET_ORDERS_URL = "/api/1/trading/orders/active";
    private static final String GET_TRADES_URL = "/api/1/trading/trades";

    private RateLimiter rateLimiter = new RateLimiter(100);

    @Value("${exchange.hitbtc.key}")
    private String key;

    @Value("${exchange.hitbtc.secret}")
    private String secret;

    @Value("${exchange.hitbtc.enabled}")
    private Boolean enabled;


    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;

    private RestTemplate restTemplate = new RestTemplate();


    @Autowired
    private HitBtcOrderBookConverter orderBookConverter;

    @Autowired
    private CommonOrderBookConverter commonOrderBookConverter;

    @Autowired
    private HitBtcBalancesConverter balancesConverter;

    @Autowired
    private HitBtcTradeConverter tradeConverter;

    @Autowired
    private HitBtcOrderConverter orderConverter;


    @Autowired
    public HitBtcClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "hitbtc";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.01");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("USD") || currency.equals("EUR") || currency.equals("GBP")) {
            return new BigDecimal("0.01");
        } else if(currency.equals("BTC") || currency.equals("BCC")) {
            return new BigDecimal("0.01"); //lot size   //quote price = 0.000001
        } else {
            return new BigDecimal("0.001"); //lot size
        }
    }

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        return getOrderBook1(currencyPair, depth);
    }

    private OrderBook getOrderBook1(CurrencyPair currencyPair, int depth) {
        String url = String.format(API_URL + GET_ORDER_BOOK_URL1, convertCurrencyPair(currencyPair));
        ResponseEntity<CommonOrderBook> orderBook = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(""), CommonOrderBook.class);
        return commonOrderBookConverter.convert(orderBook.getBody());
    }

    private OrderBook getOrderBook2(CurrencyPair currencyPair, int depth) {
        String url = String.format(API_URL + GET_ORDER_BOOK_URL2, convertCurrencyPair(currencyPair));
        ResponseEntity<HitBtcOrderBook> orderBook = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(""), HitBtcOrderBook.class);
        return orderBookConverter.convert(orderBook.getBody());
    }

    @Override
    public Balances getBalances() {
        rateLimiter.acquire();
        String path = GET_BALANCES_URL + "?" + createQueryString(createPrivateParams(), true);
        HttpEntity<String> entity = createPrivateEntity( path, "");
        ResponseEntity<HitBtcBalances> response = restTemplate.exchange(API_URL + path, HttpMethod.GET, entity, HitBtcBalances.class);
        return balancesConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrder(currencyPair, TradeType.buy, price, amount, true);
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return placeOrder(currencyPair, TradeType.sell, price, amount, true);
    }

    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        return placeOrder(currencyPair, side, price, amount, true);
    }

    private PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount, Boolean rateLimited) {
        if(rateLimited) {
            rateLimiter.acquire();
        }
        String path = NEW_ORDER_URL + "?" + createQueryString(createPrivateParams(), true);

        Map<String, String> params = new HashMap<>();
        params.put("symbol", convertCurrencyPair(currencyPair));
        params.put("side", side.name());
        params.put("price", price.toPlainString());
        params.put("quantity", quantityToLots(currencyPair, amount).toPlainString());
        params.put("type", "limit");
        params.put("timInForce", "GTC");
        String body = createQueryString(params, false);

        HttpEntity<String> entity = createPrivateEntity(path, body);
        ResponseEntity<HitBtcExecutionReportWrapper> response = restTemplate.postForEntity(API_URL + path, entity, HitBtcExecutionReportWrapper.class);
        if(response.getBody().getExecutionReport().getExecReportType().equals("rejected")) {
            throw convertException(response.getBody());
        } else {
            return new PlacedOrder(response.getBody().getExecutionReport().getClientOrderId(), side, currencyPair, price, amount);
        }
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        rateLimiter.acquire(2);
        try {
            cancelOrder(order.getOrderId(), null, false);
        } catch(ExchangeClientException e) {
            if(!e.getType().equals(ExchangeClientExceptionType.ORDER_FILLED) && !e.getType().equals(ExchangeClientExceptionType.ORDER_NOT_FOUND)) {
                throw e;
            }
        }
        return placeOrder(order.getCurrencyPair(), order.getType(), price, amount, false);
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return cancelOrder(orderId, currencyPair, true);
    }

    private boolean cancelOrder(String orderId, CurrencyPair currencyPair, Boolean rateLimited) throws ExchangeClientException {
        if(rateLimited) {
            rateLimiter.acquire();
        }
        String path = CANCEL_ORDER_URL + "?" + createQueryString(createPrivateParams(), true);

        Map<String, String> params = new HashMap<>();
        params.put("clientOrderId", orderId);
        String body = createQueryString(params, false);

        HttpEntity<String> entity = createPrivateEntity(path, body);
        ResponseEntity<HitBtcExecutionReportWrapper> response = restTemplate.postForEntity(API_URL + path, entity, HitBtcExecutionReportWrapper.class);

        if(response.getBody().getCancelReject() != null) {
            throw convertException(response.getBody());
        }

        return true;
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        rateLimiter.acquire();
        Map<String, String> params = createPrivateParams();
        params.put("by", "ts");
        params.put("start_index", "0");
        params.put("max_results", "100");
        params.put("sort", "desc");
        params.put("symbols", convertCurrencyPair(currencyPair));

        String path = GET_TRADES_URL + "?" + createQueryString(params, true);
        HttpEntity<String> entity = createPrivateEntity(path, "");
        ResponseEntity<HitBtcTradesWrapper> response = restTemplate.exchange(API_URL + path, HttpMethod.GET, entity, HitBtcTradesWrapper.class);

        return tradeConverter.convertList(response.getBody().getTrades());
    }

//    @Override
//    public List<Trade> getOrderTrades(PlacedOrder order) {
//        rateLimiter.acquire();
//        Map<String, String> params = new HashMap<>();
//        params.put("order_id", order.getOrderId());
//        String url = "/fills?" + createQueryString(params, false);
//        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
//        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);
//        return tradesConverter.convert(response.getBody());
//    }

//    @Override
//    public List<Trade> getNewTrades(CurrencyPair currencyPair) {
//        rateLimiter.acquire();
//        ProductData productData = getProductData(currencyPair);
//        Map<String, String> params = new HashMap<>();
//        String url = "/fills";
//        params.put("product_id", formatCurrencyPair(currencyPair));
//        if(productData.getLastTradeId() != null) {
//            params.put("before", productData.getLastTradeId());
//        }
//        if(params.size() > 0) {
//            url += "?" + createQueryString(params, false);
//        }
//        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
//        ResponseEntity<List> response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, List.class);
//
//        List<Trade> trades = tradesConverter.convert(response.getBody());
//
//        if(productData.getLastTradeId() == null) {
//            trades = new ArrayList<>();
//        }
//
//        if(response.getHeaders().get("CB-BEFORE") != null) {
//            productData.setLastTradeId(response.getHeaders().get("CB-BEFORE").get(0));
//        }
//        return trades;
//    }

    public List<PlacedOrder> getOpenOrders() {
        rateLimiter.acquire();
        String path = GET_ORDERS_URL + "?" + createQueryString(createPrivateParams(), true);
        HttpEntity<String> entity = createPrivateEntity(path, "");
        ResponseEntity<HitBtcOrdersWrapper> response = restTemplate.exchange(API_URL + path, HttpMethod.GET, entity, HitBtcOrdersWrapper.class);
        return orderConverter.convertList(response.getBody().getOrders());
    }

    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        return getOpenOrders().stream()
                .filter(o -> o.getCurrencyPair().equals(currencyPair)).collect(Collectors.toList());
    }


//    @Override
//    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
//        try {
//            rateLimiter.acquire();
//            PlacedOrder placedOrder = getOrderById(orderId);
//            if (placedOrder.getStatus().equals(OrderStatus.DONE)) {
//                return Optional.empty();
//            } else {
//                return Optional.of(placedOrder);
//            }
//        } catch(HttpClientErrorException e) {
//            logger.error("getOpenOrderById error", e);
//            if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
//                throw new OrderNotPlacedException();
//            } else {
//                throw new RuntimeException(e);
//            }
//        }
//    }

//    private PlacedOrder getOrderById(String orderId) {
//        rateLimiter.acquire();
//        String url = "/orders/" + orderId;
//        HttpEntity<String> entity = createPrivateEntity("GET", url, "");
//        ResponseEntity<Map> response = null;
//        try {
//            response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, Map.class);
//        } catch(HttpClientErrorException e) {
//            logger.error(e.getResponseBodyAsString());
//            sleep(1000);
//            response = restTemplate.exchange(API_URL + url, HttpMethod.GET, entity, Map.class);
//        }
//
//        return orderConverter.convert(response.getBody());
//    }



    private long createNonce() {
        return (System.currentTimeMillis())/100;
    }

    private Map<String, String> createPrivateParams() {
        HashMap<String, String> params = new HashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("apiKey", key);
        return params;
    }

    private HttpEntity<String> createPublicEntity(String body) {
        HttpHeaders headers = new HttpHeaders();
        return new HttpEntity<>(body, headers);
    }

    private HttpEntity<String> createPrivateEntity(String requestPath, String body) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Signature", getSignature(requestPath, body));
        return new HttpEntity<>(body, headers);
    }

    private String getSignature(String requestPath, String body) {
        try {
            String prehash = requestPath + body;
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(prehash.getBytes())).toLowerCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(HitBtcExecutionReportWrapper executionReport) {
        if(executionReport.getExecutionReport() != null) {
            if(executionReport.getExecutionReport().getOrderRejectReason().equals("unknownOrder")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, executionReport.getExecutionReport().getOrderRejectReason());
            } else if(executionReport.getExecutionReport().getOrderRejectReason().equals("orderExceedsLimit") ||
                    executionReport.getExecutionReport().getOrderRejectReason().equals("badQuantity")) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, executionReport.getExecutionReport().getOrderRejectReason());
            } else {
                logger.error(executionReport.getExecutionReport().getOrderRejectReason());
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, executionReport.getExecutionReport().getOrderRejectReason());
            }
        } else if(executionReport.getCancelReject() != null) {
            if(executionReport.getCancelReject().getRejectReasonCode().equals("orderNotFound")) {
                return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, executionReport.getCancelReject().getRejectReasonCode());
            } else {
                logger.error(executionReport.getExecutionReport().getOrderRejectReason());
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, executionReport.getCancelReject().getRejectReasonCode());
            }
        } else {
            //logger.error(executionReport.getExecutionReport().getOrderRejectReason());
            return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, null);
        }
    }







    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
    }


}
