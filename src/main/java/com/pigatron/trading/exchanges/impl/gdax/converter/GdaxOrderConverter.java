package com.pigatron.trading.exchanges.impl.gdax.converter;

import com.pigatron.trading.exchanges.entity.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class GdaxOrderConverter implements Converter<Map, PlacedOrder> {

    @Override
    public PlacedOrder convert(Map source) {
        String[] currency = source.get("product_id").toString().split("-");
        PlacedOrder placedOrder = new PlacedOrder(source.get("id").toString(),
                TradeType.valueOf(source.get("side").toString()),
                new CurrencyPair(currency[1], currency[0]),
                new BigDecimal(source.get("price").toString()),
                new BigDecimal(source.get("size").toString()));
        if(source.get("status").equals("done")) {
            placedOrder.setStatus(OrderStatus.DONE);
        } else {
            placedOrder.setStatus(OrderStatus.OPEN);
        }
        return placedOrder;
    }
}

