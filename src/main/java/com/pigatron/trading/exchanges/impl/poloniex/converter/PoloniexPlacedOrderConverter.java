package com.pigatron.trading.exchanges.impl.poloniex.converter;


import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.impl.poloniex.entity.PoloniexPlacedOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class PoloniexPlacedOrderConverter implements Converter<PoloniexPlacedOrder, PlacedOrder> {

    @Override
    public PlacedOrder convert(PoloniexPlacedOrder source) {
        //TODO not used
        return null;
    }

}
