package com.pigatron.trading.exchanges.impl.zaif;


import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.exceptions.RetryableExchangeClientException;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.zaif.converter.*;
import com.pigatron.trading.exchanges.impl.zaif.entity.*;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.websocket.*;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Component
@ClientEndpoint
@Profile("zaif")
public class ZaifClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(ZaifClient.class);

    private static final String PUBLIC_API_URL = "https://api.zaif.jp/api/1/";
    private static final String TRADING_API_URL = "https://api.zaif.jp/tapi";
    private static final String CHART_API_URL = "https://d1vakfx86mls6g.cloudfront.net/json/candle_%s_%s.json.gz";

    private static final String WS_API_URL = "wss://ws.zaif.jp:8888/stream?currency_pair=btc_jpy";

    private static final String GET_ORDER_BOOK_URL = PUBLIC_API_URL + "depth/%s";
    private static final String GET_TRADES_URL = PUBLIC_API_URL + "trades/%s";
    private static final String GET_TICKER_URL = PUBLIC_API_URL + "ticker/%s";


    @Value("${exchange.zaif.key}")
    private String key;

    @Value("${exchange.zaif.secret}")
    private String secret;

    @Value("${exchange.zaif.enabled}")
    private Boolean enabled;

    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = new BigDecimal("0");


    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private ZaifBalancesConverter balancesConverter;

    @Autowired
    private ZaifTradesConverter tradesConverter;

    @Autowired
    private ZaifTradesListConverter tradesListConverter;

    @Autowired
    private ZaifOrdersConverter ordersConverter;

    @Autowired
    private ZaifChartDataConverter chartDataConverter;


    @Autowired
    public ZaifClient(StatsService statsService) {
        super(statsService);
    }


    /** Info **/

    @Override
    public String getName() {
        return "zaif";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.0002");
        } else {
            return null;
        }
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.0001");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("JPY")) {
            return new BigDecimal("5");
        } else { //BTC
            return new BigDecimal("0.0001");
        }
    }



    /** Public API **/

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrencyPair(currencyPair));
        CommonOrderBook orderBook = restTemplate.getForObject(url, CommonOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }

    @Override
    public List<Trade> getTrades(CurrencyPair currencyPair) throws NotImplementedException {
        String url = String.format(GET_TRADES_URL, formatCurrencyPair(currencyPair));
        ResponseEntity<List<ZaifTrade>> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), new ParameterizedTypeReference<List<ZaifTrade>>() {});
        return tradesListConverter.convert(response.getBody());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) throws NotImplementedException {
        String url = String.format(CHART_API_URL, "1m", formatCurrencyPair(currencyPair));
        ResponseEntity<List<ZaifChartData>> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), new ParameterizedTypeReference<List<ZaifChartData>>() {});
        return chartDataConverter.convert(response.getBody());
    }

    @Override
    public TickData getTicker(CurrencyPair currencyPair) throws NotImplementedException {
        String url = String.format(GET_TICKER_URL, formatCurrencyPair(currencyPair));
        ResponseEntity<TickData> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), TickData.class);
        return response.getBody();
    }


    /** Private API **/

    @Override
    public synchronized Balances getBalances() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "get_info");
        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifBalancesResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifBalancesResponse.class);
        Balances balances = balancesConverter.convert(response.getBody().getBalances());

        // add balances on orders
        List<PlacedOrder> openOrders = getOpenOrders();
        for (PlacedOrder order : openOrders) {
            if(order.getType().equals(TradeType.buy)) {
                // currency 1
                BigDecimal amount = order.getAmount().multiply(order.getPrice());
                balances.addBalanceOnOrder(order.getCurrencyPair().getCurrency1(), amount);
            } else {
                // currency 2
                BigDecimal amount = order.getAmount();
                balances.addBalanceOnOrder(order.getCurrencyPair().getCurrency2(), amount);
            }
        }

        return balances;
    }

    @Override
    public synchronized PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place buy order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
//        try {
            return placeOrder(currencyPair, TradeType.buy, price, amount);
//        } catch(HttpClientErrorException e) {
//            throw convertException(e);
//        }
    }

    @Override
    public synchronized PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        logger.info("Place sell order: " + currencyPair + " Price: " + price.toPlainString() + "  Amount: " + amount.toPlainString());
//        try {
            return placeOrder(currencyPair, TradeType.sell, price, amount);
//        } catch(HttpClientErrorException e) {
//            throw convertException(e);
//        }
    }

    public synchronized PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "trade");
        params.put("currency_pair", formatCurrencyPair(currencyPair));
        params.put("action", side.equals(TradeType.buy) ? "bid" : "ask");
        params.put("price", price.toPlainString());
        params.put("amount", amount.toPlainString());
        //params.put("limit", price.toPlainString());

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifPlaceOrderResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifPlaceOrderResponse.class);

        if(response.getBody().getSuccess() == 0) {
            throw convertException(response.getBody().getError());
        } else {
            return new PlacedOrder(response.getBody().getOrder().getOrderId(), side, currencyPair, price, amount);
        }
    }


    @Override
    public synchronized PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        cancelOrder(order.getOrderId(), null);
        return placeOrder(order.getCurrencyPair(), order.getType(), price, amount);
    }

    @Override
    @Retryable(value=RetryableExchangeClientException.class)
    public synchronized boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "cancel_order");
        params.put("order_id", orderId);

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifPlaceOrderResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifPlaceOrderResponse.class);
        if(response.getBody().getSuccess() == 0) {
            throw convertException(response.getBody().getError());
        } else {
            return true;
        }
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "trade_history");
        params.put("currency_pair", formatCurrencyPair(currencyPair));
        newTradesFilter.getLastTradeId(currencyPair)
                .ifPresent(id -> params.put("from_id", id));

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifTradesResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifTradesResponse.class);
        return tradesConverter.convert(response.getBody());
    }

    @Override
    public synchronized List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "active_orders");
        params.put("currency_pair", formatCurrencyPair(currencyPair));

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifOrdersResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifOrdersResponse.class);
        return ordersConverter.convert(response.getBody());
    }

    public synchronized List<PlacedOrder> getOpenOrders() {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("nonce", createNonce()+"");
        params.put("method", "active_orders");

        HttpEntity<String> entity = createPrivateEntity(params);
        ResponseEntity<ZaifOrdersResponse> response = restTemplate.exchange(TRADING_API_URL, HttpMethod.POST, entity, ZaifOrdersResponse.class);
        return ordersConverter.convert(response.getBody());
    }


    /** Polling **/

    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 2000;
    }


    /** Utils **/

    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2().toLowerCase() + "_" + currencyPair.getCurrency1().toLowerCase();
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        return new HttpEntity<>("", headers);
    }

    private HttpEntity<String> createPrivateEntity(Map<String, String> params) {
        //params.put("nonce", createNonce()+"");
        String body = createQueryString(params, false);

        HttpHeaders headers = new HttpHeaders();
        headers.add("accept", "application/json");
        headers.add("Content-Type", "application/x-www-form-urlencoded");
        headers.add("Key", key);
        headers.add("Sign", getSignature(body));
        return new HttpEntity<>(body, headers);
    }

    private long createNonce() {
        try {
            long base_date = (new SimpleDateFormat("yyyy-MM-dd")).parse("2016-01-01").getTime();
            return (System.currentTimeMillis()-base_date)/100;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    private String getSignature(String body) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF8"), "HmacSHA512");
            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(body.getBytes()));
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertException(String error) {
        if(error.equals("insufficient funds")) {
            return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, error);
        } else if(error.equals("order not found") || error.equals("invalid order_id parameter")) {
            return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, error);
        } else if(error.equals("order is too new")) {
            return new RetryableExchangeClientException(ExchangeClientExceptionType.RATE_LIMIT, null, error);
        } else {
            return new RetryableExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, error);
        }
    }






    public void connectWs() {
        try {
            URI uri = URI.create(WS_API_URL);
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            Session session = container.connectToServer(this, uri);
            long s=System.currentTimeMillis();
        } catch (Exception e) {
            // Catch exceptions that will be thrown in case of invalid configuration
            e.printStackTrace();
        }
    }

    @OnOpen
    public void onOpen(Session p) {
        logger.info("onOpen");
//        try {
//            p.getBasicRemote().sendText("{\"type\":\"subscribe\",\"product_id\":\"BTC-EUR\"}");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }
    @OnMessage
    public void onMessage(String message, Session session) {
        logger.info("Message came from the server ! " + message);
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        logger.info("Websocket closed." + reason.getReasonPhrase());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        connectWs();
    }

    @OnError
    public void onError(Throwable t) {
        logger.error("Websocket error", t);
    }

}
