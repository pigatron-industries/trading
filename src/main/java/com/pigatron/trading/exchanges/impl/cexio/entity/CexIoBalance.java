package com.pigatron.trading.exchanges.impl.cexio.entity;


import java.math.BigDecimal;

public class CexIoBalance {

    private BigDecimal available;
    private BigDecimal orders;

    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }

    public BigDecimal getOrders() {
        return orders;
    }

    public void setOrders(BigDecimal orders) {
        this.orders = orders;
    }
}
