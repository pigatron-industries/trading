package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message;


import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.CoinFloorAssetCode;

import java.math.BigDecimal;

public class BalanceChangedMessage extends AbstractMessage {

    private int asset;
    private BigDecimal balance;

    public int getAsset() {
        return asset;
    }

    public void setAsset(int asset) {
        this.asset = asset;
    }

    public String getCurrency() {
        return CoinFloorAssetCode.getById(asset).name();
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
