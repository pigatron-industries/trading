package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class ZaifTradesResponse {

    private int success;

    @JsonProperty("return")
    private Map<String, ZaifTrade> trades;

    private String error;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public Map<String, ZaifTrade> getTrades() {
        return trades;
    }

    public void setTrades(Map<String, ZaifTrade> trades) {
        this.trades = trades;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
