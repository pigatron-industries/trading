package com.pigatron.trading.exchanges.impl.poloniex.entity;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PoloniexPlacedOrder {

    private String orderNumber;
    //private List<Trade> resultingTrades;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

}
