package com.pigatron.trading.exchanges.impl.kraken.entity;



import java.util.List;

public class KrakenOrdersResponse {

    private List<String> error;
    private KrakenOrders result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public KrakenOrders getResult() {
        return result;
    }

    public void setResult(KrakenOrders result) {
        this.result = result;
    }
}
