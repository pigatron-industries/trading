package com.pigatron.trading.exchanges.impl.hitbtc.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcOrder;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcTrade;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.pigatron.trading.exchanges.entity.Trade.aTrade;
import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.convertCurrenyPair;
import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.lotsToQuantity;

@Component
public class HitBtcTradeConverter implements Converter<HitBtcTrade, Trade> {

    @Override
    public Trade convert(HitBtcTrade source) {
        CurrencyPair currencyPair = convertCurrenyPair(source.getSymbol());
        return aTrade()
                .withTradeId(source.getTradeId())
                .withOrderId(source.getClientOrderId())
                .withCurrencyPair(currencyPair)
                .withPrice(source.getExecPrice())
                .withQuantity(lotsToQuantity(currencyPair, source.getExecQuantity()))
                .withType(source.getSide())
                .build();
    }

    public List<Trade> convertList(List<HitBtcTrade> trades) {
        return trades.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

}
