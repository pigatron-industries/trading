package com.pigatron.trading.exchanges.impl.gdax.converter;


import com.pigatron.trading.exchanges.entity.ChartData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Component
public class GdaxChartDataConverter implements Converter<List, ChartData> {

    @Override
    public ChartData convert(List source) {

        ChartData chartData = new ChartData();

        for(List dataPoint : (List<List>)source) {
            String time = dataPoint.get(0).toString();
            Double low = Double.parseDouble(dataPoint.get(1).toString());
            Double high = Double.parseDouble(dataPoint.get(2).toString());
            Double open = Double.parseDouble(dataPoint.get(3).toString());
            Double close = Double.parseDouble(dataPoint.get(4).toString());
            Double volume = Double.parseDouble(dataPoint.get(5).toString());
            chartData.addData(open, high, low, close);
        }

        return chartData;
    }

}
