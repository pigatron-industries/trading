package com.pigatron.trading.exchanges.impl.spacebtc.entity;


public class SpaceBtcOrder {

    private SpaceBtcOrderResult result;

    public SpaceBtcOrderResult getResult() {
        return result;
    }

    public void setResult(SpaceBtcOrderResult result) {
        this.result = result;
    }
}
