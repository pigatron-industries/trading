package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import java.util.List;

public class HitBtcOrdersWrapper {

    private List<HitBtcOrder> orders;

    public List<HitBtcOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<HitBtcOrder> orders) {
        this.orders = orders;
    }
}
