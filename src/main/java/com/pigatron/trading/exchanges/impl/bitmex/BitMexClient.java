package com.pigatron.trading.exchanges.impl.bitmex;


import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.bitmex.converter.BitMexOrderBookConverter;
import com.pigatron.trading.stats.StatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Profile("bitmex")
public class BitMexClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(BitMexClient.class);

    private static final String API_URL = "https://www.bitmex.com/api/v1";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/orderBook/L2";

    @Value("${exchange.bitmex.enabled}")
    private Boolean enabled;

    @Value("${exchange.bitmex.key}")
    private String key;

    @Value("${exchange.bitmex.secret}")
    private String secret;

    private BigDecimal makerFee = null;
    private BigDecimal takerFee = null;
    private BigDecimal withdrawFee = null;

    @Autowired
    private BitMexOrderBookConverter orderBookConverter;

    private RestTemplate restTemplate = new RestTemplate();


    @Autowired
    public BitMexClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "bitmex";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return null;
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        return new BigDecimal("1");
    }


    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) throws ExchangeClientException {
        Map<String, String> params = new HashMap<>();
        params.put("symbol", formatCurrencyPair(currencyPair));
        params.put("depth", depth+"");
        String url = GET_ORDER_BOOK_URL + "?" + createQueryString(params, false);

        ResponseEntity<List> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), List.class);
        return orderBookConverter.convert(response.getBody());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        return null;
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        return null;
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        return false;
    }



    private String formatCurrencyPair(CurrencyPair currencyPair) throws ExchangeClientException {
        if(currencyPair.getCurrency2().equals("BTC")) {
            return "XBT" + currencyPair.getCurrency1();
        } else {
            throw new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN_CURRENCY, null, null);
        }
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        return new HttpEntity<>("", headers);
    }

}
