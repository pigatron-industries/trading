package com.pigatron.trading.exchanges.impl.coinfloor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.coinfloor.converter.CoinFloorBalancesConverter;
import com.pigatron.trading.exchanges.impl.coinfloor.converter.CoinFloorOrderConverter;
import com.pigatron.trading.exchanges.impl.coinfloor.converter.CoinFloorTradeConverter;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.rest.CoinFloorMessage;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.rest.CoinFloorOrder;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.*;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message.AbstractMessage;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message.OrdersMatchedMessage;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message.WelcomeMessage;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request.AuthenticateRequest;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request.GetBalancesRequest;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request.WatchOrdersRequest;
import com.pigatron.trading.stats.StatsService;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.signers.ECDSASigner;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.websocket.*;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.stream.Collectors;

@Component
@ClientEndpoint
@Profile("coinfloor")
public class CoinFloorClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(CoinFloorClient.class);

    private static final String API_URL = "https://webapi.coinfloor.co.uk:8090/bist";
    private static final String WS_API_URL = "wss://api.coinfloor.co.uk/";

    private static final String GET_ORDER_BOOK_URL = API_URL + "/%s/%s/order_book/";
    private static final String GET_BALANCE_URL = API_URL + "/%s/%s/balance/";
    private static final String GET_ORDERS_URL = API_URL + "/%s/%s/open_orders/";
    private static final String GET_TRADES_URL = API_URL + "/%s/%s/user_transactions/";
    private static final String CANCEL_ORDER_URL = API_URL + "/%s/%s/cancel_order/";
    private static final String BUY_ORDER_URL = API_URL + "/%s/%s/buy/";
    private static final String SELL_ORDER_URL = API_URL + "/%s/%s/sell/";

    @Value("${exchange.coinfloor.userid}")
    private Long userId;

    @Value("${exchange.coinfloor.key}")
    private String key;

    @Value("${exchange.coinfloor.password}")
    private String password;

    @Value("${exchange.coinfloor.enabled}")
    private Boolean enabled;


    private BigDecimal makerFee = new BigDecimal("0");
    private BigDecimal takerFee = new BigDecimal("0");
    private BigDecimal withdrawFee = null;

    @Autowired
    private RestTemplate restTemplate;


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private CoinFloorBalancesConverter balancesConverter;

    @Autowired
    private CoinFloorOrderConverter orderConverter;

    @Autowired
    private CoinFloorTradeConverter tradeConverter;


    @Autowired
    public CoinFloorClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "coinfloor";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        return withdrawFee;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.0001");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("USD") || currency.equals("EUR") || currency.equals("GBP")) {
            return new BigDecimal("1");
        } else { //BTC
            return new BigDecimal("0.0001");
        }
    }


    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        String url = String.format(GET_ORDER_BOOK_URL, formatCurrency(currencyPair.getCurrency2()), formatCurrency(currencyPair.getCurrency1()));
        CommonOrderBook orderBook = restTemplate.getForObject(url, CommonOrderBook.class);
        return orderBookConverter.convert(orderBook);
    }



    @Override
    public Balances getBalances() {
        List<CurrencyPair> currencyPairs = Arrays.asList(new CurrencyPair("GBP", "BTC"), new CurrencyPair("USD", "BTC"));
        Map<String, BigDecimal> coinFloorBalances = currencyPairs.parallelStream()
                .map(this::getBalances)
                .map(Map::entrySet)
                .flatMap(Collection::stream)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, BigDecimal::max));

        return balancesConverter.convert(coinFloorBalances);
    }

    private Map<String, BigDecimal> getBalances(CurrencyPair currencyPair) {
        HttpEntity<String> entity = createPrivateEntity("");
        String url = String.format(GET_BALANCE_URL, formatCurrency(currencyPair.getCurrency2()), formatCurrency(currencyPair.getCurrency1()));
        return restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<Map<String, BigDecimal>>() {}).getBody();
    }


    @Override
    public PlacedOrder placeOrder(CurrencyPair currencyPair, TradeType side, BigDecimal price, BigDecimal amount) {
        Map<String, String> params = new HashMap<>();
        amount = amount.setScale(4, RoundingMode.DOWN);
        params.put("amount", amount.toPlainString());
        params.put("price", price.toPlainString());
        String url = String.format(side==TradeType.buy?BUY_ORDER_URL:SELL_ORDER_URL, formatCurrency(currencyPair.getCurrency2()), formatCurrency(currencyPair.getCurrency1()));
        url += '?' + createQueryString(params, false);
        HttpEntity<String> entity = createPrivateEntity("");
        try {
            ResponseEntity<CoinFloorOrder> response = restTemplate.exchange(url, HttpMethod.POST, entity, CoinFloorOrder.class);
            PlacedOrder placedOrder = orderConverter.convert(response.getBody());
            placedOrder.setCurrencyPair(currencyPair);
            return placedOrder;
        } catch(HttpClientErrorException e) {
            throw convertException(e);
        }
    }


    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        Map<String, String> params = new HashMap<>();
        params.put("id", orderId);
        String url = String.format(CANCEL_ORDER_URL, formatCurrency(currencyPair.getCurrency2()), formatCurrency(currencyPair.getCurrency1()));
        url += '?' + createQueryString(params, false);
        HttpEntity<String> entity = createPrivateEntity("");
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        if(!Boolean.valueOf(response.getBody())) {
            throw new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, "Order not found");
        }
        return true;
    }


    @Override
    public List<PlacedOrder> getOpenOrders(CurrencyPair currencyPair) {
        HttpEntity<String> entity = createPrivateEntity("");
        String url = String.format(GET_ORDERS_URL, formatCurrency(currencyPair.getCurrency2()), formatCurrency(currencyPair.getCurrency1()));
        ResponseEntity<List<CoinFloorOrder>> response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<CoinFloorOrder>>() {});
        return orderConverter.convert(response.getBody(), currencyPair);
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        // handled by websocket
        return new ArrayList<>();
    }


    //    @Override
//    public Optional<PlacedOrder> getOpenOrderById(String orderId, CurrencyPair currencyPair) throws OrderNotPlacedException {
//        //TODO
//    }


    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 1000;
    }


    private String formatCurrency(String currency) {
        if(currency.equals("BTC")) {
            return "XBT";
        } else {
            return currency;
        }
    }


    private HttpEntity<String> createPrivateEntity(String body) {
        try {
            HttpHeaders headers = new HttpHeaders();
            String auth = Base64.getEncoder().encodeToString((userId + "/" + key + ":" + password).getBytes("UTF-8"));
            headers.add("Authorization", "Basic " + auth);
            headers.add("Accept", "*/*");
            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("User-Agent", "Java");
            return new HttpEntity<>(headers);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }


    private ExchangeClientException convertException(HttpClientErrorException exception) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            CoinFloorMessage message = objectMapper.readValue(exception.getResponseBodyAsByteArray(), CoinFloorMessage.class);
            if(message.getCode() == 4) {
                return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, exception, message.getMessage());
            } else {
                logger.error(message.getMessage(), exception);
                return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, exception, message.getMessage());
            }
        } catch(IOException e) {
            logger.error("IOException", e);
            return null;
        }
    }



    //**************************** Websocket **************************//



    private ObjectMapper mapper = new ObjectMapper();

    private Session session;

    public void subscribe(ExchangeDataSubscriber subscriber, CurrencyPair currencyPair) throws IOException {
        connect();
        super.subscribe(subscriber, currencyPair);
    }

    private void connect() {
        try {
            if(session == null || !session.isOpen()) {
                WebSocketContainer container = ContainerProvider.getWebSocketContainer();
                container.connectToServer(this, new URI(WS_API_URL));
            }
        } catch (Exception e) {
            // Catch exceptions that will be thrown in case of invalid configuration
            logger.error(e.getMessage(), e);
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        logger.info("onOpen");
        this.session = session;
    }


    @OnMessage
    public void onMessage(String message, Session session) {
        try {
            logger.info("Receive: " + message);
            if (message.contains("notice")) {
                AbstractMessage messageObj = mapper.readValue(message, AbstractMessage.class);
                if (messageObj instanceof WelcomeMessage) {
                    onWelcomeMessage((WelcomeMessage)messageObj);
                } else if(messageObj instanceof OrdersMatchedMessage) {
                    onOrdersMatched((OrdersMatchedMessage)messageObj);
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void onOrdersMatched(OrdersMatchedMessage message) {
        Trade trade = tradeConverter.convert(message);
        notifyTrade(trade);
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        logger.info("Websocket closed.");
    }

    @OnError
    public void onError(Session session, Throwable t) {
        logger.info("onError: " + t.getMessage());
    }

    @Scheduled(fixedDelay=45000)
    private void ping() throws IOException {
        if(session != null) {
            session.getBasicRemote().sendPing(ByteBuffer.wrap(new byte[0]));
        }
    }

    private void sendMessage(Object message) throws IOException {
        String text = mapper.writeValueAsString(message);
        logger.info("Send:    " + text);
        session.getBasicRemote().sendText(text);
    }

    private void onWelcomeMessage(WelcomeMessage welcomeMessage) throws IOException {
        authenticate(welcomeMessage.getNonce());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //watchOrders();
    }

    private void watchOrders() throws IOException {
        WatchOrdersRequest watchOrdersRequest = new WatchOrdersRequest();
        watchOrdersRequest.setBase(CoinFloorAssetCode.BTC.getId());
        watchOrdersRequest.setCounter(CoinFloorAssetCode.GBP.getId());
        sendMessage(watchOrdersRequest);
    }

    private void getBalances2() throws IOException {
        sendMessage(new GetBalancesRequest());
    }


    private void authenticate(String serverNonce) throws IOException {
        Random random = new Random();
        byte[] clientNonce = new byte[16];
        random.nextBytes(clientNonce);
        final SHA224Digest sha = new SHA224Digest();
        DataOutputStream dos = new DataOutputStream(new OutputStream() {
            @Override
            public void write(int b) {
                sha.update((byte) b);
            }
            @Override
            public void write(byte[] buf, int off, int len) {
                sha.update(buf, off, len);
            }
        });

        dos.writeLong(userId);
        dos.write(password.getBytes("UTF-8"));
        dos.flush();
        byte[] digest = new byte[28];
        sha.doFinal(digest, 0);
        ECDSASigner signer = new ECDSASigner();
        ECNamedCurveParameterSpec spec = ECNamedCurveTable.getParameterSpec("secp224k1");
        ECDomainParameters secp224k1 = new ECDomainParameters(spec.getCurve(), spec.getG(), spec.getN(), spec.getH());
        signer.init(true, new ECPrivateKeyParameters(new BigInteger(1, digest), secp224k1));
        dos.writeLong(userId);
        dos.write(Base64.getDecoder().decode(serverNonce));
        dos.write(clientNonce);
        dos.close();
        sha.doFinal(digest, 0);
        BigInteger[] signature = signer.generateSignature(digest);

        AuthenticateRequest authenticateMessage = new AuthenticateRequest(userId, key,
                Base64.getEncoder().encodeToString(clientNonce),
                bigIntegerToBase64(signature[0], 28), bigIntegerToBase64(signature[1], 28));
        sendMessage(authenticateMessage);
    }

    private static String bigIntegerToBase64(BigInteger bi, int length) {
        byte[] bytes = bi.toByteArray();
        int over = bytes.length - length;
        if (over > 0) {
            for (int i = over; i > 0;) {
                if (bytes[--i] != 0) {
                    throw new IllegalArgumentException("integer is too large");
                }
            }
            return Base64.getEncoder().encodeToString(Arrays.copyOfRange(bytes, over, over+length));
        }
        if (over < 0) {
            byte[] src = bytes;
            System.arraycopy(src, 0, bytes = new byte[length], -over, src.length);
        }
        return Base64.getEncoder().encodeToString(bytes);
    }


}
