package com.pigatron.trading.exchanges.impl.quoine.entity;


import java.util.List;

public class QuoineTrades {

    private List<QuoineTrade> models;

    public List<QuoineTrade> getModels() {
        return models;
    }

    public void setModels(List<QuoineTrade> models) {
        this.models = models;
    }
}
