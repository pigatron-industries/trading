package com.pigatron.trading.exchanges.impl.gdax.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxWebsocketMatch extends GdaxWebsocketMessage {

    @JsonProperty("trade_id")
    private String tradeId;
    @JsonProperty("maker_order_id")
    private String makerOrderId;
    @JsonProperty("taker_order_id")
    private String takerOrderId;
    private BigDecimal size;
    private BigDecimal price;
    private TradeType side;

    @JsonProperty("maker_user_id")
    private String makerUserId;
    @JsonProperty("maker_profile_id")
    private String makerProfileId;
    @JsonProperty("taker_user_id")
    private String takerUserId;
    @JsonProperty("taker_profile_id")
    private String takerProfileId;


    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getMakerOrderId() {
        return makerOrderId;
    }

    public void setMakerOrderId(String makerOrderId) {
        this.makerOrderId = makerOrderId;
    }

    public String getTakerOrderId() {
        return takerOrderId;
    }

    public void setTakerOrderId(String takerOrderId) {
        this.takerOrderId = takerOrderId;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public TradeType getSide() {
        return side;
    }

    public void setSide(TradeType side) {
        this.side = side;
    }

    public String getMakerUserId() {
        return makerUserId;
    }

    public void setMakerUserId(String makerUserId) {
        this.makerUserId = makerUserId;
    }

    public String getMakerProfileId() {
        return makerProfileId;
    }

    public void setMakerProfileId(String makerProfileId) {
        this.makerProfileId = makerProfileId;
    }

    public String getTakerUserId() {
        return takerUserId;
    }

    public void setTakerUserId(String takerUserId) {
        this.takerUserId = takerUserId;
    }

    public String getTakerProfileId() {
        return takerProfileId;
    }

    public void setTakerProfileId(String takerProfileId) {
        this.takerProfileId = takerProfileId;
    }
}
