package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request;


public class WatchOrdersRequest extends AbstractWatchRequest {

    public WatchOrdersRequest() {
        super("WatchTicker");
    }

}
