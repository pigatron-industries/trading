package com.pigatron.trading.exchanges.impl.bitfinex.entity.websocket;


public class BfxWebsocketInfo extends BfxWebsocketMessage {

    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
