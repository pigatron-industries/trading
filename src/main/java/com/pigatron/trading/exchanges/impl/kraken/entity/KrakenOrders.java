package com.pigatron.trading.exchanges.impl.kraken.entity;


import java.util.Map;

public class KrakenOrders {

    private Map<String, KrakenOrder> open;

    public Map<String, KrakenOrder> getOpen() {
        return open;
    }

    public void setOpen(Map<String, KrakenOrder> open) {
        this.open = open;
    }
}
