package com.pigatron.trading.exchanges.impl.livecoin;


import com.pigatron.trading.exchanges.AbstractExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.impl.CommonOrderBook;
import com.pigatron.trading.exchanges.impl.CommonOrderBookConverter;
import com.pigatron.trading.exchanges.impl.livecoin.converter.LiveCoinBalanceConverter;
import com.pigatron.trading.exchanges.impl.livecoin.converter.LiveCoinTradeConverter;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinBalance;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinCancel;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinOrder;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinTrade;
import com.pigatron.trading.exchanges.impl.spacebtc.SpaceBtcClient;
import com.pigatron.trading.exchanges.util.NewTradesFilter;
import com.pigatron.trading.stats.StatsService;
import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;


@Component
@Profile("livecoin")
public class LiveCoinClient extends AbstractExchangeClient {

    private static final Logger logger = LoggerFactory.getLogger(SpaceBtcClient.class);

    private static final String API_URL = "https://api.livecoin.net/";

    private static final String GET_ORDER_BOOK_URL = API_URL + "exchange/order_book";
    private static final String GET_BALANCES_URL = API_URL + "payment/balances";
    private static final String PLACE_BUY_ORDER_URL = API_URL + "exchange/buylimit";
    private static final String PLACE_SELL_ORDER_URL = API_URL + "exchange/selllimit";
    private static final String CANCEL_ORDER_URL = API_URL + "exchange/cancellimit";
    private static final String GET_TRADES_URL = API_URL + "exchange/trades";

    @Value("${exchange.livecoin.enabled}")
    private Boolean enabled;

    @Value("${exchange.livecoin.key}")
    private String key;

    @Value("${exchange.livecoin.secret}")
    private String secret;

    private BigDecimal makerFee = new BigDecimal("0.002");
    private BigDecimal takerFee = new BigDecimal("0.002");


    @Autowired
    private CommonOrderBookConverter orderBookConverter;

    @Autowired
    private LiveCoinBalanceConverter balanceConverter;

    @Autowired
    private LiveCoinTradeConverter tradeConverter;

    @Autowired
    private RestTemplate restTemplate;


    private NewTradesFilter newTradesFilter = new NewTradesFilter();


    @Autowired
    public LiveCoinClient(StatsService statsService) {
        super(statsService);
    }

    @Override
    public String getName() {
        return "livecoin";
    }

    @Override
    public Boolean isEnabled() {
        return enabled;
    }

    @Override
    public BigDecimal getMakerFee() {
        return makerFee;
    }

    @Override
    public BigDecimal getTakerFee() {
        return takerFee;
    }

    @Override
    public BigDecimal getWithdrawFee(String currency) {
        if(currency.equals("BTC")) {
            return new BigDecimal("0.0004");
        }
        if(currency.equals("LTC")) {
            return new BigDecimal("0.0001");
        }
        if(currency.equals("ETH")) {
            return new BigDecimal("0.01");
        }
        return null;
    }

    @Override
    public BigDecimal getMinOrderAmount(String currency) {
        return new BigDecimal("0.005");
    }

    @Override
    public BigDecimal getPriceIncrement(String currency) {
        if(currency.equals("USD") || currency.equals("EUR")) {
            return new BigDecimal("0.00001");
        } else {
            return new BigDecimal("0.00000001");
        }
    }

    @Override
    public OrderBook getOrderBook(CurrencyPair currencyPair, int depth) {
        Map<String, String> params = new HashMap<>();
        params.put("currencyPair", formatCurrencyPair(currencyPair));
        params.put("depth", depth+"");
        params.put("groupByPrice", "true");

        String url = GET_ORDER_BOOK_URL + "?" + createQueryString(params, false);
        ResponseEntity<CommonOrderBook> response = restTemplate.exchange(url, HttpMethod.GET, createPublicEntity(), CommonOrderBook.class);
        return orderBookConverter.convert(response.getBody());
    }

    @Override
    public ChartData getChartData(CurrencyPair currencyPair, int dataSize, int periodSeconds) {
        return null;
    }

    @Override
    public Balances getBalances() {
        HttpEntity<String> entity = createPrivateEntity("");
        String url = GET_BALANCES_URL;
        ResponseEntity<List<LiveCoinBalance>> response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<LiveCoinBalance>>() {});
        return balanceConverter.convert(response.getBody());
    }

    @Override
    public PlacedOrder buy(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("currencyPair", formatCurrencyPair(currencyPair));
        params.put("price", price.toPlainString());
        params.put("quantity", amount.toPlainString());
        String query = createQueryString(params, true);
        HttpEntity<String> entity = createPrivateEntity(query);
        ResponseEntity<LiveCoinOrder> response = restTemplate.exchange(PLACE_BUY_ORDER_URL, HttpMethod.POST, entity, LiveCoinOrder.class);
        if(response.getBody().isSuccess()) {
            return new PlacedOrder(response.getBody().getOrderId(), TradeType.buy, currencyPair, price, amount);
        } else {
            throw convertError(response.getBody().getException());
        }
    }

    @Override
    public PlacedOrder sell(CurrencyPair currencyPair, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("currencyPair", formatCurrencyPair(currencyPair));
        params.put("price", price.toPlainString());
        params.put("quantity", amount.toPlainString());
        String query = createQueryString(params, true);
        HttpEntity<String> entity = createPrivateEntity(query);
        ResponseEntity<LiveCoinOrder> response = restTemplate.exchange(PLACE_SELL_ORDER_URL, HttpMethod.POST, entity, LiveCoinOrder.class);
        if(response.getBody().isAdded()) {
            return new PlacedOrder(response.getBody().getOrderId(), TradeType.sell, currencyPair, price, amount);
        } else {
            throw convertError(response.getBody().getException());
        }
    }

    @Override
    public PlacedOrder moveOrder(PlacedOrder order, BigDecimal price, BigDecimal amount) throws ExchangeClientException {
        cancelOrder(order.getOrderId(), order.getCurrencyPair());
        if(order.getType().equals(TradeType.buy)) {
            return buy(order.getCurrencyPair(), price, amount);
        } else {
            return sell(order.getCurrencyPair(), price, amount);
        }
    }

    @Override
    public boolean cancelOrder(String orderId, CurrencyPair currencyPair) throws ExchangeClientException {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("currencyPair", formatCurrencyPair(currencyPair));
        params.put("orderId", orderId);
        String query = createQueryString(params, true);
        HttpEntity<String> entity = createPrivateEntity(query);
        ResponseEntity<LiveCoinCancel> response = restTemplate.exchange(CANCEL_ORDER_URL, HttpMethod.POST, entity, LiveCoinCancel.class);
        if(response.getBody().isCancelled()) {
            return true;
        } else {
            throw convertError(response.getBody().getMessage());
        }
    }

    @Override
    public List<Trade> getOrderTrades(PlacedOrder order) {
        return null;
    }

    @Override
    public List<Trade> getPrivateTrades(CurrencyPair currencyPair) {
        Map<String, String> params = new LinkedHashMap<>();
        params.put("currencyPair", formatCurrencyPair(currencyPair));
        params.put("orderDesc", "true");
        HttpEntity<String> entity = createPrivateEntity(createQueryString(params, true));
        String url = GET_TRADES_URL + "?" + createQueryString(params, false);
        try {
            ResponseEntity<List<LiveCoinTrade>> response = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<LiveCoinTrade>>() {});
            return tradeConverter.convert(response.getBody());
        } catch(HttpMessageNotReadableException e) {
            return new ArrayList<>();
        }
    }


    @Scheduled(fixedDelay=1000)
    public void poll() {
        super.poll();
        sleep(1000);
    }

    @Override
    public int getTimeResolutionMillis() {
        return 2000;
    }

    private String formatCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2() + "/" + currencyPair.getCurrency1();
    }

    private HttpEntity<String> createPublicEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        return new HttpEntity<>("", headers);
    }

    private HttpEntity<String> createPrivateEntity(String request) {
        try {
            String body = URLEncoder.encode(request, "UTF-8");
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/x-www-form-urlencoded");
            headers.add("Accept", "application/json");
            headers.add("Api-key", key);
            headers.add("Sign", getSignature(request));
            return new HttpEntity<>(request, headers);
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public String getSignature(String request) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            return Hex.encodeHexString(mac.doFinal(request.getBytes("UTF-8"))).toUpperCase();
        } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private ExchangeClientException convertError(String error) {
        if(error.contains("Not sufficient funds")) {
            return new ExchangeClientException(ExchangeClientExceptionType.INSUFFICIENT_FUNDS, null, error);
        } else if(error.contains("can't find order") || error.contains("isn't in OrderBook")) {
            return new ExchangeClientException(ExchangeClientExceptionType.ORDER_NOT_FOUND, null, error);
        } else {
            return new ExchangeClientException(ExchangeClientExceptionType.UNKNOWN, null, error);
        }
    }
}
