package com.pigatron.trading.exchanges.impl.kraken.entity;

import java.util.Map;


public class KrakenTrades {

    private Map<String, KrakenTrade> trades;
    private int count;

    public Map<String, KrakenTrade> getTrades() {
        return trades;
    }

    public void setTrades(Map<String, KrakenTrade> trades) {
        this.trades = trades;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
