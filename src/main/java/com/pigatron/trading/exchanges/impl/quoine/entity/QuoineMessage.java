package com.pigatron.trading.exchanges.impl.quoine.entity;


import java.util.List;
import java.util.Map;

public class QuoineMessage {

    private String message;
    private Map<String, List<String>> errors;

    public String getMessage() {
        if(errors != null) {
            return ((List)errors.values().toArray()[0]).get(0).toString();
        } else {
            return message;
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }
}
