package com.pigatron.trading.exchanges.impl.kraken.entity;


import java.util.List;

public class KrakenResponse {

    private List<String> error;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

}
