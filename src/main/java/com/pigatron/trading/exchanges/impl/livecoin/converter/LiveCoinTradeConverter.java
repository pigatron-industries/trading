package com.pigatron.trading.exchanges.impl.livecoin.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinTrade;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class LiveCoinTradeConverter implements Converter<List<LiveCoinTrade>, List<Trade>> {

    @Override
    public List<Trade> convert(List<LiveCoinTrade> source) {
        List<Trade> trades = new ArrayList<>();

        for (LiveCoinTrade sourceTrade : source) {
            Trade trade = new Trade();
            trade.setTradeId(sourceTrade.getId());
            trade.setOrderId(sourceTrade.getClientorderid());
            trade.setType(sourceTrade.getType());
            trade.setPrice(sourceTrade.getPrice());
            trade.setQuantity(sourceTrade.getQuantity());
            trade.setFee(sourceTrade.getCommission());
            trades.add(trade);
        }

        return trades;
    }
}
