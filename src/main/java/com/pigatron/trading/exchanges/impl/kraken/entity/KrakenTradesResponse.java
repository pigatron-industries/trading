package com.pigatron.trading.exchanges.impl.kraken.entity;



import java.util.List;

public class KrakenTradesResponse {

    private List<String> error;
    private KrakenTrades result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public KrakenTrades getResult() {
        return result;
    }

    public void setResult(KrakenTrades result) {
        this.result = result;
    }
}
