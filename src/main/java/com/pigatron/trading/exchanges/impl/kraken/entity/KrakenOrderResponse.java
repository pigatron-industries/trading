package com.pigatron.trading.exchanges.impl.kraken.entity;


import java.util.List;

public class KrakenOrderResponse {

    private List<String> error;
    private KrakenOrder result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public KrakenOrder getResult() {
        return result;
    }

    public void setResult(KrakenOrder result) {
        this.result = result;
    }
}
