package com.pigatron.trading.exchanges.impl.cexio.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CexIoOrderDetailsWrapper {

    private CexIoOrderDetails data;

    public CexIoOrderDetails getData() {
        return data;
    }

    public void setData(CexIoOrderDetails data) {
        this.data = data;
    }
}
