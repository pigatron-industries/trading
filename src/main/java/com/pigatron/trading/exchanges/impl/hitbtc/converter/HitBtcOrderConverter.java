package com.pigatron.trading.exchanges.impl.hitbtc.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.impl.hitbtc.entity.HitBtcOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.convertCurrenyPair;
import static com.pigatron.trading.exchanges.impl.hitbtc.converter.HitBtcConverterUtils.lotsToQuantity;

@Component
public class HitBtcOrderConverter implements Converter<HitBtcOrder, PlacedOrder> {

    @Override
    public PlacedOrder convert(HitBtcOrder source) {
        CurrencyPair currencyPair = convertCurrenyPair(source.getSymbol());
        return new PlacedOrder(source.getClientOrderId(), source.getSide(), currencyPair,
                source.getOrderPrice(), lotsToQuantity(currencyPair, source.getOrderQuantity()));
    }

    public List<PlacedOrder> convertList(List<HitBtcOrder> orders) {
        return orders.stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

}
