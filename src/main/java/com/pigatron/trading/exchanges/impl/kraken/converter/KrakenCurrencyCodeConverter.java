package com.pigatron.trading.exchanges.impl.kraken.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KrakenCurrencyCodeConverter implements Converter<String, String> {

    @Override
    public String convert(String currency) {
        if(currency.length() == 4) {
            currency = currency.substring(1);
        }
        if(currency.equals("XBT")) {
            currency = "BTC";
        }
        return currency;
    }

}
