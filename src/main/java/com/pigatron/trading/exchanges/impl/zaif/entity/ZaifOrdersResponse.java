package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class ZaifOrdersResponse {

    private int success;

    @JsonProperty("return")
    private Map<String, ZaifOrder> orders;

    private String error;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public Map<String, ZaifOrder> getOrders() {
        return orders;
    }

    public void setOrders(Map<String, ZaifOrder> orders) {
        this.orders = orders;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
