package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import com.pigatron.trading.exchanges.impl.CommonOrderBook;

public class SpaceBtcOrderBook {

    private CommonOrderBook result;

    public CommonOrderBook getResult() {
        return result;
    }

    public void setResult(CommonOrderBook result) {
        this.result = result;
    }
}
