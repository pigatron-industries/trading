package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.CoinFloorAssetCode;

import java.math.BigDecimal;

public class OrderClosedMessage extends AbstractMessage {

    private String id;
    private int tonce;
    private int base;
    private int counter;
    private BigDecimal quantity;
    private BigDecimal price;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTonce() {
        return tonce;
    }

    public void setTonce(int tonce) {
        this.tonce = tonce;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public CurrencyPair getCurrencyPair() {
        return new CurrencyPair(CoinFloorAssetCode.getById(counter).name(), CoinFloorAssetCode.getById(base).name());
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
