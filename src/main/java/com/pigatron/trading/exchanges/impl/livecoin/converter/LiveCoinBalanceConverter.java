package com.pigatron.trading.exchanges.impl.livecoin.converter;


import com.pigatron.trading.exchanges.entity.Balance;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.livecoin.entity.LiveCoinBalance;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class LiveCoinBalanceConverter implements Converter<List<LiveCoinBalance>, Balances> {

    @Override
    public Balances convert(List<LiveCoinBalance> source) {
        Balances balances = new Balances();

        Map<String, Balance> balanceMap = new HashMap<>();

        for(LiveCoinBalance balanceSource : source) {
            String currency = balanceSource.getCurrency();
            Balance balance = balanceMap.get(currency);
            if(balance == null) {
                balance = new Balance();
                balanceMap.put(currency, balance);
            }

            if(balanceSource.getType().equals("trade")) {
                balance.setOnOrders(balanceSource.getValue());
            } else if(balanceSource.getType().equals("available")) {
                balance.setAvailable(balanceSource.getValue());
            }
        }

        for (Map.Entry<String, Balance> balanceEntry : balanceMap.entrySet()) {
            String currency = balanceEntry.getKey();
            Balance balance = balanceEntry.getValue();
            balances.setBalance(currency, balance.getAvailable(), balance.getOnOrders(), null);
        }

        return balances;
    }
}
