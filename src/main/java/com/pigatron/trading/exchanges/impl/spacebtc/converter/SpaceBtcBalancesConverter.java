package com.pigatron.trading.exchanges.impl.spacebtc.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.SpaceBtcAccount;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SpaceBtcBalancesConverter implements Converter<SpaceBtcAccount, Balances> {

    @Override
    public Balances convert(SpaceBtcAccount account) {
        Balances balances = new Balances();
        balances.setBalance("EUR", account.getResult().getEUR().getAvailable(), account.getResult().getEUR().getBlocked(), null);
        balances.setBalance("BTC", account.getResult().getBTC().getAvailable(), account.getResult().getBTC().getBlocked(), null);
        balances.setBalance("LTC", account.getResult().getLTC().getAvailable(), account.getResult().getLTC().getBlocked(), null);
        balances.setBalance("ETH", account.getResult().getETH().getAvailable(), account.getResult().getETH().getBlocked(), null);
        return balances;
    }
}
