package com.pigatron.trading.exchanges.impl.cexio.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CexIoBalances {

    @JsonProperty("EUR")
    private CexIoBalance EUR;
    @JsonProperty("USD")
    private CexIoBalance USD;
    @JsonProperty("BTC")
    private CexIoBalance BTC;
    @JsonProperty("LTC")
    private CexIoBalance LTC;
    @JsonProperty("ETH")
    private CexIoBalance ETH;

    public CexIoBalance getEUR() {
        return EUR;
    }

    public void setEUR(CexIoBalance EUR) {
        this.EUR = EUR;
    }

    public CexIoBalance getUSD() {
        return USD;
    }

    public void setUSD(CexIoBalance USD) {
        this.USD = USD;
    }

    public CexIoBalance getBTC() {
        return BTC;
    }

    public void setBTC(CexIoBalance BTC) {
        this.BTC = BTC;
    }

    public CexIoBalance getLTC() {
        return LTC;
    }

    public void setLTC(CexIoBalance LTC) {
        this.LTC = LTC;
    }

    public CexIoBalance getETH() {
        return ETH;
    }

    public void setETH(CexIoBalance ETH) {
        this.ETH = ETH;
    }

}
