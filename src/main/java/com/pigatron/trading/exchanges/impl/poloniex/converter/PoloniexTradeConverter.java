package com.pigatron.trading.exchanges.impl.poloniex.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.poloniex.entity.PoloniexTrade;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PoloniexTradeConverter implements Converter<PoloniexTrade, Trade> {

    @Override
    public Trade convert(PoloniexTrade poloniexTrade) {

//        poloniexTrade.get("globalTradeID");
//        poloniexTrade.get("orderNumber");

        Trade trade = new Trade();
        trade.setTradeId(poloniexTrade.getTradeID());
        trade.setType(poloniexTrade.getType());
        trade.setPrice(poloniexTrade.getRate());
        trade.setQuantity(poloniexTrade.getAmount());
        trade.setTotal(poloniexTrade.getTotal());
        trade.setFee(poloniexTrade.getFee());
        trade.setDate(poloniexTrade.getDate());

        return trade;
    }


    public List<Trade> convertList(List<PoloniexTrade> poloniexTrades) {
        return poloniexTrades.stream()
                .map(t -> convert(t))
                .collect(Collectors.toList());
    }
}
