package com.pigatron.trading.exchanges.impl.kraken.entity;


import java.math.BigDecimal;
import java.util.List;

public class KrakenOrder {

    private List<String> txid;

    private String status;
    private KrakenOrderDescription descr;
    private BigDecimal vol;



    public List<String> getTxid() {
        return txid;
    }

    public void setTxid(List<String> txid) {
        this.txid = txid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public KrakenOrderDescription getDescr() {
        return descr;
    }

    public void setDescr(KrakenOrderDescription descr) {
        this.descr = descr;
    }

    public BigDecimal getVol() {
        return vol;
    }

    public void setVol(BigDecimal vol) {
        this.vol = vol;
    }
}
