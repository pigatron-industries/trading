package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class LiquiTrade {

    private BigDecimal amount;
    private BigDecimal rate;
    private TradeType type;
    private long timestamp;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("is_your_order")
    private String isYourOrder;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIsYourOrder() {
        return isYourOrder;
    }

    public void setIsYourOrder(String isYourOrder) {
        this.isYourOrder = isYourOrder;
    }
}
