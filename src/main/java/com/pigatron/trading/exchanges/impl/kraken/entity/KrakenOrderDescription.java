package com.pigatron.trading.exchanges.impl.kraken.entity;


import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

public class KrakenOrderDescription {

    private String pair;
    private TradeType type;
    private BigDecimal price;

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
