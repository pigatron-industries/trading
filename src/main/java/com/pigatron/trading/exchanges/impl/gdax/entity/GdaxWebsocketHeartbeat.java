package com.pigatron.trading.exchanges.impl.gdax.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxWebsocketHeartbeat extends GdaxWebsocketMessage {

    @JsonProperty("last_trade_id")
    private Long lastTradeId;

    public Long getLastTradeId() {
        return lastTradeId;
    }

    public void setLastTradeId(Long lastTradeId) {
        this.lastTradeId = lastTradeId;
    }
}
