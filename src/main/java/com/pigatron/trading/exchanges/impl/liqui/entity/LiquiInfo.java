package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiquiInfo {

    private Map<String, BigDecimal> funds;
    @JsonProperty("open_orders")
    private Object openOrders;

    public Map<String, BigDecimal> getFunds() {
        return funds;
    }

    public void setFunds(Map<String, BigDecimal> funds) {
        this.funds = funds;
    }

    public Object getOpenOrders() {
        return openOrders;
    }

    public void setOpenOrders(Object openOrders) {
        this.openOrders = openOrders;
    }
}
