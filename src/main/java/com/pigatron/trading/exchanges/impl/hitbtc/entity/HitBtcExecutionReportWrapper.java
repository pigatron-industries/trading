package com.pigatron.trading.exchanges.impl.hitbtc.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

public class HitBtcExecutionReportWrapper {

    @JsonProperty("ExecutionReport")
    private HitBtcExecutionReport executionReport;

    @JsonProperty("CancelReject")
    private HitBtcCancelReject cancelReject;

    public HitBtcExecutionReport getExecutionReport() {
        return executionReport;
    }

    public void setExecutionReport(HitBtcExecutionReport executionReport) {
        this.executionReport = executionReport;
    }

    public HitBtcCancelReject getCancelReject() {
        return cancelReject;
    }

    public void setCancelReject(HitBtcCancelReject cancelReject) {
        this.cancelReject = cancelReject;
    }
}
