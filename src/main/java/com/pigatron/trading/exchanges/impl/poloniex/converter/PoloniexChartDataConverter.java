package com.pigatron.trading.exchanges.impl.poloniex.converter;


import com.pigatron.trading.exchanges.entity.ChartData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.*;


@Component
public class PoloniexChartDataConverter implements Converter<List, ChartData> {

    @Override
    public ChartData convert(List source) {

        ChartData chartData = new ChartData();

        for(Map dataPoint : (List<Map>)source) {
            Double open = Double.parseDouble(dataPoint.get("open").toString());
            Double high = Double.parseDouble(dataPoint.get("high").toString());
            Double low = Double.parseDouble(dataPoint.get("low").toString());
            Double close = Double.parseDouble(dataPoint.get("close").toString());
            chartData.addData(open, high, low, close);
        }

        return chartData;
    }

}
