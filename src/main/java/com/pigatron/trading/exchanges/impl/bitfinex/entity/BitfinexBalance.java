package com.pigatron.trading.exchanges.impl.bitfinex.entity;


import java.math.BigDecimal;

public class BitfinexBalance {

    private String currency;
    private BigDecimal amount;
    private BigDecimal available;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAvailable() {
        return available;
    }

    public void setAvailable(BigDecimal available) {
        this.available = available;
    }
}
