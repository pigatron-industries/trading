package com.pigatron.trading.exchanges.impl.cexio.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CexIoOrderDetails {

    private String id;
    private String type;
    private String lastTx;
    private String status;
    private BigDecimal remains;
    private List<CexIoTrades> vtx;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLastTx() {
        return lastTx;
    }

    public void setLastTx(String lastTx) {
        this.lastTx = lastTx;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getRemains() {
        return remains;
    }

    public void setRemains(BigDecimal remains) {
        this.remains = remains;
    }

    public List<CexIoTrades> getVtx() {
        return vtx;
    }

    public void setVtx(List<CexIoTrades> vtx) {
        this.vtx = vtx;
    }
}
