package com.pigatron.trading.exchanges.impl.gdax.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GdaxWebsocketOpen extends GdaxWebsocketMessage {

    @JsonProperty("order_id")
    private String orderId;
    private BigDecimal price;
    @JsonProperty("remaining_size")
    private BigDecimal remainingSize;
    private TradeType side;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getRemainingSize() {
        return remainingSize;
    }

    public void setRemainingSize(BigDecimal remainingSize) {
        this.remainingSize = remainingSize;
    }

    public TradeType getSide() {
        return side;
    }

    public void setSide(TradeType side) {
        this.side = side;
    }
}
