package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.request;


public class GetBalancesRequest extends AbstractRequest {

    public GetBalancesRequest() {
        super("GetBalances");
    }

}
