package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message;


import com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket.message.AbstractMessage;

public class WelcomeMessage extends AbstractMessage {

    private String nonce;

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

}
