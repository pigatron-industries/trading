package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pigatron.trading.exchanges.entity.TradeType;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiquiOrder {

    @JsonProperty("order_id")
    private String orderId;
    private String pair;
    private TradeType type;
    private BigDecimal amount;
    private BigDecimal rate;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPair() {
        return pair;
    }

    public void setPair(String pair) {
        this.pair = pair;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }
}
