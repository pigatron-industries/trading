package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class SpaceBtcTrade {

    private String id;
    private int side;
    private BigDecimal price;
    private BigDecimal qty;
    @JsonProperty("order_id")
    private String orderId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
