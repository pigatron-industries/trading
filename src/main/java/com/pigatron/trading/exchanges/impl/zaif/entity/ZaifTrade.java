package com.pigatron.trading.exchanges.impl.zaif.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class ZaifTrade {

    private BigDecimal amount;

    private BigDecimal price;

    @JsonProperty("your_action")
    private String yourAction;

    @JsonProperty("trade_type")
    private String tradeType;

    private long date;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getYourAction() {
        return yourAction;
    }

    public void setYourAction(String yourAction) {
        this.yourAction = yourAction;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
}
