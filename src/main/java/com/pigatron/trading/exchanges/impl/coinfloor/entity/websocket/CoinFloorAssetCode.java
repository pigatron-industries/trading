package com.pigatron.trading.exchanges.impl.coinfloor.entity.websocket;


public enum CoinFloorAssetCode {

    BTC(0xF800),
    GBP(0xFA20);

    private final int id;

    CoinFloorAssetCode(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static CoinFloorAssetCode getById(int n) {
        for (CoinFloorAssetCode a : values()) {
            if (a.id == n) {
                return a;
            }
        }
        throw new IllegalArgumentException(String.valueOf(n));
    }
}
