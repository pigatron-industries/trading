package com.pigatron.trading.exchanges.impl.kraken.entity;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class KrakenBalancesResponse {

    private List<String> error;
    private Map<String, BigDecimal> result;

    public List<String> getError() {
        return error;
    }

    public void setError(List<String> error) {
        this.error = error;
    }

    public Map<String, BigDecimal> getResult() {
        return result;
    }

    public void setResult(Map<String, BigDecimal> result) {
        this.result = result;
    }
}
