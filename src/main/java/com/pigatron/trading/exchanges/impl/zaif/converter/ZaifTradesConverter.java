package com.pigatron.trading.exchanges.impl.zaif.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTrade;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTradesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ZaifTradesConverter implements Converter<ZaifTradesResponse, List<Trade>> {

    @Override
    public List<Trade> convert(ZaifTradesResponse source) {
        List<Trade> trades = new ArrayList<>();

        for (Map.Entry<String, ZaifTrade> entry : source.getTrades().entrySet()) {
            Trade trade = new Trade();
            trade.setTradeId(entry.getKey());
            trade.setType(entry.getValue().getYourAction().equals("ask") ? TradeType.sell : TradeType.buy);
            trade.setPrice(entry.getValue().getPrice());
            trade.setQuantity(entry.getValue().getAmount());
            trades.add(trade);
        }

        return trades;
    }
}
