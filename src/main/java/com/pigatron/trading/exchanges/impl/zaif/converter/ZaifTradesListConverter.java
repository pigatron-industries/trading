package com.pigatron.trading.exchanges.impl.zaif.converter;


import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTrade;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTradesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class ZaifTradesListConverter implements Converter<List<ZaifTrade>, List<Trade>> {

    @Override
    public List<Trade> convert(List<ZaifTrade> source) {
        List<Trade> trades = new ArrayList<>();

        for (ZaifTrade zaifTrade : source) {
            Trade trade = new Trade();
            trade.setType(zaifTrade.getTradeType().equals("ask") ? TradeType.sell : TradeType.buy);
            trade.setPrice(zaifTrade.getPrice());
            trade.setQuantity(zaifTrade.getAmount());
            trade.setDate(new Date(zaifTrade.getDate()*1000));
            trades.add(trade);
        }

        return trades;
    }
}
