package com.pigatron.trading.exchanges.impl.zaif.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifOrder;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifOrdersResponse;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTrade;
import com.pigatron.trading.exchanges.impl.zaif.entity.ZaifTradesResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class ZaifOrdersConverter implements Converter<ZaifOrdersResponse, List<PlacedOrder>> {

    @Override
    public List<PlacedOrder> convert(ZaifOrdersResponse source) {
        List<PlacedOrder> orders = new ArrayList<>();

        for (Map.Entry<String, ZaifOrder> entry : source.getOrders().entrySet()) {
            PlacedOrder placedOrder = new PlacedOrder();
            placedOrder.setOrderId(entry.getKey());
            placedOrder.setType(entry.getValue().getAction().equals("ask") ? TradeType.sell : TradeType.buy);
            placedOrder.setAmount(entry.getValue().getAmount());
            placedOrder.setPrice(entry.getValue().getPrice());
            placedOrder.setCurrencyPair(new CurrencyPair(entry.getValue().getCurrencyPair().substring(4).toUpperCase(),
                                                         entry.getValue().getCurrencyPair().substring(0,3).toUpperCase()));
            orders.add(placedOrder);
        }

        return orders;
    }
}
