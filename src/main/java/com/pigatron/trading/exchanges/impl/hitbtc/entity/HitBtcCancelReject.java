package com.pigatron.trading.exchanges.impl.hitbtc.entity;


public class HitBtcCancelReject {

    private String rejectReasonCode;

    public String getRejectReasonCode() {
        return rejectReasonCode;
    }

    public void setRejectReasonCode(String rejectReasonCode) {
        this.rejectReasonCode = rejectReasonCode;
    }
}
