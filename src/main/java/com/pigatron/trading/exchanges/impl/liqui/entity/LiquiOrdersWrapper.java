package com.pigatron.trading.exchanges.impl.liqui.entity;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class LiquiOrdersWrapper {

    private int success;

    @JsonProperty("return")
    private Map<String, LiquiOrder> trades;

    private String error;


    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public Map<String, LiquiOrder> getTrades() {
        return trades;
    }

    public void setTrades(Map<String, LiquiOrder> trades) {
        this.trades = trades;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
