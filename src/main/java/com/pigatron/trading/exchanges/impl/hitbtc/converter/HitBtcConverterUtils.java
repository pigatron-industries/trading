package com.pigatron.trading.exchanges.impl.hitbtc.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class HitBtcConverterUtils {

    // https://api.hitbtc.com/api/1/public/symbols
    private static Map<CurrencyPair, BigDecimal> lotSizes = new HashMap<>();
    static {
        lotSizes.put(new CurrencyPair("USD", "BTC"), new BigDecimal("0.01"));
        lotSizes.put(new CurrencyPair("USD", "BCC"), new BigDecimal("0.01"));
        lotSizes.put(new CurrencyPair("USD", "ETH"), new BigDecimal("0.001"));
        lotSizes.put(new CurrencyPair("USD", "DASH"), new BigDecimal("0.001"));
        lotSizes.put(new CurrencyPair("BTC", "BCC"), new BigDecimal("0.001"));
        lotSizes.put(new CurrencyPair("BTC", "ETH"), new BigDecimal("0.001"));
    }


    public static BigDecimal quantityToLots(CurrencyPair currencyPair, BigDecimal quantity) {
        return quantity.divide(lotSizes.get(currencyPair), 0, BigDecimal.ROUND_UNNECESSARY);
    }

    public static BigDecimal lotsToQuantity(CurrencyPair currencyPair, BigDecimal lots) {
        return lots.multiply(lotSizes.get(currencyPair));
    }

    public static CurrencyPair convertCurrenyPair(String symbol) {
        return new CurrencyPair(symbol.substring(3), symbol.substring(0, 3));
    }

    public static String convertCurrencyPair(CurrencyPair currencyPair) {
        return currencyPair.getCurrency2() + currencyPair.getCurrency1();
    }

}
