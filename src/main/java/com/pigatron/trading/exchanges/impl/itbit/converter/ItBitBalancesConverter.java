package com.pigatron.trading.exchanges.impl.itbit.converter;


import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.impl.itbit.entity.ItBitBalance;
import com.pigatron.trading.exchanges.impl.itbit.entity.ItBitBalances;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ItBitBalancesConverter implements Converter<ItBitBalances, Balances> {

    @Override
    public Balances convert(ItBitBalances source) {
        Balances balances = new Balances();
        for (ItBitBalance sourceBalance : source.getBalances()) {
            balances.setBalance(sourceBalance.getCurrency(), sourceBalance.getAvailableBalance(),
                                sourceBalance.getTotalBalance().subtract(sourceBalance.getAvailableBalance()), null);
        }
        return balances;
    }
}
