package com.pigatron.trading.exchanges.impl.quoine.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.quoine.entity.QuoineOrder;
import com.pigatron.trading.exchanges.impl.quoine.entity.QuoineTrade;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class QuoineOrdersConverter implements Converter<List<QuoineOrder>, List<PlacedOrder>> {

    @Override
    public List<PlacedOrder> convert(List<QuoineOrder> source) {
        List<PlacedOrder> orders = new ArrayList<>();

        for (QuoineOrder sourceOrder : source) {
            PlacedOrder order = new PlacedOrder();
            order.setOrderId(sourceOrder.getId());
            order.setType(sourceOrder.getSide());
            order.setAmount(sourceOrder.getQuantity());
            order.setPrice(sourceOrder.getPrice());
            order.setFilledSize(sourceOrder.getFilledQuantity());
            order.setCurrencyPair(new CurrencyPair(sourceOrder.getCurrencyPair().substring(0, 3), sourceOrder.getCurrencyPair().substring(3, 6)));
            orders.add(order);
        }

        return orders;
    }
}
