package com.pigatron.trading.exchanges.impl.spacebtc.entity;


import java.util.List;

public class SpaceBtcTrades {

    private List<SpaceBtcTrade> result;

    public List<SpaceBtcTrade> getResult() {
        return result;
    }

    public void setResult(List<SpaceBtcTrade> result) {
        this.result = result;
    }
}
