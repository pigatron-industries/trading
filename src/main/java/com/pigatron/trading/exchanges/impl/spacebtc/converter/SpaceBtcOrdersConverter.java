package com.pigatron.trading.exchanges.impl.spacebtc.converter;


import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.impl.spacebtc.entity.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpaceBtcOrdersConverter implements Converter<SpaceBtcOrders, List<PlacedOrder>> {

    @Override
    public List<PlacedOrder> convert(SpaceBtcOrders source) {
        List<PlacedOrder> orders = new ArrayList<>();
        for(SpaceBtcOrderResult sourceOrder : source.getResult()) {
            PlacedOrder order = new PlacedOrder();
            order.setOrderId(sourceOrder.getId()+"");
            orders.add(order);
        }
        return orders;
    }
}
