package com.pigatron.trading.exchanges.impl.livecoin.entity;


import java.math.BigDecimal;

public class LiveCoinBalance {

    private String type;
    private String currency;
    private BigDecimal value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
