package com.pigatron.trading.notifications;

import com.pigatron.trading.util.RateLimiter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Component
public class PushNotificationClient {

    private static final String API_URL = "https://api.pushover.net/1/messages.json";

    @Value("${pushover.token}")
    private String token;

    @Value("${pushover.user}")
    private String user;

    @Value("${pushover.device}")
    private String device;

    private RestTemplate restTemplate = new RestTemplate();

    private Map<String, RateLimiter> rateLimiters = new HashMap<>();


    public void send(String message) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("token", token);
        params.add("user", user);
        params.add("device", device);
        params.add("message", message);
        restTemplate.postForEntity(API_URL, params, String.class);
    }

    public void sendLimit(String message) {
        if(canAcquire(message)) {
            acquire(message);
            send(message);
        }
    }

    private boolean canAcquire(String message) {
        RateLimiter rateLimiter = rateLimiters.get(message);
        if(rateLimiter == null) {
            rateLimiter = new RateLimiter(Duration.ofMinutes(30));
            rateLimiters.put(message, rateLimiter);
        }

        return rateLimiter.canAcquire();
    }

    private void acquire(String message) {
        RateLimiter rateLimiter = rateLimiters.get(message);
        if(rateLimiter == null) {
            rateLimiter = new RateLimiter(Duration.ofMinutes(30));
            rateLimiters.put(message, rateLimiter);
        }

        rateLimiter.acquire();
    }

}
