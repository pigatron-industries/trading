package com.pigatron.trading.util;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.Trade;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CurrencyUtil {


    public static BigDecimal round(BigDecimal value, BigDecimal increment, RoundingMode roundingMode) {
        if (increment.signum() == 0) {
            // 0 increment does not make much sense, but prevent division by 0
            return value;
        } else {
            BigDecimal divided = value.divide(increment, 0, roundingMode);
            BigDecimal multiplied = divided.multiply(increment);
            return multiplied.setScale(increment.scale(), RoundingMode.DOWN);
        }
    }

    public static BigDecimal getVolumeWeightedAveragePriceForTrades(List<Trade> trades) {
        BigDecimal price = new BigDecimal("0");
        BigDecimal quantity = new BigDecimal("0");
        for(Trade trade : trades) {
            price = price.add(trade.getPrice().multiply(trade.getQuantity()));
            quantity = quantity.add(trade.getQuantity());
        }
        price = price.divide(quantity, BigDecimal.ROUND_HALF_UP);
        return price;
    }


    public static BigDecimal getCostForAmount(Set<MarketOrder> marketOrders, BigDecimal amount) {
        BigDecimal amountLeft = amount;
        List<Trade> trades = new ArrayList<>();

        for(MarketOrder order : marketOrders) {
            if(order.getAmount().compareTo(amountLeft) <= 0) {
                // buy whole order
                amountLeft = amountLeft.subtract(order.getAmount());
                trades.add(new Trade(order.getPrice(), order.getAmount()));
            } else {
                // buy partial order
                trades.add(new Trade(order.getPrice(), amountLeft));
                break;
            }
            if(amountLeft.compareTo(new BigDecimal("0")) == 0) {
                break;
            }
        }

        BigDecimal cost = new BigDecimal("0");
        for(Trade trade : trades) {
            cost = cost.add(trade.getPrice().multiply(trade.getQuantity()));
        }

        return cost;
    }

    public static BigDecimal getAmountForCost(Set<MarketOrder> marketOrders, BigDecimal cost) {
        BigDecimal costLeft = cost;
        List<Trade> trades = new ArrayList<>();

        for(MarketOrder order : marketOrders) {
            if(order.getCost().compareTo(costLeft) <= 0) {
                // buy whole order
                costLeft = costLeft.subtract(order.getCost());
                trades.add(new Trade(order.getPrice(), order.getAmount()));
            } else {
                // buy partial order
                trades.add(new Trade(order.getPrice(), costLeft.divide(order.getPrice(), RoundingMode.HALF_UP)));
                break;
            }
            if(costLeft.compareTo(new BigDecimal("0")) == 0) {
                break;
            }
        }

        BigDecimal amount = new BigDecimal("0");
        for(Trade trade : trades) {
            amount = amount.add(trade.getQuantity());
        }

        return amount;
    }

    public static BigDecimal getPercentageDiff(BigDecimal price1, BigDecimal price2) {
        BigDecimal diff = price1.subtract(price2).abs();
        return diff.divide(price1, 4, RoundingMode.HALF_DOWN);
    }
}
