package com.pigatron.trading.util;


public class ExponentialMovingAverage {
    private double alpha;
    private Double oldValue;
    public ExponentialMovingAverage(int periods) {
        this.alpha = 2/((double)periods+1);

        //this.alpha = alpha;
    }

    public double add(double value) {
        if (oldValue == null) {
            oldValue = value;
            return value;
        }
        double newValue = oldValue + alpha * (value - oldValue);
        oldValue = newValue;
        return newValue;
    }

    public void setValue(double value) {
        oldValue = value;
    }

    public double getValue() {
        return oldValue;
    }
}
