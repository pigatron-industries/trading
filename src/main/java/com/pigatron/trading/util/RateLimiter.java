package com.pigatron.trading.util;


import com.google.common.util.concurrent.Uninterruptibles;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

public class RateLimiter {

    Instant lastAcquired;

    Duration minDuration;

    public RateLimiter(long minDurationMillis) {
        minDuration = Duration.ofMillis(minDurationMillis);
    }

    public RateLimiter(Duration minDuration) {
        this.minDuration = minDuration;
    }

    public void acquire() {
        long timeToWait = getTimeToWaitMillis();
        if(timeToWait <= 0) {
            lastAcquired = Instant.now();
            return;
        }
        Uninterruptibles.sleepUninterruptibly(timeToWait, TimeUnit.MILLISECONDS);
        lastAcquired = Instant.now();
    }

    public void acquire(int n) {
        acquire();
        lastAcquired = lastAcquired.plus(minDuration.multipliedBy(n-1));
    }

    public boolean canAcquire() {
        return getTimeToWaitMillis() <= 0;
    }

    private long getTimeToWaitMillis() {
        if(lastAcquired == null) {
            return 0;
        }
        return minDuration.toMillis() - Duration.between(lastAcquired, Instant.now()).toMillis();
    }
}
