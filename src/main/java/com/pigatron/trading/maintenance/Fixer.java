package com.pigatron.trading.maintenance;


public abstract class Fixer implements Runnable {

    protected String name;

    protected String output;

    public Fixer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}
