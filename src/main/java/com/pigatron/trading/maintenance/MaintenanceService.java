package com.pigatron.trading.maintenance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class MaintenanceService {


    @Autowired
    private List<Fixer> fixers;

    public String runFix(String fixName) {
        Fixer fixer = getFixer(fixName).orElseThrow(() -> new RuntimeException("Fix not found"));
        fixer.setOutput("");
        fixer.run();
        return fixer.getOutput();
    }

    private Optional<Fixer> getFixer(String name) {
        for(Fixer fixer : fixers) {
            if(fixer.getName().equals(name)) {
                return Optional.of(fixer);
            }
        }
        return Optional.empty();
    }


}
