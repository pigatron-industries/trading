package com.pigatron.trading.maintenance;

import com.pigatron.trading.exchanges.entity.PlacedOrder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class OrderConsolidationService {


    public PlacedOrder consolidateAll(List<PlacedOrder> orders) {
        BigDecimal price = new BigDecimal("0");
        BigDecimal quantity = new BigDecimal("0");
        for(PlacedOrder order : orders) {
            price = price.add(order.getPrice().multiply(order.getAmount()));
            quantity = quantity.add(order.getAmount());
        }
        price = price.divide(quantity, BigDecimal.ROUND_HALF_UP);

        return new PlacedOrder(null, orders.get(0).getType(), orders.get(0).getCurrencyPair(), price, quantity);
    }

}
