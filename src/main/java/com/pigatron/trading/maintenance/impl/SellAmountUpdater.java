package com.pigatron.trading.maintenance.impl;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import com.pigatron.trading.util.CurrencyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SellAmountUpdater extends Fixer {

    private static final Logger logger = LoggerFactory.getLogger(SellAmountUpdater.class);

    @Autowired
    private TaskRunnerService taskRunnerService;
    @Autowired
    private ExchangeProvider exchangeProvider;


    public SellAmountUpdater() {
        super("sellamount");
    }

    @Override
    //@Scheduled(cron = "${cron.sellamount}")
    public void run() {
        exchangeProvider.getExchanges().forEach(this::updateSellAmount);
    }

    private void updateSellAmount(ExchangeClient exchange) {
        List<TaskWrapper> tasks = taskRunnerService.getTasks(exchange);
        List<String> sellCurrencies = getSellCurrencies(tasks);

        for(String currency : sellCurrencies) {
            List<TaskWrapper> tasksForCurrency = tasks.stream()
                    .filter(taskWrapper -> taskWrapper.getCurrencyPair().getCurrency2().equals(currency))
                    .collect(Collectors.toList());
            updateSellAmount(currency, exchange, tasksForCurrency);
        }

    }

    private List<String> getSellCurrencies(List<TaskWrapper> taskWrappers) {
        return taskWrappers.stream()
                .map(taskWrapper -> taskWrapper.getCurrencyPair().getCurrency2())
                .distinct()
                .collect(Collectors.toList());
    }

    private void updateSellAmount(String currency, ExchangeClient exchange, List<TaskWrapper> tasks) {
        BigDecimal totalAmountOnExchange = exchange.getBalances().getBalance(currency).getTotal().setScale(8, RoundingMode.DOWN);

        List<SellTask> allSellTasks = tasks.stream()
                .map(task -> (AbstractAlgorithm) task.getExchangeDataSubscriber())
                .map(AbstractAlgorithm::getSellTasks)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        BigDecimal totalAmountOnSellTasks = allSellTasks.stream()
                .map(TradeTask::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .setScale(8, RoundingMode.DOWN);

        logger.info(exchange.getName() + ":" + currency + " Total amount on exchange = " + totalAmountOnExchange + ", total amount on sell tasks = " + totalAmountOnSellTasks);

        BigDecimal difference = totalAmountOnExchange.subtract(totalAmountOnSellTasks);
        if(difference.compareTo(BigDecimal.ZERO) != 0) {
            logger.info("Updating sell amounts. difference = " + difference);
            //TODO
        }
    }


}
