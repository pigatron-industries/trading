package com.pigatron.trading.maintenance.impl;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.shell.TradingCommands;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SpaceBtcSkewFix extends Fixer {

    @Autowired
    private ExchangeProvider exchangeProvider;

    @Autowired
    private TaskRunnerService taskRunnerService;

    @Autowired
    private TradingCommands tradingCommands;

    private ExchangeClient exchange;


    public SpaceBtcSkewFix() {
        super("spacebtcskew");
    }



    @Override
    public void run() {
        exchange = exchangeProvider.getExchange("spacebtc");
        Balances balances = exchange.getBalances();

        for (Map.Entry<String, Balance> balanceEntry : balances.getBalances().entrySet()) {
            String currency = balanceEntry.getKey();
            if(balanceEntry.getValue().getAvailable().compareTo(new BigDecimal("0")) == -1) {
                try {
                    fix(currency);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void fix(String currency) throws Exception {
        CurrencyPair currencyPair = new CurrencyPair("EUR", currency);

        TradeCycleAlgo task = stopTasks(currencyPair);
        placeBuyOrder(currencyPair, task.getBuyTasks().get(0).getMaxAmount().divide(new BigDecimal("2")));
        startTasks(task);
    }


    private TradeCycleAlgo stopTasks(CurrencyPair currencyPair) {
        List<ExchangeDataSubscriber> tasks = new ArrayList<>();
        tasks.addAll(taskRunnerService.getTasks(exchange.getName(), currencyPair));
        for (ExchangeDataSubscriber task : tasks) {
            task.stop();
        }
        if(tasks.size() > 0) {
            return (TradeCycleAlgo)tasks.get(0);
        }
        return null;
    }

    private void startTasks(TradeCycleAlgo task) throws Exception {
        BigDecimal maxAmount = task.getBuyTasks().get(0).getMaxAmount();
        BigDecimal buySellAmount = maxAmount.divide(new BigDecimal("2"));
        tradingCommands.cycleMinSpread(buySellAmount, buySellAmount, new BigDecimal("0.01"));
    }


    private void placeBuyOrder(CurrencyPair currencyPair, BigDecimal amount) throws ExchangeClientException {
        OrderBook orderBook = exchange.getOrderBook(currencyPair, 10);
        MarketOrder lowestAsk = orderBook.getAskByIndex(0);
        PlacedOrder order = exchange.buy(currencyPair, lowestAsk.getPrice().subtract(new BigDecimal("0.01")), amount);

        boolean orderComplete = false;
        while(!orderComplete) {
            sleep(30000);
            if(exchange.getOpenOrders(currencyPair).size() == 0) {
                orderComplete = true;
            }
        }
    }


    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /** Old negative balance correction **/

    public void negativeBalanceCorrection() {
        for (ExchangeClient exchange : exchangeProvider.getExchanges()) {
            Balances balances = exchange.getBalances();
            for (Map.Entry<String, Balance> currencyBalance : balances.getBalances().entrySet()) {
                String currency = currencyBalance.getKey();
                BigDecimal amountAvailable = currencyBalance.getValue().getAvailable();
                if (amountAvailable.compareTo(new BigDecimal("0")) == -1) {
                    makeBuyAdjustment(exchange, currency, amountAvailable.negate());
                }
            }
        }
    }

    private void makeBuyAdjustment(ExchangeClient exchange, String currency, BigDecimal correctionAmount) {
        List<TaskWrapper> tasks = taskRunnerService.getTasks(exchange);
        for(TaskWrapper task : tasks) {
            if(task.getCurrencyPair().getCurrency2().equals(currency) &&
                    task.getExchangeDataSubscriber() instanceof TradeCycleAlgo) {
                makeBuyAdjustment((TradeCycleAlgo)task.getExchangeDataSubscriber(), correctionAmount);
                break;
            }
        }
    }

    private void makeBuyAdjustment(TradeCycleAlgo tradeCycleAlgo, BigDecimal amount) {
        BigDecimal currentBuyAmount = tradeCycleAlgo.getBuyTasks().get(0).getAmount();
        BigDecimal newBuyAmount = currentBuyAmount.add(amount);
        tradeCycleAlgo.getBuyTasks().get(0).setAmount(newBuyAmount);

        BigDecimal currentSellAmount = tradeCycleAlgo.getSellTasks().get(0).getAmount();
        BigDecimal newSellAmount = currentSellAmount.subtract(amount);
        tradeCycleAlgo.getSellTasks().get(0).setAmount(newSellAmount);

        tradeCycleAlgo.setTaskModified();
    }
}
