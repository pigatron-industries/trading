package com.pigatron.trading.maintenance.impl;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.util.CurrencyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.expression.Lists;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class TradeAmountUpdater extends Fixer {

    private static final Logger logger = LoggerFactory.getLogger(TradeAmountUpdater.class);

    @Autowired
    private TaskRunnerService taskRunnerService;



    public TradeAmountUpdater() {
        super("tradeamount");
    }

    @Override
    @Scheduled(cron = "${cron.tradeamountupdate}")
    public void run() {
        List<TaskWrapper> tasks = taskRunnerService.getTasks();
        tasks.forEach(this::update);
    }

    private void update(TaskWrapper taskWrapper) {
        if(taskWrapper.getExchangeDataSubscriber() instanceof AbstractAlgorithm) {
            AbstractAlgorithm algo = (AbstractAlgorithm)taskWrapper.getExchangeDataSubscriber();
            if(algo.getBuyTasks().size() > 0) {
                logger.info("Auto adjusting trade amount for " + taskWrapper.getExchange().getName() + "/" + taskWrapper.getCurrencyPair());
                BigDecimal sellAmount = algo.getSellTasks().stream()
                        .map(TradeTask::getAmount)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                BigDecimal totalAmount = calculateMaxTradeAmount(taskWrapper.getExchange(), taskWrapper.getCurrencyPair(), sellAmount, algo.getBalancePercentageToUse());
                updateTotalAmount(algo, totalAmount, sellAmount);
                logger.info("Auto adjusted trade amount to " + totalAmount.toPlainString());
            }
        }

    }


    public void updateTotalAmount(AbstractAlgorithm algo, BigDecimal totalAmount, BigDecimal sellAmount) {
        totalAmount = CurrencyUtil.round(totalAmount, algo.getExchange().getPriceIncrement(algo.getCurrencyPair().getCurrency2()), RoundingMode.DOWN);

        List<BuyTask> buyTasks = new ArrayList<>(algo.getBuyTasks());

        BigDecimal firstAmount = buyTasks.get(0).getAmount();
        BigDecimal lastAmount = buyTasks.get(buyTasks.size()-1).getAmount();
        if(firstAmount.compareTo(lastAmount) < 0) {
            Collections.reverse(buyTasks);
        }

        BigDecimal amountPerTask = totalAmount.divide(new BigDecimal(buyTasks.size()), 3, RoundingMode.DOWN);
        amountPerTask = CurrencyUtil.round(amountPerTask, algo.getExchange().getPriceIncrement(algo.getCurrencyPair().getCurrency2()), RoundingMode.DOWN);

        for(BuyTask buyTask : buyTasks) {
            BigDecimal percentAmount = buyTask.getAmount().divide(buyTask.getMaxAmount(), 3, RoundingMode.HALF_DOWN);
            buyTask.setMaxAmount(amountPerTask);
            buyTask.setAmount(amountPerTask.multiply(percentAmount));
        }
    }

    public BigDecimal calculateMaxTradeAmount(ExchangeClient exchange, CurrencyPair currencyPair, BigDecimal sellAmount, BigDecimal balancePercentage) {
        OrderBook orderBook = exchange.getOrderBook(currencyPair, 10);
        Balances balances = exchange.getBalances();
        BigDecimal midPrice = orderBook.getMidPrice();
        BigDecimal balance = balances.getBalances().get(currencyPair.getCurrency1()).getTotal();
        balance = balance.multiply(balancePercentage);
        return calculateMaxTradeAmount(midPrice, balance, sellAmount);
    }

    private BigDecimal calculateMaxTradeAmount(BigDecimal currentPrice, BigDecimal balance, BigDecimal sellAmount) {
        BigDecimal tradeAmount = balance.divide(currentPrice, 3, RoundingMode.DOWN);
        return tradeAmount.add(sellAmount);
    }

}
