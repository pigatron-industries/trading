package com.pigatron.trading.maintenance.impl;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.ChartData;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.shell.TradingCommands;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
public class SpreadAutoAdjust extends Fixer {

    @Autowired
    private TaskRunnerService taskRunnerService;

    @Autowired
    private TradingCommands tradingCommands;

    public SpreadAutoAdjust() {
        super("spreadadjust");
    }

    @Override
    public void run() {
        List<TaskWrapper> tasks = taskRunnerService.getTasks();
        tasks.forEach(t -> autoAdjust(t.getExchange(), (AbstractAlgorithm)t.getExchangeDataSubscriber(), t.getCurrencyPair()));
    }

    private void autoAdjust(ExchangeClient exchange, AbstractAlgorithm exchangeDataSubscriber, CurrencyPair currencyPair) {
        if(exchangeDataSubscriber instanceof TradeCycleAlgo) {
            TradeCycleAlgo tradeCycle = (TradeCycleAlgo)exchangeDataSubscriber;
            BigDecimal totalSpreadPercent = tradingCommands.autoAdjustTotalSpread(tradeCycle);
            output += tradeCycle.getCurrencyPair().toString() + "  totalSpread = " + totalSpreadPercent;
        }
    }

}
