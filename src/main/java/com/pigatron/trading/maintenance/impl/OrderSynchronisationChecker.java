package com.pigatron.trading.maintenance.impl;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.shell.TradingCommands;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class OrderSynchronisationChecker extends Fixer {

    @Autowired
    private TaskRunnerService taskRunnerService;



    public OrderSynchronisationChecker() {
        super("ordersync");
    }


    @Override
    @Scheduled(cron = "${cron.ordersync}")
    public void run() {
        List<TaskWrapper> tasks = taskRunnerService.getTasks();
        tasks.forEach(t -> check(t.getExchange(), (AbstractAlgorithm)t.getExchangeDataSubscriber(), t.getCurrencyPair()));
    }

    private void check(ExchangeClient exchange, AbstractAlgorithm algorithm, CurrencyPair currencyPair) {
        synchronized(algorithm) {
            //find open orders which are not tracked by algorithm, and cancel them
            List<PlacedOrder> openOrders = exchange.getOpenOrders(currencyPair);
            openOrders.stream()
                    .filter(o -> !isOrderInList(o, (List<TradeTask>) (Object) algorithm.getBuyTasks()))
                    .filter(o -> !isOrderInList(o, (List<TradeTask>) (Object) algorithm.getSellTasks()))
                    .forEach(o -> cancelOrder(exchange, o));

            algorithm.getBuyTasks().stream()
                    .filter(t -> !isOrderInList(t, openOrders))
                    .forEach(t -> t.setOrder(null));

            algorithm.getSellTasks().stream()
                    .filter(t -> !isOrderInList(t, openOrders))
                    .forEach(t -> t.setOrder(null));
        }
    }

    private boolean isOrderInList(PlacedOrder order, List<TradeTask> tasks) {
        for(TradeTask task : tasks) {
            if(task.getOrder() != null && task.getOrder().getOrderId().equals(order.getOrderId())) {
                return true;
            }
        }
        return false;
    }

    private boolean isOrderInList(TradeTask task, List<PlacedOrder> orders) {
        for(PlacedOrder order : orders) {
            if(task.getOrder() != null && task.getOrder().getOrderId().equals(order.getOrderId())) {
                return true;
            }
        }
        return false;
    }

    private void cancelOrder(ExchangeClient exchange, PlacedOrder order) {
        try {
            exchange.cancelOrder(order.getOrderId(), order.getCurrencyPair());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
