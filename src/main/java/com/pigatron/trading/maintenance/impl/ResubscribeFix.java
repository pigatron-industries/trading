package com.pigatron.trading.maintenance.impl;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.impl.gdax.GdaxClient;
import com.pigatron.trading.maintenance.Fixer;
import com.pigatron.trading.shell.TradingCommands;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

@Component
public class ResubscribeFix extends Fixer {

    @Autowired
    private ExchangeProvider exchangeProvider;

    private static final Logger logger = LoggerFactory.getLogger(ResubscribeFix.class);

    public ResubscribeFix() {
        super("resubscribe");
    }

    @Override
    public void run() {
        ExchangeClient exchange = exchangeProvider.getExchange("gdax");
        ((GdaxClient)exchange).disconnect();
    }



}
