package com.pigatron.trading.stats;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.stats.entity.BalanceHistory;
import com.pigatron.trading.stats.entity.DayStats;
import com.pigatron.trading.stats.entity.TradeHistory;
import com.pigatron.trading.stats.repository.BalanceRepository;
import com.pigatron.trading.stats.repository.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.pigatron.trading.stats.entity.TradeHistory.aTradeHistory;

@Service
public class StatsService {

    @Autowired
    private TradeRepository tradeRepository;

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private ExchangeProvider exchangeProvider;


    public List<TradeHistory> getLatestTrades(int days) {
        LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS).plus(1, ChronoUnit.DAYS);
        LocalDateTime startDate = now.minus(days, ChronoUnit.DAYS);
        return tradeRepository.findByDateGreaterThan(toDate(startDate));
    }

    public List<BalanceHistory> getBalanceHistory(int days) {
        LocalDateTime date = LocalDateTime.now().minus(days, ChronoUnit.DAYS);
        return balanceRepository.findByDateBetween(toDate(date), new Date());
    }

    public List<DayStats> getDailyStats(int days) {
        LocalDateTime date = LocalDateTime.now().minus(days-1, ChronoUnit.DAYS);
        List<DayStats> stats = new ArrayList<>();

        for(int i = 0; i<days; i++) {
            stats.add(getDayStats(date, "gdax", new CurrencyPair("GBP", "BTC")));
            stats.add(getDayStats(date, "gdax", new CurrencyPair("EUR", "BTC")));
            stats.add(getDayStats(date, "zaif", new CurrencyPair("JPY", "BTC")));
            stats.add(getDayStats(date, "quoine", new CurrencyPair("JPY", "BTC")));
            date = date.plus(1, ChronoUnit.DAYS);
        }

        return stats;
    }

    public DayStats getDayStats(LocalDateTime day, String exchangeName, CurrencyPair currencyPair) {
        LocalDateTime from = day.truncatedTo(ChronoUnit.DAYS);
        LocalDateTime to = from.plus(1, ChronoUnit.DAYS);
        List<TradeHistory> trades = tradeRepository.findByExchangeAndCurrencyPairAndDateBetweenOrderByDate(exchangeName, currencyPair, toDate(from), toDate(to));

        DayStats dayStats = new DayStats();
        BigDecimal change1 = new BigDecimal(0);
        BigDecimal change2 = new BigDecimal(0);
        BigDecimal lastPrice = new BigDecimal(0);
        BigDecimal volume = new BigDecimal(0);

        for(TradeHistory trade : trades) {
            if(trade.getType().equals(TradeType.buy)) {
                change1 = change1.subtract(trade.getTotal());
                change2 = change2.add(trade.getAmount());
            } else {
                change1 = change1.add(trade.getTotal());
                change2 = change2.subtract(trade.getAmount());
            }
            volume = volume.add(trade.getAmount());
            lastPrice = trade.getPrice();
        }

        change1 = change1.add(change2.multiply(lastPrice));

        dayStats.setExchange(exchangeName);
        dayStats.setCurrencyPair(currencyPair);
        dayStats.setDate(toDate(from));
        dayStats.setProfit(change1);
        dayStats.setNumberTrades(trades.size());
        dayStats.setVolume(volume);
        return dayStats;
    }


    public void recordTrades(List<Trade> trades, String exchange, CurrencyPair currencyPair) {
        try {
            for (Trade trade : trades) {
                TradeHistory tradeHistory = aTradeHistory()
                        .withId(trade.getTradeId())
                        .withDate(trade.getDate() != null ? trade.getDate() : new Date())
                        .withExchange(exchange)
                        .withCurrencyPair(currencyPair)
                        .withPrice(trade.getPrice())
                        .withAmount(trade.getQuantity())
                        .withFee(trade.getFee())
                        .withType(trade.getType())
                        .withTotal(trade.getPrice().multiply(trade.getQuantity()))
                        .build();
                tradeRepository.save(tradeHistory);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void recordBalances() {
        boolean retry = true;
        while(retry) {
            try {
                BalanceHistory balanceHistory = new BalanceHistory();
                for (ExchangeClient exchange : exchangeProvider.getExchanges()) {
                    Balances balances = exchange.getBalances();
                    for (Map.Entry<String, Balance> currencyBalance : balances.getBalances().entrySet()) {
                        String currency = currencyBalance.getKey();
                        BigDecimal amount = currencyBalance.getValue().getTotal();
                        if (amount.compareTo(new BigDecimal("0")) == 1) {
                            balanceHistory.putBalance(exchange.getName(), currency, amount);
                        }
                    }
                }

                balanceRepository.save(balanceHistory);
                retry = false;
            } catch (Exception e) {
                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
