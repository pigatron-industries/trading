package com.pigatron.trading.stats.repository;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.stats.entity.TradeHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface TradeRepository extends MongoRepository<TradeHistory, String> {

    List<TradeHistory> findByDateGreaterThan(Date startDate);

    List<TradeHistory> findByExchangeAndCurrencyPairAndDateBetweenOrderByDate(String exchange, CurrencyPair currencyPair, Date from, Date to);

}
