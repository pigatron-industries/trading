package com.pigatron.trading.stats.repository;


import com.pigatron.trading.stats.entity.BalanceHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;


public interface BalanceRepository extends MongoRepository<BalanceHistory, String> {

    List<BalanceHistory> findByDateBetween(Date from, Date to);

}
