package com.pigatron.trading.stats.entity;


import com.pigatron.trading.exchanges.entity.CurrencyPair;

import java.math.BigDecimal;
import java.util.Date;

public class DayStats {

    private Date date;
    private String exchange;
    private CurrencyPair currencyPair;
    private BigDecimal profit;
    private BigDecimal volume;
    private int numberTrades;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public int getNumberTrades() {
        return numberTrades;
    }

    public void setNumberTrades(int numberTrades) {
        this.numberTrades = numberTrades;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "DayStats{" +
                "date=" + date +
                ", exchange=" + exchange +
                ", currencyPair=" + currencyPair.toString() +
                ", numberTrades=" + numberTrades +
                ", profit=" + profit.toPlainString() +
                ", volume=" + volume.toPlainString() +
                '}';
    }
}
