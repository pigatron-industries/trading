package com.pigatron.trading.stats.entity;

import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.TradeType;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Date;

@Document(collection = "trades")
public class TradeHistory {

    private String id;
    private Date date;
    private String exchange;
    private CurrencyPair currencyPair;

    private TradeType type;
    private BigDecimal price;
    private BigDecimal amount;
    private BigDecimal fee;

    private BigDecimal total;


    public TradeHistory() {
    }

    private TradeHistory(Builder builder) {
        setId(builder.id);
        setDate(builder.date);
        setExchange(builder.exchange);
        setCurrencyPair(builder.currencyPair);
        setType(builder.type);
        setPrice(builder.price);
        setAmount(builder.amount);
        setFee(builder.fee);
        setTotal(builder.total);
    }

    public static Builder aTradeHistory() {
        return new Builder();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public TradeType getType() {
        return type;
    }

    public void setType(TradeType type) {
        this.type = type;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "TradeHistory{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", exchange='" + exchange + '\'' +
                ", currencyPair=" + currencyPair +
                ", type=" + type +
                ", price=" + price +
                ", amount=" + amount +
                ", fee=" + fee +
                ", total=" + total +
                '}';
    }

    public static final class Builder {
        private String id;
        private Date date;
        private String exchange;
        private CurrencyPair currencyPair;
        private TradeType type;
        private BigDecimal price;
        private BigDecimal amount;
        private BigDecimal fee;
        private BigDecimal total;

        private Builder() {
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withDate(Date val) {
            date = val;
            return this;
        }

        public Builder withExchange(String val) {
            exchange = val;
            return this;
        }

        public Builder withCurrencyPair(CurrencyPair val) {
            currencyPair = val;
            return this;
        }

        public Builder withType(TradeType val) {
            type = val;
            return this;
        }

        public Builder withPrice(BigDecimal val) {
            price = val;
            return this;
        }

        public Builder withAmount(BigDecimal val) {
            amount = val;
            return this;
        }

        public Builder withFee(BigDecimal val) {
            fee = val;
            return this;
        }

        public Builder withTotal(BigDecimal val) {
            total = val;
            return this;
        }

        public TradeHistory build() {
            return new TradeHistory(this);
        }
    }
}
