package com.pigatron.trading.stats.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Document(collection = "balances")
public class BalanceHistory {

    @Id
    private String id;
    private Date date = new Date();
    private Map<String, Map<String, BigDecimal>> balances = new HashMap<>();

    @Transient
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getPrevDate() {
        return Date.from(date.toInstant().minus(1, ChronoUnit.DAYS));
    }

    public String prevDateFormatted() {
        return dateFormat.format(getPrevDate());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Map<String, BigDecimal>> getBalances() {
        return balances;
    }

    public void setBalances(Map<String, Map<String, BigDecimal>> balances) {
        this.balances = balances;
    }

    public void putBalance(String exchange, String currency, BigDecimal amount) {
        Map<String, BigDecimal> exchangeBalance = balances.get(exchange);
        if(exchangeBalance == null) {
            exchangeBalance = new HashMap<>();
            balances.put(exchange, exchangeBalance);
        }
        exchangeBalance.put(currency, amount);
    }


    public String toString() {
        String output = dateFormat.format(getPrevDate()) + "\n";
        for (Map.Entry<String, Map<String, BigDecimal>> exchangeBalances : balances.entrySet()) {
            String exchange = exchangeBalances.getKey();
            output += "   " + exchange + "  ";
            for (Map.Entry<String, BigDecimal> currencyBalance : exchangeBalances.getValue().entrySet()) {
                String currency = currencyBalance.getKey();
                output += currency + "=" + currencyBalance.getValue().toPlainString() + "  ";
            }
            output += "\n";
        }
        return output;
    }
}
