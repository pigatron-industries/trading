package com.pigatron.trading.arbitrage;


import com.pigatron.trading.exchanges.entity.CurrencyPair;

import java.math.BigDecimal;

public class ArbitrageCheckResult {

    private CurrencyPair currencyPair;
    private String exchange1;
    private String exchange2;

    private BigDecimal startAmount;
    private BigDecimal endAmount;

    public ArbitrageCheckResult(String exchange1, String exchange2, CurrencyPair currencyPair, BigDecimal startAmount, BigDecimal endAmount) {
        this.currencyPair = currencyPair;
        this.exchange1 = exchange1;
        this.exchange2 = exchange2;
        this.startAmount = startAmount;
        this.endAmount = endAmount;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getExchange1() {
        return exchange1;
    }

    public void setExchange1(String exchange1) {
        this.exchange1 = exchange1;
    }

    public String getExchange2() {
        return exchange2;
    }

    public void setExchange2(String exchange2) {
        this.exchange2 = exchange2;
    }


    public BigDecimal getProfit() {
        return endAmount.subtract(startAmount);
    }

    public boolean isPositive() {
        return endAmount.compareTo(startAmount) == 1;
    }

    public String toString() {
        return exchange1 + " (" + currencyPair + ") --> " +
                exchange2 + "(" + currencyPair + ")  =  " +
                "Start Amount: " + startAmount.toPlainString() + " " + currencyPair.getCurrency1() + ", Profit: " + getProfit() + " " + currencyPair.getCurrency1();
    }

}
