package com.pigatron.trading.arbitrage;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.util.CurrencyUtil;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimulatedExchange {

    private ExchangeClient exchange;

    private Map<String, BigDecimal> funds = new HashMap<>();

    private Map<CurrencyPair, OrderBook> orderBooks = new HashMap<>();


    public SimulatedExchange(ExchangeClient exchange, List<CurrencyPair> currencyPairs) {
        this.exchange = exchange;
        loadOrderBooks(currencyPairs);
    }

    private void loadOrderBooks(List<CurrencyPair> currencyPairs) {
        for(CurrencyPair currencyPair : currencyPairs) {
            try {
                OrderBook orderBook = exchange.getOrderBook(currencyPair, 20);
                orderBooks.put(currencyPair, orderBook);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void reset() {
        funds = new HashMap<>();
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    private OrderBook getOrderBook(CurrencyPair currencyPair) {
        return orderBooks.get(currencyPair);
    }

    public BigDecimal getFundAmount(String currency) {
        if(funds.containsKey(currency)) {
            return funds.get(currency);
        } else {
            return BigDecimal.ZERO;
        }
    }

    private void setFundAmount(String currency, BigDecimal amount) {
        funds.put(currency, amount);
    }

    private void addFundAmount(String currency, BigDecimal amount) {
        setFundAmount(currency, getFundAmount(currency).add(amount));
    }

    private void subtractFundAmount(String currency, BigDecimal amount) {
        setFundAmount(currency, getFundAmount(currency).subtract(amount));
    }

    public void deposit(String currency, BigDecimal amount) {
        addFundAmount(currency, amount);
    }

    public BigDecimal withdrawAll(String currency) {
        BigDecimal amount = getFundAmount(currency).subtract(exchange.getWithdrawFee(currency));
        setFundAmount(currency, BigDecimal.ZERO);
        return amount;
    }

    public void buyAtCost(CurrencyPair currencyPair, BigDecimal cost) {
        OrderBook orderBook = getOrderBook(currencyPair);
        BigDecimal fee = exchange.getTakerFee().multiply(cost);
        BigDecimal amount = CurrencyUtil.getAmountForCost(orderBook.getAsks(), cost.subtract(fee));

        subtractFundAmount(currencyPair.getCurrency1(), cost);
        addFundAmount(currencyPair.getCurrency2(), amount);
    }

    public void sellAmount(CurrencyPair currencyPair, BigDecimal amount) {
        OrderBook orderBook = getOrderBook(currencyPair);
        BigDecimal cost = CurrencyUtil.getCostForAmount(orderBook.getBids(), amount);
        BigDecimal fee = exchange.getTakerFee().multiply(cost);
        cost = cost.subtract(fee);

        subtractFundAmount(currencyPair.getCurrency2(), amount);
        addFundAmount(currencyPair.getCurrency1(), cost);
    }

    public void buyAll(CurrencyPair currencyPair) {
        buyAtCost(currencyPair, getFundAmount(currencyPair.getCurrency1()));
    }

    public void sellAll(CurrencyPair currencyPair) {
        sellAmount(currencyPair, getFundAmount(currencyPair.getCurrency2()));
    }

    public void tradeAll(String currency1, String currency2) {
        CurrencyPair currencyPair = findCurrencyPair(currency1, currency2);
        if(currencyPair.getCurrency1().equals(currency1)) {
            buyAll(currencyPair);
        } else {
            sellAll(currencyPair);
        }
    }

    private CurrencyPair findCurrencyPair(String currency1, String currency2) {
        return orderBooks.keySet().stream()
                .filter(currencyPair -> (currencyPair.getCurrency1().equals(currency1) && currencyPair.getCurrency2().equals(currency2) ||
                                        (currencyPair.getCurrency1().equals(currency2) && currencyPair.getCurrency2().equals(currency1))))
                .findFirst().get();
    }

}
