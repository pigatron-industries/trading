package com.pigatron.trading.arbitrage;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.Currency;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.util.CurrencyUtil;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class SimulationService {


    /**
     * Check for arbitrage opportunities using 2 currencies across any number of exchanges
     */
    public List<ArbitrageCheckResult> arbitrageOpportunitySearch(List<ExchangeClient> exchanges, List<CurrencyPair> currencyPairs, BigDecimal amount) {
        List<ArbitrageCheckResult> results = new ArrayList<>();

        List<SimulatedExchange> simulatedExchanges = exchanges.stream()
                .map(e -> new SimulatedExchange(e, currencyPairs))
                .collect(Collectors.toList());

        for (SimulatedExchange simulatedExchange1 : simulatedExchanges) {
            for (SimulatedExchange simulatedExchange2 : simulatedExchanges) {
                if(simulatedExchange1 != simulatedExchange2) {
                    for(CurrencyPair currencyPair : currencyPairs) {
                        try {
                            ArbitrageCheckResult result = simulateArbitrage(simulatedExchange1, simulatedExchange2, currencyPair, amount);
                            //if(result.isPositive()) {
                            results.add(result);
                            //}
                        } catch(NoSuchElementException e) {
                            e.printStackTrace();
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return results;
    }


    private ArbitrageCheckResult simulateArbitrage(SimulatedExchange exchange1, SimulatedExchange exchange2,
                                   CurrencyPair currencyPair, BigDecimal amount) {
        exchange1.reset();
        exchange2.reset();
        exchange1.deposit(currencyPair.getCurrency1(), amount);
        exchange1.tradeAll(currencyPair.getCurrency1(), currencyPair.getCurrency2());
        exchange2.deposit(currencyPair.getCurrency2(), exchange1.withdrawAll(currencyPair.getCurrency2()));
        exchange2.tradeAll(currencyPair.getCurrency2(), currencyPair.getCurrency1());
        BigDecimal endAmount = exchange2.withdrawAll(currencyPair.getCurrency1());
        return new ArbitrageCheckResult(exchange1.getExchange().getName(), exchange2.getExchange().getName(),
                currencyPair, amount, endAmount);
    }


    public void simulateArbitrage(ExchangeClient exchange) {
        SimulatedExchange simulatedExchange = new SimulatedExchange(exchange,
                Arrays.asList(new CurrencyPair("GBP", "BTC"), new CurrencyPair("BTC", "ETH"), new CurrencyPair("GBP", "ETH")));
        simulateArbitrage(simulatedExchange);
    }

    private void simulateArbitrage(SimulatedExchange exchange) {
        exchange.deposit("GBP", new BigDecimal("100"));
        exchange.tradeAll("GBP", "BTC");
        exchange.tradeAll("BTC", "ETH");
        exchange.tradeAll("ETH", "GBP");
    }



}
