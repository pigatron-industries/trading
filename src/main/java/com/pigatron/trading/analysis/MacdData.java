package com.pigatron.trading.analysis;


import java.util.List;

public class MacdData {

    private List<Double> line;
    private List<Double> signal;
    private List<Double> hist;

    public MacdData(List<Double> line, List<Double> signal, List<Double> hist) {
        this.line = line;
        this.signal = signal;
        this.hist = hist;
    }

    public List<Double> getLine() {
        return line;
    }

    public List<Double> getSignal() {
        return signal;
    }

    public List<Double> getHist() {
        return hist;
    }
}
