package com.pigatron.trading.analysis;


import com.pigatron.trading.exchanges.entity.ChartData;
import com.pigatron.trading.util.ExponentialMovingAverage;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MacdAnalyser {

    public List<Double> calculateOHLC4(ChartData chartData) {
        return calculateAverage(chartData.getOpen(), chartData.getHigh(), chartData.getLow(), chartData.getClose());
    }

    public MacdData calculateMacdData(List<Double> data) {
        List<Double> macdData = new MacdAnalyser().calculateMACD(data);
        List<Double> macdSignalData = new MacdAnalyser().calculateMACDSignal(macdData);
        List<Double> macdHistogram = new MacdAnalyser().calculateMACDHist(macdData, macdSignalData);
        return new MacdData(macdData, macdSignalData, macdHistogram);
    }

    private List<Double> calculateMACD(List<Double> data) {
        List<Double> macdData = new ArrayList<>();
        List<Double> ema12 = calculateEMA(data, 12);
        List<Double> ema26 = calculateEMA(data, 26);
        for(int i = 0; i < ema26.size(); i++) {
            macdData.add(ema12.get(i) - ema26.get(i));
        }
        return macdData;
    }

    private List<Double> calculateMACDSignal(List<Double> macdData) {
        return calculateEMA(macdData, 9);
    }

    private List<Double> calculateMACDHist(List<Double> macdData, List<Double> macdSignalData) {
        List<Double> macdHist = new ArrayList<>();
        for(int i=0; i<macdSignalData.size(); i++) {
            macdHist.add(macdData.get(i) - macdSignalData.get(i));
        }
        return macdHist;
    }

    /**
     * Assumes most recent values are at beginning of list
     * @param data
     * @param n
     * @return
     */
    public List<Double> calculateEMA(List<Double> data, int n) {
        List<Double> ema = new ArrayList<>();

        //Simple moving average for first n periods
        double simp = 0;
        for(int i = data.size()-1; i > data.size()-n-1; i--) {
            simp += data.get(i);
        }
        simp = simp/n;

        //Exponential moving average for the rest
        ExponentialMovingAverage average = new ExponentialMovingAverage(n);
        average.add(simp);
        for(int i = data.size()-n-1; i >= 0; i--) {
            average.add(data.get(i));
            ema.add(0, average.getValue());
        }

        return ema;
    }

    public List<Double> calculateAverage(List<Double>... data) {
        List<Double> averages = new ArrayList<>();
        for(int i=0; i<data[0].size(); i++) {
            double total = 0;
            for(int j=0; j<data.length; j++) {
                total += data[j].get(i);
            }
            averages.add(total/data.length);
        }
        return averages;
    }


}
