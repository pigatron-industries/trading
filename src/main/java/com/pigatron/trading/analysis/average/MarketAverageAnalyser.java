package com.pigatron.trading.analysis.average;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MarketAverageAnalyser {


    private List<MarketAnalysis> marketHistories = new ArrayList<>();



    public MarketAnalysis analyse(ExchangeClient exchange, CurrencyPair currencyPair) throws Exception {
        MarketAnalysis marketAnalysis = getOrCreateMarketHistory(exchange, currencyPair);
        marketAnalysis.start();
        return marketAnalysis;
    }

    public MarketAnalysis getOrCreateMarketHistory(ExchangeClient exchange, CurrencyPair currencyPair) throws Exception {
        MarketAnalysis marketHistory = getMarketHistory(exchange, currencyPair);
        if(marketHistory != null) {
            return marketHistory;
        } else {
            MarketAnalysis marketAnalysis = new MarketAnalysis(exchange, currencyPair);
            marketHistories.add(marketAnalysis);
            return marketAnalysis;
        }
    }

    public MarketAnalysis getMarketHistory(ExchangeClient exchange, CurrencyPair currencyPair) {
        Optional<MarketAnalysis> optionalMarketHistory = marketHistories.stream()
                .filter(h -> h.getExchange().getName().equals(exchange.getName()) && h.getCurrencyPair().equals(currencyPair))
                .findFirst();
        if(optionalMarketHistory.isPresent()) {
            return optionalMarketHistory.get();
        } else {
            return null;
        }
    }


    public BigDecimal getAverageExchangeRate(ExchangeClient fromExchange, String fromCurrency, ExchangeClient toExchange, String toCurrency, String intermediateCurrency) {
        MarketAnalysis analysis1 = marketHistories.stream()
                .filter(h -> h.getExchange().getName().equals(fromExchange.getName())
                          && h.getCurrencyPair().getCurrency1().equals(fromCurrency)
                          && h.getCurrencyPair().getCurrency2().equals(intermediateCurrency))
                .findFirst().get();
        MarketAnalysis analysis2 = marketHistories.stream()
                .filter(h -> h.getExchange().getName().equals(toExchange.getName())
                          && h.getCurrencyPair().getCurrency1().equals(toCurrency)
                          && h.getCurrencyPair().getCurrency2().equals(intermediateCurrency))
                .findFirst().get();
        return new BigDecimal(analysis2.getAveragePrice() / analysis1.getAveragePrice());
    }

    public BigDecimal convert(ExchangeClient fromExchange, String fromCurrency, ExchangeClient toExchange, String toCurrency, String intermediateCurrency, BigDecimal fromAmount) {
        return getAverageExchangeRate(fromExchange, fromCurrency, toExchange, toCurrency, intermediateCurrency).multiply(fromAmount);
    }


    public BigDecimal getLowestAskPrice(ExchangeClient exchange, CurrencyPair currencyPair) {
        MarketAnalysis marketAnalysis = getMarketHistory(exchange, currencyPair);
        if(marketAnalysis.getOrderBook() != null) {
            return marketAnalysis.getOrderBook().getAskByIndex(0).getPrice();
        } else {
            return null;
        }
    }

    public BigDecimal getLowestAskPrice(ExchangeClient fromExchange, CurrencyPair currencyPair, ExchangeClient toExchange, String inCurrency) {
        BigDecimal lowestAskPrice = getLowestAskPrice(fromExchange, currencyPair);
        if(lowestAskPrice != null) {
            return convert(fromExchange, currencyPair.getCurrency1(), toExchange, inCurrency, currencyPair.getCurrency2(), lowestAskPrice);
        } else {
            return null;
        }
    }

    public BigDecimal getHighestBidPrice(ExchangeClient exchange, CurrencyPair currencyPair) {
        MarketAnalysis marketAnalysis = getMarketHistory(exchange, currencyPair);
        if(marketAnalysis.getOrderBook() != null) {
            return marketAnalysis.getOrderBook().getBidByIndex(0).getPrice();
        } else {
            return null;
        }
    }

    public BigDecimal getHighestBidPrice(ExchangeClient fromExchange, CurrencyPair fromCurrencyPair, ExchangeClient toExchange, String toCurrency) {
        BigDecimal highestBidPrice = getHighestBidPrice(fromExchange, fromCurrencyPair);
        if(highestBidPrice != null) {
            return convert(fromExchange, fromCurrencyPair.getCurrency1(), toExchange, toCurrency, fromCurrencyPair.getCurrency2(), highestBidPrice);
        } else {
            return null;
        }
    }

}
