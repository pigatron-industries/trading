package com.pigatron.trading.analysis.average;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.util.ExponentialMovingAverage;

import java.io.IOException;
import java.util.List;

public class MarketAnalysis extends AbstractAlgorithm {

    private OrderBook orderBook;

    private ExponentialMovingAverage averageMidPrice = new ExponentialMovingAverage(3600);




    public MarketAnalysis(ExchangeClient exchange, CurrencyPair currencyPair) throws IOException {
        super(exchange, currencyPair);
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public Double getAveragePrice() {
        return averageMidPrice.getValue();
    }

    public OrderBook getOrderBook() {
        return orderBook;
    }

    public void setOrderBook(OrderBook orderBook) {
        this.orderBook = orderBook;
    }

    @Override
    public void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        this.orderBook = orderBook;
        averageMidPrice.add(orderBook.getMidPrice().doubleValue());
    }


    @Override
    public void stop() {
        exchange.unsubscribe(this, currencyPair);
    }

    @Override
    public void pause() {
    }

    @Override
    public void unpause() {
    }
}
