package com.pigatron.trading.analysis.alerts;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskContext;
import com.pigatron.trading.util.RateLimiter;
import org.apache.tomcat.jni.Local;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class DumpAlert extends AbstractAlgorithm {


    private List<BigDecimal> bidPrices = new ArrayList<>();
    private int sampleSize;
    private BigDecimal maxDropPercent;

    private RateLimiter alertRateLimiter = new RateLimiter(Duration.ofMinutes(30));


    public DumpAlert(ExchangeClient exchange, CurrencyPair currencyPair, int sampleTimeSeconds, BigDecimal maxDropPercent) {
        super(exchange, currencyPair);
        sampleSize = sampleTimeSeconds*1000 / exchange.getTimeResolutionMillis();
        this.maxDropPercent = maxDropPercent;
    }

    @Override
    public void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        BigDecimal lastBidPrice = orderBook.getBidByIndex(0).getPrice();
        bidPrices.add(lastBidPrice);
        if(bidPrices.size() > sampleSize) {
            BigDecimal firstBidPrice = bidPrices.get(0);
            BigDecimal dropPercent = firstBidPrice.subtract(lastBidPrice).divide(firstBidPrice, 5, RoundingMode.UP);
            if(dropPercent.compareTo(maxDropPercent) > 0) {
                sendAlert();
            }
            bidPrices.remove(0);
        }
    }

    private void sendAlert() {
        if(alertRateLimiter.canAcquire()) {
            alertRateLimiter.acquire();
            TaskContext.getTaskRunner().getPushNotificationClient().send("Price drop alert");
        }
    }


    @Override
    public void pause() {

    }

    @Override
    public void unpause() {

    }
}
