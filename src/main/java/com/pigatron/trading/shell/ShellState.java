package com.pigatron.trading.shell;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ShellState {

    @Autowired
    private ExchangeProvider exchangeProvider;

    private ExchangeClient exchange;

    private CurrencyPair currencyPair = new CurrencyPair("GBP", "BTC");


    public void setExchange(String exchangeName) {
        exchange = exchangeProvider.getExchange(exchangeName);
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }


    @EventListener
    public void init(ContextRefreshedEvent event) {
        try {
            exchange = exchangeProvider.getExchange("gdax");
        } catch(Exception e) {

        }
    }
}
