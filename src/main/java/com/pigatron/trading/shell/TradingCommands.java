package com.pigatron.trading.shell;


import com.pigatron.trading.analysis.alerts.DumpAlert;
import com.pigatron.trading.analysis.average.MarketAverageAnalyser;
import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.ChartData;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.maintenance.impl.TradeAmountUpdater;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.AlertStatus;
import com.pigatron.trading.tasks.impl.PlaceAheadMode;
import com.pigatron.trading.tasks.impl.buylow.BuyLowSellAndForgetAlgo;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import com.pigatron.trading.tasks.impl.cycle.tasks.fixedspread.FixedSpreadBuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.fixedspread.FixedSpreadSellTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.DeepBuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.ShallowBuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.VolumeFloorBuyTask;
import com.pigatron.trading.util.CurrencyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TradingCommands {

    @Autowired
    private ShellState shellState;

    @Autowired
    private TaskRunnerService taskRunnerService;

    @Autowired
    private TradeAmountUpdater tradeAmountUpdater;

    @Autowired
    private ExchangeProvider exchangeProvider;



    @Autowired
    private MarketAverageAnalyser marketAverageAnalyser;


    public TradeCycleAlgo cycle(BigDecimal buyAmount, BigDecimal sellAmount, BigDecimal minSpreadPercent,
                                PlaceAheadMode alwaysPlaceAhead, boolean subtractFeeFromSell,
                                BigDecimal aggressionPercent, int emaPeriods) throws Exception {

        BuyTask buyTask = new ShallowBuyTask(buyAmount, minSpreadPercent, emaPeriods);
        buyTask.setAmount(buyAmount.subtract(sellAmount));
        buyTask.setAggressionPercent(aggressionPercent);
        buyTask.setPlaceAheadMode(alwaysPlaceAhead);

        SellTask sellTask = new SellTask();
        sellTask.setAmount(sellAmount);
        sellTask.setMinSpreadPercent(minSpreadPercent);
        sellTask.setAggressionPercent(aggressionPercent);
        sellTask.setPlaceAheadMode(alwaysPlaceAhead);

        TradeCycleAlgo task = new TradeCycleAlgo(shellState.getExchange(), shellState.getCurrencyPair(), buyTask, sellTask);
        task.setSubtractFeeFromSell(subtractFeeFromSell);
        task.setTaskRunnerService(taskRunnerService);
        task.start();

        return task;
    }

    public TradeCycleAlgo autospread(BigDecimal amountPercent, BigDecimal sellAmount, BigDecimal minSpreadPercent,
                                     BigDecimal totalSpreadPercent, int numOrders, int numVisible, BigDecimal stopLossPercent,
                                     PlaceAheadMode alwaysPlaceAhead, boolean subtractFeeFromSell, int emaPeriods,
                                     BigDecimal filterAmount, String followExchangeName, String followCurrency, BigDecimal maxOrderPercent) throws Exception {
        ExchangeClient exchange = shellState.getExchange();
        CurrencyPair currencyPair = shellState.getCurrencyPair();

        BigDecimal buyAmount = tradeAmountUpdater.calculateMaxTradeAmount(exchange, currencyPair, sellAmount, amountPercent);

        if(totalSpreadPercent == null) {
            totalSpreadPercent = calculateTotalSpreadPercent(exchange, currencyPair, TradeCycleAlgo.DEFAULT_MIN_TOTAL_SPREAD);
        }

        List<BuyTask> buyTasks = createBuyTasks(buyAmount, buyAmount.subtract(sellAmount), minSpreadPercent, totalSpreadPercent,
                                                numOrders, numVisible, emaPeriods, filterAmount, alwaysPlaceAhead, maxOrderPercent);

        if(stopLossPercent == null) {
            stopLossPercent = calculateDepthPercent(totalSpreadPercent, numOrders);
        }

        SellTask sellTask = new SellTask();
        sellTask.setAmount(sellAmount);
        sellTask.setMinSpreadPercent(minSpreadPercent);
        sellTask.setPlaceAheadMode(alwaysPlaceAhead);

        TradeCycleAlgo task = new TradeCycleAlgo(shellState.getExchange(), shellState.getCurrencyPair(), buyTasks, sellTask);
        task.setSubtractFeeFromSell(subtractFeeFromSell);
        task.setTaskRunnerService(taskRunnerService);
        task.setMarketAverageAnalyser(marketAverageAnalyser);
        task.setMinorStopLossPercent(stopLossPercent);
        task.setBalancePercentageToUse(amountPercent);
        if(followCurrency != null) {
            ExchangeClient followExchange = followExchangeName != null ? exchangeProvider.getExchange(followExchangeName) : exchange;
            task.follow(followExchange, new CurrencyPair(followCurrency, currencyPair.getCurrency2()));
        }
        task.start();

        return task;
    }

    public List<BuyTask> createBuyTasks(BigDecimal maxAmount, BigDecimal actualAmount, BigDecimal minSpreadPercent,
                                        BigDecimal totalSpreadPercent, int numOrders, int numVisible, int emaPeriods,
                                        BigDecimal filterAmount, PlaceAheadMode placeAheadMode, BigDecimal maxOrderPercent) {
        List<BuyTask> buyTasks = new ArrayList<>();
        BigDecimal amount = maxAmount.divide(new BigDecimal(numOrders), 8, RoundingMode.DOWN);
        amount = CurrencyUtil.round(amount, shellState.getExchange().getPriceIncrement(shellState.getCurrencyPair().getCurrency2()), RoundingMode.DOWN);

        // First
        ShallowBuyTask shallowBuyTask = new ShallowBuyTask(amount, minSpreadPercent, emaPeriods);
        shallowBuyTask.setOrderBookFilterAmount(filterAmount);
        shallowBuyTask.setPlaceAheadMode(placeAheadMode);
        shallowBuyTask.setMaxOrderPercent(maxOrderPercent);
        if(actualAmount.compareTo(amount) == -1) {
            shallowBuyTask.setAmount(actualAmount);
            actualAmount = BigDecimal.ZERO;
        } else {
            actualAmount = actualAmount.subtract(amount);
        }
        buyTasks.add(shallowBuyTask);

        // Rest
        if(numOrders > 1) {
            BigDecimal depthSpreadPercent = calculateDepthPercent(totalSpreadPercent, numOrders);
            for (int i = 1; i < numOrders; i++) {
                DeepBuyTask buyTask = new DeepBuyTask(amount, depthSpreadPercent);
                buyTask.setMaxOrderPercent(maxOrderPercent);
                buyTasks.add(buyTask);
                if (actualAmount.compareTo(amount) == -1) {
                    buyTask.setAmount(actualAmount);
                    actualAmount = BigDecimal.ZERO;
                } else {
                    actualAmount = actualAmount.subtract(amount);
                }
                buyTask.setHidden(i >= numVisible);
            }
        }

        return buyTasks;
    }

    public void autoadjust(int taskId, BigDecimal totalAmount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();

        List<BuyTask> buyTasks = tradeCycleAlgo.getBuyTasks();
        BigDecimal amount = totalAmount.divide(new BigDecimal(buyTasks.size()), 8, RoundingMode.DOWN);

        for(BuyTask buyTask : buyTasks) {
            buyTask.setMaxAmount(amount);
            buyTask.setAmount(amount);
        }
    }

    public void adjustSpread(int taskId, BigDecimal minSpreadPercent, BigDecimal totalSpreadPercent, BigDecimal minorStopLossPercent) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();

        tradeCycleAlgo.setMinorStopLossPercent(minorStopLossPercent);

        ShallowBuyTask shallowBuyTask = (ShallowBuyTask) tradeCycleAlgo.getBuyTasks().get(0);
        shallowBuyTask.setMinSpreadPercent(minSpreadPercent);

        adjustTotalSpread(tradeCycleAlgo, totalSpreadPercent);
    }

    public void adjustTotalSpread(TradeCycleAlgo tradeCycleAlgo, BigDecimal totalSpreadPercent) {
        int numOrders = tradeCycleAlgo.getBuyTasks().size();
        if(numOrders > 1) {
            BigDecimal depthSpreadPercent = calculateDepthPercent(totalSpreadPercent, numOrders);
            for (int i = 1; i < tradeCycleAlgo.getBuyTasks().size(); i++) {
                DeepBuyTask deepBuyTask = (DeepBuyTask) tradeCycleAlgo.getBuyTasks().get(i);
                deepBuyTask.setDepthPercent(depthSpreadPercent);
            }
        }
    }

    public BigDecimal autoAdjustTotalSpread(TradeCycleAlgo tradeCycle) {
        BigDecimal totalSpreadPercent = calculateTotalSpreadPercent(tradeCycle.getExchange(), tradeCycle.getCurrencyPair(), tradeCycle.getMinTotalSpread());
        adjustTotalSpread(tradeCycle, totalSpreadPercent);
        return totalSpreadPercent;
    }

    public BigDecimal calculateTotalSpreadPercent(ExchangeClient exchange, CurrencyPair currencyPair, BigDecimal minTotalSpreadPercent) {
        ChartData chartData = exchange.getChartData(currencyPair, 48, 60 * 60);
        Double low = chartData.getLow().stream().min(Double::compare).get();
        Double high = chartData.getHigh().stream().max(Double::compare).get();
        Double spread = (high-low);
        Double close = chartData.getClose().get(chartData.getClose().size()-1);
        BigDecimal spreadPercent = new BigDecimal(spread/close);
        if(spreadPercent.compareTo(minTotalSpreadPercent) == -1) {
            return minTotalSpreadPercent;
        } else {
            return spreadPercent;
        }
    }

    private BigDecimal calculateDepthPercent(BigDecimal totalSpreadPercent, int numOrders) {
        return totalSpreadPercent.divide(new BigDecimal(numOrders-1), 5, BigDecimal.ROUND_HALF_UP);
    }

    public void redistribute(int taskId) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();
        tradeCycleAlgo.redistribute();
    }

    public void reset(int taskId, BigDecimal amount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();
        tradeCycleAlgo.resetFromBottom(amount);
    }

    public void resetFromTop(int taskId, BigDecimal amount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();
        tradeCycleAlgo.resetFromTop(amount);
    }


    public void cycleBuy(BigDecimal amount, BigDecimal depthPercent) {
        List<ExchangeDataSubscriber> tasks = taskRunnerService.getTasks(shellState.getExchange().getName(), shellState.getCurrencyPair());
        Optional<ExchangeDataSubscriber> task = tasks.stream()
                .filter(t -> t instanceof TradeCycleAlgo)
                .findFirst();
        ((TradeCycleAlgo)task.get()).startSecondaryBuyTask(amount, depthPercent);
    }

    public void cycleSell(int taskId, BigDecimal amount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();
        tradeCycleAlgo.getSellTasks().get(0).setAmount(amount);
    }

    public void cycleSellAdjust(int taskId, BigDecimal amount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();

        SellTask sellTask = tradeCycleAlgo.createNewSellTask();
        sellTask.setAmount(amount);
    }

    public void cycleBuyAdjust(int taskId, BigDecimal amount) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();

        BigDecimal currentAmount = tradeCycleAlgo.getSellTasks().get(0).getAmount();
        BigDecimal newAmount = currentAmount.subtract(amount);
        tradeCycleAlgo.getSellTasks().get(0).setAmount(newAmount);

        tradeCycleAlgo.setTaskModified();
    }


    public void cycleMinSpread(BigDecimal buyAmount, BigDecimal sellAmount, BigDecimal fixedSpread) throws Exception {
        BuyTask buyTask = new FixedSpreadBuyTask(buyAmount.add(sellAmount), fixedSpread);
        buyTask.setAmount(buyAmount);
        SellTask sellTask = new FixedSpreadSellTask(fixedSpread);
        sellTask.setAmount(sellAmount);

        TradeCycleAlgo task = new TradeCycleAlgo(shellState.getExchange(), shellState.getCurrencyPair(), buyTask, sellTask);
        task.setTaskRunnerService(taskRunnerService);
        task.start();
    }

    public void cycleVolumeFloor(BigDecimal buyAmount, BigDecimal sellAmount, BigDecimal volumeFloor,
                                 PlaceAheadMode placeAheadMode, boolean subtractFeeFromSell) throws Exception {
        BuyTask buyTask = new VolumeFloorBuyTask(buyAmount.add(sellAmount), volumeFloor);
        buyTask.setAmount(buyAmount);

        SellTask sellTask = new SellTask();
        sellTask.setAmount(sellAmount);
        sellTask.setPlaceAheadMode(placeAheadMode);

        TradeCycleAlgo task = new TradeCycleAlgo(shellState.getExchange(), shellState.getCurrencyPair(), buyTask, sellTask);
        task.setSubtractFeeFromSell(subtractFeeFromSell);
        task.setTaskRunnerService(taskRunnerService);
        task.start();
    }


    public BuyLowSellAndForgetAlgo buyLow(BigDecimal buyAmount, BigDecimal sellIncrement, BigDecimal minSpreadPercent,
                                          PlaceAheadMode alwaysPlaceAhead, boolean subtractFeeFromSell,
                                          BigDecimal aggressionPercent, int emaPeriods) throws Exception {

        BuyTask buyTask = new ShallowBuyTask(buyAmount, minSpreadPercent, emaPeriods);
        buyTask.setAmount(buyAmount);
        buyTask.setAggressionPercent(aggressionPercent);
        buyTask.setPlaceAheadMode(alwaysPlaceAhead);

        BuyLowSellAndForgetAlgo task = new BuyLowSellAndForgetAlgo(shellState.getExchange(), shellState.getCurrencyPair(), buyTask, sellIncrement);
        task.setSubtractFeeFromSell(subtractFeeFromSell);
        task.setTaskRunnerService(taskRunnerService);
        task.start();

        return task;
    }

    public DumpAlert dumpAlert(int sampleTime, BigDecimal dropPercent) throws Exception {
        DumpAlert dumpAlert = new DumpAlert(shellState.getExchange(), shellState.getCurrencyPair(), sampleTime, dropPercent);
        dumpAlert.start();
        return dumpAlert;
    }

    public void alertStatus(int taskId, String status) {
        Optional<TaskWrapper> task = taskRunnerService.getTask(taskId);
        TradeCycleAlgo tradeCycleAlgo = (TradeCycleAlgo)task.get().getExchangeDataSubscriber();
        tradeCycleAlgo.setAlertStatus(AlertStatus.valueOf(status));
    }
}
