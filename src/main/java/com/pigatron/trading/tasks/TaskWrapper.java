package com.pigatron.trading.tasks;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.entity.CurrencyPair;

public class TaskWrapper {

    private ExchangeClient exchange;

    private CurrencyPair currencyPair;

    private ExchangeDataSubscriber exchangeDataSubscriber;


    public TaskWrapper(ExchangeClient exchange, CurrencyPair currencyPair, ExchangeDataSubscriber exchangeDataSubscriber) {
        this.exchange = exchange;
        this.currencyPair = currencyPair;
        this.exchangeDataSubscriber = exchangeDataSubscriber;
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public void setExchange(ExchangeClient exchange) {
        this.exchange = exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public ExchangeDataSubscriber getExchangeDataSubscriber() {
        return exchangeDataSubscriber;
    }

    public void setExchangeDataSubscriber(ExchangeDataSubscriber exchangeDataSubscriber) {
        this.exchangeDataSubscriber = exchangeDataSubscriber;
    }
}
