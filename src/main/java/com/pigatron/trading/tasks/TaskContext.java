package com.pigatron.trading.tasks;

import com.pigatron.trading.stats.repository.TradeRepository;
import com.pigatron.trading.shell.ShellState;
import com.pigatron.trading.stats.StatsService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class TaskContext implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        TaskContext.applicationContext = applicationContext;
    }

    public static TaskRunnerService getTaskRunner() {
        return applicationContext.getBean(TaskRunnerService.class);
    }

    public static ShellState getShellState() {
        return applicationContext.getBean(ShellState.class);
    }

    public static TradeRepository getTradeRepository() {
        return applicationContext.getBean(TradeRepository.class);
    }

    public static StatsService getStatsService() {
        return applicationContext.getBean(StatsService.class);
    }
}
