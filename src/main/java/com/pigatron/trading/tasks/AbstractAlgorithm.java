package com.pigatron.trading.tasks;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.tasks.impl.AlertStatus;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAlgorithm implements ExchangeDataSubscriber {

    private int processId;

    protected ExchangeClient exchange;

    protected CurrencyPair currencyPair;

    @Transient
    private List<BuyTask> buyTasks = new ArrayList<>();

    @Transient
    private List<SellTask> sellTasks = new ArrayList<>();

    private BigDecimal balancePercentageToUse = BigDecimal.ONE;

    protected AlertStatus alertStatus = AlertStatus.NORMAL;


    public AbstractAlgorithm(ExchangeClient exchange, CurrencyPair currencyPair) {
        this.exchange = exchange;
        this.currencyPair = currencyPair;
        processId = TaskContext.getTaskRunner().getNextProcessId();
    }

    @Override
    public int getProcessId() {
        return processId;
    }

    public List<BuyTask> getBuyTasks() {
        return buyTasks;
    }

    public List<SellTask> getSellTasks() {
        return sellTasks;
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    @Override
    public void start() throws Exception {
        exchange.subscribe(this, currencyPair);
    }

    @Override
    public void stop() {
        exchange.unsubscribe(this, currencyPair);
    }

    public BigDecimal getBalancePercentageToUse() {
        return balancePercentageToUse;
    }

    public void setBalancePercentageToUse(BigDecimal balancePercentageToUse) {
        this.balancePercentageToUse = balancePercentageToUse;
    }

    public AlertStatus getAlertStatus() {
        return alertStatus;
    }

    public void setAlertStatus(AlertStatus alertStatus) {
        this.alertStatus = alertStatus;
    }
}
