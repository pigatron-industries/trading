package com.pigatron.trading.tasks;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.notifications.PushNotificationClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
@Lazy
public class TaskRunnerService {

    @Autowired
    private ExchangeProvider exchangeProvider;

    @Autowired
    private PushNotificationClient pushNotificationClient;

    private int lastProcessId = 0;


    public List<TaskWrapper> getTasks() {
        List<TaskWrapper> taskWrappers = new ArrayList<>();

        List<ExchangeClient> exchanges = exchangeProvider.getExchanges();
        for(ExchangeClient exchange : exchanges) {
            Map<CurrencyPair, List<ExchangeDataSubscriber>> subscribers = exchange.getSubscribers();
            for (Map.Entry<CurrencyPair, List<ExchangeDataSubscriber>> currencyPairListEntry : subscribers.entrySet()) {
                CurrencyPair currencyPair = currencyPairListEntry.getKey();
                for (ExchangeDataSubscriber subscriber : currencyPairListEntry.getValue()) {
                    taskWrappers.add(new TaskWrapper(exchange, currencyPair, subscriber));
                }
            }
        }

        return taskWrappers;
    }

    public List<TaskWrapper> getTasks(ExchangeClient exchange) {
        List<TaskWrapper> taskWrappers = new ArrayList<>();
        Map<CurrencyPair, List<ExchangeDataSubscriber>> subscribers = exchange.getSubscribers();
        for (Map.Entry<CurrencyPair, List<ExchangeDataSubscriber>> currencyPairListEntry : subscribers.entrySet()) {
            CurrencyPair currencyPair = currencyPairListEntry.getKey();
            for (ExchangeDataSubscriber subscriber : currencyPairListEntry.getValue()) {
                taskWrappers.add(new TaskWrapper(exchange, currencyPair, subscriber));
            }
        }
        return taskWrappers;
    }

    public Optional<TaskWrapper> getTask(int id) {
        return getTasks().stream()
                .filter(t -> t.getExchangeDataSubscriber().getProcessId() == id)
                .findFirst();
    }

    public List<ExchangeDataSubscriber> getTasks(String exchange, CurrencyPair currencyPair) {
        return exchangeProvider.getExchange(exchange).getSubscribers(currencyPair);
    }

    public void stopTask(int processId) throws ExchangeClientException {
        List<TaskWrapper> tasks = getTasks();
        for (TaskWrapper task : tasks) {
            ExchangeDataSubscriber subscriber = task.getExchangeDataSubscriber();
            if(subscriber.getProcessId() == processId) {
                subscriber.stop();
            }
        }
    }

    public void pauseTask(int processId) throws ExchangeClientException {
        List<TaskWrapper> tasks = getTasks();
        for (TaskWrapper task : tasks) {
            ExchangeDataSubscriber subscriber = task.getExchangeDataSubscriber();
            if(subscriber.getProcessId() == processId) {
                subscriber.pause();
            }
        }
    }

    public void unpauseTask(int processId) throws ExchangeClientException {
        List<TaskWrapper> tasks = getTasks();
        for (TaskWrapper task : tasks) {
            ExchangeDataSubscriber subscriber = task.getExchangeDataSubscriber();
            if(subscriber.getProcessId() == processId) {
                subscriber.unpause();
            }
        }
    }

    public void stopAllTasks() {
        for(TaskWrapper task : getTasks()) {
            task.getExchangeDataSubscriber().stop();
        }
    }

    public void pauseAllTasks() {
        for(TaskWrapper task : getTasks()) {
            task.getExchangeDataSubscriber().pause();
        }
    }

    public void pauseAllTasks(String currency) {
        for(TaskWrapper task : getTasks()) {
            if(task.getCurrencyPair().getCurrency2().equals(currency)) {
                task.getExchangeDataSubscriber().pause();
            }
        }
    }

    public void unpauseAllTasks() {
        for(TaskWrapper task : getTasks()) {
            task.getExchangeDataSubscriber().unpause();
        }
    }

    int getNextProcessId() {
        return lastProcessId++;
    }

    public PushNotificationClient getPushNotificationClient() {
        return pushNotificationClient;
    }

}
