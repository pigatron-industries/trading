package com.pigatron.trading.tasks.impl.buylow;


import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;

import java.math.BigDecimal;

public class SellAndForgetTask extends SellTask {

    public SellAndForgetTask(BigDecimal price, BigDecimal amount) {
        this.bestPrice = price;
        this.setAmount(amount);
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        BigDecimal lowestAsk = orderBook.getAskByIndex(0).getPrice();
        if(bestPrice.compareTo(orderBook.getAskByIndex(0).getPrice()) == -1) {
            bestPrice = lowestAsk;
        }
        return bestPrice;
    }
}
