package com.pigatron.trading.tasks.impl;


public enum PlaceAheadMode {
    ALWAYS, NEVER, VOLUME
}
