package com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;

import java.math.BigDecimal;
import java.util.Optional;

public class VolumeFloorBuyTask extends BuyTask {

    protected BigDecimal volume;

    public VolumeFloorBuyTask(BigDecimal maxAmount, BigDecimal volume) {
        super(maxAmount);
        this.volume = volume;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        MarketOrder lowestAsk = orderBook.getAskByIndex(0);
        Optional<MarketOrder> orderAboveVolume = orderBook.getBids().stream()
                .filter(o -> o.getAmount().compareTo(volume) == 1)
                .findFirst();

        if(orderAboveVolume.isPresent()) {
            BigDecimal spread = lowestAsk.getPrice().subtract(orderAboveVolume.get().getPrice());
            if(shouldPlaceAhead(orderAboveVolume.get(), spread)) {
                bestPrice = orderAboveVolume.get().getPrice().add(priceIncrement);
                return bestPrice;
            } else {
                bestPrice = orderAboveVolume.get().getPrice();
                return bestPrice;
            }
        } else {
            bestPrice = null;
            return null;
        }
    }
}
