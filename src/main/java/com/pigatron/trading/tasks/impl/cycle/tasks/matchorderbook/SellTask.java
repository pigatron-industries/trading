package com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.AlertStatus;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * Standard Sell
 *
 *
 */
public class SellTask extends TradeTask {

    private static final Logger logger = LoggerFactory.getLogger(SellTask.class);

    protected BigDecimal minSpreadPercent = new BigDecimal("0");

    private BigDecimal boughtPrice = BigDecimal.ZERO;

    private BigDecimal compensatedBoughtPrice = BigDecimal.ZERO;

    private boolean emergencyStop;

    private BigDecimal stopLossSellVolume = new BigDecimal("10");

    private BigDecimal stopLossPercent = new BigDecimal("0.01");


    public BigDecimal getMinSpreadPercent() {
        return minSpreadPercent;
    }

    public void setMinSpreadPercent(BigDecimal minSpreadPercent) {
        this.minSpreadPercent = minSpreadPercent;
    }

    public BigDecimal getBoughtPrice() {
        return boughtPrice;
    }

    public void setBoughtPrice(BigDecimal boughtPrice) {
        this.boughtPrice = boughtPrice;
        this.compensatedBoughtPrice = boughtPrice;
    }

    public BigDecimal getCompensatedBoughtPrice() {
        return compensatedBoughtPrice;
    }

    public void setCompensatedBoughtPrice(BigDecimal compensatedBoughtPrice) {
        this.compensatedBoughtPrice = compensatedBoughtPrice;
    }

    public void setEmergencyStop(boolean emergencyStop) {
        this.emergencyStop = emergencyStop;
    }

    public boolean hasBoughtPrice() {
        return boughtPrice.compareTo(BigDecimal.ZERO) == 1;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        MarketOrder lowestAsk = orderBook.getAskByIndex(0);
        if(orderMatches(lowestAsk)) {
            lowestAsk = orderBook.getAskByIndex(1);
        }

        MarketOrder highestBid = orderBook.getBidByIndex(0);

        BigDecimal minSpreadWithFee = highestBid.getPrice().multiply(makerFee.multiply(new BigDecimal(2)));
        BigDecimal spread = lowestAsk.getPrice().subtract(highestBid.getPrice());

        BigDecimal orderDistance = priceIncrement;
        if(hasBoughtPrice()) {
            BigDecimal spreadFromBoughtPrice = lowestAsk.getPrice().subtract(getCompensatedOrBoughtPrice(orderBook));
            BigDecimal minSpread = highestBid.getPrice().multiply(minSpreadPercent);
            orderDistance = getOrderDistance(spreadFromBoughtPrice, minSpread);
        }

        // get min sell price for profit
        BigDecimal minSellPrice = getMinSellPrice(orderBook);

        // match highest order that is lower than max price
        synchronized(orderBook.getAsks()) {
            for (MarketOrder order : orderBook.getAsks()) {
                if (getOrder() == null || !orderMatches(order, (List<TradeTask>) (List<?>) parentAlgorithm.getSellTasks())) {
                    if (!hasBoughtPrice() || minSellPrice.compareTo(order.getPrice()) == -1) {
                        if (shouldPlaceAhead(order, spread)) {
                            bestPrice = order.getPrice().subtract(orderDistance);
                            return bestPrice;
                        } else {
                            bestPrice = order.getPrice();
                            return bestPrice;
                        }
                    }
                }
            }
        }

        bestPrice = getCompensatedOrBoughtPrice(orderBook);
        return bestPrice;
    }

    private BigDecimal getMinSellPrice(OrderBook orderBook) {
        BigDecimal minSellPrice = getCompensatedOrBoughtPrice(orderBook);
        BigDecimal weightedFollowPrice = getWeightedFollowPrice(orderBook);
        if(weightedFollowPrice != null && weightedFollowPrice.compareTo(minSellPrice) > 0) {
            minSellPrice = weightedFollowPrice;
        }
        return minSellPrice;
    }

    BigDecimal minWeight = new BigDecimal("0.3");

    private BigDecimal getWeightedFollowPrice(OrderBook orderBook) {
        BigDecimal followAskPrice = ((TradeCycleAlgo)parentAlgorithm).getFollowAskPrice();
        BigDecimal thisAskPrice = orderBook.getAskByIndex(0).getPrice();
        BigDecimal weightedFollowPrice = null;
        if(followAskPrice != null && followAskPrice.compareTo(thisAskPrice) > 0) {
            BigDecimal diff = followAskPrice.subtract(thisAskPrice);
            BigDecimal weight = calculateFollowWeight(diff, thisAskPrice);

            // BEAR mode. Don't chase follow market highs unless weight > 0.3
            if((parentAlgorithm.getAlertStatus().equals(AlertStatus.BEAR) ||
                    parentAlgorithm.getAlertStatus().equals(AlertStatus.VOLUME)) &&
                    weight.compareTo(minWeight) < 0) {
                return null;
            }

            weightedFollowPrice = thisAskPrice.add(diff.multiply(weight));
        }
        return weightedFollowPrice;
    }

    private BigDecimal getCompensatedOrBoughtPrice(OrderBook orderBook) {
        BigDecimal lowestAsk = orderBook.getAskByIndex(0).getPrice();
        if(compensatedBoughtPrice.compareTo(lowestAsk) == -1) {
            return compensatedBoughtPrice;
        } else {
            return boughtPrice;
        }
    }

    private boolean majorStopLoss(OrderBook orderBook) {
        if(getAmount().compareTo(new BigDecimal("0")) == 1 && hasBoughtPrice() &&
                orderBook.getAskByIndex(0).getPrice().compareTo(boughtPrice) == -1) {

            BigDecimal askVolume = getAskVolume(orderBook, boughtPrice);
            BigDecimal lowestAsk = orderBook.getAskByIndex(0).getPrice();
            BigDecimal priceDifference = boughtPrice.subtract(lowestAsk);
            BigDecimal maxPriceDifference = orderBook.getMidPrice().multiply(stopLossPercent);

            if(askVolume.compareTo(stopLossSellVolume) == 1 && priceDifference.compareTo(maxPriceDifference) == 1) {
                return true;
            }

        }
        return false;
    }

    private BigDecimal getAskVolume(OrderBook orderBook, BigDecimal upperPrice) {
        return orderBook.getAsks().stream()
                .filter(ask -> ask.getPrice().compareTo(upperPrice) == -1)
                .map(MarketOrder::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private long getAskOrderCount(OrderBook orderBook, BigDecimal upperPrice) {
        return orderBook.getAsks().stream()
                .filter(ask -> ask.getPrice().compareTo(upperPrice) == -1)
                .count();
    }

    private boolean isLowestAskAndLessThanBoughtPrice(BigDecimal lowestAsk, BigDecimal price) {
        return !emergencyStop && hasBoughtPrice() &&
                price.compareTo(lowestAsk) == 0 &&
                price.compareTo(boughtPrice) == -1;
    }

    @Override
    protected boolean shouldPlaceAhead(MarketOrder order, BigDecimal spread) {
        //don't place ahead if sell price <= bought price
        if(!emergencyStop && hasBoughtPrice() && order.getPrice().compareTo(getCompensatedBoughtPrice()) <= 0) {
            return false;
        }
        return super.shouldPlaceAhead(order, spread);
    }

    public BigDecimal calculateProfitAtPrice(BigDecimal sellPrice) {
        return sellPrice.subtract(getBoughtPrice()).multiply(getAmount());
    }

}
