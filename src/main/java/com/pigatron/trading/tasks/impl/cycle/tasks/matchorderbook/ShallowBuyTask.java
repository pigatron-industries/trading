package com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.AlertStatus;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleAlgo;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.util.ExponentialMovingAverage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

import static com.pigatron.trading.exchanges.entity.OrderBook.anOrderBook;

/**
 * Shallow Buy
 *
 * Places an order right at the top of the order book if the spread is higher than the specified amount
 * minSpreadPercent: The minimum amount of spread to place order on top of book.
 *                   If the spread is less than this then it will place the order further down in the order book
 */
public class ShallowBuyTask extends BuyTask {

    private static final Logger logger = LoggerFactory.getLogger(ShallowBuyTask.class);

    protected BigDecimal minSpreadPercent;

    private ExponentialMovingAverage exponentialMovingAverage;
    private ExponentialMovingAverage exponentialMovingAverageMax;

    private BigDecimal orderBookFilterAmount;

    private double pushDownFactor = 0;
    private double pushUpFactor = 1;


    public ShallowBuyTask(BigDecimal maxAmount, BigDecimal minSpreadPercent, int emaPeriods) {
        super(maxAmount);
        this.exponentialMovingAverage = new ExponentialMovingAverage(emaPeriods);
        this.exponentialMovingAverageMax = new ExponentialMovingAverage(emaPeriods);
        this.minSpreadPercent = minSpreadPercent;
    }

    public BigDecimal getMinSpreadPercent() {
        return minSpreadPercent;
    }

    public void setMinSpreadPercent(BigDecimal minSpreadPercent) {
        this.minSpreadPercent = minSpreadPercent;
    }

    public void setOrderBookFilterAmount(BigDecimal orderBookFilterAmount) {
        this.orderBookFilterAmount = orderBookFilterAmount;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        orderBook = filterOrderBook(orderBook);

        MarketOrder highestBid = orderBook.getBidByIndex(0);
        if(orderMatches(highestBid)) {
            highestBid = orderBook.getBidByIndex(1);
        }

        // prevent price from going too high
        MarketOrder lowestAsk = orderBook.getAskByIndex(0);

        BigDecimal midPrice = highestBid.getPrice().add(lowestAsk.getPrice()).divide(new BigDecimal("2"), 5, RoundingMode.HALF_UP);
        BigDecimal makerFeeAbs = midPrice.multiply(makerFee);
        BigDecimal minSpreadAbs = midPrice.multiply(minSpreadPercent.divide(new BigDecimal(2), 5, BigDecimal.ROUND_HALF_UP));
        BigDecimal maxBuyPrice = midPrice.subtract(makerFeeAbs).subtract(minSpreadAbs);
        maxBuyPrice = amountBelowAverage(maxBuyPrice);

        // follow main currency price
        maxBuyPrice = adjustForFollowMarket(maxBuyPrice, orderBook);

        BigDecimal spread = lowestAsk.getPrice().subtract(highestBid.getPrice());
        BigDecimal orderDistance = getOrderDistance(maxBuyPrice, highestBid.getPrice());

        BigDecimal newBestPrice = null;

        // match highest order that is lower than max price
        synchronized(orderBook.getBids()) {
            for (MarketOrder order : orderBook.getBids()) {
                if (maxBuyPrice.compareTo(order.getPrice()) == 1) {
                    if (this.getOrder() == null || !orderMatches(order, (List<TradeTask>) (List<?>) parentAlgorithm.getBuyTasks())) {
                        if (shouldPlaceAhead(order, spread)) {
                            newBestPrice = order.getPrice().add(orderDistance);
                        } else {
                            newBestPrice = order.getPrice();
                        }
                        break;
                    }
                }
            }
        }

        if(newBestPrice == null) {
            newBestPrice = maxBuyPrice;
        }

        bestPrice = newBestPrice;
        return bestPrice;
    }

    BigDecimal minWeight = new BigDecimal("0.3");

    private BigDecimal getWeightedFollowPrice(OrderBook orderBook) {
        BigDecimal followBidPrice = ((TradeCycleAlgo)parentAlgorithm).getFollowBidPrice();
        BigDecimal thisBidPrice = orderBook.getBidByIndex(0).getPrice();
        BigDecimal weightedFollowPrice = null;
        if(followBidPrice != null && followBidPrice.compareTo(thisBidPrice) < 0) {
            BigDecimal diff = thisBidPrice.subtract(followBidPrice);
            BigDecimal weight = calculateFollowWeight(diff, thisBidPrice);

            // BEAR mode. Don't chase follow market highs unless weight > 0.3
            if((parentAlgorithm.getAlertStatus().equals(AlertStatus.BULL) ||
                    parentAlgorithm.getAlertStatus().equals(AlertStatus.VOLUME)) &&
                    weight.compareTo(minWeight) < 0) {
                return null;
            }

            weightedFollowPrice = thisBidPrice.subtract(diff.multiply(weight));
        }
        return weightedFollowPrice;
    }

    private BigDecimal adjustForFollowMarket(BigDecimal maxBuyPrice, OrderBook orderBook) {
        BigDecimal weightedFollowPrice = getWeightedFollowPrice(orderBook);
        if(weightedFollowPrice != null && weightedFollowPrice.compareTo(maxBuyPrice) < 0) {
            maxBuyPrice = weightedFollowPrice;
        }
        return maxBuyPrice;
    }

    public void pushDown() {
        exponentialMovingAverage.setValue(exponentialMovingAverage.getValue()/2);
    }

    private BigDecimal amountBelowAverage(BigDecimal price) {
        exponentialMovingAverage.add(price.doubleValue());
        exponentialMovingAverageMax.add(price.doubleValue());
        if(price.doubleValue() > exponentialMovingAverageMax.getValue()) {
            exponentialMovingAverageMax.setValue(price.doubleValue());
        }

        double emaPushUpDiff = (price.doubleValue() - exponentialMovingAverage.getValue() ) * pushUpFactor;
        if(emaPushUpDiff < 0) {
            emaPushUpDiff = 0;
        }
        double emaPushDownDiff = (exponentialMovingAverageMax.getValue() - price.doubleValue()) * pushDownFactor;
        if(emaPushDownDiff < 0) {
            emaPushDownDiff = 0;
        }
        price = price.subtract(new BigDecimal(Math.max(emaPushDownDiff, emaPushUpDiff)));

        return price;
    }

    private OrderBook filterOrderBook(OrderBook orderBook) {
        if(orderBookFilterAmount == null || orderBookFilterAmount.compareTo(BigDecimal.ZERO) == 0) {
            return orderBook;
        }

        return anOrderBook()
                .withAsks(orderBook.getAsks())
                .withBids(orderBook.getBids().stream()
                        .filter(o -> o.getAmount().compareTo(orderBookFilterAmount) > 0)
                        .collect(Collectors.toSet()))
                .build();
    }

    public void setPushDownFactor(double pushDownFactor) {
        this.pushDownFactor = pushDownFactor;
    }

    public void setPushUpFactor(double pushUpFactor) {
        this.pushUpFactor = pushUpFactor;
    }

}
