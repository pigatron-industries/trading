package com.pigatron.trading.tasks.impl.cycle.tasks.fixedspread;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.util.CurrencyUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class FixedSpreadBuyTask extends BuyTask {

    protected BigDecimal fixedSpread;

    public FixedSpreadBuyTask(BigDecimal maxAmount, BigDecimal fixedSpread) {
        super(maxAmount);
        this.fixedSpread = fixedSpread;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        MarketOrder highestBid = orderBook.getBidByIndex(0);
        if(orderMatches(highestBid)) {
            highestBid = orderBook.getBidByIndex(1);
        }

        MarketOrder lowestAsk = orderBook.getAskByIndex(0);
        if(parentAlgorithm.getSellTasks().get(0).orderMatches(lowestAsk)) {
            lowestAsk = orderBook.getAskByIndex(1);
        }

//        bestPrice = highestBid.getPrice().add(minPriceChange);
//        if(bestPrice.compareTo(lowestAsk.getPrice()) == 0) {
//            bestPrice = highestBid.getPrice();
//        }
        BigDecimal midPrice = lowestAsk.getPrice().add(highestBid.getPrice()).divide(new BigDecimal("2"), RoundingMode.HALF_UP);

        bestPrice = CurrencyUtil.round(midPrice, priceIncrement, RoundingMode.DOWN);

        // if amount is under minimum order amount then increase it
        //setAmount(new BigDecimal("0.0001"));
//        if(getAmount().compareTo(parentAlgorithm.getMinOrderAmount()) == -1) {
//            setAmount(parentAlgorithm.getMinOrderAmount());
//        }

        return bestPrice;
    }

}
