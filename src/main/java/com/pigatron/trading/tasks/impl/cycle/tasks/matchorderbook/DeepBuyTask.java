package com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook;


import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;

import java.math.BigDecimal;
import java.util.List;

/**
 * Deep Buy
 *
 * Places an order at a specified amount underneath the Shallow Buy task.
 * depthPercent: The depth underneath the primary buy task to place the order.
 */
public class DeepBuyTask extends BuyTask {

    private BigDecimal depthPercent;
    private BigDecimal minPriceMovementPercent = new BigDecimal("0.0001");

    public DeepBuyTask(BigDecimal maxAmount, BigDecimal depthPercent) {
        super(maxAmount);
        this.depthPercent = depthPercent;
    }

    public BigDecimal getDepthPercent() {
        return depthPercent;
    }

    public void setDepthPercent(BigDecimal depthPercent) {
        this.depthPercent = depthPercent;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        setMinPriceMovement(orderBook.getMidPrice().multiply(minPriceMovementPercent));

        BuyTask previousTask = getPreviousTask();
        BigDecimal depth = orderBook.getMidPrice().multiply(depthPercent);
        BigDecimal maxBuyPrice = previousTask.getBestPrice().subtract(depth);

        // match highest order that is lower than max price
        synchronized(orderBook.getBids()) {
            for (MarketOrder order : orderBook.getBids()) {
                if (maxBuyPrice.compareTo(order.getPrice()) == 1) {
                    if (this.getOrder() == null || !orderMatches(order, (List<TradeTask>) (List<?>) parentAlgorithm.getBuyTasks())) {
                        bestPrice = order.getPrice().add(priceIncrement);
                        return bestPrice;
                    }
                }
            }
        }

        bestPrice = maxBuyPrice;
        return bestPrice;
    }

    private boolean previousTaskIsLowerPrice() {
        BuyTask previousTask = getPreviousTask();
        return previousTask != null && previousTask.getBestPrice().compareTo(bestPrice) <= 0;
    }

    private BuyTask getPreviousTask() {
        List<BuyTask> buyTasks = parentAlgorithm.getBuyTasks();
        int thisIndex = buyTasks.indexOf(this);
        if(thisIndex > 0) {
            return buyTasks.get(thisIndex - 1);
        } else {
            return null;
        }
    }

}
