package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.analysis.average.MarketAnalysis;
import com.pigatron.trading.analysis.average.MarketAverageAnalyser;
import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskContext;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.DeepBuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.ShallowBuyTask;
import com.pigatron.trading.util.CurrencyUtil;
import com.pigatron.trading.util.ExponentialMovingAverage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;


public class TradeCycleAlgo extends AbstractAlgorithm {

    private static final Logger logger = LoggerFactory.getLogger(TradeCycleAlgo.class);

    /** True if fee is subtracted from currency 2 */
    private boolean subtractFeeFromSell = true;

    /** The maxmimum percent of loss in consecutive trades before emergency stop */
    private BigDecimal maxConsecutiveLossPercent = new BigDecimal("0.01");

    private BigDecimal minorStopLossPercent = new BigDecimal("0.001");

    private BigDecimal sellTaskSplitPercent = new BigDecimal("0.002");

    public static BigDecimal DEFAULT_MIN_TOTAL_SPREAD = new BigDecimal("0.015");
    private BigDecimal minTotalSpread = DEFAULT_MIN_TOTAL_SPREAD;


    @Transient
    private TaskRunnerService taskRunnerService;

    @Transient
    private MarketAverageAnalyser marketAverageAnalyser;

    @Transient
    private TradeCycleStatus status;

    @Transient
    private BigDecimal primaryBuyPrice;

    @Transient
    private boolean taskModified;

    @Transient
    private BigDecimal consecutiveLoss = new BigDecimal("0");

    @Transient
    private boolean firstRun = true;

    @Transient
    private ExponentialMovingAverage averagePrice = new ExponentialMovingAverage(900);

    @Transient
    private BigDecimal volatility;

    private MarketAnalysis marketAnalysis;

    public TradeCycleAlgo(ExchangeClient exchange, CurrencyPair currencyPair, BuyTask buyTask, SellTask sellTask) {
        super(exchange, currencyPair);
        this.getBuyTasks().add(buyTask);
        this.getSellTasks().add(sellTask);
        status = TradeCycleStatus.RUNNING;
        setCommonTaskProperties(buyTask);
        setCommonTaskProperties(sellTask);
    }

    public TradeCycleAlgo(ExchangeClient exchange, CurrencyPair currencyPair, List<BuyTask> buyTasks, SellTask sellTask) {
        super(exchange, currencyPair);
        this.getBuyTasks().addAll(buyTasks);
        this.getSellTasks().add(sellTask);
        status = TradeCycleStatus.RUNNING;
        buyTasks.forEach(this::setCommonTaskProperties);
        setCommonTaskProperties(sellTask);
    }

    public void follow(ExchangeClient exchange, CurrencyPair currencyPair) throws Exception {
        marketAverageAnalyser.analyse(this.exchange, this.currencyPair);
        marketAnalysis = marketAverageAnalyser.analyse(exchange, currencyPair);
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public BigDecimal getMinTotalSpread() {
        return minTotalSpread;
    }

    public void setMinTotalSpread(BigDecimal minTotalSpread) {
        this.minTotalSpread = minTotalSpread;
    }

    public void setTaskRunnerService(TaskRunnerService taskRunnerService) {
        this.taskRunnerService = taskRunnerService;
    }

    public void setMarketAverageAnalyser(MarketAverageAnalyser marketAverageAnalyser) {
        this.marketAverageAnalyser = marketAverageAnalyser;
    }

    public BigDecimal getFollowAskPrice() {
        if(marketAnalysis != null) {
            return marketAverageAnalyser.getLowestAskPrice(marketAnalysis.getExchange(), marketAnalysis.getCurrencyPair(),
                                                           exchange, currencyPair.getCurrency1());
        } else {
            return null;
        }
    }

    public BigDecimal getFollowBidPrice() {
        if(marketAnalysis != null) {
            return marketAverageAnalyser.getHighestBidPrice(marketAnalysis.getExchange(), marketAnalysis.getCurrencyPair(),
                                                            exchange, currencyPair.getCurrency1());
        } else {
            return null;
        }
    }

    public void setMinorStopLossPercent(BigDecimal minorStopLossPercent) {
        this.minorStopLossPercent = minorStopLossPercent;
    }

    public void setSellTaskSplitPercent(BigDecimal sellTaskSplitPercent) {
        this.sellTaskSplitPercent = sellTaskSplitPercent;
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public BigDecimal getMinOrderAmount() {
        return exchange.getMinOrderAmount(currencyPair.getCurrency2());
    }

    public SellTask getSellTask() {
        return getSellTasks().get(0);
    }



    public SellTask createNewSellTask() {
        SellTask floatingSellTask = new SellTask();
        floatingSellTask.setMinSpreadPercent(getSellTask().getMinSpreadPercent());
        floatingSellTask.setAmount(BigDecimal.ZERO);
        setCommonTaskProperties(floatingSellTask);
        getSellTasks().add(0, floatingSellTask);
        return floatingSellTask;
    }

    @Deprecated
    public void startSecondaryBuyTask(BigDecimal maxAmount, BigDecimal depthPercent) {
        BuyTask buyTask = new DeepBuyTask(maxAmount, depthPercent);
        buyTask.setAmount(maxAmount);
        setCommonTaskProperties(buyTask);
        getBuyTasks().add(buyTask);
    }

    private void setCommonTaskProperties(TradeTask task) {
        task.setParentAlgorithm(this);
        task.setMakerFee(exchange.getMakerFee());
        task.setPriceIncrement(exchange.getPriceIncrement(currencyPair.getCurrency1()));
    }



    @Override
    public void stop() {
        status = TradeCycleStatus.STOPPING;
        processOrders();
        exchange.unsubscribe(this, currencyPair);
    }

    @Override
    public void pause() {
        status = TradeCycleStatus.PAUSING;
        getSellTask().setEmergencyStop(true);
    }

    @Override
    public void unpause() {
        status = TradeCycleStatus.RUNNING;
        getSellTask().setEmergencyStop(false);
    }

    public void setTaskModified() {
        taskModified = true;
    }

    public void setSubtractFeeFromSell(boolean subtractFeeFromSell) {
        this.subtractFeeFromSell = subtractFeeFromSell;
    }


    @Override
    public synchronized void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        saveDetails(orderBook);

        if(!firstRun) { // ignore new trades on first run
            processTrades(newTrades);
            processRisk(orderBook);
        }

        for(BuyTask buyTask : getBuyTasks()) {
            processBuyTask(buyTask, orderBook);
        }
        for(SellTask sellTask : getSellTasks()) {
            processSellTask(sellTask, orderBook);
        }
        //TODO merge sell tasks if they are the same price

        processOrders();

        taskModified = false;
        firstRun = false;
    }

    private MarketOrder lowestAsk;
    private MarketOrder highestBid;

    private void saveDetails(OrderBook orderBook) {
        lowestAsk = orderBook.getAskByIndex(0);
        highestBid = orderBook.getBidByIndex(0);
    }


    /** Process Trades **/

    private void processTrades(List<Trade> newTrades) {

        if(newTrades.size() > 0) {
            logger.info(newTrades.toString());

            if(hasTrades(newTrades, TradeType.buy)) {
                // Calculate how much was bought and which side to subtract fee from
                BigDecimal boughtAmount = getAmount(newTrades, TradeType.buy);
                if(subtractFeeFromSell) {
                    BigDecimal fee = boughtAmount.multiply(exchange.getMakerFee());
                    fee = CurrencyUtil.round(fee, exchange.getPriceIncrement(currencyPair.getCurrency2()), RoundingMode.UP);
                    boughtAmount = boughtAmount.subtract(fee);
                }

                // split sell task up to sell separately at different prices
                BigDecimal newTradesBoughtPrice = getAveragePrice(newTrades, TradeType.buy, null, null);
                if(shouldSplitSellTask(newTradesBoughtPrice, boughtAmount)) {
                    createNewSellTask();
                }

                // Calculate new price to sell at
                BigDecimal averageBoughtPrice = getAveragePrice(newTrades, TradeType.buy, getSellTask().getBoughtPrice(), getSellTask().getAmount());
                if (averageBoughtPrice.compareTo(new BigDecimal("0")) != 0) {
                    BigDecimal amountToCoverFee = averageBoughtPrice.multiply(exchange.getMakerFee().multiply(new BigDecimal(2)));
                    getSellTask().setBoughtPrice(averageBoughtPrice.add(amountToCoverFee));
                }

                // Add amount to lowest sell task and subtract from buy tasks
                getSellTask().addAmount(boughtAmount);
                for (BuyTask buyTask : getBuyTasks()) {
                    boughtAmount = buyTask.subtractAmount(boughtAmount);
                }

                ((ShallowBuyTask)getBuyTasks().get(0)).pushDown();

                removeUnusedSellTasks();
            }

            if(hasTrades(newTrades, TradeType.sell)) {

                //Calculate profit
                BigDecimal soldAmount = getAmount(newTrades, TradeType.sell);
                BigDecimal soldPrice = getAveragePrice(newTrades, TradeType.sell, null, null);
                BigDecimal boughtPrice = getAverageBoughtPrice(soldAmount);
                registerPriceDifference(boughtPrice, soldPrice);
                BigDecimal profit = BigDecimal.ZERO;  //profit = 0 if bought price = 0
                if(boughtPrice.compareTo(BigDecimal.ZERO) != 0) {
                    profit = soldAmount.multiply(soldPrice.subtract(boughtPrice));
                }

                // Subtract from sell tasks and add amount back onto buy tasks
                subtractAmountFromSellTasks(soldAmount);
                for (BuyTask buyTask : getBuyTasks()) {
                    if (buyTask.getAmount().compareTo(buyTask.getMaxAmount()) < 0) {
                        soldAmount = buyTask.addAmount(soldAmount);
                    }
                }
                removeUnusedSellTasks();

                // Adjust floating sell task price to compensate
                BigDecimal finalProfit = profit;
                SellTask sellTaskToCompensate = getLowestUsedFloatingSellTask();
                if(sellTaskToCompensate.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                    BigDecimal priceMovement = finalProfit.divide(sellTaskToCompensate.getAmount(), 5, RoundingMode.HALF_UP);
                    if (priceMovement.compareTo(BigDecimal.ZERO) > 0) {
                        sellTaskToCompensate.setCompensatedBoughtPrice(sellTaskToCompensate.getCompensatedBoughtPrice().subtract(priceMovement));
                    }
                }

                // If all sold, then reset buy tasks back to max amount (in case of rounding errors)
                if(getTotalSellAmount().compareTo(BigDecimal.ZERO) == 0) {
                    getBuyTasks().forEach(t -> t.setAmount(t.getMaxAmount()));
                }

                sortSellTasksByPrice();
            }

            cancelFilledOrders();

            logger.info("processTrades: currencyPair=" + currencyPair.toString());
            logger.info("processTrades: buyAmount=" + getBuyTasks().get(0).getAmount().toPlainString());
            logger.info("processTrades: sellAmount=" + getSellTask().getAmount().toPlainString());
        }
    }

    private boolean shouldSplitSellTask(BigDecimal boughtPrice, BigDecimal boughtAmount) {
        if(boughtAmount.compareTo(getMinOrderAmount()) >= 0 && getSellTask().hasBoughtPrice()) {
            BigDecimal percentageDiff = CurrencyUtil.getPercentageDiff(getSellTask().getCompensatedBoughtPrice(), boughtPrice);
            if(percentageDiff.compareTo(sellTaskSplitPercent) >= 0) {
                return true;
            }
        }

        return false;
    }

    private void subtractAmountFromSellTasks(BigDecimal subtractAmount) {
        for(SellTask sellTask : getSellTasks()) {
            subtractAmount = sellTask.subtractAmount(subtractAmount);
        }
    }

    private Optional<SellTask> getHighestUsedSellTask() {
        return getSellTasks().stream()
                .filter(s -> s.getAmount().compareTo(BigDecimal.ZERO) != 0)
                .max(Comparator.comparing(SellTask::getCompensatedBoughtPrice));
    }

    private SellTask getLowestUsedFloatingSellTask() {
        return getSellTasks().subList(1, getSellTasks().size()).stream()
                .filter(s -> s.getAmount().compareTo(BigDecimal.ZERO) != 0)
                .min(Comparator.comparing(SellTask::getCompensatedBoughtPrice))
                .orElse(getSellTask());
    }

    private void cancelFilledOrders() {
        getBuyTasks().forEach(this::cancelFilledOrder);
        getSellTasks().forEach(this::cancelFilledOrder);
    }

    private void cancelFilledOrder(TradeTask task) {
        if(task.getFilledOrder() != null) {
            try {
                exchange.cancelOrder(task.getFilledOrder().getOrderId(), currencyPair);
            } catch (ExchangeClientException e) {
                handleTaskException(task, e);
            }
            task.setFilledOrder(null);
        }

    }


    private void registerPriceDifference(BigDecimal boughtPrice, BigDecimal soldPrice) {
        if(boughtPrice != null && soldPrice != null && boughtPrice.compareTo(soldPrice) == 1) {
            BigDecimal loss = soldPrice.subtract(boughtPrice);
            consecutiveLoss = consecutiveLoss.add(loss);
        } else {
            consecutiveLoss = new BigDecimal("0");
        }
    }

    private boolean hasTrades(List<Trade> trades, TradeType type) {
        return trades.stream().anyMatch(t -> t.getType().equals(type));
    }


    /** Risk processing **/

    private void processRisk(OrderBook orderBook) {
        recordStats(orderBook);
        if(shouldRedistribute(orderBook)) {
            redistribute();
        }
        buySideDepletionWarning();

        if(consecutiveLoss.compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal price = orderBook.getAskByIndex(0).getPrice();
            BigDecimal consecutiveLossPercent = consecutiveLoss.divide(price, RoundingMode.HALF_UP);
            if(consecutiveLossPercent.compareTo(maxConsecutiveLossPercent) == 1) {
                emergencyStop();
            }
        }
    }

    private void recordStats(OrderBook orderBook) {
        //moving average
        averagePrice.add(orderBook.getAskByIndex(0).getPrice().doubleValue());
    }

    private void buySideDepletionWarning() {
        if(getTotalBuyAmount().compareTo(BigDecimal.ZERO) == 0) {
            TaskContext.getTaskRunner().getPushNotificationClient()
                    .sendLimit(exchange.getName() + " " + currencyPair.toString() + " WARNING: Buy side depleted.");
        }
    }

    private boolean shouldRedistribute(OrderBook orderBook) {
        BigDecimal totalBuyAmount = getTotalBuyAmount();
        BigDecimal boughtPrice = getSellTask().getBoughtPrice();
        BigDecimal lowestAsk = orderBook.getAskByIndex(0).getPrice();
        volatility = calculateVolatility(orderBook);

        if(getBuyTasks().get(0).getAmount().compareTo(BigDecimal.ZERO) == 0 && totalBuyAmount.compareTo(BigDecimal.ZERO) > 0 &&
                boughtPrice != null && lowestAsk.compareTo(boughtPrice) < 0) {

            BigDecimal priceDrop = boughtPrice.subtract(lowestAsk);
            BigDecimal maxPriceDrop = orderBook.getMidPrice().multiply(minorStopLossPercent);

            BigDecimal maxVolatility = new BigDecimal("0.0005");

            if(priceDrop.compareTo(maxPriceDrop) > 0 && volatility.compareTo(maxVolatility) < 0) {
                return true;
            }
        }
        return false;
    }

    public BigDecimal getVolatility() {
        return volatility;
    }

    private BigDecimal calculateVolatility(OrderBook orderBook) {
        BigDecimal price = orderBook.getAskByIndex(0).getPrice();
        BigDecimal averageMidPrice = new BigDecimal(averagePrice.getValue());
        BigDecimal deltaAveragePriceActualPrice = averageMidPrice.subtract(price).abs();
        BigDecimal volatilityPercent = deltaAveragePriceActualPrice.divide(price, 5, RoundingMode.HALF_UP);
        return volatilityPercent;
    }


    public void redistribute() {
        redistributeBuyOrders();
    }


    public void redistributeBuyOrders() {
        BigDecimal totalAmount = getTotalBuyAmount();

        for(BuyTask buyTask : getBuyTasks()) {
            if(totalAmount.compareTo(new BigDecimal("0")) == 0) {
                buyTask.setAmount(new BigDecimal("0"));
                cancelOrder(buyTask);
            } else if(buyTask.getMaxAmount().compareTo(totalAmount) <= 0) {
                buyTask.setAmount(buyTask.getMaxAmount());
                totalAmount = totalAmount.subtract(buyTask.getMaxAmount());
            } else if(buyTask.getMaxAmount().compareTo(totalAmount) == 1) {
                buyTask.setAmount(totalAmount);
                totalAmount = new BigDecimal("0");
                cancelOrder(buyTask);
            }
        }
    }

    public void resetFromBottom(BigDecimal resetAmount) {
        if(resetAmount == null) {
            resetAmount = getTotalSellAmount();
        }

        SellTask mainSellTask = createNewSellTask();

        // take amount from lowest sell tasks and put it on lowest sell task
        mainSellTask.addAmount(resetAmount);
        mainSellTask.setBoughtPrice(BigDecimal.ZERO);
        for(int i = 1; i < getSellTasks().size(); i++){
            SellTask sellTask = getSellTasks().get(i);
            resetAmount = sellTask.subtractAmount(resetAmount);
        }

        removeUnusedSellTasks();
    }

    public void resetFromTop(BigDecimal resetAmount) {
        // take amount from highest sell tasks and put it on a new sell task
        SellTask mainSellTask = createNewSellTask();

        mainSellTask.addAmount(resetAmount);
        mainSellTask.setBoughtPrice(BigDecimal.ZERO); //TODO replace with reset flag
        for(int i = getSellTasks().size() - 1; i >= 1; i--){
            SellTask sellTask = getSellTasks().get(i);
            resetAmount = sellTask.subtractAmount(resetAmount);
        }

        removeUnusedSellTasks();
    }

    private void removeUnusedSellTasks() {
        Iterator<SellTask> it = getSellTasks().iterator();

        while(it.hasNext() && getSellTasks().size() > 1) {
            SellTask sellTask = it.next();
            if(sellTask.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                it.remove();
            }
        }
    }

    private void sortSellTasksByPrice() {
        Collections.sort(getSellTasks(), Comparator.comparing(SellTask::getCompensatedBoughtPrice));
    }

    private BigDecimal getTotalBuyAmount() {
        BigDecimal totalAmount = new BigDecimal("0");
        for(BuyTask buyTask : getBuyTasks()) {
            totalAmount = totalAmount.add(buyTask.getAmount());
        }
        return totalAmount;
    }

    private BigDecimal getTotalSellAmount() {
        BigDecimal totalAmount = new BigDecimal("0");
        for(SellTask sellTask : getSellTasks()) {
            totalAmount = totalAmount.add(sellTask.getAmount());
        }
        return totalAmount;
    }

    private void emergencyStop() {
        if(status.equals(TradeCycleStatus.RUNNING)) {
            logger.warn("Emergency Stop initiated!!!!");
            try {
                taskRunnerService.getPushNotificationClient().send("Emergency Stop Initiated! - " + exchange.getName() + "/" + currencyPair.toString());
            } catch(Exception e) {
                logger.error("Error sending notification", e);
            }
            taskRunnerService.pauseAllTasks(currencyPair.getCurrency2());
        }
    }

    private BigDecimal getAmount(List<Trade> trades, TradeType type) {
        BigDecimal amount = new BigDecimal("0");
        for(Trade trade : trades) {
            if(trade.getType().equals(type)) {
                amount = amount.add(trade.getQuantity());
            }
        }
        return amount;
    }

    private BigDecimal getAverageBoughtPrice(BigDecimal amount) {
        List<Trade> tradesList = new ArrayList<>();

        BigDecimal amountLeft = amount;
        for(SellTask sellTask : getSellTasks()) {
            if(amountLeft.compareTo(BigDecimal.ZERO) > 0) {
                if (sellTask.getAmount().compareTo(amountLeft) > 0) {
                    tradesList.add(new Trade(sellTask.getCompensatedBoughtPrice(), sellTask.getAmount().subtract(amountLeft)));
                    amountLeft = BigDecimal.ZERO;
                } else {
                    tradesList.add(new Trade(sellTask.getCompensatedBoughtPrice(), sellTask.getAmount()));
                    amountLeft = amountLeft.subtract(sellTask.getAmount());
                }
            }
        }

        BigDecimal price = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(tradesList);
        price = CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
        return price;
    }

    private BigDecimal getAveragePrice(List<Trade> trades, TradeType type, BigDecimal extraPrice, BigDecimal extraAmount) {
        List<Trade> tradesList = trades.stream()
                .filter(t -> t.getType().equals(type))
                .collect(Collectors.toList());

        if(extraPrice != null && extraPrice.compareTo(BigDecimal.ZERO) > 0 &&
                extraAmount != null && extraAmount.compareTo(BigDecimal.ZERO) > 0) {
            tradesList.add(new Trade(extraPrice, extraAmount));
        }

        BigDecimal price = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(tradesList);
        price = CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
        return price;
    }

    private BigDecimal getAveragePrice(BigDecimal price1, BigDecimal amount1, BigDecimal price2, BigDecimal amount2) {
        List<Trade> tradesList = new ArrayList<>();
        if(price1.compareTo(BigDecimal.ZERO) == 1) {
            tradesList.add(new Trade(price1, amount1));
        }
        if(price2.compareTo(BigDecimal.ZERO) == 1) {
            tradesList.add(new Trade(price2, amount2));
        }
        BigDecimal price = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(tradesList);
        price = CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
        return price;
    }

    public BigDecimal round(BigDecimal price) {
        return CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
    }

    public BigDecimal roundAmount(BigDecimal price) {
        return CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency2()), RoundingMode.HALF_UP);
    }



    /** Process Price Calculations **/

    private void processBuyTask(BuyTask buyTask, OrderBook orderBook) {
        if(status.equals(TradeCycleStatus.RUNNING)) {
            buyTask.setPrimaryPrice(primaryBuyPrice);
            BigDecimal bestBuyPrice = round(buyTask.getBestPrice(orderBook));
            buyTask.setBestPrice(bestBuyPrice);
            if(buyTask == getBuyTasks().get(0)) {
                primaryBuyPrice = bestBuyPrice;
            }
        } else if(status.equals(TradeCycleStatus.STOPPING) || status.equals(TradeCycleStatus.PAUSING)) {
            stopBuyTasks();
        }
    }

    private void processSellTask(SellTask sellTask, OrderBook orderBook) {
        sellTask.setPrimaryPrice(primaryBuyPrice);
        BigDecimal bestSellPrice = round(sellTask.getBestPrice(orderBook));
        sellTask.setBestPrice(bestSellPrice);
    }


    /** Process Move Orders **/

    private void processOrders() {
        for(SellTask sellTask : getSellTasks()) {
            processOrder(sellTask);
        }
        for(BuyTask buyTask : getBuyTasks()) {
            if(!buyTask.isHidden()) {
                processOrder(buyTask);
            }
        }
    }

    private void processOrder(TradeTask task) {
        if(shouldMoveOrder(task)) {
            moveOrder(task);
        } else if (shouldCancelOrder(task)) {
            cancelOrder(task);
        } else if (shouldPlaceOrder(task)) {
            placeOrder(task);
        }
    }

    private void moveOrder(TradeTask task) {
        try {
            BigDecimal orderAmount = roundAmount(task.getOrderAmount());
            PlacedOrder order = exchange.moveOrder(task.getOrder(), task.getBestPrice(), orderAmount);
            task.setOrder(order);
        } catch (ExchangeClientException e) {
            handleTaskException(task, e);
        }
    }

    private void cancelOrder(TradeTask task) {
        try {
            if(task.getOrder() != null) {
                exchange.cancelOrder(task.getOrder().getOrderId(), currencyPair);
                task.setOrder(null);
            }
        } catch (ExchangeClientException e) {
            handleTaskException(task, e);
        }
    }

    private void placeOrder(TradeTask task) {
        try {
            PlacedOrder order;
            BigDecimal orderAmount = roundAmount(task.getOrderAmount());
            if(getSellTasks().contains(task)) {
                order = exchange.sell(currencyPair, task.getBestPrice(), orderAmount);
            } else {
                order = exchange.buy(currencyPair, task.getBestPrice(), orderAmount);
            }
            task.setOrder(order);
        } catch (ExchangeClientException e) {
            handleTaskException(task, e);
        }
    }

    private boolean shouldMoveOrder(TradeTask task) {
        if(task.getOrder() != null) {
            // task doesn't meets minimum order amount
            if(task.getAmount().compareTo(exchange.getMinOrderAmount(currencyPair.getCurrency2())) < 0) {
                return false;
            }
            // no best price
            if(task.getBestPrice() == null) {
                return false;
            }
            // task stopping
            if(status.equals(TradeCycleStatus.STOPPING)) {
                return false;
            }
            // task modified
            if(taskModified) {
                return true;
            }
            // price changed
            BigDecimal priceChange = task.getBestPrice().subtract(task.getOrder().getPrice()).abs();
            if(priceChange.compareTo(task.getMinPriceMovement()) > 0) {
                return true;
            }
            // amount increased
            if(task instanceof SellTask && task.getAmount().compareTo(task.getOrder().getAmountAfterFills()) == 1) {
                return true;
            }
        }
        return false;
    }

    private boolean shouldCancelOrder(TradeTask task) {
        if(task.getOrder() != null) {
            // task doesn't meets minimum order amount
            if(task.getAmount().compareTo(exchange.getMinOrderAmount(currencyPair.getCurrency2())) < 0) {
                return false;
            }
            // no best price
            if(task.getBestPrice() == null) {
                return true;
            }
            // task stopping
            if(status.equals(TradeCycleStatus.STOPPING)) {
                return true;
            }
//            // task modified
//            if(taskModified) {
//                return true;
//            }
//            // price changed
//            if(task.getBestPrice().compareTo(task.getOrder().getPrice()) != 0) {
//                return true;
//            }
//            // amount increased
//            if(task.getAmount().compareTo(task.getOrder().getAmountAfterFills()) == 1) {
//                return true;
//            }
            // pausing buy tasks
            if(!getSellTasks().contains(task) && status.equals(TradeCycleStatus.PAUSING)) {
                return true;
            }
        }
        return false;
    }

    private boolean shouldPlaceOrder(TradeTask task) {
        // order already placed
        if(task.getOrder() != null) {
            return false;
        }
        // no best price
        if(task.getBestPrice() == null) {
            return false;
        }
        // task meets minimum order amount
        if (task.getAmount().compareTo(exchange.getMinOrderAmount(currencyPair.getCurrency2())) < 0) {
            return false;
        }
        // pausing buy tasks
        if(!getSellTasks().contains(task) && status.equals(TradeCycleStatus.PAUSING)) {
            return false;
        }
        // task stopping
        if(status.equals(TradeCycleStatus.STOPPING)) {
            return false;
        }

//        // order not placed yet
//        if(task.getOrder() == null) {
//            return true;
//        }
//        // task modified
//        if (taskModified) {
//            return true;
//        }
//        // price changed
//        if(task.getBestPrice().compareTo(task.getOrder().getPrice()) != 0) {
//            return true;
//        }
//        // amount increased
//        if(task.getAmount().compareTo(task.getOrder().getAmountAfterFills()) == 1) {
//            return true;
//        }

        return true;
    }

    private void stopBuyTasks() {
        for(BuyTask buyTask : getBuyTasks()) {
            if(buyTask.getOrder() != null) {
                try {
                    exchange.cancelOrder(buyTask.getOrder().getOrderId(), currencyPair);
                    buyTask.setOrder(null);
                } catch (ExchangeClientException e) {
                    handleTaskException(buyTask, e);
                }
            }
        }
    }

    private void handleTaskException(TradeTask task, ExchangeClientException e) {
        if (e.getType().equals(ExchangeClientExceptionType.ORDER_NOT_FOUND)) {
            task.setOrder(null);
        } else if (e.getType().equals(ExchangeClientExceptionType.INSUFFICIENT_FUNDS)) {
            // cancelled order but failed to create new order because of insufficient funds
            task.setOrder(null);
            handleInsufficientFunds();
        } else if (e.getType().equals(ExchangeClientExceptionType.ORDER_FILLED)) {
            // order was filled before cancel, process fills on next iteration
            task.setOrder(null);
        } else if (e.getType().equals(ExchangeClientExceptionType.ORDER_PARITALLY_FILLED)) {
            try {
                exchange.cancelOrder(task.getOrder().getOrderId(), currencyPair);
            } catch (ExchangeClientException e1) {
                e1.printStackTrace();
            }
            task.setOrder(null);
        } else {
            // anything else?
            logger.error("Unexpected error placing order", e);
        }
    }


    //TODO only handle if this happens twice
    private void handleInsufficientFunds() {
//        // cancel all orders
//        exchange.cancelOrders(currencyPair, null);
//
//        // check balance
//        Balances balances = exchange.getBalances();
//
//        //check sell task balances
//        BigDecimal currency2Available = balances.getBalances().get(currencyPair.getCurrency2()).getAvailable();
//        currency2Available = CurrencyUtil.round(currency2Available, exchange.getPriceIncrement(currencyPair.getCurrency2()), RoundingMode.DOWN);
//        getSellTask().setAmount(currency2Available);
//
//        //remove sell amount from buy tasks
//        for(BuyTask buyTask : getBuyTasks()) {
//
//        }

    }

    @Override
    public String toString() {
        String output = "      TradeCycle:" + status.name() + "\n";
        output += "      sl=" + minorStopLossPercent.toPlainString();
        if(volatility != null)
            output += "      Volatility = " + volatility.setScale(5, RoundingMode.HALF_UP).toPlainString();
        output += "\n";

        for(SellTask sellTask : getSellTasks()) {
            output += "      " + getSellTasks().indexOf(sellTask) + ":Sell:  ";
            output += toStringTask(sellTask);
            output += "\n";
        }

        if(lowestAsk != null)
            output += "      Ask = " + lowestAsk.getPrice() + "\n";
        if(highestBid != null)
            output += "      Bid = " + highestBid.getPrice() + "\n";

        for(BuyTask buyTask : getBuyTasks()) {
            output += "      " + getBuyTasks().indexOf(buyTask) + ":Buy:  ";
            output += toStringTask(buyTask);
            output += "\n";
        }
        output += "\n";
        return output;
    }


    private String toStringTask(TradeTask task) {
        String output =  "Price = " + task.getBestPrice() + "  Amount = " + task.getAmount().toPlainString();
        if(task.getOrder() != null) {
            output += " (Order id = " + task.getOrder().getOrderId() + ")";
        }
        if(task instanceof ShallowBuyTask) {
            output += " s=" + ((ShallowBuyTask)task).getMinSpreadPercent();
        } else if(task instanceof DeepBuyTask) {
            output += " d=" + ((DeepBuyTask)task).getDepthPercent();
        } else if (task instanceof  SellTask) {
            output += " boughtPrice=" + ((SellTask)task).getBoughtPrice().toPlainString();
            output += " (compensated=" + ((SellTask)task).getCompensatedBoughtPrice().toPlainString() + ")";
            if(lowestAsk != null) {
                output += " potential=" + ((SellTask) task).calculateProfitAtPrice(lowestAsk.getPrice()).setScale(2, RoundingMode.HALF_DOWN).toPlainString();
            }
        }
        return output;
    }
}
