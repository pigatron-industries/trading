package com.pigatron.trading.tasks.impl.buylow;

import com.pigatron.trading.analysis.MacdAnalyser;
import com.pigatron.trading.analysis.MacdData;
import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientExceptionType;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.TradeCycleStatus;
import com.pigatron.trading.tasks.impl.cycle.TradeTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import com.pigatron.trading.util.CurrencyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class BuyLowSellAndForgetAlgo extends AbstractAlgorithm {

    private static final Logger logger = LoggerFactory.getLogger(BuyLowSellAndForgetAlgo.class);

    /** True if fee is subtracted from currency 2 */
    private boolean subtractFeeFromSell = true;

    /** The maxmimum percent of loss in consecutive trades before emergency stop */
    private BigDecimal maxConsecutiveLossPercent = new BigDecimal("0.01");

    /** Amount to increment bought price by when setting sell price */
    private BigDecimal sellPriceIncrement;


    @Transient
    private TaskRunnerService taskRunnerService;

    @Transient
    private MacdAnalyser analyser = new MacdAnalyser();

    @Transient
    private TradeCycleStatus status;

    @Transient
    private BigDecimal primaryBuyPrice;

    @Transient
    private boolean taskModified;

    @Transient
    private BigDecimal consecutiveLoss = new BigDecimal("0");

    @Transient
    private boolean firstRun = true;

    @Transient
    private ChartData chartData;

    @Transient
    private Trend trend = Trend.DOWN;

    @Transient
    private double ema;

    @Transient
    private double max;

    @Transient
    private Date statsUpdated;


    public BuyLowSellAndForgetAlgo(ExchangeClient exchange, CurrencyPair currencyPair, BuyTask buyTask, BigDecimal sellPriceIncrement) {
        super(exchange, currencyPair);
        this.getBuyTasks().add(buyTask);
        this.sellPriceIncrement = sellPriceIncrement;
        status = TradeCycleStatus.RUNNING;
        setCommonTaskProperties(buyTask);
    }

    public void setTaskRunnerService(TaskRunnerService taskRunnerService) {
        this.taskRunnerService = taskRunnerService;
    }

    public ExchangeClient getExchange() {
        return exchange;
    }

    public BigDecimal getMinOrderAmount() {
        return exchange.getMinOrderAmount(currencyPair.getCurrency2());
    }



    private void setCommonTaskProperties(TradeTask task) {
        task.setMakerFee(exchange.getMakerFee());
        task.setPriceIncrement(exchange.getPriceIncrement(currencyPair.getCurrency1()));
    }


    public void start() throws IOException {
        exchange.subscribe(this, currencyPair);
    }

    @Override
    public void stop() {
        status = TradeCycleStatus.STOPPING;
        processCancelOrders();
        exchange.unsubscribe(this, currencyPair);
    }

    @Override
    public void pause() {
        status = TradeCycleStatus.PAUSING;
    }

    @Override
    public void unpause() {
        status = TradeCycleStatus.RUNNING;
    }

    public void setTaskModified() {
        taskModified = true;
    }

    public void setSubtractFeeFromSell(boolean subtractFeeFromSell) {
        this.subtractFeeFromSell = subtractFeeFromSell;
    }


    @Override
    public void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        if(!firstRun) { // ignore new trades on first run
            processTrades(newTrades);
        }

        for(BuyTask buyTask : getBuyTasks()) {
            processBuyTask(buyTask, orderBook);
        }
        for(SellTask sellTask : getSellTasks()) {
            processSellTask((SellAndForgetTask)sellTask, orderBook);
        }

        processCancelOrders();
        processPlaceOrders();

        processRisk(orderBook);

        taskModified = false;
        firstRun = false;
    }

    private void processTrades(List<Trade> newTrades) {
        if(newTrades.size() > 0) {

            if(hasTrades(newTrades, TradeType.buy)) {

                BigDecimal boughtAmount = getAmount(newTrades, TradeType.buy);
                BigDecimal sellAmount = boughtAmount;
                if(subtractFeeFromSell) {
                    BigDecimal fee = boughtAmount.multiply(exchange.getMakerFee());
                    fee = CurrencyUtil.round(fee, exchange.getPriceIncrement(currencyPair.getCurrency2()), RoundingMode.UP);
                    sellAmount = boughtAmount.subtract(fee);
                }

                for (BuyTask buyTask : getBuyTasks()) {
                    boughtAmount = buyTask.subtractAmount(boughtAmount);
                    buyTask.setAmount(buyTask.getMaxAmount());
                }

                BigDecimal boughtPrice = getAveragePrice(newTrades, TradeType.buy);
                if (boughtPrice.compareTo(new BigDecimal("0")) != 0) {
                    // calculate sell price
                    BigDecimal amountToCoverFee = boughtPrice.multiply(exchange.getMakerFee().multiply(new BigDecimal(2)));
                    BigDecimal sellPrice = boughtPrice.add(amountToCoverFee);
                    sellPrice = sellPrice.add(sellPriceIncrement);
                    getSellTasks().add(new SellAndForgetTask(sellPrice, sellAmount));
                }

            }
        }
    }


    private boolean hasTrades(List<Trade> trades, TradeType type) {
        return trades.stream().anyMatch(t -> t.getType().equals(type));
    }



    private BigDecimal getAmount(List<Trade> trades, TradeType type) {
        BigDecimal amount = new BigDecimal("0");
        for(Trade trade : trades) {
            if(trade.getType().equals(type)) {
                amount = amount.add(trade.getQuantity());
            }
        }
        return amount;
    }

    private BigDecimal getAveragePrice(List<Trade> trades, TradeType type) {
        List<Trade> tradesList = trades.stream()
                .filter(t -> t.getType().equals(type))
                .collect(Collectors.toList());
        BigDecimal price = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(tradesList);
        price = CurrencyUtil.round(price, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
        return price;
    }


    private void processBuyTask(BuyTask buyTask, OrderBook orderBook) {
        if(status.equals(TradeCycleStatus.RUNNING)) {
            buyTask.setPrimaryPrice(primaryBuyPrice);
            BigDecimal bestBuyPrice = buyTask.getBestPrice(orderBook);
            if(buyTask == getBuyTasks().get(0)) {
                primaryBuyPrice = bestBuyPrice;
            }
        } else if(status.equals(TradeCycleStatus.STOPPING) || status.equals(TradeCycleStatus.PAUSING)) {
            stopBuyTasks();
        }
    }

    private void processSellTask(SellAndForgetTask sellTask, OrderBook orderBook) {
        sellTask.getBestPrice(orderBook);
    }

    private void processCancelOrders() {
        for(BuyTask buyTask : getBuyTasks()) {
            processCancelOrder(buyTask);
        }
    }

    private void processCancelOrder(TradeTask task) {
        if (shouldCancelOrder(task)) {
            try {
                cancelOrder(task);
            } catch (ExchangeClientException e) {
                handleTaskException(task, e);
            }
        }
    }

    private void processPlaceOrders() {
        List<SellTask> sellTasksCopy = new ArrayList<>();
        sellTasksCopy.addAll(getSellTasks());
        for(SellTask sellTask : sellTasksCopy) {
            processPlaceOrder(sellTask);
        }
        for(BuyTask buyTask : getBuyTasks()) {
            processPlaceOrder(buyTask);
        }
    }


    private void processPlaceOrder(TradeTask task) {
        if (shouldPlaceOrder(task)) {
            try {
                placeOrder(task);
            } catch (ExchangeClientException e) {
                handleTaskException(task, e);
            }
        }
    }

    private boolean shouldCancelOrder(TradeTask task) {
        if(task.getOrder() != null) {
            // task meets minimum order amount
            if (task.getAmount().compareTo(exchange.getMinOrderAmount(currencyPair.getCurrency2())) == -1) {
                return false;
            }
            // task stopping
            if(status.equals(TradeCycleStatus.STOPPING)) {
                return true;
            }
            // task modified
            if(taskModified) {
                return true;
            }
            // price changed
            if(task.getBestPrice().compareTo(task.getOrder().getPrice()) != 0) {
                return true;
            }
            // amount increased
            if(task.getAmount().compareTo(task.getOrder().getAmountAfterFills()) == 1) {
                return true;
            }
            // pausing buy tasks
            if(!(task instanceof SellAndForgetTask)) {
                if(status.equals(TradeCycleStatus.PAUSING) || trend.equals(Trend.DOWN)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean shouldPlaceOrder(TradeTask task) {
        // task meets minimum order amount
        if (task.getAmount().compareTo(exchange.getMinOrderAmount(currencyPair.getCurrency2())) == -1) {
            return false;
        }
        // pausing buy tasks
        if(!(task instanceof SellAndForgetTask)) {
            if(status.equals(TradeCycleStatus.PAUSING) || trend.equals(Trend.DOWN)) {
                return false;
            }
        }
        // task stopping
        if(status.equals(TradeCycleStatus.STOPPING)) {
            return false;
        }

        // order not placed yet
        if(task.getOrder() == null) {
            return true;
        }
        // task modified
        if (taskModified) {
            return true;
        }
        // price changed
        if(task.getBestPrice().compareTo(task.getOrder().getPrice()) != 0) {
            return true;
        }
        // amount increased
        if(task.getAmount().compareTo(task.getOrder().getAmountAfterFills()) == 1) {
            return true;
        }

        return false;
    }

    private void stopBuyTasks() {
        for(BuyTask buyTask : getBuyTasks()) {
            if(buyTask.getOrder() != null) {
                try {
                    exchange.cancelOrder(buyTask.getOrder().getOrderId(), currencyPair);
                    buyTask.setOrder(null);
                } catch (ExchangeClientException e) {
                    handleTaskException(buyTask, e);
                }
            }
        }
    }

    private void handleTaskException(TradeTask task, ExchangeClientException e) {
        if (e.getType().equals(ExchangeClientExceptionType.ORDER_NOT_FOUND)) {
            task.setOrder(null);
        } else if (e.getType().equals(ExchangeClientExceptionType.INSUFFICIENT_FUNDS)) {
            // cancelled order but failed to create new order because of insufficient funds
            task.setOrder(null);
        } else if (e.getType().equals(ExchangeClientExceptionType.ORDER_FILLED)) {
            // order was filled before cancel, process fills on next iteration
            task.setOrder(null);
        } else {
            // anything else?
            logger.error("Unexpected error placing order", e);
        }
    }

    private void cancelOrder(TradeTask task) throws ExchangeClientException {
        try {
            if (task.getOrder() != null) {
                exchange.cancelOrder(task.getOrder().getOrderId(), currencyPair);
                task.setOrder(null);
            }
        } catch (ExchangeClientException e) {
            handleTaskException(task, e);
        }
    }

    private void placeOrder(TradeTask task) throws ExchangeClientException {
        try {
            BigDecimal price = CurrencyUtil.round(task.getBestPrice(), exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
            PlacedOrder order;
            if(task instanceof SellAndForgetTask) {
                order = exchange.sell(currencyPair, price, task.getAmount());
                getSellTasks().remove(task);
            } else {
                order = exchange.buy(currencyPair, price, task.getAmount());
            }
            task.setOrder(order);
        } catch (ExchangeClientException e) {
            handleTaskException(task, e);
        }
    }


    private void processRisk(OrderBook orderBook) {
        updateDataAnalysis(orderBook);

    }

    private void updateDataAnalysis(OrderBook orderBook) {
        long updateTime = new Date().getTime() - (1000*60); //5 mins
        if(chartData == null || statsUpdated.getTime() < updateTime) {
            try {
                chartData = exchange.getChartData(currencyPair, 60, 60);
                List<Double> ohlc4 = analyser.calculateOHLC4(chartData);
                ema = analyser.calculateEMA(ohlc4, 50).get(0);
                max = ohlc4.stream().mapToDouble(Double::intValue).max().getAsDouble();
                calculateCurrentTrend(ohlc4);
                statsUpdated = new Date();
                moveSellOrders(orderBook);
            } catch (NotImplementedException e) {
                e.printStackTrace();
            }
        }
    }

    private void calculateCurrentTrend(List<Double> ohlc4) {
        MacdData macdData = analyser.calculateMacdData(ohlc4);
        if(macdData.getHist().get(0) > macdData.getHist().get(1)) {
            trend = Trend.UP;
        } else {
            trend = Trend.DOWN;
        }
    }

    private void moveSellOrders(OrderBook orderBook) {
        double currentPrice = orderBook.getAskByIndex(0).getPrice().doubleValue();
        double highPrice = currentPrice+(currentPrice*0.002); // plus 0.2%
        double maxPrice = max > highPrice ? max : highPrice;

        exchange.getOpenOrders(currencyPair, TradeType.sell).stream()
                .filter(o -> o.getPrice().doubleValue() > maxPrice)
                .forEach(o -> moveSellOrder(o, orderBook.getAskByIndex(0).getPrice()));
    }

    private void moveSellOrder(PlacedOrder order, BigDecimal newPrice) {
        try {
            newPrice = CurrencyUtil.round(newPrice, exchange.getPriceIncrement(currencyPair.getCurrency1()), RoundingMode.HALF_UP);
            exchange.moveOrder(order, newPrice, order.getAmount());
        } catch (ExchangeClientException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        String output = "      BuyLowSellAndForgetAlgo:" + status.name() + "  (Trend " + trend.name() + ")\n";
        for(BuyTask buyTask : getBuyTasks()) {
            if(buyTask.getOrder() != null) {
                output += "      " + getBuyTasks().indexOf(buyTask) + ":Buy:  Amount = " + buyTask.getAmount().toPlainString() + "  Price = " + buyTask.getOrder().getPrice() + "\n";
            }
        }
        output += "\n";

        return output;
    }

}
