package com.pigatron.trading.tasks.impl.cycle;


public enum TradeCycleStatus {
    RUNNING, STOPPING, PAUSING
}
