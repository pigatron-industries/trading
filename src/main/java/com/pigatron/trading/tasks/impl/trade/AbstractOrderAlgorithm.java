package com.pigatron.trading.tasks.impl.trade;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.OrderNotPlacedException;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Transient;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractOrderAlgorithm extends AbstractAlgorithm {

    private static final Logger logger = LoggerFactory.getLogger(AbstractOrderAlgorithm.class);


    @Transient
    protected BigDecimal makerFee;

    protected BigDecimal amount;
    protected BigDecimal profitMargin;

    protected PlacedOrder order;

    @Transient
    protected Set<Trade> trades = new HashSet<>();

    @Transient
    protected boolean firstInPosition;

    public AbstractOrderAlgorithm(ExchangeClient exchange, CurrencyPair currencyPair) {
        super(exchange, currencyPair);
    }

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getProfitMargin() {
        return profitMargin;
    }

    public void setProfitMargin(BigDecimal profitMargin) {
        this.profitMargin = profitMargin;
    }

    public PlacedOrder getOrder() {
        return order;
    }

    public void setOrder(PlacedOrder order) {
        this.order = order;
    }

    public Set<Trade> getTrades() {
        return trades;
    }


    @Override
    public void stop() {
        if(order != null) {
            cancelOrder();
        }
        exchange.unsubscribe(this, currencyPair);
    }

    @Override
    public void pause() {

    }

    @Override
    public void unpause() {

    }

    @Override
    public void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        try {
            if(order == null) {
                start();
            }
            Optional<PlacedOrder> openOrder;
            try {
                openOrder = exchange.getOpenOrderById(order.getOrderId(), currencyPair);
            } catch(OrderNotPlacedException e) {
                // Order does not exists, restart task
                logger.error("onOrderBookChanged error", e);
                logger.info("Restarting task...");
                start();
                return;
            }
            checkForNewTrades();
            if (!openOrder.isPresent()) {
                onOrderComplete();
            } else {
                try {
                    moveOrder(orderBook);
                } catch(ExchangeClientException e) {
                    // Could be order was filled while moving order
                    logger.error("onOrderBookChanged error", e);
                }
            }
        } catch(Exception e) {
            logger.error("onOrderBookChanged error", e);
        }
    }

    /**
     * Determine of order should be placed ahead of other order
     */
    protected boolean shouldPlaceAhead(BigDecimal highestOrderAmount, BigDecimal highestOrderPrice, BigDecimal spread) {
        if(order != null && firstInPosition && highestOrderPrice.compareTo(order.getPrice()) == 0) {
            return false;
        }

        firstInPosition = true;
        if(spread.doubleValue() > 2) {
            return true;
        } else if(spread.doubleValue() > 1.5 && highestOrderAmount.doubleValue() > 0.2) {
            return true;
        } else if(spread.doubleValue() > 1 && highestOrderAmount.doubleValue() > 0.3) {
            return true;
        } else if(spread.doubleValue() > 0.5 && highestOrderAmount.doubleValue() > 0.5) {
            return true;
        }

        firstInPosition = false;
        return false;
    }


    protected boolean checkForNewTrades() {
        List<Trade> orderTrades = exchange.getOrderTrades(order);
        Set<Trade> newTrades = orderTrades.stream()
                .filter(t -> !trades.contains(t))
                .collect(Collectors.toSet());
        trades.addAll(newTrades);

        if(newTrades.size() > 0) {
            onNewOrderTrades(newTrades);
        }

        return newTrades.size() > 0;
    }

    protected BigDecimal getAverageTradesPrice(Set<Trade> trades) {
        BigDecimal totalQuantity = new BigDecimal(0);
        BigDecimal totalPrice = new BigDecimal(0);
        for(Trade trade : trades) {
            totalQuantity = totalQuantity.add(trade.getQuantity());
            totalPrice = totalPrice.add(trade.getTotal());
        }

        return totalPrice.divide(totalQuantity, 8, RoundingMode.HALF_UP);
    }


    protected BigDecimal getTotalTradesPrice(Set<Trade> trades) {
        BigDecimal totalPrice = new BigDecimal(0);
        for(Trade trade : trades) {
            totalPrice = totalPrice.add(trade.getTotal());
        }

        return totalPrice;
    }

    protected BigDecimal getTotalTradesAmount(Set<Trade> trades) {
        BigDecimal totalAmount = new BigDecimal(0);
        for(Trade trade : trades) {
            totalAmount = totalAmount.add(trade.getQuantity());
        }

        return totalAmount;
    }

    protected BigDecimal getTotalTradesPrice() {
        return getTotalTradesPrice(trades);
    }

    protected BigDecimal getTotalTradesAmount() {
        return getTotalTradesAmount(trades);
    }

    protected BigDecimal getAverageTradesPrice() {
        return getAverageTradesPrice(trades);
    }

    protected void cancelOrder() {
        try {
            exchange.cancelOrder(order.getOrderId(), null);
        } catch(Exception e) {
            //e.printStackTrace();
        }
    }

    protected abstract void moveOrder(OrderBook orderBook) throws ExchangeClientException;
    protected abstract void onOrderComplete();
    protected abstract void onNewOrderTrades(Set<Trade> newTrades);

}
