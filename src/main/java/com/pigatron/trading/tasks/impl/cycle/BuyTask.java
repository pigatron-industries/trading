package com.pigatron.trading.tasks.impl.cycle;



import java.math.BigDecimal;

public abstract class BuyTask extends TradeTask  {

    public BuyTask(BigDecimal maxAmount) {
        super(maxAmount);
    }

}
