package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.impl.PlaceAheadMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public abstract class TradeTask {

    private static final Logger logger = LoggerFactory.getLogger(TradeTask.class);

    protected AbstractAlgorithm parentAlgorithm;

    private PlacedOrder order;
    private PlacedOrder filledOrder;
    protected BigDecimal bestPrice;
    private String prevOrderId;
    private Set<Trade> trades = new HashSet<>();
    private BigDecimal maxAmount;
    private BigDecimal amount;
    private BigDecimal price;
    protected BigDecimal primaryPrice;
    protected BigDecimal priceIncrement;
    protected BigDecimal makerFee = new BigDecimal("0");
    private boolean firstInPosition;
    private PlaceAheadMode placeAheadMode = PlaceAheadMode.VOLUME;
    private BigDecimal aggressionPercent = new BigDecimal("0");
    private Boolean hidden = false;
    private BigDecimal minWeight = new BigDecimal("0");
    private BigDecimal maxWeight = new BigDecimal("0.8");
    private BigDecimal minPriceMovement = BigDecimal.ZERO;
    private BigDecimal maxOrderPercent = BigDecimal.ONE;

    public TradeTask() {
    }

    public TradeTask(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
        this.amount = maxAmount;
    }


    public void setParentAlgorithm(AbstractAlgorithm parentAlgorithm) {
        this.parentAlgorithm = parentAlgorithm;
    }

    public BigDecimal getBestPrice() {
        return bestPrice;
    }

    public void setBestPrice(BigDecimal bestPrice) {
        this.bestPrice = bestPrice;
    }

    public abstract BigDecimal getBestPrice(OrderBook orderBook);

    public PlacedOrder getOrder() {
        return order;
    }

    public void setOrder(PlacedOrder order) {
        this.prevOrderId = order != null ? order.getOrderId() : null;
        this.order = order;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getOrderAmount() {
        if(maxAmount == null || maxOrderPercent == null) {
            return amount;
        }
        BigDecimal maxOrderAmount = maxAmount.multiply(maxOrderPercent);
        if(amount.compareTo(maxOrderAmount) > 0) {
            return maxOrderAmount;
        } else {
            return amount;
        }
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setPrimaryPrice(BigDecimal primaryPrice) {
        this.primaryPrice = primaryPrice;
    }

    public BigDecimal getMakerFee() {
        return makerFee;
    }

    public void setMakerFee(BigDecimal makerFee) {
        this.makerFee = makerFee;
    }

    public PlaceAheadMode isPlaceAheadMode() {
        return placeAheadMode;
    }

    public void setPlaceAheadMode(PlaceAheadMode placeAheadMode) {
        this.placeAheadMode = placeAheadMode;
    }

    public BigDecimal getAggressionPercent() {
        return aggressionPercent;
    }

    public void setAggressionPercent(BigDecimal aggressionPercent) {
        this.aggressionPercent = aggressionPercent;
    }

    public Boolean isHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public void setPriceIncrement(BigDecimal priceIncrement) {
        this.priceIncrement = priceIncrement;
    }

    public BigDecimal getMaxOrderPercent() {
        return maxOrderPercent;
    }

    public void setMaxOrderPercent(BigDecimal maxOrderPercent) {
        this.maxOrderPercent = maxOrderPercent;
    }

    public BigDecimal addAmount(BigDecimal addAmount) {
        BigDecimal excess = BigDecimal.ZERO;
        amount = amount.add(addAmount);

        if(maxAmount != null && amount.compareTo(maxAmount) == 1) {
            excess = amount.subtract(maxAmount);
            amount = maxAmount;
        }
        return excess;
    }

    public BigDecimal subtractAmount(BigDecimal subAmount) {
        BigDecimal excess = BigDecimal.ZERO;
        amount = amount.subtract(subAmount);
        if(order != null) {
            order.setFilledSize(order.getFilledSize().add(subAmount));
        }

        if(amount.compareTo(BigDecimal.ZERO) == -1) {
            excess = BigDecimal.ZERO.subtract(amount);
            amount = BigDecimal.ZERO;
        }
        if(this.amount.compareTo(BigDecimal.ZERO) == 0) {
            filledOrder = order;
            order = null;
        }
        return excess;
    }

    public PlacedOrder getFilledOrder() {
        return filledOrder;
    }

    public void setFilledOrder(PlacedOrder filledOrder) {
        this.filledOrder = filledOrder;
    }

    public void setFirstInPosition(boolean firstInPosition) {
        this.firstInPosition = firstInPosition;
    }

    public boolean isFirstInPosition() {
        return firstInPosition;
    }

    public boolean orderMatches(MarketOrder order) {
        if(this.order != null) {
            if(order instanceof PlacedOrder) {
                return this.getOrder().getOrderId().equals(((PlacedOrder) order).getOrderId());
            } else {
                return this.getAmount().compareTo(order.getAmount()) >= 0 &&
                        this.getOrder().getPrice().compareTo(order.getPrice()) == 0;
            }
        } else {
            return false;
        }
    }

    //TODO PERFORMANCE cache result in each iteration
    protected boolean orderMatches(MarketOrder order, List<TradeTask> orderTasks) {
        if(order instanceof PlacedOrder) {
            return orderMatchesPlacedOrder((PlacedOrder)order, orderTasks);
        } else {
            return orderMatchesMarketOrder(order, orderTasks);
        }
    }

    private boolean orderMatchesPlacedOrder(PlacedOrder order, List<TradeTask> orderTasks) {
        return orderTasks.stream()
                .anyMatch(task -> task.getOrder() != null && task.getOrder().getOrderId().equals(order.getOrderId()));
    }

    private boolean orderMatchesMarketOrder(MarketOrder order, List<TradeTask> orderTasks) {
        // combine order of same price into a consolidated order list
        List<MarketOrder> consolidatedOrders = new ArrayList<>();

        for (TradeTask task : orderTasks) {
            if (task.getOrder() != null) {
                BigDecimal amount = task.getAmount();
                BigDecimal price = task.getOrder().getPrice();

                if (consolidatedOrders.size() > 0 && consolidatedOrders.get(0).getPrice().compareTo(price) == 0) {
                    MarketOrder consolidatedOrder = consolidatedOrders.get(0);
                    consolidatedOrder.setAmount(consolidatedOrder.getAmount().add(amount));
                } else {
                    MarketOrder consolidatedOrder = new MarketOrder(order.getType(), price, amount);
                    consolidatedOrders.add(consolidatedOrder);
                }
            }
        }

        // if any orders match on price and have greater size then part of the order is ours
        for (MarketOrder consolidatedOrder : consolidatedOrders) {
            if (consolidatedOrder.getAmount().compareTo(order.getAmount()) >= 0 &&
                    consolidatedOrder.getPrice().compareTo(order.getPrice()) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine of order should be placed ahead of other order
     */
    protected boolean shouldPlaceAhead(MarketOrder order, BigDecimal spread) {
        if(placeAheadMode.equals(PlaceAheadMode.NEVER)) {
            return false;
        }
        if(spread.compareTo(priceIncrement) == 0) {
            return false;
        }
        if(this.getOrder() != null && this.isFirstInPosition() && order.getPrice().compareTo(this.getOrder().getPrice()) == 0) {
            return false;
        }

        this.setFirstInPosition(true);
        if(placeAheadMode.equals(PlaceAheadMode.ALWAYS)) {
            return true;
        } else if(spread.doubleValue() > 2) {
            return true;
        } else if(spread.doubleValue() > 1.5 && order.getAmount().doubleValue() > 0.2) {
            return true;
        } else if(spread.doubleValue() > 1 && order.getAmount().doubleValue() > 0.3) {
            return true;
        } else if(spread.doubleValue() > 0.5 && order.getAmount().doubleValue() > 0.5) {
            return true;
        }

        this.setFirstInPosition(false);
        return false;
    }

    protected BigDecimal getOrderDistance(BigDecimal price1, BigDecimal price2) {
        BigDecimal room = price1.subtract(price2);
        if(room.compareTo(priceIncrement) <= 0) {
            return priceIncrement;
        }

        BigDecimal orderDistance = room.multiply(aggressionPercent);
        if(orderDistance.compareTo(priceIncrement) <= 0) {
            return priceIncrement;
        }

        return orderDistance;
    }

    protected BigDecimal calculateFollowWeight(BigDecimal diff, BigDecimal price) {
        // weight increases to 1 as diff increases to 1% (0.01 -> 1)
        BigDecimal diffPercent = diff.divide(price, 5, RoundingMode.UP);
        BigDecimal weight = diffPercent.divide(new BigDecimal(0.01), 5, RoundingMode.UP);
        if(weight.compareTo(minWeight) < 0) {
            weight = minWeight;
        } else if(weight.compareTo(maxWeight) > 0) {
            weight = maxWeight;
        }
        return weight;
    }

    public BigDecimal getMinPriceMovement() {
        return minPriceMovement;
    }

    public void setMinPriceMovement(BigDecimal minPriceMovement) {
        this.minPriceMovement = minPriceMovement;
    }

    @Override
    public String toString() {
        return "TradeTask{" +
                "order=" + (order==null ? "null" : order.toString()) +
                ", prevOrderId='" + prevOrderId + '\'' +
                ", maxAmount=" + maxAmount +
                ", amount=" + amount +
                ", price=" + price +
                '}';
    }
}
