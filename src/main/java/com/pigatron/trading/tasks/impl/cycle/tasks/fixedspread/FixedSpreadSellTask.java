package com.pigatron.trading.tasks.impl.cycle.tasks.fixedspread;


import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;

import java.math.BigDecimal;


public class FixedSpreadSellTask extends SellTask {

    protected BigDecimal fixedSpread;


    public FixedSpreadSellTask(BigDecimal fixedSpread) {
        this.fixedSpread = fixedSpread;
    }

    @Override
    public BigDecimal getBestPrice(OrderBook orderBook) {
        bestPrice = primaryPrice.add(fixedSpread);

        //setAmount(new BigDecimal("0.0001"));
        // if amount is under minimum order amount then increase it
//        if(getAmount().compareTo(parentAlgorithm.getMinOrderAmount()) == -1) {
//            setAmount(parentAlgorithm.getMinOrderAmount());
//        }

        return bestPrice;
    }

}
