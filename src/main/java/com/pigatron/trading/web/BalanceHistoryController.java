package com.pigatron.trading.web;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.stats.entity.BalanceHistory;
import com.pigatron.trading.stats.repository.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class BalanceHistoryController {

    private static final String VIEW_BALANCE_HISTORY = "balance";

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private ExchangeProvider exchangeProvider;


    @ModelAttribute("balanceHistory")
    public List<BalanceHistory> getBalanceHistory() {
        LocalDateTime from = LocalDateTime.now().minus(30, ChronoUnit.DAYS);
        return balanceRepository.findByDateBetween(toDate(from), new Date());
    }

    @ModelAttribute("balanceNowGdax")
    public Balances getBalanceNowGdax() {
        try {
            return exchangeProvider.getExchange("gdax").getBalances();
        } catch(Exception e) {
            return new Balances();
        }
    }

//    @ModelAttribute("balanceNowSpaceBtc")
//    public Balances getBalanceNowSpaceBtc() {
//        try {
//            return exchangeProvider.getExchange("spacebtc").getBalances();
//        } catch(Exception e) {
//            return new Balances();
//        }
//    }

//    @ModelAttribute("balanceNowLiveCoin")
//    public Balances getBalanceNowLiveCoin() {
//        try {
//            return exchangeProvider.getExchange("livecoin").getBalances();
//        } catch(Exception e) {
//            return new Balances();
//        }
//    }

    @ModelAttribute("balanceNowQuoine")
    public Balances balanceNowQuoine() {
        try {
            return exchangeProvider.getExchange("quoine").getBalances();
        } catch(Exception e) {
            return new Balances();
        }
    }

    @ModelAttribute("balanceNowKraken")
    public Balances balanceNowKraken() {
        try {
            return exchangeProvider.getExchange("kraken").getBalances();
        } catch(Exception e) {
            return new Balances();
        }
    }


    @ModelAttribute("now")
    public Date now() {
        return new Date();
    }

    @RequestMapping(value = "/balances", method = RequestMethod.GET)
    public String balances() {
        return VIEW_BALANCE_HISTORY;
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

}
