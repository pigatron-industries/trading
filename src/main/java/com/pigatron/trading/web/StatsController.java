package com.pigatron.trading.web;


import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.stats.entity.BalanceHistory;
import com.pigatron.trading.stats.repository.BalanceRepository;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/stats")
public class StatsController {

    @Autowired
    private BalanceRepository balanceRepository;

    @Autowired
    private ExchangeProvider exchangeProvider;

    @Autowired
    private TaskRunnerService taskRunnerService;


    @RequestMapping(value = "/balancehistory", method = RequestMethod.GET)
    public List<BalanceHistory> getBalanceHistory() {
        return balanceRepository.findAll();
    }

    @RequestMapping(value = "/balances", method = RequestMethod.GET)
    public Map<String, Balances> getBalances() {
        return exchangeProvider.getExchanges().parallelStream()
                .collect(Collectors.toMap(ExchangeClient::getName, this::getBalances));
    }

    private Balances getBalances(ExchangeClient exchangeClient) {
        try {
            return exchangeClient.getBalances();
        } catch(Exception e) {
            return new Balances();
        }
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public List<TaskWrapper> getTasks() {
        return taskRunnerService.getTasks();
    }

}
