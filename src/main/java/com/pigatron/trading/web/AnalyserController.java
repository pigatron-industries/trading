package com.pigatron.trading.web;

import com.pigatron.trading.analysis.MacdAnalyser;
import com.pigatron.trading.analysis.MacdData;
import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.ChartData;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/chart")
public class AnalyserController {

    @Autowired
    private ExchangeProvider exchangeProvider;

    @Autowired
    private MacdAnalyser macdAnalyser;


    @RequestMapping(value = "/{exchangeName}/{currency1}_{currency2}", method = RequestMethod.GET)
    public ChartData getChartData(@PathVariable String exchangeName, @PathVariable String currency1,
                                     @PathVariable String currency2) throws NotImplementedException {
        ExchangeClient exchange = exchangeProvider.getExchange(exchangeName);
        return exchange.getChartData(new CurrencyPair(currency1, currency2), 52, 900);
    }

    @RequestMapping(value = "/{exchangeName}/{currency1}_{currency2}/macd", method = RequestMethod.GET)
    public MacdData getChartDataMacd(@PathVariable String exchangeName, @PathVariable String currency1,
                                     @PathVariable String currency2) throws NotImplementedException {
        ExchangeClient exchange = exchangeProvider.getExchange(exchangeName);
        ChartData chartData = exchange.getChartData(new CurrencyPair(currency1, currency2), 52, 900);
        return macdAnalyser.calculateMacdData(macdAnalyser.calculateOHLC4(chartData));
    }

    @RequestMapping(value = "/{exchangeName}/{currency1}_{currency2}/ema{n}", method = RequestMethod.GET)
    public List<Double> getChartDataEma(@PathVariable String exchangeName, @PathVariable String currency1,
                                     @PathVariable String currency2, @PathVariable int n) throws NotImplementedException {
        ExchangeClient exchange = exchangeProvider.getExchange(exchangeName);
        List<Double> close = exchange.getChartData(new CurrencyPair(currency1, currency2), 52, 900).getClose();
        return macdAnalyser.calculateEMA(close, n);
    }

}
