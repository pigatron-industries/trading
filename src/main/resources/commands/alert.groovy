package commands

import com.pigatron.trading.shell.TradingCommands
import com.pigatron.trading.tasks.impl.PlaceAheadMode
import org.crsh.cli.Command
import org.crsh.cli.Option
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class alert {

    @Usage("alert dump")
    @Command
    def dump(@Required @Option(names=["t","sampleTime"]) int sampleTime,
             @Required @Option(names=["d","dropPercent"]) String dropPercent,
             InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        tradingCommands.dumpAlert(sampleTime, new BigDecimal(dropPercent));
    }

}
