package commands

import com.pigatron.trading.exchanges.ExchangeClient
import com.pigatron.trading.exchanges.entity.PlacedOrder
import com.pigatron.trading.shell.ShellState
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class myorders {

    @Usage("myorders")
    @Command
    def main(InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        ShellState shellState = beanFactory.getBean(ShellState.class);
        ExchangeClient exchange = shellState.getExchange();

        List<PlacedOrder> orders = exchange.getOpenOrders(shellState.getCurrencyPair())

        String output = "";
        for(PlacedOrder order : orders) {
            output += order.getCurrencyPair().toString() + "> " + order.getType().name() + ": Price = " + order.getPrice().toPlainString() +
                    "  Amount = " + order.getAmount().toPlainString() + "\n";
        }


        return output;
    }

}
