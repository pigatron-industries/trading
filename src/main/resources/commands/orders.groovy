package commands

import com.pigatron.trading.exchanges.ExchangeClient
import com.pigatron.trading.exchanges.entity.MarketOrder
import com.pigatron.trading.exchanges.entity.OrderBook
import com.pigatron.trading.shell.ShellState
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class orders {

    @Usage("orders")
    @Command
    def main(InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        ShellState shellState = beanFactory.getBean(ShellState.class);
        ExchangeClient exchange = shellState.getExchange();

        OrderBook orderBook = exchange.getOrderBook(shellState.getCurrencyPair(), 10);

        String output = "\nAsks:\n";
        for(MarketOrder marketOrder : orderBook.getAsks()) {
            output += "Price: " + marketOrder.getPrice() + "  Quantity: " + marketOrder.getAmount() + "\n";
        }
        output += "\nBids:\n";
        for(MarketOrder marketOrder : orderBook.getBids()) {
            output += "Price: " + marketOrder.getPrice() + "  Quantity: " + marketOrder.getAmount() + "\n";
        }

        return output;
    }

}
