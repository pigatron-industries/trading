package commands

import com.pigatron.trading.exchanges.ExchangeClient
import com.pigatron.trading.exchanges.ExchangeProvider
import com.pigatron.trading.exchanges.entity.Balance
import com.pigatron.trading.exchanges.entity.Balances
import com.pigatron.trading.shell.ShellState
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class balance {

    @Usage("balance")
    @Command
    def main(InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        ExchangeProvider exchangeProvider = beanFactory.getBean(ExchangeProvider.class);

        String output = "\n";
        for(ExchangeClient exchange : exchangeProvider.getExchanges()) {
            Balances balances = exchange.getBalances();

            output += "Exchange: " + exchange.getName() + "\n";
            for(Map.Entry<String, Balance> balance : balances.getBalances().entrySet()) {
                String currency = balance.getKey();
                BigDecimal available = balance.getValue().getAvailable();
                BigDecimal onOrders = balance.getValue().getOnOrders();
                if(available.doubleValue() > 0 || onOrders.doubleValue() > 0) {
                    output += currency + "  Avaliable: " + available.toPlainString() + "  On Orders: " + onOrders.toPlainString() +
                            "  Total: " + available.add(onOrders).toPlainString() + "\n";
                }
            }
            output += "\n";
        }

        return output;
    }

}
