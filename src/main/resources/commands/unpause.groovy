package commands

import com.pigatron.trading.tasks.TaskRunnerService
import org.crsh.cli.Argument;
import org.crsh.cli.Command
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class unpause {

    @Usage("pause [taskid]")
    @Command
    def main(@Argument @Required String processId, InvocationContext context) {

        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TaskRunnerService taskRunnerService = beanFactory.getBean(TaskRunnerService.class);

        if(processId == "all") {
            taskRunnerService.unpauseAllTasks();
        } else {
            taskRunnerService.unpauseTask(Integer.parseInt(processId));
        }
    }

}
