package commands

import com.pigatron.trading.shell.TradingCommands
import com.pigatron.trading.tasks.impl.PlaceAheadMode
import org.crsh.cli.Command
import org.crsh.cli.Option
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class run {

    @Usage("cycle buylow")
    @Command
    def buylow(@Required @Option(names=["a","amount"]) String amount,
            @Option(names=["i","sellIncrement"]) String sellIncrement,
            @Option(names=["s","spread"]) String minSpread,
            @Option(names=["g","aggression"]) String aggressionPercent,
            @Option(names=["f","fee"]) String subtractFeeFromSell,
            @Option(names=["h","ahead"]) String placeAheadMode,
            InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal amountDec = new BigDecimal(amount);
        BigDecimal spreadDec = new BigDecimal("0.001");
        if(minSpread != null) {
            spreadDec = new BigDecimal(minSpread);
        }

        BigDecimal sellIncrementDec = new BigDecimal("0");
        if(sellIncrement != null) {
            sellIncrementDec = new BigDecimal(sellIncrement);
        }

        PlaceAheadMode placeAheadModeEnum = PlaceAheadMode.VOLUME;
        if(placeAheadMode != null) {
            placeAheadModeEnum = PlaceAheadMode.valueOf(placeAheadMode.toUpperCase());
        }

        if(subtractFeeFromSell == null) {
            subtractFeeFromSell = "true";
        }

        BigDecimal aggressionPercentDec = new BigDecimal("0");
        if(aggressionPercent != null) {
            aggressionPercentDec = new BigDecimal(aggressionPercent);
        }

        tradingCommands.buyLow(amountDec, sellIncrementDec, spreadDec, placeAheadModeEnum, Boolean.parseBoolean(subtractFeeFromSell), aggressionPercentDec);
    }


}
