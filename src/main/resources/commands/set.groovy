package commands

import com.pigatron.trading.exchanges.entity.CurrencyPair
import com.pigatron.trading.shell.ShellState
import org.crsh.cli.Argument;
import org.crsh.cli.Command
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class set {

    @Usage("set exchange [exchangename] - Set the current exchange")
    @Command
    def exchange(@Argument @Required String exchangeName, InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        ShellState shellState = beanFactory.getBean(ShellState.class);
        shellState.setExchange(exchangeName);
        return "exchange = " + shellState.getExchange().getName();
    }

    @Usage("set currency [currency1] [currency2]")
    @Command
    def currency(@Argument @Required String currency1,
                 @Argument @Required String currency2,
                 InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        ShellState shellState = beanFactory.getBean(ShellState.class);
        shellState.setCurrencyPair(new CurrencyPair(currency1, currency2));
        return "currency pair = " + shellState.getCurrencyPair().toString();
    }

}
