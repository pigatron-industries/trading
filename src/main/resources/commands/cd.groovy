package commands

import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext


class cd {

    @Usage("cd spacebtc_EUR_BTC")
    @Command
    def spacebtc_EUR_BTC(InvocationContext context) {
        new set().exchange("spacebtc", context);
        new set().currency("EUR", "BTC", context);
    }

    @Usage("cd spacebtc_EUR_ETH")
    @Command
    def spacebtc_EUR_ETH(InvocationContext context) {
        new set().exchange("spacebtc", context);
        new set().currency("EUR", "ETH", context);
    }

    @Usage("cd spacebtc_EUR_LTC")
    @Command
    def spacebtc_EUR_LTC(InvocationContext context) {
        new set().exchange("spacebtc", context);
        new set().currency("EUR", "LTC", context);
    }

    @Usage("cd gdax_GBP_BTC")
    @Command
    def gdax_GBP_BTC(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("GBP", "BTC", context);
    }

    @Usage("cd gdax_EUR_BTC")
    @Command
    def gdax_EUR_BTC(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("EUR", "BTC", context);
    }

    @Usage("cd gdax_BTC_ETH")
    @Command
    def gdax_BTC_ETH(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("BTC", "ETH", context);
    }

    @Usage("cd gdax_BTC_LTC")
    @Command
    def gdax_BTC_LTC(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("BTC", "LTC", context);
    }

    @Usage("cd gdax_EUR_ETH")
    @Command
    def gdax_EUR_ETH(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("EUR", "ETH", context);
    }

    @Usage("cd gdax_EUR_LTC")
    @Command
    def gdax_EUR_LTC(InvocationContext context) {
        new set().exchange("gdax", context);
        new set().currency("EUR", "LTC", context);
    }

    @Usage("cd cexio_EUR_BTC")
    @Command
    def cexio_EUR_BTC(InvocationContext context) {
        new set().exchange("cexio", context);
        new set().currency("EUR", "BTC", context);
    }

    @Usage("cd cexio_USD_BTC")
    @Command
    def cexio_USD_BTC(InvocationContext context) {
        new set().exchange("cexio", context);
        new set().currency("USD", "BTC", context);
    }

    @Usage("cd livecoin_USD_BTC")
    @Command
    def livecoin_USD_BTC(InvocationContext context) {
        new set().exchange("livecoin", context);
        new set().currency("USD", "BTC", context);
    }

    @Usage("cd livecoin_USD_LTC")
    @Command
    def livecoin_USD_LTC(InvocationContext context) {
        new set().exchange("livecoin", context);
        new set().currency("USD", "LTC", context);
    }

    @Usage("cd livecoin_BTC_LTC")
    @Command
    def livecoin_BTC_LTC(InvocationContext context) {
        new set().exchange("livecoin", context);
        new set().currency("BTC", "LTC", context);
    }

    @Usage("cd poloniex_USDT_BTC")
    @Command
    def poloniex_USDT_BTC(InvocationContext context) {
        new set().exchange("poloniex", context);
        new set().currency("USDT", "BTC", context);
    }

    @Usage("cd poloniex_BTC_LTC")
    @Command
    def poloniex_BTC_LTC(InvocationContext context) {
        new set().exchange("poloniex", context);
        new set().currency("BTC", "LTC", context);
    }

    @Usage("cd zaif_JPY_BTC")
    @Command
    def zaif_JPY_BTC(InvocationContext context) {
        new set().exchange("zaif", context);
        new set().currency("JPY", "BTC", context);
    }

    @Usage("cd quoine_USD_BTC")
    @Command
    def quoine_USD_BTC(InvocationContext context) {
        new set().exchange("quoine", context);
        new set().currency("USD", "BTC", context);
    }

    @Usage("cd quoine_JPY_BTC")
    @Command
    def quoine_JPY_BTC(InvocationContext context) {
        new set().exchange("quoine", context);
        new set().currency("JPY", "BTC", context);
    }

    @Usage("cd kraken_GBP_BTC")
    @Command
    def kraken_GBP_BTC(InvocationContext context) {
        new set().exchange("kraken", context);
        new set().currency("GBP", "BTC", context);
    }

    @Usage("cd coinfloor_GBP_BTC")
    @Command
    def coinfloor_GBP_BTC(InvocationContext context) {
        new set().exchange("coinfloor", context);
        new set().currency("GBP", "BTC", context);
    }

    @Usage("cd liqui_USDT_BTC")
    @Command
    def liqui_USDT_BTC(InvocationContext context) {
        new set().exchange("liqui", context);
        new set().currency("USDT", "BTC", context);
    }

    @Usage("cd liqui_USDT_ETH")
    @Command
    def liqui_USDT_ETH(InvocationContext context) {
        new set().exchange("liqui", context);
        new set().currency("USDT", "ETH", context);
    }

    @Usage("cd hitbtc_USD_BTC")
    @Command
    def hitbtc_USD_BTC(InvocationContext context) {
        new set().exchange("hitbtc", context);
        new set().currency("USD", "BTC", context);
    }

    @Usage("cd hitbtc_BTC_BCC")
    @Command
    def hitbtc_BTC_BCC(InvocationContext context) {
        new set().exchange("hitbtc", context);
        new set().currency("BTC", "BCC", context);
    }

}
