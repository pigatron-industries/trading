package commands

import com.pigatron.trading.stats.entity.BalanceHistory
import com.pigatron.trading.stats.entity.TradeHistory
import com.pigatron.trading.stats.entity.DayStats
import com.pigatron.trading.stats.StatsService
import org.crsh.cli.Argument
import org.crsh.cli.Command
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class stats {

    @Usage("run test")
    @Command
    def trades(@Argument @Required int days, InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        StatsService stats = beanFactory.getBean(StatsService.class);

        String output = "";
        List<TradeHistory> trades = stats.getLatestTrades(days);
        for(TradeHistory trade : trades) {
            output += trade.toString() + "\n";
        }
        return output;
    }

    @Usage("stats daily [days]")
    @Command
    def daily(@Argument @Required int days, InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        StatsService stats = beanFactory.getBean(StatsService.class);

        String output = "";
        List<DayStats> allStats = stats.getDailyStats(days);
        for(DayStats dayStats : allStats) {
            output += dayStats.toString() + "\n";
        }
        return output;
    }

    @Usage("stats balance [days]")
    @Command
    def balance(@Argument @Required int days, InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        StatsService stats = beanFactory.getBean(StatsService.class);

        String output = "";
        List<BalanceHistory> balances = stats.getBalanceHistory(days);
        for(BalanceHistory balance : balances) {
            output += balance.toString() + "\n";
        }
        return output;
    }

}
