package commands

import com.pigatron.trading.maintenance.MaintenanceService
import org.crsh.cli.Argument
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class fix {

    @Usage("fix")
    @Command
    def main(@Argument String fixName, InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        MaintenanceService maintenanceService = beanFactory.getBean(MaintenanceService.class);
        return maintenanceService.runFix(fixName);
    }

}
