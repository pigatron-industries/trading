package commands

import com.pigatron.trading.exchanges.ExchangeDataSubscriber
import com.pigatron.trading.shell.ShellState
import com.pigatron.trading.tasks.TaskRunnerService
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class ls {

    @Usage("ls - Lists running task")
    @Command
    def main(InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TaskRunnerService taskRunner = beanFactory.getBean(TaskRunnerService.class);
        ShellState shellState = beanFactory.getBean(ShellState.class);

        List<ExchangeDataSubscriber> tasks = taskRunner.getTasks(shellState.getExchange().getName(), shellState.getCurrencyPair());

        String exchangeName = shellState.getExchange().getName();
        String currencyPair = shellState.getCurrencyPair();

        String output = "   " + exchangeName + "/" + currencyPair + ">";
        for(ExchangeDataSubscriber task : tasks) {
            output += task.toString();
        }

        return output;
    }

}