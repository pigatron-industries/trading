package commands

import com.pigatron.trading.tasks.TaskContext

prompt = { ->
    String prompt = "";
    prompt += TaskContext.getShellState().getExchange() != null ? TaskContext.getShellState().getExchange().getName() : "null";
    prompt += "/";
    prompt += TaskContext.getShellState().getCurrencyPair() != null ? TaskContext.getShellState().getCurrencyPair().toString() : "null"
    prompt += "> "
    return prompt;
}

welcome = { ->
    return "Hello.\n";
}