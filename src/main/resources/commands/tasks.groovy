package commands

import com.pigatron.trading.exchanges.ExchangeDataSubscriber
import com.pigatron.trading.tasks.TaskRunnerService
import com.pigatron.trading.tasks.TaskWrapper
import org.crsh.cli.Command
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class tasks {

    @Usage("tasks - Lists all running tasks")
    @Command
    def main(InvocationContext context) {

        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TaskRunnerService taskRunner = beanFactory.getBean(TaskRunnerService.class);

        List<TaskWrapper> tasks = taskRunner.getTasks();

        String output = "";
        for(TaskWrapper task : tasks) {
            String exchangeName = task.getExchange().getName();
            String currencyPair = task.getCurrencyPair().toString();
            ExchangeDataSubscriber subscriber = task.getExchangeDataSubscriber();
            output += "   " + subscriber.getProcessId() + ":" + exchangeName + "/" + currencyPair + ">\n" + subscriber.toString() + "\n";
        }

        return output;
    }

}
