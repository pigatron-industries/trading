package commands

import com.pigatron.trading.shell.TradingCommands
import com.pigatron.trading.tasks.impl.PlaceAheadMode
import org.crsh.cli.Command
import org.crsh.cli.Option
import org.crsh.cli.Required
import org.crsh.cli.Usage
import org.crsh.command.InvocationContext
import org.springframework.beans.factory.BeanFactory


class cycle {


    @Usage("cycle run")
    @Command
    def run(@Required @Option(names=["a","amount"]) String amount,
            @Option(names=["b","sellAmount"]) String sellAmount,
            @Option(names=["s","spread"]) String minSpread,
            @Option(names=["g","aggression"]) String aggressionPercent,
            @Option(names=["f","fee"]) String subtractFeeFromSell,
            @Option(names=["h","ahead"]) String placeAheadMode,
            @Option(names=["p","emaPeriods"]) String emaPeriods,
            InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal amountDec = new BigDecimal(amount);
        BigDecimal spreadDec = new BigDecimal("0.001");
        if(minSpread != null) {
            spreadDec = new BigDecimal(minSpread);
        }

        BigDecimal sellAmountDec = new BigDecimal("0");
        if(sellAmount != null) {
            sellAmountDec = new BigDecimal(sellAmount);
        }

        PlaceAheadMode placeAheadModeEnum = PlaceAheadMode.VOLUME;
        if(placeAheadMode != null) {
            placeAheadModeEnum = PlaceAheadMode.valueOf(placeAheadMode.toUpperCase());
        }

        if(subtractFeeFromSell == null) {
            subtractFeeFromSell = "false";
        }

        BigDecimal aggressionPercentDec = new BigDecimal("0");
        if(aggressionPercent != null) {
            aggressionPercentDec = new BigDecimal(aggressionPercent);
        }

        int emaPeriodsInt = 60;
        if(emaPeriods != null) {
            emaPeriodsInt = Integer.parseInt(emaPeriods);
        }

        tradingCommands.cycle(amountDec, sellAmountDec, spreadDec, placeAheadModeEnum,
                              Boolean.parseBoolean(subtractFeeFromSell), aggressionPercentDec, emaPeriodsInt);
    }


    @Usage("cycle test")
    @Command
    def test() {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        return tradingCommands.calculateMaxTradeAmount();
    }

    @Usage("cycle autospread")
    @Command
    def autospread(@Option(names=["a","amount"]) String amount,
                   @Option(names=["b","sellAmount"]) String sellAmount,
                   @Option(names=["s","minSpread"]) String minSpread,
                   @Option(names=["d","depthSpread"]) String depthSpread,
                   @Option(names=["n","numOrders"]) String numOrders,
                   @Option(names=["v","numVisible"]) String numVisible,
                   @Option(names=["f","fee"]) String subtractFeeFromSell,
                   @Option(names=["h","ahead"]) String placeAheadMode,
                   @Option(names=["l","stopLoss"]) String stopLossPercent,
                   @Option(names=["p","emaPeriods"]) String emaPeriods,
                   @Option(names=["g","filterAmount"]) String filterAmount,
                   @Option(names=["e","followExchange"]) String followExchange,
                   @Option(names=["c","followCurrency"]) String followCurrency,
                   @Option(names=["i","balancePercent"]) String balancePercent,
                   @Option(names=["x", "maxOrderPercent"]) String maxOrderPercent,
                   InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        PlaceAheadMode placeAheadModeEnum = PlaceAheadMode.VOLUME;
        if(placeAheadMode != null) {
            placeAheadModeEnum = PlaceAheadMode.valueOf(placeAheadMode.toUpperCase());
        }

        if(subtractFeeFromSell == null) {
            subtractFeeFromSell = "false";
        }

        tradingCommands.autospread(toBigDecimal(amount, BigDecimal.ONE), toBigDecimal(sellAmount, "0"), toBigDecimal(minSpread),
                toBigDecimal(depthSpread), toInteger(numOrders, 10), toInteger(numVisible, numOrders),
                toBigDecimal(stopLossPercent), placeAheadModeEnum, Boolean.parseBoolean(subtractFeeFromSell),
                toInteger(emaPeriods, 60), toBigDecimal(filterAmount), followExchange, followCurrency,
                toBigDecimal(maxOrderPercent, "1"));
    }

    def toBigDecimal(param) {
        if(param != null) {
            return new BigDecimal(param);
        } else {
            return null;
        }
    }

    def toBigDecimal(param, defaultVal) {
        if(param != null) {
            return new BigDecimal(param);
        } else {
            return new BigDecimal(defaultVal);
        }
    }

    def toInteger(param, defaultVal) {
        if(param != null) {
            return Integer.parseInt(param);
        } else {
            return defaultVal;
        }
    }

    @Usage("cycle volume")
    @Command
    def volume(@Required @Option(names=["a","amount"]) String amount,
            @Option(names=["b","sellAmount"]) String sellAmount,
            @Option(names=["v","volumeFloor"]) String volumeFloor,
            @Option(names=["f","fee"]) String subtractFeeFromSell,
            @Option(names=["h","ahead"]) String placeAheadMode,
            InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal amountDec = new BigDecimal(amount);
        BigDecimal volumeFloorDec = new BigDecimal("5");
        if(volumeFloorDec != null) {
            volumeFloorDec = new BigDecimal(volumeFloor);
        }

        BigDecimal sellAmountDec = new BigDecimal("0");
        if(sellAmount != null) {
            sellAmountDec = new BigDecimal(sellAmount);
        }

        PlaceAheadMode placeAheadModeEnum = PlaceAheadMode.VOLUME;
        if(placeAheadMode != null) {
            placeAheadModeEnum = PlaceAheadMode.valueOf(placeAheadMode.toUpperCase());
        }

        if(subtractFeeFromSell == null) {
            subtractFeeFromSell = "false";
        }

        tradingCommands.cycleVolumeFloor(amountDec, sellAmountDec, volumeFloorDec, placeAheadModeEnum, Boolean.parseBoolean(subtractFeeFromSell));
    }

    @Usage("cycle minspread")
    @Command
    def minspread(@Required @Option(names=["a", "buyamount"]) String buyAmount,
                  @Required @Option(names=["b", "sellamount"]) String sellAmount,
                  @Required @Option(names=["s", "spread"]) String fixedSpread,
                  InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        tradingCommands.cycleMinSpread(new BigDecimal(buyAmount), new BigDecimal(sellAmount), new BigDecimal(fixedSpread));
    }

    @Usage("cycle buy")
    @Command
    def buy(@Required @Option(names=["a","amount"]) String amount,
            @Option(names=["d","depth"]) String depthPercent,
            InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal depthPercentDec = null;
        if(depthPercent != null) {
            depthPercentDec = new BigDecimal(depthPercent);
        }
        tradingCommands.cycleBuy(new BigDecimal(amount), depthPercentDec);
    }

    @Usage("cycle sell")
    @Command
    def sell(@Required @Option(names=["t","task"]) int id,
             @Required @Option(names=["a","amount"]) String amount,
             InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        BigDecimal amountDec = new BigDecimal(amount);

        tradingCommands.cycleSell(id, amountDec);
    }

    @Usage("cycle selladjust")
    @Command
    def selladjust(@Required @Option(names=["t","task"]) int id,
                   @Required @Option(names=["a","amount"]) String amount,
                   InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        BigDecimal amountDec = new BigDecimal(amount);

        tradingCommands.cycleSellAdjust(id, amountDec);
    }

    @Usage("cycle buyadjust")
    @Command
    def buyadjust(@Required @Option(names=["t","task"]) int id,
                  @Required @Option(names=["a","amount"]) String amount,
                  InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        BigDecimal amountDec = new BigDecimal(amount);

        tradingCommands.cycleBuyAdjust(id, amountDec);
    }

    @Usage("cycle autoadjust")
    @Command
    def autoadjust(@Required @Option(names=["t","task"]) int id,
                   @Required @Option(names=["a","amount"]) String amount,
                   InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        BigDecimal amountDec = new BigDecimal(amount);

        tradingCommands.autoadjust(id, amountDec);
    }

    @Usage("cycle redistribute")
    @Command
    def redistribute(@Required @Option(names=["t","task"]) int id,
                   InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        tradingCommands.redistribute(id);
    }

    @Usage("cycle reset")
    @Command
    def reset(@Required @Option(names=["t","task"]) int id,
              @Option(names=["a","amount"]) String amount,
                     InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal amountDec = null;
        if(amount != null) {
            amountDec = new BigDecimal(amount);
        }

        tradingCommands.reset(id, amountDec);
    }

    @Usage("cycle resettop")
    @Command
    def resetTop(@Required @Option(names=["t","task"]) int id,
              @Option(names=["a","amount"]) String amount,
              InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);

        BigDecimal amountDec = null;
        if(amount != null) {
            amountDec = new BigDecimal(amount);
        }

        tradingCommands.resetFromTop(id, amountDec);
    }

    @Usage("cycle adjustspread")
    @Command
    def adjustspread(@Required @Option(names=["t","task"]) int id,
                     @Required @Option(names=["s","minSpread"]) String minSpread,
                     @Required @Option(names=["d","depthSpread"]) String depthSpread,
                     @Required @Option(names=["l","minorStopLoss"]) String minorStopLossPercent,
                     InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        BigDecimal minSpreadDec = new BigDecimal(minSpread);
        BigDecimal depthSpreadDec = new BigDecimal(depthSpread);
        BigDecimal minorStopLossDec = new BigDecimal(minorStopLossPercent);

        tradingCommands.adjustSpread(id, minSpreadDec, depthSpreadDec, minorStopLossDec);

    }

    @Usage("cycle alertstatus -t taskid -s [NORMAL|BEAR|BULL|VOLUME]")
    @Command
    def alertstatus(@Required @Option(names=["t","task"]) int id,
                     @Required @Option(names=["s","status"]) String mode,
                     InvocationContext context) {
        BeanFactory beanFactory = (BeanFactory) context.getAttributes().get("spring.beanfactory");
        TradingCommands tradingCommands = beanFactory.getBean(TradingCommands.class);
        tradingCommands.alertStatus(id, mode);
    }

}
