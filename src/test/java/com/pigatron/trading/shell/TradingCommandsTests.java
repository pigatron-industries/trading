package com.pigatron.trading.shell;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.tasks.impl.PlaceAheadMode;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.DeepBuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.ShallowBuyTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class TradingCommandsTests {

    @Mock
    private ShellState shellState;

    @Mock
    private ExchangeClient exchangeClient;

    @InjectMocks
    private TradingCommands tradingCommands;


    @Before
    public void setup() {
        given(shellState.getExchange()).willReturn(exchangeClient);
        given(shellState.getCurrencyPair()).willReturn(new CurrencyPair("GBP", "BTC"));
        given(exchangeClient.getPriceIncrement("BTC")).willReturn(d("0.0001"));
    }

    @Test
    public void testCreateBuyTasks() {
        List<BuyTask> buyTasks = tradingCommands.createBuyTasks(d("1"), d("0.7"), d("0.001"), d("0.004"), 5, 5, 60, d("0"), PlaceAheadMode.VOLUME, null);

        assertThat(buyTasks.size()).isEqualTo(5);
        assertThat(buyTasks.get(0).getMaxAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(0).getAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(1).getMaxAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(1).getAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(2).getMaxAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(2).getAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(3).getMaxAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(3).getAmount()).isEqualTo(d("0.1000"));
        assertThat(buyTasks.get(4).getMaxAmount()).isEqualTo(d("0.2000"));
        assertThat(buyTasks.get(4).getAmount()).isEqualTo(d("0"));
        assertThat(((ShallowBuyTask)buyTasks.get(0)).getMinSpreadPercent()).isEqualTo(d("0.001"));
        assertThat(((DeepBuyTask)buyTasks.get(1)).getDepthPercent()).isEqualTo(d("0.00100"));
        assertThat(((DeepBuyTask)buyTasks.get(2)).getDepthPercent()).isEqualTo(d("0.00100"));
        assertThat(((DeepBuyTask)buyTasks.get(3)).getDepthPercent()).isEqualTo(d("0.00100"));
        assertThat(((DeepBuyTask)buyTasks.get(4)).getDepthPercent()).isEqualTo(d("0.00100"));
    }


    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}
