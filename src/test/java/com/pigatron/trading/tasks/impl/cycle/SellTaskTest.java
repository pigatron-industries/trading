package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.tasks.impl.AlertStatus;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.pigatron.trading.exchanges.entity.OrderBook.anOrderBook;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class SellTaskTest {

    @Mock
    TradeCycleAlgo parentAlgorithm;


    @Before
    public void setup() {
        given(parentAlgorithm.getAlertStatus()).willReturn(AlertStatus.NORMAL);
    }

    @Test
    public void testGetBestSellPrice() {
        SellTask sellTask = createSellTask();
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("409.99"));
    }

    @Test
    public void testGetBestSellPrice_whenLowestAskIsLessThanBoughtPrice() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("411"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("0.1")))
                .addAsk(new MarketOrder(TradeType.sell, d("412"), d("0.1")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.1")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("411.99"));
    }

    @Test
    public void testGetBestSellPrice_when2ndLowestAskIsLessThanBoughtPrice() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("413"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("0.1")))
                .addAsk(new MarketOrder(TradeType.sell, d("412"), d("0.1")))
                .addAsk(new MarketOrder(TradeType.sell, d("414"), d("0.1")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.1")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("413.99"));
    }

    @Test
    public void testGetBestSellPrice_whenBeyondOrderBook() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("415"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("411"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("412"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("413"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("414"), d("0.6")))
                .addAsk(new MarketOrder(TradeType.sell, d("415"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("415"));
    }

    @Test
    public void testUncompensatedBoughtPrice() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("415"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("420"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("419.99"));
    }

    @Test
    public void testUseUncompensatedBoughtPrice() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("415"));
        sellTask.setCompensatedBoughtPrice(d("413"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("414"), d("0.5"))) //would use this only if lowest ask
                .addAsk(new MarketOrder(TradeType.sell, d("420"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("419.99"));
    }

    @Test
    public void testUseCompensatedBoughtPrice() {
        SellTask sellTask = createSellTask();
        sellTask.setBoughtPrice(d("415"));
        sellTask.setCompensatedBoughtPrice(d("413"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("414"), d("0.5"))) //would use this only if lowest ask
                .addAsk(new MarketOrder(TradeType.sell, d("420"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("413.99"));
    }

    @Test
    public void testWeightedFollowPriceAboveBoughPrice() {
        SellTask sellTask = createSellTask();
        given(parentAlgorithm.getFollowAskPrice()).willReturn(d("1100"));
        sellTask.setBoughtPrice(d("1000"));
        sellTask.setCompensatedBoughtPrice(d("1000"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("1200"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("1100"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("1050"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("950"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);
    }

    @Test
    public void testWeightedFollowPriceAboveBoughPriceZero() {
        SellTask sellTask = createSellTask();
        given(parentAlgorithm.getFollowAskPrice()).willReturn(d("1100"));
        sellTask.setBoughtPrice(d("0"));
        sellTask.setCompensatedBoughtPrice(d("0"));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("1200"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("1100"), d("0.5")))
                .addAsk(new MarketOrder(TradeType.sell, d("1050"), d("0.5")))
                .addBid(new MarketOrder(TradeType.buy, d("950"), d("0.5")))
                .build();

        BigDecimal price = sellTask.getBestPrice(orderBook);
    }


    private SellTask createSellTask() {
        SellTask sellTask = new SellTask();
        sellTask.setParentAlgorithm(parentAlgorithm);
        sellTask.setPriceIncrement(d("0.01"));
        sellTask.setMinSpreadPercent(d("0.001"));
        sellTask.setAmount(d("0.1"));
        return sellTask;
    }

    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}
