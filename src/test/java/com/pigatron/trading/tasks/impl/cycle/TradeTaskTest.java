package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.DeepBuyTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.fest.assertions.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class TradeTaskTest {

    @Test
    public void testOrderMatches_true_whenPriceAndAmountIsSame() {
        MarketOrder marketOrder = new MarketOrder(TradeType.buy, new BigDecimal("500"), new BigDecimal("1"));
        TradeTask orderTask = createOrderTask(new BigDecimal("500"), new BigDecimal("1"));

        boolean result = orderTask.orderMatches(marketOrder, Arrays.asList(orderTask));

        assertThat(result).isTrue();
    }

    @Test
    public void testOrderMatches_false_whenPriceIsDifferent() {
        MarketOrder marketOrder = new MarketOrder(TradeType.buy, new BigDecimal("500"), new BigDecimal("1"));
        TradeTask orderTask = createOrderTask(new BigDecimal("499"), new BigDecimal("1"));

        boolean result = orderTask.orderMatches(marketOrder, Arrays.asList(orderTask));

        assertThat(result).isFalse();
    }

    @Test
    public void testOrderMatches_false_whenAmountIsDifferent() {
        MarketOrder marketOrder = new MarketOrder(TradeType.buy, new BigDecimal("500"), new BigDecimal("1"));
        TradeTask orderTask = createOrderTask(new BigDecimal("500"), new BigDecimal("0.5"));

        boolean result = orderTask.orderMatches(marketOrder, Arrays.asList(orderTask));

        assertThat(result).isFalse();
    }


    @Test
    public void testOrderMatches_true_whenTwoOrdersSamePriceAndAmount() {
        MarketOrder marketOrder = new MarketOrder(TradeType.buy, new BigDecimal("500"), new BigDecimal("1"));
        TradeTask orderTask1 = createOrderTask(new BigDecimal("500"), new BigDecimal("0.5"));
        TradeTask orderTask2 = createOrderTask(new BigDecimal("500"), new BigDecimal("0.5"));

        boolean result = orderTask1.orderMatches(marketOrder, Arrays.asList(orderTask1, orderTask2));

        assertThat(result).isTrue();
    }

    @Test
    public void testOrderMatches_true_whenTwoOrdersDifferentAmount() {
        MarketOrder marketOrder = new MarketOrder(TradeType.buy, new BigDecimal("500"), new BigDecimal("1.5"));
        TradeTask orderTask1 = createOrderTask(new BigDecimal("500"), new BigDecimal("0.5"));
        TradeTask orderTask2 = createOrderTask(new BigDecimal("500"), new BigDecimal("0.5"));

        boolean result = orderTask1.orderMatches(marketOrder, Arrays.asList(orderTask1, orderTask2));

        assertThat(result).isFalse();
    }

    private TradeTask createOrderTask(BigDecimal price, BigDecimal amount) {
        TradeTask orderTask = new DeepBuyTask(new BigDecimal("0"), new BigDecimal("0.001"));
        orderTask.setAmount(amount);
        orderTask.setOrder(new PlacedOrder("1", TradeType.buy, new CurrencyPair("", ""), price, amount));
        return orderTask;
    }

}
