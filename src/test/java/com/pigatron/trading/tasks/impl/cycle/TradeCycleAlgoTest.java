package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.impl.gdax.converter.GdaxTradesConverter;
import com.pigatron.trading.maintenance.impl.TradeAmountUpdater;
import com.pigatron.trading.notifications.PushNotificationClient;
import com.pigatron.trading.shell.ShellState;
import com.pigatron.trading.shell.TradingCommands;
import com.pigatron.trading.tasks.TaskContext;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.impl.PlaceAheadMode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static com.pigatron.trading.exchanges.entity.OrderBook.anOrderBook;
import static com.pigatron.trading.exchanges.entity.Trade.aTrade;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TradeCycleAlgoTest {

    @Mock
    private ExchangeClient exchange;

    @Mock
    private TaskRunnerService taskRunnerService;

    @Mock
    private ShellState shellState;

    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private PushNotificationClient pushNotificationClient;

    @Mock
    private TradeAmountUpdater tradeAmountUpdater;

    @InjectMocks
    private TradingCommands tradingCommands;

    private CurrencyPair currencyPair;

    private TradeCycleAlgo tradeCycleAlgo;


    @Before
    public void setup() throws Exception {
        new TaskContext().setApplicationContext(applicationContext);
        given(applicationContext.getBean(TaskRunnerService.class)).willReturn(taskRunnerService);
        given(taskRunnerService.getPushNotificationClient()).willReturn(pushNotificationClient);
        when(exchange.getMakerFee()).thenReturn(d("0"));
        when(exchange.getMinOrderAmount("BTC")).thenReturn(d("0.01"));
        when(exchange.getPriceIncrement("GBP")).thenReturn(new BigDecimal("0.01"));
        when(exchange.getPriceIncrement("BTC")).thenReturn(new BigDecimal("0.00001"));
        currencyPair = new CurrencyPair("GBP", "BTC");
        when(shellState.getExchange()).thenReturn(exchange);
        when(shellState.getCurrencyPair()).thenReturn(currencyPair);
        when(tradeAmountUpdater.calculateMaxTradeAmount(any(ExchangeClient.class), any(CurrencyPair.class), any(BigDecimal.class), any(BigDecimal.class)))
            .thenReturn(new BigDecimal("0.4"));

        tradeCycleAlgo = tradingCommands.cycle(d("0.4"), d("0"), d("0.001"), PlaceAheadMode.VOLUME, false, d("0"), 60);

        tradeCycleAlgo.setTaskRunnerService(taskRunnerService);
    }

    @Test
    public void testPlaceInitialOrder() throws ExchangeClientException {
        when(exchange.buy(currencyPair, d("400.01"), d("0.40000")))
                .thenReturn(new PlacedOrder("1", TradeType.buy, currencyPair, d("400.01"), d("0.40000")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange).buy(currencyPair, d("400.01"), d("0.40000"));
        verify(exchange, times(0)).sell(anyObject(), anyObject(), anyObject());
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("400.01"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.4"));
    }

    @Test
    public void whenOrderBookUnchanged_thenDontMoveOrder() throws ExchangeClientException {
        testPlaceInitialOrder();
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("0.4"))) //Our inital order
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange, times(1)).buy(currencyPair, d("400.01"), d("0.40000"));
        verify(exchange, times(0)).cancelOrder("1", currencyPair);
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("400.01"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.4"));
    }

    @Test
    public void whenOrderPlacedOverOurOrder_thenDontMoveOrder() throws ExchangeClientException {
        testPlaceInitialOrder();
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("1.4"))) //Our initial order + another order
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange, times(1)).buy(currencyPair, d("400.01"), d("0.40000"));
        verify(exchange, times(0)).cancelOrder("1", currencyPair);
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("400.01"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.4"));
    }

    @Test
    public void whenOrderPlacedOverOurOrder_thenDontMoveOrder_withFullOrderBook() throws ExchangeClientException {
        testPlaceInitialOrder();
        OrderBook orderBook = anOrderBook()
                .addAsk(new PlacedOrder("1", TradeType.sell, null, d("410"), d("1")))
                .addBid(new PlacedOrder("4", TradeType.buy, null, d("400.01"), d("0.5"))) //Another order at same price
                .addBid(new PlacedOrder("2", TradeType.buy, null, d("400.01"), d("0.4"))) //Our initial order
                .addBid(new PlacedOrder("3", TradeType.buy, null, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange, times(1)).buy(currencyPair, d("400.01"), d("0.40000"));
        verify(exchange, times(0)).cancelOrder("1", currencyPair);
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("400.01"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.4"));
    }

    @Test
    public void whenOrderPlacedInFrontOfOurOrder_thenMoveOrder() throws ExchangeClientException {
        testPlaceInitialOrder();
        when(exchange.moveOrder(anyObject(), eq(d("400.03")), eq(d("0.40000"))))
                .thenReturn(new PlacedOrder("2", TradeType.buy, currencyPair, d("400.03"), d("0.4")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.02"), d("1")))   //Other new order
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("0.4"))) //Our initial order
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange, times(1)).buy(currencyPair, d("400.01"), d("0.40000")); // initial buy
        verify(exchange, times(1)).moveOrder(anyObject(),eq(d("400.03")), eq(d("0.40000"))); // move order
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("400.03"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.4"));
    }

    @Test
    public void whenBuyOrderHasNewTrades_thenSellOrderIsPlacedAndBuyOrderIsUpdated() throws ExchangeClientException {
        testPlaceInitialOrder();

        Trade trade = aTrade().withOrderId("1")
                .withTradeId("1")
                .withType(TradeType.buy)
                .withQuantity(d("0.1"))
                .withPrice(d("400.01"))
                .build();
        when(exchange.sell(currencyPair, d("409.99"), d("0.10000")))
                .thenReturn(new PlacedOrder("3", TradeType.sell, currencyPair, d("409.99"), d("0.1")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("411"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("0.3"))) //Our initial order minus new trade
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade));

        verify(exchange).sell(currencyPair, d("409.99"), d("0.10000"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("409.99"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.1"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.1"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo(d("0.3")); //Our initial order minus new trade
    }

    @Test
    public void whenOrderIsPlacedOnTopOfOurSellOrder_thenSellOrderDoesntMove() throws ExchangeClientException {
        whenBuyOrderHasNewTrades_thenSellOrderIsPlacedAndBuyOrderIsUpdated();

        PlacedOrder sellOrder = new PlacedOrder("3", TradeType.sell, currencyPair, d("409.99"), d("0.1"));
        when(exchange.sell(currencyPair, d("409.99"), d("0.10000")))
                .thenReturn(sellOrder);
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("409.99"), d("1.1"))) // Our current sell order plus another order
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("0.3"))) //Our initial buy order minus new trade
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, new ArrayList<>());

        verify(exchange, times(0)).moveOrder(eq(sellOrder), anyObject(), anyObject());
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getPrice()).isEqualByComparingTo(d("409.99"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.1"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.1"));
    }

    @Test
    public void whenBuyOrderHasNewTradesForFullAmount_thenSellOrderIsUpdatedAndBuyOrderIsFinished() throws ExchangeClientException {
        whenBuyOrderHasNewTrades_thenSellOrderIsPlacedAndBuyOrderIsUpdated();

        Trade trade = aTrade().withOrderId("1")
                .withTradeId("2")
                .withType(TradeType.buy)
                .withQuantity(d("0.3"))
                .withPrice(d("400.01"))
                .build();
        when(exchange.moveOrder(anyObject(), eq(d("409.99")), eq(d("0.40000"))))
                .thenReturn(new PlacedOrder("3", TradeType.sell, currencyPair, d("409.99"), d("0.4")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("409.99"), d("0.1"))) // Our current sell order
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade));

        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo(d("0.0"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.4"));
        verify(exchange).moveOrder(anyObject(), eq(d("409.99")), eq(d("0.40000")));
    }

    @Test
    public void whenBuyOrderHasNewTrades_thenSellOrderBoughtPriceIsSetToAverage() throws ExchangeClientException {
        testPlaceInitialOrder();
        Trade trade1 = new Trade();
        trade1.setTradeId("1");
        trade1.setOrderId("1");
        trade1.setQuantity(d("0.1"));
        trade1.setPrice(d("400.01"));
        trade1.setType(TradeType.buy);
        Trade trade2 = new Trade();
        trade2.setTradeId("2");
        trade2.setOrderId("1");
        trade2.setQuantity(d("0.2"));
        trade2.setPrice(d("400.03"));
        trade2.setType(TradeType.buy);

        when(exchange.sell(currencyPair, d("409.99"), d("0.30000")))
                .thenReturn(new PlacedOrder("3", TradeType.sell, currencyPair, d("409.99"), d("0.3")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("411"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("400.01"), d("0.3"))) //Our initial order minus new trade
                .addBid(new MarketOrder(TradeType.buy, d("400"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade1, trade2));

        assertThat(tradeCycleAlgo.getSellTasks().get(0).getBoughtPrice()).isEqualByComparingTo(d("400.02")); //Average bought price
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.3")); //Total trades amount
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.3")); //Total trades amount
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo("0.1"); //Our initial order minus new trade
    }

    @Test
    public void whenBuyOrderHasNewTrades_andSellOrderIsSplit() throws ExchangeClientException {
        testPlaceInitialOrder();
        tradeCycleAlgo.setMinorStopLossPercent(new BigDecimal("1")); //prevent redistribution
        Trade trade1 = new Trade();
        trade1.setTradeId("1");
        trade1.setOrderId("1");
        trade1.setQuantity(d("0.1"));
        trade1.setPrice(d("410.00"));
        trade1.setType(TradeType.buy);
        Trade trade2 = new Trade();
        trade2.setTradeId("2");
        trade2.setOrderId("1");
        trade2.setQuantity(d("0.2"));
        trade2.setPrice(d("400.00"));
        trade2.setType(TradeType.buy);

        when(exchange.sell(currencyPair, d("409.99"), d("0.20000")))
                .thenReturn(new PlacedOrder("2", TradeType.sell, currencyPair, d("409.99"), d("0.2")));
        when(exchange.sell(currencyPair, d("410.00"), d("0.10000")))
                .thenReturn(new PlacedOrder("3", TradeType.sell, currencyPair, d("410.00"), d("0.1")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("400"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("390"), d("0.3")))
                .addBid(new MarketOrder(TradeType.buy, d("380"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade1));
        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade2));

        assertThat(tradeCycleAlgo.getSellTasks().get(0).getBoughtPrice()).isEqualByComparingTo(d("400.00"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.2"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.2"));
        assertThat(tradeCycleAlgo.getSellTasks().get(1).getBoughtPrice()).isEqualByComparingTo(d("410.00"));
        assertThat(tradeCycleAlgo.getSellTasks().get(1).getOrder().getAmount()).isEqualByComparingTo(d("0.1"));
        assertThat(tradeCycleAlgo.getSellTasks().get(1).getAmount()).isEqualByComparingTo(d("0.1"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo("0.1"); //Our initial order minus new trades
    }

    @Test
    public void whenBuyOrderHasNewTrades_andSellOrderIsNotSplit() throws ExchangeClientException {
        testPlaceInitialOrder();
        tradeCycleAlgo.setMinorStopLossPercent(new BigDecimal("1")); //prevent redistribution
        Trade trade1 = new Trade();
        trade1.setTradeId("1");
        trade1.setOrderId("1");
        trade1.setQuantity(d("0.1"));
        trade1.setPrice(d("410.00"));
        trade1.setType(TradeType.buy);
        Trade trade2 = new Trade();
        trade2.setTradeId("2");
        trade2.setOrderId("1");
        trade2.setQuantity(d("0.2"));
        trade2.setPrice(d("409.50"));
        trade2.setType(TradeType.buy);

        when(exchange.sell(currencyPair, d("409.99"), d("0.30000")))
                .thenReturn(new PlacedOrder("2", TradeType.sell, currencyPair, d("410"), d("0.3")));
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("400"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("410"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("390"), d("0.3")))
                .addBid(new MarketOrder(TradeType.buy, d("380"), d("1")))
                .build();

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade1));
        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList(trade2));

        assertThat(tradeCycleAlgo.getSellTasks().get(0).getBoughtPrice()).isEqualByComparingTo(d("409.67"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getOrder().getAmount()).isEqualByComparingTo(d("0.3"));
        assertThat(tradeCycleAlgo.getSellTasks().get(0).getAmount()).isEqualByComparingTo(d("0.3"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo("0.1"); //Our initial order minus new trades
    }

    @Test
    public void testEmergencyStop_whenLowestAskIsBelowBoughtPrice() throws Exception {
        whenBuyOrderHasNewTrades_thenSellOrderIsPlacedAndBuyOrderIsUpdated(); // bought price = 400.01

        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.001"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, d("1"));
        tradeCycleAlgo.getBuyTasks().clear();
        tradeCycleAlgo.getBuyTasks().addAll(autospread.getBuyTasks());
        tradeCycleAlgo.getBuyTasks().get(0).setAmount(d("0"));
        tradeCycleAlgo.getBuyTasks().get(1).setAmount(d("0.5"));
        tradeCycleAlgo.getBuyTasks().get(2).setAmount(d("1"));
        tradeCycleAlgo.getBuyTasks().get(0).setMaxAmount(d("1"));
        tradeCycleAlgo.getBuyTasks().get(1).setMaxAmount(d("1"));
        tradeCycleAlgo.getBuyTasks().get(2).setMaxAmount(d("1"));

        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("399"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("400"), d("1"))) // Ask less than 0.2% below bought price
                .addBid(new MarketOrder(TradeType.buy, d("398"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("397"), d("1")))
                .build();

        for(int i = 0; i < 1808; i++) {
            tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList());
            //System.out.println(tradeCycleAlgo.getVolatility());
        }

        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo(d("0"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(1).getAmount()).isEqualByComparingTo(d("0.5"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(2).getAmount()).isEqualByComparingTo(d("1"));

        tradeCycleAlgo.onOrderBookChanged(orderBook, Arrays.asList());

        assertThat(tradeCycleAlgo.getBuyTasks().get(0).getAmount()).isEqualByComparingTo(d("1"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(1).getAmount()).isEqualByComparingTo(d("0.5"));
        assertThat(tradeCycleAlgo.getBuyTasks().get(2).getAmount()).isEqualByComparingTo(d("0"));
    }

    @Test
    public void testRedistributeBuyOrders() throws Exception {
        when(tradeAmountUpdater.calculateMaxTradeAmount(any(ExchangeClient.class), any(CurrencyPair.class), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(new BigDecimal("3"));
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.001"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, null);
        autospread.getBuyTasks().get(0).setAmount(d("0"));
        autospread.getBuyTasks().get(1).setAmount(d("0.5"));
        autospread.getBuyTasks().get(2).setAmount(d("1"));

        autospread.redistribute();

        assertThat(autospread.getBuyTasks().get(0).getAmount()).isEqualByComparingTo(d("1.00000"));
        assertThat(autospread.getBuyTasks().get(1).getAmount()).isEqualByComparingTo(d("0.50000"));
        assertThat(autospread.getBuyTasks().get(2).getAmount()).isEqualByComparingTo(d("0"));
    }

    @Test
    public void testUnredistributedTrade() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.01"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, d("1"));
        autospread.setSellTaskSplitPercent(d("0.2"));
        autospread.getSellTask().setAmount(d("0.2"));
        autospread.getSellTask().setBoughtPrice(d("401"));

        Trade trade = aTrade().withOrderId("1")
                .withTradeId("2")
                .withType(TradeType.buy)
                .withQuantity(d("0.1"))
                .withPrice(d("400"))
                .build();
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("400"), d("1")))
                .addAsk(new MarketOrder(TradeType.sell, d("402"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("390"), d("1")))
                .build();

        autospread.onOrderBookChanged(orderBook, Arrays.asList()); // nothing happens on first run
        autospread.onOrderBookChanged(orderBook, Arrays.asList(trade));

        assertThat(autospread.getSellTask().getAmount()).isEqualTo(d("0.3"));
        assertThat(autospread.getSellTask().getBoughtPrice()).isEqualTo(d("400.70")); //average
        assertThat(autospread.getSellTasks().size()).isEqualTo(1);
        verify(exchange).sell(currencyPair, d("401.99"), d("0.20000"));
        verify(exchange).sell(currencyPair, d("401.99"), d("0.30000"));

        trade = aTrade().withOrderId("1")
                .withTradeId("3")
                .withType(TradeType.sell)
                .withQuantity(d("0.1"))
                .withPrice(d("410"))
                .build();
        autospread.onOrderBookChanged(orderBook, Arrays.asList(trade));

        assertThat(autospread.getSellTask().getAmount()).isEqualByComparingTo(d("0.2"));
        assertThat(autospread.getSellTask().getBoughtPrice()).isEqualByComparingTo(d("400.70"));
        assertThat(autospread.getSellTask().getCompensatedBoughtPrice()).isEqualByComparingTo(d("396.05000"));
        assertThat(autospread.getSellTasks().size()).isEqualTo(1);
    }

    @Test
    public void testResetAll() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.01"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, null);
        autospread.createNewSellTask();
        autospread.getSellTask().setAmount(d("0.5"));
        autospread.getSellTask().setBoughtPrice(d("100"));
        autospread.getSellTasks().get(1).setAmount(d("1"));
        autospread.getSellTasks().get(1).setBoughtPrice(d("200"));

        autospread.resetFromBottom(null);

        assertThat(autospread.getSellTask().getAmount()).isEqualByComparingTo(d("1.5"));
        assertThat(autospread.getSellTasks().size()).isEqualTo(1);
    }

    @Test
    public void testResetSome() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.01"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, null);
        autospread.createNewSellTask();
        autospread.getSellTask().setAmount(d("0.5"));
        autospread.getSellTask().setBoughtPrice(d("100"));
        autospread.getSellTasks().get(1).setAmount(d("1"));
        autospread.getSellTasks().get(1).setBoughtPrice(d("200"));

        autospread.resetFromBottom(d("0.7"));

        assertThat(autospread.getSellTask().getAmount()).isEqualByComparingTo(d("0.7"));
        assertThat(autospread.getSellTask().getBoughtPrice()).isEqualByComparingTo(d("0"));
        assertThat(autospread.getSellTasks().size()).isEqualTo(2);
        assertThat(autospread.getSellTasks().get(1).getAmount()).isEqualByComparingTo(d("0.8"));
        assertThat(autospread.getSellTasks().get(1).getBoughtPrice()).isEqualByComparingTo(d("200"));
    }

    @Test
    public void testResetSome_multipleSellTasks() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.01"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, null);
        autospread.createNewSellTask();
        autospread.createNewSellTask();
        autospread.createNewSellTask();
        autospread.getSellTask().setAmount(d("0.5"));
        autospread.getSellTask().setBoughtPrice(d("100"));
        autospread.getSellTasks().get(1).setAmount(d("1"));
        autospread.getSellTasks().get(1).setBoughtPrice(d("200"));
        autospread.getSellTasks().get(2).setAmount(d("1"));
        autospread.getSellTasks().get(2).setBoughtPrice(d("300"));
        autospread.getSellTasks().get(3).setAmount(d("1"));
        autospread.getSellTasks().get(3).setBoughtPrice(d("400"));

        autospread.resetFromBottom(d("1.7"));

        assertThat(autospread.getSellTasks().size()).isEqualTo(3);
        assertThat(autospread.getSellTask().getAmount()).isEqualByComparingTo(d("1.7"));
        assertThat(autospread.getSellTask().getBoughtPrice()).isEqualByComparingTo(d("0"));
        assertThat(autospread.getSellTasks().get(1).getAmount()).isEqualByComparingTo(d("0.8"));
        assertThat(autospread.getSellTasks().get(1).getBoughtPrice()).isEqualByComparingTo(d("300"));
        assertThat(autospread.getSellTasks().get(2).getAmount()).isEqualByComparingTo(d("1"));
        assertThat(autospread.getSellTasks().get(2).getBoughtPrice()).isEqualByComparingTo(d("400"));
    }

    @Test
    public void testResetFromTop_multipleSellTasks() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.01"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, null);
        autospread.createNewSellTask();
        autospread.createNewSellTask();
        autospread.createNewSellTask();
        autospread.getSellTask().setAmount(d("0.5"));
        autospread.getSellTask().setBoughtPrice(d("100"));
        autospread.getSellTasks().get(1).setAmount(d("1"));
        autospread.getSellTasks().get(1).setBoughtPrice(d("200"));
        autospread.getSellTasks().get(2).setAmount(d("1"));
        autospread.getSellTasks().get(2).setBoughtPrice(d("300"));
        autospread.getSellTasks().get(3).setAmount(d("1"));
        autospread.getSellTasks().get(3).setBoughtPrice(d("400"));

        autospread.resetFromTop(d("1.7"));

        assertThat(autospread.getSellTasks().size()).isEqualTo(4);
        assertThat(autospread.getSellTask().getAmount()).isEqualByComparingTo(d("1.7"));
        assertThat(autospread.getSellTask().getBoughtPrice()).isEqualByComparingTo(d("0"));
        assertThat(autospread.getSellTasks().get(1).getAmount()).isEqualByComparingTo(d("0.5"));
        assertThat(autospread.getSellTasks().get(1).getBoughtPrice()).isEqualByComparingTo(d("100"));
        assertThat(autospread.getSellTasks().get(2).getAmount()).isEqualByComparingTo(d("1"));
        assertThat(autospread.getSellTasks().get(2).getBoughtPrice()).isEqualByComparingTo(d("200"));
        assertThat(autospread.getSellTasks().get(3).getAmount()).isEqualByComparingTo(d("0.3"));
        assertThat(autospread.getSellTasks().get(3).getBoughtPrice()).isEqualByComparingTo(d("300"));
    }

    @Test
    public void testToString() throws Exception {
        TradeCycleAlgo autospread = tradingCommands.autospread(d("3"), d("0"), d("0.001"), d("0.001"), 3, 3, d("0.001"), PlaceAheadMode.ALWAYS, false, 60, null, null, null, d("1"));

        String result = autospread.toString();

        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("400"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("390"), d("1")))
                .build();
        autospread.onOrderBookChanged(orderBook, Arrays.asList());
        autospread.onOrderBookChanged(orderBook, Arrays.asList());

        autospread.getSellTask().setBoughtPrice(new BigDecimal("500.00"));
        autospread.getSellTask().setAmount(d("0.1"));
        autospread.getSellTask().setOrder(new PlacedOrder("1234", TradeType.sell, new CurrencyPair("GBP", "BTC"), d("399.99"), d("0.1")));

        result = autospread.toString();
    }


    @InjectMocks
    private GdaxTradesConverter tradesConverter;

    @Test
    public void testParseDate() throws ParseException {
        Date date = tradesConverter.toDate("2014-11-07T22:19:28.578544Z");
        date = tradesConverter.toDate("2017-02-18T09:21:21Z");
    }

    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}
