package com.pigatron.trading.tasks.impl.cycle;

import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.ShallowBuyTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static com.pigatron.trading.exchanges.entity.OrderBook.anOrderBook;
import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ShallowBuyTaskTest {

    @Mock
    TradeCycleAlgo parentAlgorithm;

    ShallowBuyTask buyTask;


    @Before
    public void setup() {
        buyTask = new ShallowBuyTask(d("1"), d("0.001"), 60);
        buyTask.setPriceIncrement(new BigDecimal("0.01"));
        buyTask.setParentAlgorithm(parentAlgorithm);
    }

    @Test
    public void testInitialBestBuyPrice() {
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("501"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("499"), d("1")))
                .build();

        BigDecimal price = buyTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("499.01"));
    }

    @Test
    public void testInitialBestBuyPrice2() {
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("511"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("509"), d("1")))
                .build();

        BigDecimal price = buyTask.getBestPrice(orderBook);

        assertThat(price).isEqualTo(d("509.01"));
    }

    @Test
    public void testBestBuyPrice_suddenDownwardMovement_PushDownFactorZero() {
        testInitialBestBuyPrice2();
        buyTask.setPushDownFactor(0);
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("501"), d("1"))) //Drop 10
                .addBid(new MarketOrder(TradeType.buy, d("499"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("498"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("497"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("496"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("495"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("494"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("493"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("492"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("491"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("490"), d("1")))
                .build();

        BigDecimal price = buyTask.getBestPrice(orderBook);
        assertThat(price).isEqualTo(d("499.01")); //Stays at top
    }

    @Test
    public void testBestBuyPrice_suddenDownwardMovement_PushDownFactorOne() {
        testInitialBestBuyPrice2();
        buyTask.setPushDownFactor(1);
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("501"), d("1"))) //Drop 10
                .addBid(new MarketOrder(TradeType.buy, d("499"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("498"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("497"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("496"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("495"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("494"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("493"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("492"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("491"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("490"), d("1")))
                .build();

        BigDecimal price = buyTask.getBestPrice(orderBook);
        assertThat(price).isEqualTo(d("490.01")); //Pushed down to bottom

        for(int i = 0 ; i < 100; i++) {
            price = buyTask.getBestPrice(orderBook);
        }
        assertThat(price).isEqualTo(d("499.01")); //Eventually rises to top again
    }

    @Test
    public void testBestBuyPrice_suddenUpwardMovement() {
        testInitialBestBuyPrice();
        OrderBook orderBook = anOrderBook()
                .addAsk(new MarketOrder(TradeType.sell, d("511"), d("1"))) //Rise 10
                .addBid(new MarketOrder(TradeType.buy, d("509"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("508"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("507"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("506"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("505"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("504"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("503"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("502"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("501"), d("1")))
                .addBid(new MarketOrder(TradeType.buy, d("500"), d("1")))
                .build();

        for(int i = 0 ; i < 4; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("500.01"));
        }
        for(int i = 0 ; i < 3; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("501.01"));
        }
        for(int i = 0 ; i < 4; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("502.01"));
        }
        for(int i = 0 ; i < 5; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("503.01"));
        }
        for(int i = 0 ; i < 6; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("504.01"));
        }
        for(int i = 0 ; i < 7; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("505.01"));
        }
        for(int i = 0 ; i < 9; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("506.01"));
        }
        for(int i = 0 ; i < 14; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("507.01"));
        }
        for(int i = 0 ; i < 25; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("508.01"));
        }
        for(int i = 0 ; i < 100; i++) {
            BigDecimal price = buyTask.getBestPrice(orderBook);
            assertThat(price).isEqualTo(d("509.01"));
        }


    }



    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}
