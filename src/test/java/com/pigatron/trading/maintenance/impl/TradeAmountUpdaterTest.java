package com.pigatron.trading.maintenance.impl;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.BuyTask;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TradeAmountUpdaterTest {

    @Mock
    private TaskRunnerService taskRunnerService;

    @Mock
    private ExchangeClient exchangeClient;

    @Mock
    private AbstractAlgorithm abstractAlgorithm;

    private CurrencyPair currencyPair;

    private Balances balances;

    @Mock
    private BuyTask buyTask1;

    @Mock
    private BuyTask buyTask2;

    @Mock
    private BuyTask buyTask3;

    @Mock
    private BuyTask buyTask4;

    @Mock
    private SellTask sellTask;

    @Mock
    private OrderBook orderBook;


    @InjectMocks
    private TradeAmountUpdater tradeAmountUpdater;


    @Before
    public void setup() {
        currencyPair = new CurrencyPair("GBP", "BTC");
        TaskWrapper taskWrapper = new TaskWrapper(exchangeClient, currencyPair, abstractAlgorithm);
        given(taskRunnerService.getTasks()).willReturn(Arrays.asList(taskWrapper));

        given(exchangeClient.getOrderBook(currencyPair, 10)).willReturn(orderBook);

        balances = new Balances();
        balances.setBalance("GBP", new BigDecimal("110"), new BigDecimal("100"), null);
        given(exchangeClient.getBalances()).willReturn(balances);
        given(exchangeClient.getPriceIncrement("BTC")).willReturn(new BigDecimal("0.001"));

        given(abstractAlgorithm.getExchange()).willReturn(exchangeClient);
        given(abstractAlgorithm.getBuyTasks()).willReturn(Arrays.asList(buyTask1, buyTask2));
        given(abstractAlgorithm.getSellTasks()).willReturn(Arrays.asList(sellTask));
        given(abstractAlgorithm.getCurrencyPair()).willReturn(currencyPair);
        given(abstractAlgorithm.getBalancePercentageToUse()).willReturn(BigDecimal.ONE);
    }

    @Test
    public void testUpdateTradeAmount_withNoSellTask() throws Exception {
        given(orderBook.getMidPrice()).willReturn(new BigDecimal("1000"));
        balances.setBalance("GBP", new BigDecimal("100"), new BigDecimal("200"), null); //300GBP buy side

        given(sellTask.getAmount()).willReturn(BigDecimal.ZERO);

        given(buyTask1.getMaxAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getMaxAmount()).willReturn(new BigDecimal("0.1"));

        given(buyTask1.getAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getAmount()).willReturn(new BigDecimal("0.1"));

        tradeAmountUpdater.run();

        verify(buyTask1).setMaxAmount(new BigDecimal("0.150"));
        verify(buyTask2).setMaxAmount(new BigDecimal("0.150"));

        verify(buyTask2).setAmount(new BigDecimal("0.150000"));
        verify(buyTask1).setAmount(new BigDecimal("0.150000"));
    }

    @Test
    public void testUpdateTradeAmount_withSellTask() throws Exception {
        given(orderBook.getMidPrice()).willReturn(new BigDecimal("1000"));
        balances.setBalance("GBP", new BigDecimal("10"), new BigDecimal("200"), null); //210GBP buy side

        given(sellTask.getAmount()).willReturn(new BigDecimal("0.09")); //90GBP sell side

        given(buyTask1.getMaxAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getMaxAmount()).willReturn(new BigDecimal("0.1"));

        given(buyTask1.getAmount()).willReturn(new BigDecimal("0.01"));
        given(buyTask2.getAmount()).willReturn(new BigDecimal("0.1"));

        tradeAmountUpdater.run();

        verify(buyTask1).setMaxAmount(new BigDecimal("0.150"));
        verify(buyTask2).setMaxAmount(new BigDecimal("0.150"));

        verify(buyTask1).setAmount(new BigDecimal("0.015000"));  //0.15 - 0.09
        verify(buyTask2).setAmount(new BigDecimal("0.150000"));
    }

    @Test
    public void testUpdateTradeAmount_withSellTaskAndRedistributed() throws Exception {
        given(orderBook.getMidPrice()).willReturn(new BigDecimal("1000"));
        balances.setBalance("GBP", new BigDecimal("10"), new BigDecimal("200"), null); //210GBP buy side

        given(sellTask.getAmount()).willReturn(new BigDecimal("0.09")); //90GBP sell side

        given(buyTask1.getMaxAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getMaxAmount()).willReturn(new BigDecimal("0.1"));

        given(buyTask1.getAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getAmount()).willReturn(new BigDecimal("0.01"));

        tradeAmountUpdater.run();

        verify(buyTask1).setMaxAmount(new BigDecimal("0.150"));
        verify(buyTask2).setMaxAmount(new BigDecimal("0.150"));

        verify(buyTask1).setAmount(new BigDecimal("0.150000"));
        verify(buyTask2).setAmount(new BigDecimal("0.015000"));
    }

    @Test
    public void testUpdateTradeAmount_withSellTaskAndRedistributed2() throws Exception {
        given(abstractAlgorithm.getBuyTasks()).willReturn(Arrays.asList(buyTask1, buyTask2, buyTask3, buyTask4));
        given(orderBook.getMidPrice()).willReturn(new BigDecimal("1000"));
        balances.setBalance("GBP", new BigDecimal("100"), new BigDecimal("700"), null); //800GBP buy side

        given(sellTask.getAmount()).willReturn(new BigDecimal("0.2")); //200GBP sell side

        given(buyTask1.getMaxAmount()).willReturn(new BigDecimal("0.2"));
        given(buyTask2.getMaxAmount()).willReturn(new BigDecimal("0.2"));
        given(buyTask3.getMaxAmount()).willReturn(new BigDecimal("0.2"));
        given(buyTask4.getMaxAmount()).willReturn(new BigDecimal("0.2"));

        given(buyTask1.getAmount()).willReturn(new BigDecimal("0.05"));
        given(buyTask2.getAmount()).willReturn(new BigDecimal("0.2"));
        given(buyTask3.getAmount()).willReturn(new BigDecimal("0.2"));
        given(buyTask4.getAmount()).willReturn(new BigDecimal("0.15"));

        tradeAmountUpdater.run();

        verify(buyTask1).setMaxAmount(new BigDecimal("0.250"));
        verify(buyTask2).setMaxAmount(new BigDecimal("0.250"));
        verify(buyTask3).setMaxAmount(new BigDecimal("0.250"));
        verify(buyTask4).setMaxAmount(new BigDecimal("0.250"));

        verify(buyTask1).setAmount(new BigDecimal("0.062500"));
        verify(buyTask2).setAmount(new BigDecimal("0.250000"));
        verify(buyTask3).setAmount(new BigDecimal("0.250000"));
        verify(buyTask4).setAmount(new BigDecimal("0.187500"));  // 0.75
    }

    @Test
    public void testUpdateTradeAmount_withUsePercentageBalance() throws Exception {
        given(orderBook.getMidPrice()).willReturn(new BigDecimal("1000"));
        balances.setBalance("GBP", new BigDecimal("100"), new BigDecimal("200"), null); //300GBP buy side

        given(sellTask.getAmount()).willReturn(BigDecimal.ZERO);

        given(buyTask1.getMaxAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getMaxAmount()).willReturn(new BigDecimal("0.1"));

        given(buyTask1.getAmount()).willReturn(new BigDecimal("0.1"));
        given(buyTask2.getAmount()).willReturn(new BigDecimal("0.1"));

        given(abstractAlgorithm.getBalancePercentageToUse()).willReturn(new BigDecimal("0.5"));

        tradeAmountUpdater.run();

        verify(buyTask1).setMaxAmount(new BigDecimal("0.075"));
        verify(buyTask2).setMaxAmount(new BigDecimal("0.075"));

        verify(buyTask1).setAmount(new BigDecimal("0.075000"));
        verify(buyTask2).setAmount(new BigDecimal("0.075000"));
    }

}