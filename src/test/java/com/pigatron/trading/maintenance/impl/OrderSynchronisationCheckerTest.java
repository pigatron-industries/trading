package com.pigatron.trading.maintenance.impl;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.PlacedOrder;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class OrderSynchronisationCheckerTest {

    @Mock
    private TaskRunnerService taskRunnerService;

    @Mock
    private ExchangeClient exchangeClient;

    @Mock
    private AbstractAlgorithm abstractAlgorithm;


    private CurrencyPair currencyPair;

    @InjectMocks
    private OrderSynchronisationChecker orderSynchronisationChecker;


    @Before
    public void setup() {
        currencyPair = new CurrencyPair("GBP", "BTC");
        TaskWrapper taskWrapper = new TaskWrapper(exchangeClient, currencyPair, abstractAlgorithm);
        given(taskRunnerService.getTasks()).willReturn(Arrays.asList(taskWrapper));
    }

    private void ordersOnExchange(List<String> orderIds) {
        List<PlacedOrder> orders = orderIds.stream()
                .map(id -> new PlacedOrder(id, null, currencyPair, null, null))
                .collect(Collectors.toList());
        given(exchangeClient.getOpenOrders(currencyPair)).willReturn(orders);
    }

    private void ordersInAlgorithm(List<String> orderIds) {
        List<SellTask> tasks = orderIds.stream().map(id -> {
            SellTask sellTask = new SellTask();
            if(id != null) {
                sellTask.setOrder(new PlacedOrder(id, null, currencyPair, null, null));
            }
            return sellTask;
        }).collect(Collectors.toList());
        given(abstractAlgorithm.getSellTasks()).willReturn(tasks);
    }

    @Test
    public void exchangeOrderInAlgorithm_isNotCancelled() throws Exception {
        ordersOnExchange(Arrays.asList("1"));
        ordersInAlgorithm(Arrays.asList("1"));

        orderSynchronisationChecker.run();

        verify(exchangeClient, times(0)).cancelOrder("1", currencyPair);
    }

    @Test
    public void exchangeOrderNotInAlgorithm_isCancelled() throws Exception {
        ordersOnExchange(Arrays.asList("1", "2"));
        ordersInAlgorithm(Arrays.asList("2"));

        orderSynchronisationChecker.run();

        verify(exchangeClient).cancelOrder("1", currencyPair);
    }

    @Test
    public void algorithmOrderNotOnExchange_isRemoved() throws Exception {
        ordersOnExchange(Arrays.asList("2"));
        ordersInAlgorithm(Arrays.asList("1", "2"));

        orderSynchronisationChecker.run();

        assertThat(abstractAlgorithm.getSellTasks().get(0).getOrder()).isNull();
    }

    @Test
    public void algorithmOrderNull() throws Exception {
        ordersInAlgorithm(Arrays.asList((String)null));

        orderSynchronisationChecker.run();

        assertThat(abstractAlgorithm.getSellTasks().get(0).getOrder()).isNull();
    }

}