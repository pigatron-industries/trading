package com.pigatron.trading.maintenance.impl;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.ExchangeProvider;
import com.pigatron.trading.exchanges.entity.Balance;
import com.pigatron.trading.exchanges.entity.Balances;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.tasks.AbstractAlgorithm;
import com.pigatron.trading.tasks.TaskRunnerService;
import com.pigatron.trading.tasks.TaskWrapper;
import com.pigatron.trading.tasks.impl.cycle.tasks.matchorderbook.SellTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class SellAmountUpdaterTest {

    @Mock
    private ExchangeProvider exchangeProvider;
    @Mock
    private TaskRunnerService taskRunnerService;
    @Mock
    private ExchangeClient exchange;
    @Mock
    private AbstractAlgorithm algo;

    private CurrencyPair currencyPair = new CurrencyPair("GBP", "BTC");

    @InjectMocks
    private SellAmountUpdater sellAmountUpdater;

    @Before
    public void setup() {
        TaskWrapper taskWrapper = new TaskWrapper(exchange, currencyPair, algo);
        given(exchangeProvider.getExchanges()).willReturn(Arrays.asList(exchange));
        given(taskRunnerService.getTasks(exchange)).willReturn(Arrays.asList(taskWrapper));
        given(exchange.getName()).willReturn("gdax");
        given(exchange.getPriceIncrement("BTC")).willReturn(new BigDecimal("0.00000001"));
    }

    @Test
    public void run_amountOnExchangeIsHigher() throws Exception {
        given(exchange.getBalances()).willReturn(createBalance("0.5"));
        given(algo.getSellTasks()).willReturn(Arrays.asList(createSellTask("0.1"), createSellTask("0.2")));

        sellAmountUpdater.run();


    }

    @Test
    public void run_amountOnExchangeIsLower() throws Exception {
        given(exchange.getBalances()).willReturn(createBalance("0.2"));
        given(algo.getSellTasks()).willReturn(Arrays.asList(createSellTask("0.1"), createSellTask("0.2")));

        sellAmountUpdater.run();


    }

    private SellTask createSellTask(String amount) {
        SellTask sellTask = new SellTask();
        sellTask.setAmount(new BigDecimal(amount));
        return sellTask;
    }

    private Balances createBalance(String amount) {
        Balances balances = new Balances();
        balances.setBalance("BTC", new BigDecimal(amount), new BigDecimal("0"), null);
        return balances;
    }

}