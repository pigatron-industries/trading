package com.pigatron.trading.notifications;


import com.pigatron.trading.TradingApplication;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TradingApplication.class)
@WebAppConfiguration
public class PushNotificationClientTest {

    @Autowired
    private PushNotificationClient pushNotificationClient;

    @Test
    @Ignore
    public void testPush() {
        pushNotificationClient.send("It works!");
    }

}
