package com.pigatron.trading.util;


import com.pigatron.trading.exchanges.entity.Trade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CurrencyUtilTest {

    @Test
    public void testRound() {
        BigDecimal result = CurrencyUtil.round(d("100.25"), d("0.1"), RoundingMode.HALF_UP);
        assertThat(result).isEqualByComparingTo(d("100.3"));
    }

    @Test
    public void testRound2() {
        BigDecimal result = CurrencyUtil.round(d("100.24"), d("0.1"), RoundingMode.HALF_UP);
        assertThat(result).isEqualByComparingTo(d("100.2"));
    }

    @Test
    public void testRound3() {
        BigDecimal result = CurrencyUtil.round(d("102.5"), d("5"), RoundingMode.HALF_UP);
        assertThat(result).isEqualByComparingTo(d("105"));
    }

    @Test
    public void testRound4() {
        BigDecimal result = CurrencyUtil.round(d("102.5"), d("5"), RoundingMode.DOWN);
        assertThat(result).isEqualByComparingTo(d("100"));
    }

    @Test
    public void testVolumeWeightedAverage() {
        List<Trade> trades = new ArrayList<>();
        trades.add(new Trade(d("500"), d("1")));
        trades.add(new Trade(d("600"), d("1")));
        BigDecimal result = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(trades);
        assertThat(result).isEqualByComparingTo(d("550"));
    }

    @Test
    public void testVolumeWeightedAverage2() {
        List<Trade> trades = new ArrayList<>();
        trades.add(new Trade(d("500"), d("1.5")));
        trades.add(new Trade(d("600"), d("0.5")));
        BigDecimal result = CurrencyUtil.getVolumeWeightedAveragePriceForTrades(trades);
        assertThat(result).isEqualByComparingTo(d("525"));
    }

    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}
