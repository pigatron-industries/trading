package com.pigatron.trading.analysis.alerts;

import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.MarketOrder;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.TradeType;
import com.pigatron.trading.notifications.PushNotificationClient;
import com.pigatron.trading.tasks.TaskContext;
import com.pigatron.trading.tasks.TaskRunnerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.math.BigDecimal;

import static com.pigatron.trading.exchanges.entity.OrderBook.anOrderBook;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DumpAlertTest {

    @Mock
    private ExchangeClient exchange;

    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private TaskRunnerService taskRunnerService;

    @Mock
    private PushNotificationClient pushNotificationClient;


    @Before
    public void setup() {
        new TaskContext().setApplicationContext(applicationContext);
        given(applicationContext.getBean(TaskRunnerService.class)).willReturn(taskRunnerService);
        given(taskRunnerService.getPushNotificationClient()).willReturn(pushNotificationClient);
        given(exchange.getTimeResolutionMillis()).willReturn(1000);
    }


    @Test
    public void priceDrop_lessThan10Percent() throws Exception {
        OrderBook orderBook = anOrderBook().addBid(new MarketOrder(TradeType.buy, d("1000"), d("1"))).build();
        DumpAlert dumpAlert = new DumpAlert(exchange, new CurrencyPair("USD", "BTC"), 60, d("0.01"));

        for(int i = 0; i < 60; i++) {
            dumpAlert.onOrderBookChanged(orderBook, null);
        }

        orderBook = anOrderBook().addBid(new MarketOrder(TradeType.buy, d("991"), d("1"))).build();
        dumpAlert.onOrderBookChanged(orderBook, null);

        verify(pushNotificationClient, times(0)).send("Price drop alert");
    }

    @Test
    public void priceDrop_moreThan10Percent() throws Exception {
        OrderBook orderBook = anOrderBook().addBid(new MarketOrder(TradeType.buy, d("1000"), d("1"))).build();
        DumpAlert dumpAlert = new DumpAlert(exchange, new CurrencyPair("USD", "BTC"), 60, d("0.01"));

        for(int i = 0; i < 60; i++) {
            dumpAlert.onOrderBookChanged(orderBook, null);
        }

        orderBook = anOrderBook().addBid(new MarketOrder(TradeType.buy, d("989"), d("1"))).build();
        dumpAlert.onOrderBookChanged(orderBook, null);

        verify(pushNotificationClient).send("Price drop alert");
    }

    private BigDecimal d(String number) {
        return new BigDecimal(number);
    }

}