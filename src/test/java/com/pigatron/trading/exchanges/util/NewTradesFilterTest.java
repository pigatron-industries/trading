package com.pigatron.trading.exchanges.util;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.entity.Trade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.pigatron.trading.exchanges.entity.Trade.aTrade;
import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class NewTradesFilterTest {

    private NewTradesFilter filter = new NewTradesFilter(5);
    private CurrencyPair currencyPair = new CurrencyPair("GBP", "BTC");

    @Test
    public void test() {
        List<Trade> trades = new ArrayList<>();
        trades.add(0, aTrade().withTradeId("1").build());
        trades.add(0, aTrade().withTradeId("2").build());
        List<Trade> filtered = filter.filter(trades, currencyPair);

        assertThat(filtered.size()).isEqualTo(0);

        filtered = filter.filter(trades, currencyPair);

        assertThat(filtered.size()).isEqualTo(0);

        trades.add(0, aTrade().withTradeId("4").build());
        filtered = filter.filter(trades, currencyPair);

        assertThat(filtered.size()).isEqualTo(1);
        assertThat(filtered.get(0).getTradeId()).isEqualTo("4");

        trades.add(0, aTrade().withTradeId("3").build());
        trades.add(0, aTrade().withTradeId("5").build());
        filtered = filter.filter(trades, currencyPair);

        assertThat(filtered.size()).isEqualTo(2);
        assertThat(filtered.get(0).getTradeId()).isEqualTo("5");
        assertThat(filtered.get(1).getTradeId()).isEqualTo("3");
        assertThat(filter.getLastTradeId(currencyPair).get()).isEqualTo("5");

        trades.add(0, aTrade().withTradeId("6").build());
        filtered = filter.filter(trades, currencyPair);

        assertThat(filtered.size()).isEqualTo(1);
        assertThat(filtered.get(0).getTradeId()).isEqualTo("6");
    }


}
