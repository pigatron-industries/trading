package com.pigatron.trading.exchanges.impl;

import com.pigatron.trading.TradingApplication;
import com.pigatron.trading.analysis.MacdAnalyser;
import com.pigatron.trading.exchanges.ExchangeClient;
import com.pigatron.trading.exchanges.entity.*;
import com.pigatron.trading.exchanges.exceptions.ExchangeClientException;
import com.pigatron.trading.exchanges.exceptions.NotImplementedException;
import com.pigatron.trading.exchanges.impl.bitfinex.BitfinexClient;
import com.pigatron.trading.exchanges.impl.coinfloor.CoinFloorClient;
import com.pigatron.trading.exchanges.impl.gdax.GdaxClient;
import com.pigatron.trading.exchanges.impl.poloniex.PoloniexClient;
import com.pigatron.trading.exchanges.impl.quoine.QuoineClient;
import com.pigatron.trading.exchanges.impl.spacebtc.SpaceBtcClient;
import com.pigatron.trading.exchanges.impl.zaif.ZaifClient;
import com.pigatron.trading.maintenance.OrderConsolidationService;
import com.pigatron.trading.maintenance.impl.SpreadAutoAdjust;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TradingApplication.class)
@WebAppConfiguration
public class ClientTest {

//    @Autowired
//    private CoinFloorClient client;

    @Autowired
    private SpreadAutoAdjust spreadAutoAdjust;

//
//    @Autowired
//    private OrderConsolidationService orderConsolidationService;
//
//    @Autowired
//    private MacdAnalyser macdAnalyser;


    //@Test
    //public void testChartData() throws NotImplementedException {
        //ChartData chartData = client.getChartData(new CurrencyPair("GBP", "BTC"), 48, 60 * 60);
    //}
//
//    @Test
//    public void testTicker() throws NotImplementedException {
//        TickData tickData = client.getTicker(new CurrencyPair("JPY", "BTC"));
//    }

//    @Test
//    public void testOrderBook() throws ExchangeClientException {
//        OrderBook orderBook = client.getOrderBook(new CurrencyPair("USD", "BTC"), 10);
//    }
//
//    @Test
//    @Ignore
//    public void testGetTrades() throws NotImplementedException {
//        List<Trade> trades = client.getTrades(new CurrencyPair("JPY", "BTC"));
//    }
//
//    @Test
//    public void testBalances() {
//        Balances balances = client.getBalances();
//    }
//
//    @Test
//    public void testGetPrivateTrades() {
//        List<Trade> newTrades = client.getNewTrades(new CurrencyPair("GBP", "BTC"));
//        newTrades = client.getNewTrades(new CurrencyPair("GBP", "BTC"));
//    }
//
//    @Test
//    public void testPlaceOrder() throws ExchangeClientException {
//        PlacedOrder order = client.buy(new CurrencyPair("USD", "BTC"), new BigDecimal("700"), new BigDecimal("0.01"));
//        client.cancelOrder(order.getOrderId(), new CurrencyPair("GBP", "BTC"));
//        //client.cancelOrder(order.getOrderId(), new CurrencyPair("USD", "BTC"));
//    }
//
//    @Test
//    public void testGetOpenOrders() {
//        List<PlacedOrder> openOrders = client.getOpenOrders(new CurrencyPair("GBP", "BTC"));
//    }

//    @Test
//    public void testOrderConsolidation() {
//        List<PlacedOrder> orders = client.getOpenOrders(new CurrencyPair("JPY", "BTC"), TradeType.sell);
//        PlacedOrder newOrder = orderConsolidationService.consolidateAll(orders);
//    }
//
//    @Test
//    public void testWamp() {
//        client.testWamp();
//        while(true){
//
//        }
//    }

    @Test
    public void test() {
    }

}
