package com.pigatron.trading.exchanges.impl.kraken.converter;


import com.pigatron.trading.exchanges.entity.CurrencyPair;
import com.pigatron.trading.exchanges.impl.kraken.converter.KrakenCurrencyCodeConverter;
import com.pigatron.trading.exchanges.impl.kraken.converter.KrakenCurrencyPairConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class KrakenCurrencyPairConverterTest {

    KrakenCurrencyPairConverter currencyPairConverter;

    @Before
    public void setup() {
        KrakenCurrencyCodeConverter currencyCodeConverter = new KrakenCurrencyCodeConverter();
        currencyPairConverter = new KrakenCurrencyPairConverter(currencyCodeConverter);
    }

    @Test
    public void testConvertCurrencyPair() {
        String krakenPair = "XXBTZGBP";

        CurrencyPair currencyPair = currencyPairConverter.convert(krakenPair);

        assertThat(currencyPair.getCurrency1()).isEqualTo("GBP");
        assertThat(currencyPair.getCurrency2()).isEqualTo("BTC");
    }

}
