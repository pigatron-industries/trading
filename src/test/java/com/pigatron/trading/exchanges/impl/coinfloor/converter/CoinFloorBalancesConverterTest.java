package com.pigatron.trading.exchanges.impl.coinfloor.converter;

import com.pigatron.trading.exchanges.entity.Balances;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.fest.assertions.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class CoinFloorBalancesConverterTest {

    @InjectMocks
    private CoinFloorBalancesConverter converter;

    @Test
    public void testConvert() throws Exception {
        Map<String, BigDecimal> sourceBalances = new HashMap<>();
        sourceBalances.put("gbp_balance", new BigDecimal("100"));
        sourceBalances.put("gbp_reserved", new BigDecimal("5"));
        sourceBalances.put("gbp_available", new BigDecimal("95"));
        sourceBalances.put("xbt_balance", new BigDecimal("1"));
        sourceBalances.put("xbt_reserved", new BigDecimal("0.05"));
        sourceBalances.put("xbt_available", new BigDecimal("0.95"));

        Balances balances = converter.convert(sourceBalances);

        assertThat(balances.getBalances().get("GBP").getTotal()).isEqualByComparingTo(new BigDecimal("100"));
        assertThat(balances.getBalances().get("GBP").getAvailable()).isEqualByComparingTo(new BigDecimal("95"));
        assertThat(balances.getBalances().get("GBP").getOnOrders()).isEqualByComparingTo(new BigDecimal("5"));
        assertThat(balances.getBalances().get("BTC").getTotal()).isEqualByComparingTo(new BigDecimal("1"));
        assertThat(balances.getBalances().get("BTC").getAvailable()).isEqualByComparingTo(new BigDecimal("0.95"));
        assertThat(balances.getBalances().get("BTC").getOnOrders()).isEqualByComparingTo(new BigDecimal("0.05"));
    }

}