package com.pigatron.trading.exchanges.impl;


import com.pigatron.trading.exchanges.ExchangeDataSubscriber;
import com.pigatron.trading.exchanges.entity.OrderBook;
import com.pigatron.trading.exchanges.entity.Trade;
import com.pigatron.trading.exchanges.impl.bitfinex.BitfinexClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class NoOpExchangeSubscriber implements ExchangeDataSubscriber {

    private static final Logger logger = LoggerFactory.getLogger(NoOpExchangeSubscriber.class);

    @Override
    public void onOrderBookChanged(OrderBook orderBook, List<Trade> newTrades) {
        logger.info("ASK: " + orderBook.getAskByIndex(0).getPrice().toPlainString() + "  BID: " + orderBook.getBidByIndex(0).getPrice().toPlainString());
    }

    @Override
    public int getProcessId() {
        return 0;
    }

    @Override
    public void start() throws Exception {

    }

    @Override
    public void stop() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void unpause() {

    }
}
